<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\Job;
use App\Models\user;
use App\Models\Stint;
use App\Models\StintExtra;
use App\Models\StintFaq;
use App\Models\StintGalleryExtra;
use App\Models\StintPricingPlan;
use App\Models\StintRequirement;
use App\Models\StintGallery;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StintsController extends Controller
{
    //
    public function dashboard()
    {
        return view('admin.stints.dashboard');
    }

    public function getAllStints()
    {

        $stints = Stint::findRequested();
        $stintCategories = Category::where('category_type', '=', 'stint')->get();
        $users = User::where('activated', '=', '0')->get();
//                dd($stints);
        return view('admin.stints.index')->with('stints', $stints)
            ->with('stintCategories', $stintCategories)
            ->with('users', $users);

    }

    public function stintDetail($stintId)
    {
        $singleStint = Stint::with('category')->with('user')->with('price')->with('faq')->with('requirement')
            ->where('id', '=', $stintId)->first();
//        dd($stintRecord);
        return view('admin.stints.stint-detail')->with('singleStint', $singleStint);
    }

    public function stintStatus(Request $request, $stintId)
    {
        $stintRecord = Stint::where('id', '=', $stintId)->update(
            [
                'status' => $request->get('status')
            ]);
        if ($stintRecord) {
            return response()->json(['message' => 'Yup. This request succeeded.'], 200);;
        } else {
            return response()->json(['message' => 'something went wrong'], 203);
        }
    }
}
