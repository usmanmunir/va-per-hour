<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Membership;
use App\Models\MembershipVariation;
use Illuminate\Http\Request;

class MembershipController extends Controller
{
    protected $dir='admin/memberships';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $memberships = Membership::findRequested();
        return $this->view('index', ['memberships'=> $memberships]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return $this->view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $data['can_view_course_details'] = !empty($data['can_view_course_details']) ? $data['can_view_course_details'] : 0;
        $data['can_view_product_details'] = !empty($data['can_view_product_details']) ? $data['can_view_product_details'] : 0;
        $data['can_invite_friends'] = !empty($data['can_invite_friends']) ? $data['can_invite_friends'] : 0;
        $data['visible_in_seller_search'] = !empty($data['visible_in_seller_search']) ? $data['visible_in_seller_search'] : 0;
        $data['can_contact_seller'] = !empty($data['can_contact_seller']) ? $data['can_contact_seller'] : 0;
        $data['can_submit_proposals'] = !empty($data['can_submit_proposals']) ? $data['can_submit_proposals'] : 0;
        $data['can_register_to_course'] = !empty($data['can_register_to_course']) ? $data['can_register_to_course'] : 0;
        $data['can_add_product_to_market_place'] = !empty($data['can_add_product_to_market_place']) ? $data['can_add_product_to_market_place'] : 0;
        $data['eligible_for_cash_reward'] = !empty($data['eligible_for_cash_reward']) ? $data['eligible_for_cash_reward'] : 0;
        $data['can_purchase_product'] = !empty($data['can_purchase_product']) ? $data['can_purchase_product'] : 0;
        $data['can_submit_bid'] = !empty($data['can_submit_bid']) ? $data['can_submit_bid'] : 0;
        $data['can_create_course'] = !empty($data['can_create_course']) ? $data['can_create_course'] : 0;
        $data['can_order_stint'] = !empty($data['can_order_stint']) ? $data['can_order_stint'] : 0;
        $data['seller_contact_limit_single_user'] = !empty($data['seller_contact_limit_single_user']) ? $data['seller_contact_limit_single_user'] : 0;
        $data['number_of_stints_allowed_single_user'] = !empty($data['number_of_stints_allowed_single_user']) ? $data['number_of_stints_allowed_single_user'] : 0;
        $data['number_of_stints_allowed_agency_user'] = !empty($data['number_of_stints_allowed_agency_user']) ? $data['number_of_stints_allowed_agency_user'] : 0;
        $data['number_of_proposal_allowed'] = !empty($data['number_of_proposal_allowed']) ? $data['number_of_proposal_allowed'] : 0;
        $data['seller_contact_limit_agency_user'] = !empty($data['seller_contact_limit_agency_user']) ? $data['seller_contact_limit_agency_user'] : 0;

        $obj = new Membership();
        $obj->fill($data);
        $obj->save();
        if(!empty($data['variations'])) {
            $this->saveVariations($data['variations'], $obj);
        }
        return redirect()->to('/superaccess/memberships')->with('success','Membership created successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($membership)
    {
        return $this->view('create', ['membership'=> Membership::find($membership)->with('variations')->first()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        $data['can_view_course_details'] = !empty($data['can_view_course_details']) ? $data['can_view_course_details'] : 0;
        $data['can_view_product_details'] = !empty($data['can_view_product_details']) ? $data['can_view_product_details'] : 0;
        $data['can_invite_friends'] = !empty($data['can_invite_friends']) ? $data['can_invite_friends'] : 0;
        $data['visible_in_seller_search'] = !empty($data['visible_in_seller_search']) ? $data['visible_in_seller_search'] : 0;
        $data['can_contact_seller'] = !empty($data['can_contact_seller']) ? $data['can_contact_seller'] : 0;
        $data['can_submit_proposals'] = !empty($data['can_submit_proposals']) ? $data['can_submit_proposals'] : 0;
        $data['can_register_to_course'] = !empty($data['can_register_to_course']) ? $data['can_register_to_course'] : 0;
        $data['can_add_product_to_market_place'] = !empty($data['can_add_product_to_market_place']) ? $data['can_add_product_to_market_place'] : 0;
        $data['eligible_for_cash_reward'] = !empty($data['eligible_for_cash_reward']) ? $data['eligible_for_cash_reward'] : 0;
        $data['can_purchase_product'] = !empty($data['can_purchase_product']) ? $data['can_purchase_product'] : 0;
        $data['can_submit_bid'] = !empty($data['can_submit_bid']) ? $data['can_submit_bid'] : 0;
        $data['can_create_course'] = !empty($data['can_create_course']) ? $data['can_create_course'] : 0;
        $data['can_order_stint'] = !empty($data['can_order_stint']) ? $data['can_order_stint'] : 0;
        $data['seller_contact_limit_single_user'] = !empty($data['seller_contact_limit_single_user']) ? $data['seller_contact_limit_single_user'] : 0;
        $data['number_of_stints_allowed_single_user'] = !empty($data['number_of_stints_allowed_single_user']) ? $data['number_of_stints_allowed_single_user'] : 0;
        $data['number_of_stints_allowed_agency_user'] = !empty($data['number_of_stints_allowed_agency_user']) ? $data['number_of_stints_allowed_agency_user'] : 0;
        $data['number_of_proposal_allowed'] = !empty($data['number_of_proposal_allowed']) ? $data['number_of_proposal_allowed'] : 0;
        $data['seller_contact_limit_agency_user'] = !empty($data['seller_contact_limit_agency_user']) ? $data['seller_contact_limit_agency_user'] : 0;

        $obj = Membership::find($id);
        $obj->fill($data);
        $obj->save();
        if(!empty($data['variations'])) {
            $this->saveVariations($data['variations'], $obj);
        }
        return redirect()->to('/superaccess/memberships')->with('success','Membership updated successfully!');
    }

    protected function saveVariations($variations, $membership) {

        if($variations) {
            $keys = array_keys($variations);
            $tempVariations = [];
            for ( $i = 0; $i < count($variations['name']); $i++) {
                foreach ($keys as $key) {
                    $tempVariations[$i][$key] = $variations[$key][$i];
                }
            }
            foreach ($tempVariations as $variation) {
                $obj = new MembershipVariation();
                $variation['membership_id'] = $membership->id;
                if(empty($variation['id'])) {
                    $obj->fill($variation)
                        ->save();
                } else {
                    $obj->find($variation['id'])
                        ->fill($variation)
                        ->save();
                }
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Membership $membership)
    {
        if($membership) {
            $membership->delete();
            return redirect()->to('/superaccess/memberships')->with('success','memberships has been deleted successfully!');
        }
        return redirect()->to('/superaccess/memberships')->with('error','Something went wrong please try again.');
    }

    protected function view($view, $data = [] ) {
        return view($this->dir.'/'.$view, $data);
    }
}
