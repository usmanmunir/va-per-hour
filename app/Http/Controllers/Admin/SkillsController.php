<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Skill;
use Illuminate\Http\Request;

class SkillsController extends Controller
{
    protected $dir='admin/skills';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $skills = Skill::findRequested();

        return $this->view('index', ['skills'=> $skills]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return redirect()->to('/superaccess/skills');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $obj = new Skill();
        $obj->fill($data);
        $this->validate($request, Skill::$rules);
        $obj->save();
        return redirect()->to('/superaccess/skills')->with('success','Skill created successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $skills = Skill::paginate(15);
        $cats = Skill::where('id', $id)->first();

        return $this->view('index', ['skills'=> $skills, 'cats'=> $cats, 'skillToEdit' => $cats]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = Skill::$rules;
        $data = $request->all();

        $obj = Skill::find($id);
        $obj->fill($data);
        $this->validate($request, $rules);
        $obj->save();
        return redirect()->to('/superaccess/skills')->with('success','Skill updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Skill $skill)
    {
        if($$skill) {
            $skill->delete();
            return redirect()->to('/superaccess/skills')->with('success','Skill has been deleted successfully!');
        }
        return redirect()->to('/superaccess/skills')->with('error','Something went wrong please try again.');
    }

    protected function view($view, $data = [] ) {
        return view($this->dir.'/'.$view, $data);
    }
}
