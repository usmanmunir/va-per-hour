<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Cartalyst\Sentry\Hashing\NativeHasher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class DashboardController extends Controller
{

    protected $redirectTo = '/superacccess';

    protected $dir = 'admin/dashboard';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function index() {
        return $this->view('index');
    }

    public function view ($view) {
        return view($this->dir.'/'.$view);
    }
}
