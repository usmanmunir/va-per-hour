<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    protected $dir='admin/categories';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::findRequested();
        $cats = Category::where('status', '1')->get();

        return $this->view('index', ['categories'=> $categories, 'cats'=>$cats]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return redirect()->to('/superaccess/categories');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['is_featured'] = !empty($data['is_featured']) ? $data['is_featured'] : 1;
        $data['put_in_menu'] = !empty($data['put_in_menu']) ? $data['put_in_menu'] : 1;
        $data['put_in_home'] = !empty($data['put_in_home']) ? $data['put_in_home'] : 1;

        $obj = new Category();
        $obj->fill($data);
        $this->validate($request, Category::$rules);

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $name = str_slug($request->get('slug')).'.'.$image->getClientOriginalExtension();
            $destinationPath = 'uploads/categories';

            $imagePath = $destinationPath. "/".  $name;
            $image->move($destinationPath, $name);
            $obj->image = $imagePath;
        }

        $obj->save();
        return redirect()->to('/superaccess/categories')->with('success','Category created successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $categories = Category::paginate(15);
        $cats = Category::where('status', '1')->get();

        return $this->view('index', ['categories'=> $categories, 'cats'=> $cats, 'categoryToEdit' => $category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = Category::$rules;
        $rules['name'] = 'required|unique:categories,category_type';
        $rules['slug'] = 'required|unique:categories,category_type';
        $data = $request->all();
        $data['is_featured'] = !empty($data['is_featured']) ? $data['is_featured'] : 1;
        $data['put_in_menu'] = !empty($data['put_in_menu']) ? $data['put_in_menu'] : 1;
        $data['put_in_home'] = !empty($data['put_in_home']) ? $data['put_in_home'] : 1;

        $obj = Category()::find($id);
        $obj->fill($data);
        $this->validate($request, $rules);

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $name = str_slug($request->get('slug')).'.'.$image->getClientOriginalExtension();
            $destinationPath = 'uploads/categories';

            $imagePath = $destinationPath. "/".  $name;
            $image->move($destinationPath, $name);
            $obj->image = $imagePath;
        }

        $obj->save();
        return redirect()->to('/superaccess/categories')->with('success','Category updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        if($category) {
            $category->delete();
            return redirect()->to('/superaccess/categories')->with('success','Category has been deleted successfully!');
        }
        return redirect()->to('/superaccess/categories')->with('error','Something went wrong please try again.');
    }

    protected function view($view, $data = [] ) {
        return view($this->dir.'/'.$view, $data);
    }
}
