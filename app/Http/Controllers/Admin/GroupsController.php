<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class GroupsController extends Controller
{

    protected $redirectTo = '/superacccess';

    protected $dir = 'sentinel/groups';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function index() {
        return $this->view('index');
    }

    public function view ($view) {
        return view($this->dir.'/'.$view);
    }
}
