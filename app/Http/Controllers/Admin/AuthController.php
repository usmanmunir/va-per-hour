<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Cartalyst\Sentry\Sentry;

class AuthController extends Controller
{

    protected $redirectTo = '/superacccess';

    protected $dir = 'admin/auth';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login() {
        return $this->view('login');
    }

    public function view ($view) {
        return view($this->dir.'/'.$view);
    }
}
