<?php

namespace App\Http\Controllers\Admin;

use App\Models\Course;
use App\Models\user;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CourseController extends Controller
{
    //
    public function dashboard()
    {
        return view('admin.course.dashboard');
    }

    public function getAllCources()
    {
        $cources = Course::findRequested();
//        $cources = Course::with('user')->with('category')->paginate(15);
        $courseCategories = Category::where('category_type', '=', 'course')->get();
        $users = User::where('activated', '=', '0')->get();
        return view('admin.course.index')->with('cources', $cources)
            ->with('courseCategories', $courseCategories)
            ->with('users', $users);
    }

    public function courseOrders()
    {
        return view('admin.course.order');
    }

    public function instructorUsers()
    {
        return view('admin.course.instructor-user');
    }

    public function courseDetail($courseId)
    {
        $singleCourse = Course::with('user')->with('category')
            ->where('id', '=', $courseId)->first();
//        dd($singleCourse);
        return view('admin.course.course-detail')->with('singleCourse', $singleCourse);
    }
}
