<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Tag;
use Illuminate\Http\Request;

class TagsController extends Controller
{
    protected $dir='admin/tags';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tags = Tag::findRequested();

        return $this->view('index', ['tags'=> $tags]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return redirect()->to('/superaccess/tags');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $obj = new Tag();
        $obj->fill($data);
        $this->validate($request, Tag::$rules);
        $obj->save();
        return redirect()->to('/superaccess/tags')->with('success','Tags created successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tags = Tag::findRequested();
        $cats = Tag::where('id', $id)->first();

        return $this->view('index', ['tags'=> $tags, 'cats'=> $cats, 'tagToEdit' => $cats]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = Tag::$rules;
        $data = $request->all();

        $obj = Tag::find($id);
        $obj->fill($data);
        $this->validate($request, $rules);
        $obj->save();
        return redirect()->to('/superaccess/tags')->with('success','Tags updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tag $tag)
    {
        if($tag) {
            $tag->delete();
            return redirect()->to('/superaccess/tags')->with('success','Tags has been deleted successfully!');
        }
        return redirect()->to('/superaccess/tags')->with('error','Something went wrong please try again.');
    }

    protected function view($view, $data = [] ) {
        return view($this->dir.'/'.$view, $data);
    }
}
