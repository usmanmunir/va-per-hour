<?php

namespace App\Http\Controllers\Admin;

use App\Models\Marketplaceproduct;
use App\Models\user;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MarketplaceController extends Controller
{
    //
    public function dashboard()
    {
        $marketplaceProducts = Marketplaceproduct::orderBy('id', 'DESC')->get();
        $latestMarketplaceProducts = $marketplaceProducts->take(5);
//        dd($latestMarketplaceProducts);
        $totalMarketplaceProducts = count($marketplaceProducts);
//        latest Members
        $latestMembers = user::where('activated', '=', '1')
            ->orderBy('id', 'DESC')
            ->take(8)->get();

        return view('admin.marketplace.dashboard')->with('totalMarketplaceProducts', $totalMarketplaceProducts)
            ->with('latestMembers', $latestMembers)->with('latestMarketplaceProducts', $latestMarketplaceProducts);
    }

    public function marketplaceProducts()
    {
        $marketplaceProducts = Marketplaceproduct::findRequested();
        $marketplaceCategories = Category::where('category_type', '=', 'marketplace')->get();
        $users = User::where('activated', '=', '0')->get();
        return view('admin.marketplace.index')->with('marketplaceProducts', $marketplaceProducts)
            ->with('marketplaceCategories', $marketplaceCategories)
            ->with('users', $users);
    }

    public function marketplaceOrders()
    {
        return view('admin.marketplace.order');
    }

    public function marketplaceProductDetail($marketplaceProductId)
    {
        $singleMarketplaceProduct = Marketplaceproduct::with('user')
            ->with('category')->where('id', '=', $marketplaceProductId)->first();
        return view('admin.marketplace.product-detail')->with('singleMarketplaceProduct', $singleMarketplaceProduct);
    }
}
