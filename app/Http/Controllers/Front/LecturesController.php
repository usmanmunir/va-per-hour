<?php

namespace App\Http\Controllers\Front;
use Auth;
use App\Models\Chapter;
use App\Models\Course;
use App\Models\Lecture;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LecturesController extends Controller
{
    //
    public function index(Request $request, Course $course, Chapter $chapter)
    {
        $lectures = Lecture::where('chapter_id', $chapter->id)->get();
        return view('front.course.lecture.lecture')
            ->with('course', $course)
            ->with('chapter', $chapter)
            ->with('lectures', $lectures);
    }

    public function saveLecture(Request $request)
    {
        $lecture = new Lecture();
        if (!empty($request->get('id'))) {
            $lectureSave = $lecture->find($request->get('id'))->fill($request->all())->save();
        } else {
            $lectureSave = $lecture->fill($request->all())->save();
        }
        if ($lectureSave) {
            return response()->json([
                'data' => $lectureSave,
                'message' => 'Chapter has been Saved!',
                'alert-type' => 'success'
            ], 200);
        } else {
            return response()->json([
                'message' => 'Something went Wrong',
                'alert-type' => 'error'
            ], 403);
        }
    }

    public function deleteLecture($lectureId)
    {
        if (Auth::guest()) {
            return redirect('/signin')->with('warning', 'Your session has expired, please login again');
        }
        $lectureId = Lecture::find($lectureId);
        if ($lectureId->delete()) {
            return response()->json([
                'message' => 'Lecture Delete Successfully',
                'alert-type' => 'success'
            ]);
        } else {
            return response()->json([
                'message' => 'Something went wrong',
                'alert-type' => 'error'
            ]);
        }
    }

    public function saveVideo(Request $request)
    {
            $video = $request->file('file');
            $videoName = str_slug('vaperhourLecture') . '_' . time();
            $folder = '/uploads/videos/Lectures/';
            $filePath = $folder . $videoName . '.' . $video->getClientOriginalExtension();
            $video->move(public_path() . '/uploads/videos/Lectures/', $videoName . '.' . $video->getClientOriginalExtension());
            return $filePath;
    }
}


