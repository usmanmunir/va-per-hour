<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;

class AuthController extends Controller
{

    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
