<?php

namespace App\Http\Controllers\Front;

use App\Models\Proposal;
use Illuminate\Http\Request;
use App\Models\Job;
use App\User;
use Auth;
use App\Models\Category;

;

use App\Http\Controllers\Controller;

class ProposalController extends Controller
{
    private $dir = 'front.proposal';

    //
    public function index($jobId)
    {
        
        $jobDetail = Job::with('user')->with('category')->where('id', '=', $jobId)->first();
        return view('front.proposal.add-proposal')->with('jobDetail', $jobDetail);
    }

    public function submitProposal(Request $request)
    {
        $obj = new Proposal();
        $data = $request->all();
        $data['user_id'] = Auth::user()->id;
        $obj->fill($data);
        $obj->save();
        return ['code' => 203,
            'message' => 'Proposal Submit Successfully'];
    }

    public function getProposalRequest($jobId = null)
    {
//        dd($jobId);
        if ($jobId == null) {
            $submitProposals = Proposal::with('job')->with('user')->get();
        } else {
            $submitProposals = Proposal::with('job')->with('user')->
            where('job_id', '=', $jobId)->get();
        }
        return view('front.proposal.submit-proposal')->with('submitProposals', $submitProposals);
//        dd($submitProposals);
    }

    public function uploadProposalDoc(Request $request)
    {
        $folder = '/uploads/proposal/documents/';

        if (!file_exists($folder)) {
            mkdir($folder, 0777, true);
        }
        $pdf = $request->file('file');
        $pdfName = uniqid() . '_' . trim($pdf->getClientOriginalName());
        $pdfPath = $folder . $pdfName;
        $pdf->move(public_path() . '/uploads/proposal/documents/', $pdfName);
        return $pdfPath;
    }

}
