<?php

namespace App\Http\Controllers\Front;

use App\Models\Country;
use App\Models\Membership;
use App\Models\MembershipVariation;
use App\Models\UserLocation;
use App\Models\UserMembership;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserMembershipsController extends Controller
{
    protected $dir = 'front/memberships';

    public  function index() {

        $user = Auth::user();
        $memberships = Membership::where('user_type', $user->user_type)
            ->with('variations')
            ->get();
        $userMemberships = UserMembership::where('user_id', $user->id)->get();
        return $this->view('pricing-plan', ['memberships' => $memberships, 'user_memberships' => $userMemberships]);
    }

    public function buyMembership (Request $request, Membership $membership) {
        if ($membership->id == 8) {
            $nextDueDate = date('Y-m-d', time()+(60*60*24*30));
            UserMembership::where('user_id', \Auth::user()->id)->destroy();
        }
        else {
            //load pricing plan to calculate next due date
            $variation = MembershipVariation::where('id', $request->get('pricing_plans'))->first();
            $month = $variation ? $variation->month : 1;
            $nextDueDate = date('Y-m-d', time() + (60 * 60 * 24 * (30 * $month)));
        }

        //UserMembership::create(['membership_id' => $membership->id, 'user_id'=> Auth::user()->id, 'next_due_date' => $nextDueDate]);
        if ($membership->id == 8) {
            return redirect()->to('edit-profile')->with('success', 'membership updated');
        }
        $countries = Country::all();
        $userLocation = UserLocation::where('user_id', \Auth::user()->id)->first();
        return $this->view('buy-plan', [
            'membership' => $membership,
            'variation' => $variation,
            'countries' => $countries,
            'userLocation' => $userLocation,

        ]);
    }

    public function confirmMembership (Request $request, Membership $membership) {
        $variation = MembershipVariation::where('id', $request->get('variation'))->first();
        $month = $variation ? $variation->month : 1;
        $nextDueDate = date('Y-m-d', time() + (60 * 60 * 24 * (30 * $month)));
        $check = UserMembership::updateOrCreate(
            ['membership_id' => $membership->id, 'user_id'=> Auth::user()->id], // condition
            ['membership_id' => $membership->id, 'user_id'=> Auth::user()->id, 'next_due_date' => $nextDueDate] //updates
        );
        if($check) {
            return redirect()->to('/user/membership');
        } else {
            echo "error";
        }
    }

    protected function view ($view, $data) {
        return view($this->dir.'.'.$view, $data);
    }
}
