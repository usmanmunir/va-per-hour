<?php

namespace App\Http\Controllers\Front;

use App\Models\Category;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function getSubCategory (Request $request, Category $category) {
        return Category::where('parent', $category->id)->get()->pluck('name', 'id');
    }
}
