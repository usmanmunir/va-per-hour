<?php

namespace App\Http\Controllers\Front;

use App\Models\Offer;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Validator;

class OfferController extends Controller {

    public function removeOffer(Request $request, $offer_id) {
        $offer_id = str_replace(csrf_token(), "", $offer_id);
        $check = Offer::where('id', $offer_id)->delete();
        if($check) {
            return response()->json(['message'=> 'Offer has been removed'], 200);
        }
        return response()->json(['message'=> 'problem while deleting offer'], 403);
    }

    public function saveOffer (Request $request, $request_id=null) {
        $request_id = str_replace(csrf_token(), "", $request_id);
        $user_id = str_replace(csrf_token(), "", $request->get('user_id'));
        $check = Offer::where('request_id', $request_id)
            ->whereNull('deleted_at')
            ->where('user_id', $user_id)
            ->first();
        if(!$check) {
            $data = $request->all();
            $data['request_id'] = $request_id;
            $data['user_id'] = $user_id;
            $obj = new Offer();
            $obj->fill($data)->save();
            return response()->json(['message'=> 'Offer has been submitted to buyer', 'data' => $obj], 200);
        }
        return response()->json(['message'=> 'Offer Already exists for this job.'], 403);
    }
}