<?php

namespace App\Http\Controllers\Front;

use App\Models\Chapter;
use Auth;
use App\Models\Course;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ChapterController extends Controller
{
    //
    public function index(Request $request, Course $course)
    {
        $chapters = Chapter::where('course_id', $course->id)
            ->with('lectures')
            ->get();
        return view('front.course.chapter.chapter')
            ->with('course', $course)
            ->with('chapters', $chapters);
    }

    public function saveChapter(Request $request)
    {
        $chapter = new Chapter();
        if (!empty($request->get('id'))) {
            $chapterSave = $chapter->find($request->get('id'))->fill($request->all())->save();
        } else {
            $chapterSave = $chapter->fill($request->all())->save();
        }
        if ($chapterSave) {
            return response()->json([
                'data' => $chapterSave,
                'message' => 'Chapter has been Saved!',
                'alert-type' => 'success'
            ], 200);
        } else {
            return response()->json([
                'message' => 'Something went Wrong',
                'alert-type' => 'error'
            ], 403);
        }
    }


    public function deleteChapter($chapterId)
    {
        if (Auth::guest()) {
            return redirect('/signin')->with('warning', 'Your session has expired, please login again');
        }
        $chapterid = Chapter::find($chapterId);
        if ($chapterid->delete()) {
            return response()->json([
                'message' => 'Chapter Delete Successfully',
                'alert-type' => 'success'
            ]);
        } else {
            return response()->json([
                'message' => 'Something went wrong',
                'alert-type' => 'error'
            ]);
        }
    }
}
