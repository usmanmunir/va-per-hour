<?php

namespace App\Http\Controllers\Front;

use App\Models\Category;
use App\Models\Chapter;
use App\Models\Offer;
use Auth;
use App\Models\Course;
use function foo\func;
use Illuminate\Http\Request;
use \App\Models\Request as jobRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class BuyerController extends Controller
{
    public function ManageRequests (Request $request, $status=1) {
        $jobRequests = jobRequest::where('user_id', Auth::user()->id);
        $jobRequests->where('status', $status);
        $jobRequests->withCount('offers');
        return view('front.buyer.manage-requests', [ 'status' => $status,'jobRequests' =>$jobRequests->get()]);
    }

    public function manageOffers (Request $request, $job_request) {
        $job_request = str_replace(csrf_token(), "", $job_request);
        if(empty($job_request)) {
            return redirect()->back()->with('error', 'Requested is not selected to pause');
        }
        $request = jobRequest::where('id', $job_request)->withCount('offers')->first();
        $offers = Offer::where('request_id', $job_request)->with(['stint'=> function ($q){
            return $q->with('gallery');
        }])->with('user')->get();
        return view('front.buyer.manage-offers', ['request'=> $request, 'offers' => $offers]);
    }

    public function postRequest (Request $request) {
        $categories = Category::where('category_type', 'stint')->where('parent', 0)->get();
        return view('front.buyer.post-request', ['categories'=> $categories]);
    }

    public function editRequest (Request $request, $job_request) {
        $job_request = JobRequest::where('id', (str_replace(csrf_token(), '', $job_request)*1))
            ->first();
        $categories = Category::where('category_type', 'stint')->where('parent', 0)->get();
        $subcategories = Category::where('category_type', 'stint')->where('parent', $job_request->category_id)->get();
        return view('front.buyer.post-request', [
            'categories'=> $categories,
            'subcategories'=>$subcategories,
            'request' => $job_request
        ]);
    }

    public function saveRequest (Request $request) {
        $jobRequest = new jobRequest();
        $data = $request->all();
        $data['user_id'] = Auth::user()->id;
        if(empty($data['id'])) {
            $jobRequest->fill($data);
            $jobRequest->save();
        } else {
            $jobRequest->find($data['id'])->fill($data)->save();
        }
        return redirect()->to('/job/manage-requests')->with('success', 'Request has been pulished');
    }

    public function pauseRequest (Request $request, $job_request) {
        $job_request = str_replace(csrf_token(), "", $job_request);
        if(empty($job_request)) {
            return redirect()->back()->with('error', 'Requested is not selected to pause');
        }
        $jobRequest = new jobRequest();
        $jobRequest->where('id', 1*$job_request)->update(['status'=> 2]);
        return redirect()->back()->with('success', 'Request has been paused');
    }

    public function activateRequest (Request $request, $job_request) {
        $job_request = str_replace(csrf_token(), "", $job_request);
        if(empty($job_request)) {
            return redirect()->back()->with('error', 'Requested is not selected to activate');
        }
        $jobRequest = new jobRequest();
        $jobRequest->where('id', 1*$job_request)->update(['status'=> 3]);
        return redirect()->to('/job/manage-requests/3')->with('success', 'Request has been paused');
    }

    public function deleteRequest (Request $request, $job_request) {
        $job_request = str_replace(csrf_token(), "", $job_request);
        if(empty($job_request)) {
            return redirect()->back()->with('error', 'Requested is not selected to Delete');
        }
        $jobRequest = new jobRequest();
        $jobRequest->where('id', 1*$job_request)->delete();
        return redirect()->back()->with('success', 'Request has been deleted');
    }

}
