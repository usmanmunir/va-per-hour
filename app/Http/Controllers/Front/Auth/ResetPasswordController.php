<?php

namespace App\Http\Controllers\Front\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use App\User;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'new_password' => 'required',
            'confirm_password' => 'required|same:new_password'
        ]);
    }

    public function changePassword(Request $request)
    {
        $user = Auth::getUser();
        $this->validator($request->all())->validate();
        if (Hash::check($request->get('current_password'), $user->password)) {
//            $user->password = $request->get('new_password');
            $user->password = Hash::make($request->get('new_password'));

            if ($user->save()) {
                return response()->json([
                    'data' => $user,
                    'message' => 'Password Update successfully'
                ], 200);
            } else {
                return response()->json([
                    'message' => 'something went wrong'
                ], 403);
            }

        }
        else{
            return response()->json([
                'message' => 'something went wrong'
            ], 403);
        }
    }
    }

