<?php

namespace App\Http\Controllers\Front\Auth;

use App\Models\Membership;
use App\Models\UserMembership;
use App\User;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
//use http\Client\Request;
use Illuminate\Support\Facades\Hash;
Use App\Models\MasterUservarify;
use App\Models\MasterUser;
use App\Mail\VerifyMail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\User
     */
    public function showRegisterform()
    {
        if (Auth::guest()) {
            return view('front.user.register');
        } else {
            return redirect('/');
        }
    }

    protected function create(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => 'required'
        ]);
        $user = User::create([
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
            'user_type' => $request['user_type'],
        ]);
        $verifyUser = MasterUservarify::create([
            'user_id' => $user->id,
            'token' => sha1(time())
        ]);

        \Mail::to($user->email)->send(new VerifyMail($user, $verifyUser));
        return redirect('/');
    }

    public function verifyUser($token)
    {
        $verifyUser = MasterUservarify::where('token', $token)->first();
        if (isset($verifyUser)) {
            $user = $verifyUser->user;
            if (!$user->verified) {
                $verifyUser->user->verified = 1;
                $verifyUser->user->is_active = 1;
                $verifyUser->user->save();
                $status = "Your e-mail is verified. You can now login.";
            } else {
                $status = "Your e-mail is already verified. You can now login.";
            }
        } else {
            return redirect('/signin')->with('warning', "Sorry your email cannot be identified.");
        }
        return redirect('/signin')->with('status', $status);
    }

    public function authenticated(Request $request, $user)
    {
        if (!$user->verified) {
            auth()->logout();
            return back()->with('warning', 'You need to confirm your account. We have sent you an activation code, please check your email.');
        }
        return redirect()->intended($this->redirectPath());
    }
}
