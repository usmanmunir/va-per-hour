<?php

namespace App\Http\Controllers\Front\Auth;

use App\Http\Controllers\Controller;
use App\Models\UserMembership;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;
use Redirect;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
        $this->middleware('guest')->except('signout');
    }

    public function showLogin()
    {
        if (Auth::guest()) {
            return view('front.user.login');
        } else {
            return redirect('/');
        }
    }

    public function doLogin(Request $request)
    {
        $userdata = array(
            'email' => Input::get('email'),
            'password' => Input::get('password'),
            'is_active' => 1,
            'verified' => 1
        );
        $rules = array(
            'email' => 'required|email',
            'password' => 'required'
        );

        $validator = Validator::make($userdata, $rules);
        // attempt to do the login
        if ($validator->passes()) {
            if (Auth::attempt($userdata)) {
                $notification = array(
                    'message' => 'User Login successfully',
                    'alert-type' => 'success'
                );
                $check = UserMembership::where('user_id', Auth::user()->id)->get();
                if(count($check) == 0) {
                   return redirect()->intended('/user/pricing-plan');
                }
                session(['mode' => 'freelancer', 'module' => 'stint']);
                return redirect()->intended('/user-profile/'.Auth::user()->id.'/'.Auth::user()->username)->with($notification);
            } else {
                $danger = array(
                    'message' => 'wrong user Email or password',
                    'alert-type' => 'error'
                );
                // validation not successful, send back to form
                return Redirect::to('signin')->with($danger);

            }
        } else {
            $errors = $validator->errors()->messages();
            $message = '';
            foreach ($errors as $key => $value) {
                $message .= $value[0].'\n';
            }
            $danger = array(
                'message' => $message,
                'alert-type' => 'error'
            );
            // validation not successful, send back to form
            return Redirect::to('signin')->with($danger);
        }

    }

    public function signout(Request $request)
    {

        Auth::logout();
        return redirect('/');
    }
}
