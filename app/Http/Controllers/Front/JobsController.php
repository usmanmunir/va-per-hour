<?php

namespace App\Http\Controllers\Front;

use App\Models\Category;
use App\Models\Job;
use App\Models\Proposal;
use App\Models\Acceptejob;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Validator;

class JobsController extends Controller
{
    private $dir = 'front.jobs';

    //
    public function index(Request $request)
    {
        $jobs = Job::with('user')
            ->with('category')->where('status', '=', 1)
            ->paginate(6);
        return $this->view('index')->with('jobs', $jobs);
    }

    public function getJobByCategory(Request $request, $job_id='all', $job_name='') {
        $jobs = Job::with('user')
            ->with('category')
            ->where('status', '=', 1);
        if(!empty($job_id) && $job_id != 'all') {
            $jobs->where('category_id', $job_id);
        }
        $jobs = $jobs->paginate(6);
        return $this->view('index')->with('jobs', $jobs);
    }
//
//    public function getJobByCategory($categorySlug, $categoryId)
//    {
//        $jobs = Job::with('user')->with('category')->where('status', '=', '1')
//            ->where('category_id', '=', $categoryId)->paginate(6);
//        return $this->view('index')->with('jobs', $jobs);
//    }

    public function manageRequests(Request $request, $status = 1)
    {
        $jobRequests = Job::where('user_id', \Auth::user()->id)
            ->where('status', $status)
            ->with(['job_proposals' => function ($q) {
                return $q->groupBy('job_id')->count();
            }])
            ->get();
        $activeJobs = Job::where('user_id', \Auth::user()->id)->where('status', 1)->count();
        $pausedJobs = Job::where('user_id', \Auth::user()->id)->where('status', 2)->count();
        $pendingJobs = Job::where('user_id', \Auth::user()->id)->where('status', 3)->count();
        $unapprovedJobs = Job::where('user_id', \Auth::user()->id)->where('status', 4)->count();
        return $this->view('manage-requests', [
            'job_requests' => $jobRequests,
            'active_job' => $activeJobs,
            'paused_job' => $pausedJobs,
            'pending_job' => $pendingJobs,
            'unapproved_job' => $unapprovedJobs,
            'status' => $status
        ]);
    }

    public function create(Request $request)
    {
        $categories = Category::where('parent', 0)
            ->where('category_type', 'stint')
            ->get();
        return $this->view('create', ['categories' => $categories]);
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $validator = \Validator::make($data, Job::$rules);
        if ($validator->fails()) {
            return redirect('job/post-request')
                ->withErrors($validator)
                ->withInput();
        }
        $obj = new Job();
        $obj->fill($data);
        $obj->save();
        return redirect()->to('/job/manage-requests')->with('success', 'Job request has been created successfully!!');
    }

    public function pause(Request $request, Job $job)
    {
        $job->status = 2;
        $job->save();
        return redirect()->to('/job/manage-requests')->with('success', 'Job has been updated');
    }

    public function resume(Request $request, Job $job)
    {
        $job->status = 1;
        $job->save();
        return redirect()->to('/job/manage-requests')->with('success', 'Job has been updated');
    }

    public function delete(Request $request, Job $job)
    {
        $job->delete();
        return redirect()->to('/job/manage-requests')->with('success', 'Job request has been Deleted successfully!!');
    }

    protected function view($view, $data = [])
    {
        return view($this->dir . '.' . $view, $data);
    }

    public function jobDetail($jobId)
    {
        $proposalStatusForUser = Proposal::where('job_id','=',$jobId)->where('user_id','=',Auth::user()->id)->get();
        $jobDetail = Job::with('user')->with('category')->where('id','=',$jobId)->first();
        return $this->view('job-detail')->with('jobDetail',$jobDetail)->with('proposalStatusForUser',$proposalStatusForUser);
    }

    public function acceptJob(Request $request)
    {
        $data = $request->all();
        $obj = new Acceptejob();
        $data['job_id'] = 1;
        $data['proposal_id'] = 1;
        $data['status'] = 1;
        $obj->fill($data);
        $obj->save();
        return ['code' => 203,
            'message' => 'Freelance Hire Successfully'];
    }
}


