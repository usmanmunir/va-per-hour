<?php

namespace App\Http\Controllers\Front;

use App\Models\Instructor;
use Illuminate\Http\Request;
use Auth;
use App\Models\Category;
use App\Http\Controllers\Controller;

class InstructorController extends Controller
{
    //
    public function userInstructor()
    {
        if (!Auth::guest()) {
            $userId = Auth::user()->id;
            $categories = Category::where('category_type','=','course')->get();
            $instructorRecord = Instructor::with('category')->where('user_id', '=', $userId)->first();
//            dd($instructorRecord);
            return view('front.user.instructor')->with('instructorRecord', $instructorRecord)->with('categories',
                $categories);
        } else {
            return redirect('/');
        }
    }

    public function saveInstructorRecord(Request $request)
    {
        $userId = Auth::user()->id;
        $instructor = Instructor::updateOrCreate([
            'user_id' => $userId,
        ], [
            'user_id' => Auth::user()->id,
            'category_id' => $request->get('category'),
            'course_skills' => $request->get('course_skills'),
            'expertise_level' => $request->get('expertise_level'),
            'professional_experience' => $request->get('professional_experience'),
            'comments' => $request->get('other_comments'),
        ]);
        if ($instructor) {
            return response()->json([
                'data' => $instructor,
                'message' => 'Instructor Record Save Successfully',
                'alert-type' => 'success'
            ], 200);
        } else {
            return response()->json([
                'message' => 'Something went Wrong',
                'alert-type' => 'error'
            ], 403);
        }

    }
}
