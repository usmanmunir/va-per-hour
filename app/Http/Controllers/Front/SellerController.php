<?php

namespace App\Http\Controllers\Front;

use App\Models\Offer;
use App\Models\Stint;
use Auth;
use \App\Models\Request as jobRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SellerController extends Controller
{
    //
    public function index(Request $request)
    {
        return view('front.seller.dashboard');
    }

    public function analytics (Request $request) {
        return view('front.seller.analytics');
    }

    public function earnings (Request $request) {
        return view('front.seller.earnings');
    }

    public function buyerRequests (Request $request) {
        $requests = jobRequest::where('status', 1)
            ->withCount('offers')
            ->with('user')
            ->paginate(15);
        $stints = Stint::where('user_id', Auth::user()->id)->where('status', 1)->get();
        return view('front.seller.buyer-requests', ['requests' =>$requests, 'stints' => $stints]);
    }

    public function sentOffers (Request $request) {
        $offers = Offer::where('user_id', Auth::user()->id)
            ->where('status', 1)
            ->with(['request' => function($q){
                return $q->with('user');
            }])
            ->with('stint')
            ->get();
        return view('front.seller.sent-offers', ['offers' => $offers]);
    }

}
