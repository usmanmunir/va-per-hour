<?php

namespace App\Http\Controllers\Front;

use App\Models\UserLocation;
use App\Models\UserMembership;
use App\Models\Userskill;
use App\User;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Userlanguage;
use App\Models\Usereducation;
use App\Models\Language;
use App\Models\Userlink;
use App\Models\Country;
use App\Models\Usercertificate;
use App\Traits\UploadTrait;

class MasterUserController extends Controller
{
    use UploadTrait;

    public function userProfile($user_id=false)
    {
        if(!$user_id && Auth::guest()) {
            return redirect()->to('/signin');
        }
        $user_id = $user_id ? str_replace(csrf_token(), "", $user_id) : Auth::user()->id;
        $user = User::where('id', $user_id)
            ->with(['location' => function($q){
                return $q->with('country')
                    ->with('city')
                    ->with('state');
            }])
            ->with(['stints' => function($q){
                return $q->with('category')
                        ->with('gallery')
                        ->with('price')
                        ->with('extras');
            }])
            ->with('certification')
            ->with('education')
            ->with('social_links')
            ->with(['skills'=> function($q){
                return $q->with('skill');
            }])
            ->with(['memberships' => function($q) {
                return $q->with('membership');
            }])
            ->first();

            return view('front.user.user-profile')->with('user', $user);

    }

    public function editProfile()
    {
        if (!Auth::guest()) {
            $userId = Auth::user()->id;
            $countries = Country::all();
            $userLocation = UserLocation::with('country')->where('user_id', '=', $userId)->first();
            return view('front.user.profile-edit')
                ->with('countries', $countries)
                ->with('userLocation', $userLocation);
        } else {
            return redirect('/');
        }
    }

    public function userMembership()
    {
        if (!Auth::guest()) {

            return view('front.user.membership')->with([
                'memberships' => UserMembership::where('user_id', \Auth::user()->id)->with('membership')->get()
            ]);
        } else {
            return redirect('/');
        }
    }

    public function profileSetting()
    {
        if (!Auth::guest()) {
            $userId = Auth::user()->id;
            $userAccountLinks = Userlink::where('user_id', '=', $userId)->first();
            return view('front.user.profile-setting')->with('userAccountLinks', $userAccountLinks);
        } else {
            return redirect('/');
        }
    }

    public function getPaid()
    {
        if (!Auth::guest()) {
            return view('front.user.get-paid');
        } else {
            return redirect('/');
        }
    }

    public function PasswordSecurity()
    {
        if (!Auth::guest()) {
            return view('front.user.password-security');
        } else {
            return redirect('/');
        }
    }

    public function sellerProfile()
    {
        if (!Auth::guest()) {
            $userId = Auth::user()->id;
            $userLanguages = Userlanguage::where('user_id', '=', $userId)->get();
            return view('front.user.seller-profile')->with('userLanguages', $userLanguages);
        } else {
            return redirect('/');
        }
    }

    //    Update avatar
    public function updateAvatar(Request $request)
    {

        // Form validation
        $request->validate([
            'avatar' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048'
        ]);

        // Get current user
        $user = User::findOrFail(auth()->user()->id);

        // Check if a profile image has been uploaded
        if ($request->has('avatar')) {
            // Get image file
            $image = $request->file('avatar');
            // Make a image name based on user name and current timestamp
            $name = str_slug('vaperhourUser') . '_' . time();
            // Define folder path
            $folder = '/uploads/images/';
            // Make a file path where image will be stored [ folder path + file name + file extension]
            $filePath = $folder . $name . '.' . $image->getClientOriginalExtension();
            // Upload image
            $image->move(public_path() . '/uploads/images/', $name . '.' . $image->getClientOriginalExtension());

            // Set user profile image path in database to filePath
            $user->avatar = $filePath;
        }

        // Persist user record to database
        $user->save();
        $notification = array(
            'message' => 'Image is Updated successfully',
            'alert-type' => 'success'
        );

        // Return user back and show a flash message
        return redirect()->back()->with($notification);
    }

    //    update User Detail

    public function updateUserDetail(Request $request)
    {
        $userId = Auth::user()->id;
        $user = User::find($userId);
        $user->first_name = $request->get('first_name');
        $user->last_name = $request->get('last_name');
        $user->description = $request->get('description');
        $user->save();
        $notification = array(
            'message' => 'User Record Updated successfully',
            'alert-type' => 'success'
        );

        // Return user back and show a flash message
        return redirect()->back()->with($notification);
    }

    public function updateUserLocation(Request $request)
    {
        $userId = Auth::user()->id;
        $userLocation = UserLocation::updateOrCreate([
            //Add unique field combo to match here
            //For example, perhaps you only want one entry per user:
            'user_id' => Auth::user()->id,
        ], [
            'country_id' => $request->get('country'),
            'city' => $request->get('city'),
            'zipcode' => $request->get('zipcode'),
            'address' => $request->get("address"),
            'state' => $request->get('state'),
            'phone' => $request->get('phone')
        ]);
        $notification = array(
            'message' => 'User Record Updated successfully',
            'alert-type' => 'success'
        );

// Return user back and show a flash message
        return redirect()->back()->with($notification);

    }

    public function linkdAccounts(Request $request)
    {
        $validateRule = $this->validate($request, [
            'linked_in' => 'required',
            'google' => 'required',
            'facebook' => 'required',
            'twitter' => 'required',
            'github' => 'required',
            'stack_overflow' => 'required',
            'behance' => 'required'
        ]);
        if ($validateRule) {
            $notification = array(
                'message' => 'Field can not be empty',
                'alert-type' => 'warning'
            );
        }
        $userLink = Userlink::updateOrCreate([
            //Add unique field combo to match here
            //For example, perhaps you only want one entry per user:
            'user_id' => Auth::user()->id,
        ], [
            'linked_in' => $request->get('linked_in'),
            'google' => $request->get('google'),
            'facebook' => $request->get("facebook"),
            'twitter' => $request->get('twitter'),
            'github' => $request->get('github'),
            'stack_overflow' => $request->get('stack_overflow'),
            'behance' => $request->get('behance')
        ]);
        $notification = array(
            'message' => 'User Record Updated successfully',
            'alert-type' => 'success'
        );

// Return user back and show a flash message
        return redirect()->back()->with($notification);
    }

    public function getLanguages(Request $request)
    {

        $search = $request->get('language');

        $result = Language::where('name', 'LIKE', '%' . $search . '%')->get();

        return response()->json($result);
    }

    public function saveUserLanguage(Request $request)
    {
        $userLanguage = Userlanguage::updateOrCreate([
            'id' => $request->get('userlanguageId'),
        ], [
            'user_id' => Auth::user()->id,
            'language_id' => $request->get('selectlangid'),
            'proficiency' => $request->get('proficiency'),
        ]);
        if ($userLanguage) {
            return response()->json([
                'data' => $userLanguage,
                'message' => 'Record Save Successfully',
                'alert-type' => 'success'
            ], 200);
        } else {
            return response()->json([
                'message' => 'Something went Wrong',
                'alert-type' => 'error'
            ], 403);
        }


    }

    public function getUserLanguage()
    {
        $userId = Auth::user()->id;
        $userLanguages = Userlanguage::with('language')->where('user_id', '=', $userId)->get();
        return response()->json($userLanguages, 200);
    }

    public function deleteUserLanguage($userLanguageId)
    {
        $userLanguageRecord = Userlanguage::find($userLanguageId);
        if ($userLanguageRecord->delete()) {
            return response()->json([
                'message' => 'Language Delete Successfully',
                'alert-type' => 'success'
            ]);
        } else {
            return response()->json([
                'message' => 'Something went wrong',
                'alert-type' => 'error'
            ]);
        }
    }

    public function saveUserSkill(Request $request)
    {
//        dd($request->all());
        $userLanguage = Userskill::updateOrCreate([
            'id' => $request->get('skils_id'),
        ], [
            'user_id' => Auth::user()->id,
            'skill_id' => $request->get('userskillid'),
            'skill_level' => $request->get('skill_level'),
        ]);
        if ($userLanguage) {
            return response()->json([
                'data' => $userLanguage,
                'message' => 'Record Save Successfully',
                'alert-type' => 'success'
            ], 200);
        } else {
            return response()->json([
                'message' => 'Something went Wrong',
                'alert-type' => 'error'
            ], 403);
        }


    }

    public function getUserSkill()
    {
        $userId = Auth::user()->id;
        $userSkills = Userskill::with('skill')->where('user_id', '=', $userId)->get();
        return response()->json($userSkills, 200);
    }

    public function deleteUserSkill($userSkillId)
    {
        $userSkillRecord = Userskill::find($userSkillId);
        if ($userSkillRecord->delete()) {
            return response()->json([
                'message' => 'Skill Delete Successfully',
                'alert-type' => 'success'
            ]);
        } else {
            return response()->json([
                'message' => 'Something went wrong',
                'alert-type' => 'error'
            ]);
        }
    }

    public function getUserEducation()
    {
        $userId = Auth::user()->id;
        $usereducationRecords = Usereducation::where('user_id', '=', $userId)->get();
        return response()->json($usereducationRecords, 200);
    }

    public function saveUserEducation(Request $request)
    {
        $userEducation = Usereducation::updateOrCreate([
            'id' => $request->get('id'),
        ], [
            'user_id' => Auth::user()->id,
            'institute_name' => $request->get('institute_name'),
            'degree_name' => $request->get('degree_name'),
            'year' => $request->get("year")
        ]);
        if ($userEducation) {
            return response()->json([
                'data' => $userEducation,
                'message' => 'Record Save Successfully',
                'alert-type' => 'success'
            ], 200);
        } else {
            return response()->json([
                'message' => 'Something went Wrong',
                'alert-type' => 'error'
            ], 403);
        }
    }

    public function deleteUserEducation($userEducationId)
    {
        $userEducationRecord = Usereducation::find($userEducationId);
        if ($userEducationRecord->delete()) {
            return response()->json([
                'message' => 'Record Delete Successfully',
                'alert-type' => 'success'
            ]);
        } else {
            return response()->json([
                'message' => 'Something went wrong',
                'alert-type' => 'error'
            ]);
        }
    }

    public
    function getUserCertificate()
    {
        $userId = Auth::user()->id;
        $userCertificateRecords = Usercertificate::where('user_id', '=', $userId)->get();
        return response()->json($userCertificateRecords, 200);
    }

    public function saveUserCertificate(Request $request)
    {
        $userCertificate = Usercertificate::updateOrCreate([
            'id' => $request->get('cert_id'),
        ], [
            'user_id' => Auth::user()->id,
            'certificate_name' => $request->get('certificate_name'),
            'certificate_type' => $request->get('certificate_type'),
            'year' => $request->get("year")
        ]);
        if ($userCertificate) {
            return response()->json([
                'data' => $userCertificate,
                'message' => 'Record Save Successfully',
                'alert-type' => 'success'
            ], 200);
        } else {
            return response()->json([
                'message' => 'Something went Wrong',
                'alert-type' => 'error'
            ], 403);
        }
    }

    public function deleteUserCertificate($userCertificateId)
    {
        $userCertificateRecord = Usercertificate::find($userCertificateId);
        if ($userCertificateRecord->delete()) {
            return response()->json([
                'message' => 'Record Delete Successfully',
                'alert-type' => 'success'
            ]);
        } else {
            return response()->json([
                'message' => 'Something went wrong',
                'alert-type' => 'error'
            ]);
        }
    }

    public
    function saveUserWebsite(Request $request)
    {
        $userwebsite = new User();
        $userwebsite->exists = true;
        $userwebsite->id = Auth::user()->id; //already exists in database.
        $userwebsite->website = $request->get('website');
        if ($userwebsite->save()) {
            return response()->json([
                'data' => $userwebsite,
                'message' => 'Website save successfully'
            ], 200);
        } else {
            return response()->json([
                'message' => 'something went wrong'
            ], 403);
        }
    }
}
