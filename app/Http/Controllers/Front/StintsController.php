<?php

namespace App\Http\Controllers\Front;

use App\Helpers\EmailNotificationHelper;
use App\Models\Category;
use App\Models\FavoriteStint;
use App\Models\Job;
use App\Models\Stint;
use App\Models\StintExtra;
use App\Models\StintFaq;
use App\Models\StintFlag;
use App\Models\StintGalleryExtra;
use App\Models\StintPricingPlan;
use App\Models\StintRequirement;
use App\Models\StintGallery;
use App\Models\StintOrder;
use App\User;
use Auth;
use function foo\func;
use http\Env\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Validator;

class StintsController extends Controller
{
    private $dir = 'front.stints';

    //
    public function index(Request $request)
    {
        $stintRecords = Stint::where('status', 1)
            ->with('category')
            ->with('user')
            ->with('gallery')
            ->with('price')
            ->paginate(10);
        return $this->view('front.stint-list')->with('stintRecords', $stintRecords);
    }

    public function getByCategories (Request $request, $category='all', $category_name='') {
        $stintRecords = Stint::where('status', 1);
        if(!empty($category) && $category != 'all') {
            $stintRecords->where('category_id', $category);
        }
        $stintRecords = $stintRecords->with('category')
                                    ->with('user')
                                    ->with('gallery')
                                    ->with('price')
                                    ->paginate(10);
        return $this->view('front.stint-list')->with('stintRecords', $stintRecords);
    }

    public function myStints(Request $request)
    {
        $stints = Stint::where('user_id', Auth::user()->id)->with('category')->get();
        return $this->view('stints-list', ['stints' => $stints]);
    }

    public function editStint(Request $request, Stint $stint)
    {
        //        dd($stint->id);
        $stintRecord = Stint::with('category')->where('id', '=', $stint->id)->first();
        //        dd($stintRecord);
        $categories = Category::where('parent', 0)
            ->where('category_type', 'stint')
            ->get();
        $subCategories = Category::where('parent', $stint->category_id)->get();
        return $this->view('create', ['categories' => $categories, 'subcategories' => $subCategories, 'stint' => $stint,
            'stintRecord' => $stintRecord
        ]);
    }

    public function create(Request $request, Stint $stint)
    {
        $categories = Category::where('parent', 0)
            ->where('category_type', 'stint')
            ->get();
        return $this->view('create', ['categories' => $categories, 'subcategories' => [], 'stint' => $stint]);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $validator = \Validator::make($data, Stint::$rules);
        if ($validator->fails()) {
            return redirect('/stint/create-stint')
                ->withErrors($validator)
                ->withInput();
        }
        $obj = new Stint();
        if (!empty($data['stintId'])) {
            $obj = $obj::find($data['stintId']);
            $data['status'] = 0;
            $data['user_id'] = Auth::user()->id;
            $obj->fill($data);
            $obj->save();
        } else {
            //        dd($data['stintId']);
            $data['status'] = 0;
            $data['user_id'] = Auth::user()->id;
            //            $obj = new Stint();
            $obj->fill($data);
            $obj->save();
        }
        return ['data' => $obj->id, 'message' => 'Stint has been created successfully!!, now second step'];
        //return redirect()->to('/my-stint/'.$obj->id.'/pricing-plan')->with('success','Stint has been created successfully!!, now second step');
    }

    public function pricing(Request $request, Stint $stint)
    {
        $pricingPlans = StintPricingPlan::where('stint_id', '=', $stint->id)->get();
        //       dd($pricingPlan);
        return $this->view("pricing-plan", ['pricingPlans' => $pricingPlans, 'stint' => $stint]);
    }

    public function descriptions(Request $request, Stint $stint)
    {
        $stintDescription = Stint::where('id', '=', $stint->id)->first(['description']);
        $stintFaqs = StintFaq::where('stint_id', '=', $stint->id)->get();
        //        dd($stintFaqs);
        return $this->view('descriptions', ['stint' => $stint,
            'stintDescription' => $stintDescription, 'stintFaqs' => $stintFaqs]);
    }

    public function requirment(Stint $stint)
    {
        $stintRequirments = StintRequirement::where('stint_id', '=', $stint->id)->get();
        return $this->view('requirement', ['stint' => $stint, 'stintRequirments' => $stintRequirments]);
    }

    public function gallery(Stint $stint)
    {
        $stintUploadedImages = StintGallery::with('stintGallery')->where('stint_id', '=', $stint->id)->get();
        //       dd($stintUploadedImages);
        return $this->view('gallery', ['stint' => $stint, 'stintUploadImages' => $stintUploadedImages]);
    }

    public function publish(Stint $stint)
    {
        $link = url('stint/view/'.$stint->id.'/'.str_replace(" ", "-", $stint->title));
        return $this->view('publish', ['stint' => $stint, 'link' => $link]);
    }


    public function savePricing(Request $request, Stint $stint)
    {
        $data = $request->all();

        if (count($data['extra_price']) > 0) {
            StintPricingPlan::where('stint_id', $stint->id)->delete();
            $rows = [];
            for ($i = 0; $i < count($data['extra_price']); $i++) {
                $rows[] = [
                    'stint_id' => $stint->id,
                    'plan' => $data['plan'][$i],
                    'title' => $data['package_name'][$i],
                    'description' => $data['description'][$i],
                    'delivery_time' => $data['delivery_time'][$i],
                    'revisions' => $data['revisions'][$i],
                    'price' => $data['price'][$i],
                    'status' => 1
                ];
            }
            StintPricingPlan::insert($rows);
            if (!empty($data['has_extra_fast_delivery'])) {
                StintExtra::where('stint_id', $stint->id)->delete();
                $rows = [];
                for ($i = 0; $i < count($data['extra_price']); $i++) {
                    $rows[] = [
                        'stint_id' => $stint->id,
                        'plan' => $data['extra_plan'][$i],
                        'delivery_time' => $data['extra_delivery_time'][$i],
                        'price' => $data['extra_price'][$i],
                        'status' => 1
                    ];
                }
                StintExtra::insert($rows);
            } else {
                StintExtra::where('stint_id', $stint->id)->delete();
            }
        }

        return response()->json('saved', 200);
    }

    public function saveDescriptions(Request $request, Stint $stint)
    {
        $data = $request->all();
        $stintDescription = Stint::find($stint->id)->update(['description' => $data['stint_description']]);
        $this->saveStintFaq($data, $stint);
        if ($stintDescription) {
            return response()->json([
                'data' => $stintDescription,
                'message' => 'Stinit Description Update Successfully',
                'alert-type' => 'success'
            ], 200);
        } else {
            return response()->json([
                'message' => 'Something went Wrong',
                'alert-type' => 'error'
            ], 403);
        }
    }

    public function saveStintFaq($faqRecord, Stint $stint)
    {
        if(empty($faqRecord['faq_question'])) return;
        for ($i = 0; $i < count($faqRecord['faq_question']); $i++) {
            $row = [
                'stint_id' => $stint->id,
                'question' => $faqRecord['faq_question'][$i],
                'answer' => $faqRecord['faq_answer'][$i],
            ];
            $stint = new StintFaq();
            $stint->fill($row);
            if(!empty($faqRecord['faqId'][$i])) {
                $stint->where('id', $faqRecord['faqId']);
                $stint->update($row);
            } else {
                $stint->save();
            }
        }
        return true;
    }

    public function saveRequirement(Request $request, Stint $stint)
    {
        //        dd($request->all());
        $requirementData = $request->all();

        if ($requirementData['requirmentId'][0] != null) {
            if (count($requirementData['requirement']) > 0) {
                $rows = [];
                for ($i = 0; $i < count($requirementData['requirement']); $i++) {
                    $stintRequirementSave = StintRequirement::where('id', '=', $requirementData['requirmentId'][$i])->update(
                        $rows[] = [
                            'stint_id' => $stint->id,
                            'description' => $requirementData['requirement'][$i],
                            'is_mandatory' => $requirementData['is_mandatory'][$i]
                        ]
                    );
                }
            }
        } else {
            $rows = [];
            for ($i = 0; $i < count($requirementData['requirement']); $i++) {
                $rows[] = [
                    'stint_id' => $stint->id,
                    'description' => $requirementData['requirement'][$i],
                    'is_mandatory' => !empty($requirementData['is_mandatory'][$i]) ? $requirementData['is_mandatory'][$i] : 0
                ];
            }
            $stintRequirementSave = StintRequirement::insert($rows);
        }
        if ($stintRequirementSave) {
            return response()->json([
                'data' => $stintRequirementSave,
                'message' => 'Stint Requirment Save Successfully',
                'alert-type' => 'success'
            ], 200);
        } else {
            return response()->json([
                'message' => 'Something went Wrong',
                'alert-type' => 'error'
            ], 403);
        }

    }

    public function deleteStint(Request $request, Stint $stint)
    {
        $stintRecord = Stint::find($stint->id);
        $stintRecord->delete();
        return redirect()->to('/stint/my-stints')->with('success', 'selected stint has been Deleted successfully!!');
    }

    protected
    function view($view, $data = [])
    {
        return view($this->dir . '.' . $view, $data);
    }

    public function uploadGallery(Request $request)
    {
        //        dd($request->all());
        $folder = '/uploads/stint/images/';
        $images = $request->file('file');
        $imageName = uniqid() . '_' . trim($images->getClientOriginalName());
        $filePath = $folder . $imageName;
        $images->move(public_path().$folder, $imageName);
        return response()->json([
            'path' => $filePath,
            'original_name' => $images->getClientOriginalName(),
        ]);
    }

    public function uploadVideo(Request $request)
    {
        $folder = '/uploads/stint/video/';
        $video = $request->file('file');
        $videoName = uniqid() . '_' . trim($video->getClientOriginalName());
        $videoPath = $folder . $videoName;
        $video->move(public_path().$folder,  $videoName);
        return $videoPath;
    }

    //    Upload Pdf function
    public function uploadPdf(Request $request)
    {
        $folder = '/uploads/stint/pdf/';
        $pdf = $request->file('file');
        $pdfName = uniqid() . '_' . trim($pdf->getClientOriginalName());
        $pdfPath = $folder . $pdfName;
        $pdf->move(public_path() . $folder, $pdfName);
        return $pdfPath;
    }

    public function saveStintGallery(Request $request, Stint $stint)
    {
        $data = $request->all();
        $stintGalleryExtra = new StintGalleryExtra();
        $stintGalleryExtra->video = !empty($data['video']) ? $data['video'] : '';
        $stintGalleryExtra->pdf = !empty($data['pdf']) ? $data['pdf'] : '';
        $stintGalleryExtra->save();
        $stintGalleryExtraId = $stintGalleryExtra->id;

        if (count($data['galleryimages']) > 0) {
            StintGallery::where('stint_id', $stint->id)->delete();
            for ($i = 0; $i < count($data['galleryimages']); $i++) {
                $row = [
                    'stint_id' => $stint->id,
                    'stint_gallery_extra_id' => $stintGalleryExtraId,
                    'image' => $data['galleryimages'][$i]
                ];
                $galleryObj = new StintGallery();
                $stinitGallerySave = $galleryObj->fill($row)->save();
            }
        }

        if ($stinitGallerySave) {
            return response()->json([
                'data' => $stinitGallerySave,
                'message' => 'Stint Gallery Save Successfully',
                'alert-type' => 'success'
            ], 200);
        } else {
            return response()->json([
                'message' => 'Something went Wrong',
                'alert-type' => 'error'
            ], 403);
        }

    }

    public function stintLists(User $user)
    {
        $stintRecords = Stint::where('user_id', $user ? $user->id : Auth::user()->id)
            ->with('category')->with('user')->with('gallery')
            ->with('price')
            ->paginate(10);
        return $this->view('front.stint-list')->with('stintRecords', $stintRecords);
    }

    public function stintDetail( $stint_id, $stint_title='')
    {
        $StintRecord = Stint::where('id', '=', $stint_id)
            ->with(['user' => function ($q){
                return $q->with('social_accounts');
            }])
            ->with('category')
            ->with('gallery')
            ->with('price')
            ->with('faq')
            ->withCount('orders')
            ->first();
        return $this->view('front.stint-detail')->with('stintRecord', $StintRecord);
    }

    public function purchaseStint(Request $request, $stintId, $stintTitle)
    {
        if(!$request->get('package')) {
            return redirect()->back()->with('message', 'Please select package to continue')->with('alert-type', 'error');
        }
        $formData = $request->all();
        $StintRecord = Stint::where('id', '=', $stintId)
            ->with('user')
            ->with('category')
            ->with('gallery')
            ->with(['price'=> function ($q) use ($formData){
              return  $q->where('id', $formData['package']);
            }])
            ->with(['extras'=> function ($q) use ($formData){
              return  $q->where('plan', $formData['package']);
            }])
            ->first();

        return $this->view( 'front.stint-purchase', ['stintRecord'=> $StintRecord, 'package'=>$formData['package']]);
    }

    public function purchaseStintOrder(Request $request)
    {
        $check = Stint::where('id', $request->get('stint_id'))
                ->where('user_id', $request->get('user_id'))
                ->where('status', 1)
                ->first();
        if($check) {
            return redirect()->to('stint/my-orders')->with('info', 'Previous order is in progress');
        }
        $data = $request->all();
        $stintOrderObj = new StintOrder();

        $stintOrderObj->stint_id = $data['stint_id'];
        $stintOrderObj->user_id = $data['user_id'];
        $stintOrderObj->order_total = $data['subtotal'];
        $stintOrderObj->save();
        $stint = Stint::where('id', $data['stint_id'])->with('user')->first();
        $buyer = User::where('id', $data['user_id']) ->first();
        EmailNotificationHelper::sendOrderNotification($buyer, $stint);
        return redirect()->to('stint/my-orders')->with('success', 'Order has been submitted, waiting for response');
    }

    public function publishStint(Request $request, Stint $stint) {
        $stint->status = 1;
        $stint->save();
        return response()->json(['message'=>'Stint has been published'], 200);
    }

    public function myOrders (Request $request, $order_status='active') {
        $orderStatuses= [
            'active' => 1,
            'delivered' => 2,
            'completed'=> 3,
            'cancelled' => 4
        ];
        $sellerStints = StintOrder::where('seller_id', Auth::user()->id)
        ->where('order_type', 'stint');

        if(!in_array($order_status, ['priority', 'new', 'starred', 'late'])) {
            $sellerStints->where('status', $orderStatuses[$order_status]);
        }
        else {
            if($order_status == 'priority') {
                $sellerStints->orderBy('due_on', 'ASC');
                $sellerStints->where('status', 1);
            } else if ($order_status == 'new') {
                $sellerStints->orderBy('created_at', 'DESC');
                $sellerStints->where('status', 1);
            } else if ($order_status == 'starred') {
                $sellerStints->where('is_starred', 1);
            } else if ($order_status == 'late') {
                $sellerStints->where('due_on', '<', date("Y-m-d H:i:s", time()));
                $sellerStints->where('status', 1);
            }
        }
        $sellerStints->with('stint')->with('buyer');

        return view('front.stints.seller-orders', ['orders' => $sellerStints->get(), 'status'=> $order_status]);

    }

    public function markFavorite(Request $request, Stint $stint) {
        FavoriteStint::updateOrCreate(
            ['user_id'=> Auth::user()->id, 'stint_id'=> $stint->id],
            ['user_id'=> Auth::user()->id, 'stint_id'=> $stint->id]
        );
        return response()->json(['message'=>'Stint has been marked as favorite'], 200);
    }

    public function markReport (Request $request, Stint $stint) {
        $check = StintFlag::where('user_id', Auth::user()->id)
            ->where('stint_id', $stint->id)
            ->first();
        if(!$check) {
            $obj = new StintFlag();
            $obj->fill([
                    'stint_id' => $stint->id,
                    'user_id' => Auth::user()->id,
                    'reason' => $request->get('report_reason'),
                ])
                ->save();
            return response()->json('report has been forwarded, our team ll look into this', 200);
        }
        return response()->json("you have already reported it", 403);
    }
}


