<?php

namespace App\Http\Controllers\Front;

use App\Helpers\EmailNotificationHelper;
use App\Models\ConversationMessage;
use App\Models\Stint;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Conversation;
use App\Events\SendMessage;
use Auth;

class MessagesController extends Controller {

    public function loadMessages(Request $request, $limit= false) {
        $messages = ConversationMessage::with("sender");
        $messages->where('receiver_id', Auth::user()->id);
        $messages->where('direction', 'in');
        $messages->orderBy('id', 'DESC');
        $limit and $messages->skip(0)->take($limit);
        return response()->json(['html'=> ConversationMessage::renderMessageList($messages->get())], 200);
    }

    public function send(Request $request, $conversation) {
        $conversation = Conversation::where('id', $conversation)->with('stint')->first();
        $recipient = User::where('id', $request->get('receiver_id'))->first();
        ConversationMessage::createMessage(Auth::user()->id, $recipient->id, $request->get('message'), 1, 'out', $conversation->id, $request->get('offer_id'));
        ConversationMessage::createMessage(Auth::user()->id, $recipient->id, $request->get('message'), 0, 'in', $conversation->id, $request->get('offer_id'));
        //EmailNotificationHelper::sendMessageNotification($recipient, Auth::user(), $conversation->stint, $request->get('message'));
        return response()->json('Message has been sent!', 200);
    }
}