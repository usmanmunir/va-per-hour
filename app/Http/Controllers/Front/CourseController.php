<?php

namespace App\Http\Controllers\Front;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\Course;
use Auth;
use App\Http\Controllers\Controller;

class CourseController extends Controller
{
    //
    public function addCourse()
    {
        if (Auth::guest()) {
            return redirect('/signin')->with('warning', 'Your session has expired, please login again');
        }
        $categories = Category::where('category_type', '=', 'course')->get();
        return view('front.course.add-course')->with('categories', $categories);
    }

    public function courseLists()
    {
        if (Auth::guest()) {
            return redirect('/signin')->with('warning', 'Your session has expired, please login again');
        }
        if (!Auth::guest()) {
            $userCourses = Course::with('category')->where('user_id', '=', Auth::user()->id)->get();
            return view('front.course.my-course')
                ->with('courses', $userCourses);
        } else {
            return redirect('/');
        }
    }

    public function getUserCourse()
    {
        if (Auth::guest()) {
            return redirect('/signin')->with('warning', 'Your session has expired, please login again');
        }
        $userId = Auth::user()->id;
        $userCourses = Course::with('category')->where('user_id', '=', $userId)->get();
        return $userCourses;
    }

    public function editCourse($courseId)
    {
        if (Auth::guest()) {
            return redirect('/signin')->with('warning', 'Your session has expired, please login again');
        }
        $categories = Category::where('category_type', '=', 'course')->get();
        $userCourse = Course::with('category')->where('id', '=', $courseId)->first();
        return view('front.course.edit-course')->with('userCourseRecord', $userCourse)->with('categories', $categories);
    }

    public function saveCourse(Request $request)
    {
        if (Auth::guest()) {
            return redirect('/signin')->with('warning', 'Your session has expired, please login again');
        }
        $course = new Course();
        $course->user_id = Auth::user()->id;
        $course->name = $request->name;
        $str = strtolower($request->name);
        $course->slug = preg_replace('/\s+/', '-', $str);
        $course->category_id = $request->category_id;
        $course->price = $request->price;
        // Check if a profile image has been uploaded

        if ($request->has('file')) {
            $image = $request->file('file');
            $name = str_slug('vaperhourCourse') . '_' . time();
            $folder = '/uploads/images/courses/';
            $filePath = $folder . $name . '.' . $image->getClientOriginalExtension();
            $image->move(public_path() . '/uploads/images/courses/', $name . '.' . $image->getClientOriginalExtension());
            $course->image = $filePath;
        }
        $course->description = $request->description;
        $course->status = $request->status;
        $userCourse = $course->save();
        if ($userCourse) {
            return response()->json([
                'data' => $course,
                'message' => 'Record Save Successfully',
                'alert-type' => 'success'
            ], 200);
        } else {
            return response()->json([
                'message' => 'Something went Wrong',
                'alert-type' => 'error'
            ], 403);
        }

    }

    public function updateCourse(Request $request)
    {
        if (Auth::guest()) {
            return redirect('/signin')->with('warning', 'Your session has expired, please login again');
        }
        $courseId = $request->course_id;
        $course = Course::find($courseId);
        $course->user_id = Auth::user()->id;
        $course->name = $request->name;
        $str = strtolower($request->name);
        $course->slug = preg_replace('/\s+/', '-', $str);
        $course->category_id = $request->category_id;
        $course->price = $request->price;
        // Check if a profile image has been uploaded

        if ($request->has('file')) {
            $image = $request->file('file');
            $name = str_slug('vaperhourCourse') . '_' . time();
            $folder = '/uploads/images/courses/';
            $filePath = $folder . $name . '.' . $image->getClientOriginalExtension();
            $image->move(public_path() . '/uploads/images/courses/', $name . '.' . $image->getClientOriginalExtension());
            $course->image = $filePath;
        }
        $course->description = $request->description;
        $course->status = $request->status;
        $userCourse = $course->save();
        if ($userCourse) {
            return response()->json([
                'data' => $userCourse,
                'message' => 'Record Update Successfully',
                'alert-type' => 'success'
            ], 200);
        } else {
            return response()->json([
                'message' => 'Something went Wrong',
                'alert-type' => 'error'
            ], 403);
        }
    }


    public function deleteUserCourse($userCourseId)
    {
        if (Auth::guest()) {
            return redirect('/signin')->with('warning', 'Your session has expired, please login again');
        }
        $userCourseRecord = Course::find($userCourseId);
        if ($userCourseRecord->delete()) {
            return response()->json([
                'message' => 'Course Delete Successfully',
                'alert-type' => 'success'
            ]);
        } else {
            return response()->json([
                'message' => 'Something went wrong',
                'alert-type' => 'error'
            ]);
        }
    }

    public function elearningCourse()
    {
        $courseCategories = Category::where('category_type', '=', 'course')->paginate(2);
        return view('front.e-learning.home')->with('courseCategories', $courseCategories);
    }

    public function diplayCategories()
    {
        $query = Category::where('category_type', '=', 'course');
        // search results based on user input
        \Request::input('search') and $query->where(function ($q) {
            $q->where('name', 'like', '%' . \Request::input('search') . '%');
            $q->orWhere('description', 'like', '%' . \Request::input('search') . '%');
        });

        // sort results
        \Request::input("sort") and $query->orderBy(\Request::input("sort"), \Request::input("sortType", "asc"));

        // paginate results
        $categories = $query->paginate(2);
        return view('front.e-learning.category-list')->with('categories', $categories);
    }

    public function getCategroyCourse($categoryName, $categoryId)
    {
        $query = Course::with('user')
            ->with('category');
        // search results based on user input
        \Request::input('search') and $query->where(function ($q) {
            $q->where('name', 'like', '%' . \Request::input('search') . '%');
            $q->orWhere('description', 'like', '%' . \Request::input('search') . '%');
        });

        // sort results
        \Request::input("sort") and $query->orderBy(\Request::input("sort"), \Request::input("sortType", "asc"));

        // paginate results
        $courses = $query->paginate(6);
        return view('front.e-learning.course-list')->with('courses', $courses);
    }

    public function getCourseSyllabus($courseId)
    {
        $singleCourse = Course::with('chapters')
            ->with('lectures')
            ->where('id', '=', $courseId)
            ->first();
//        dd($singleCourse);
        return view('front.e-learning.course-syllabus')->with('singleCourse', $singleCourse);
    }

}
