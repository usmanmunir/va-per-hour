<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    //

    public function index()
    {
        return view('front.home');
    }

    public function switchModule(Request $request, $mode = false, $module=false, $role=false)
    {
        if($mode == 'freelancer') {
            $module = $module ? $module : 'stint';
            $role = $role ? $role : 'seller';
            $url = $role == 'seller' ? '/stint/my-stints' : '/job/manage-requests';
            session(['mode' => 'freelancer', 'module' => $module, 'role' => $role]);
            return redirect()->to($url);
        }
        elseif($mode == 'marketplace') {
            $role = $role ? $role : 'buyer';
            session(['mode' => 'marketplace', 'module' => 'marketplace', 'role' => $role]);
            return redirect()->to('/marketplace/home');
        }
        elseif($mode == 'elearning') {

            $role = $role ? $role : 'student';
            session(['mode' => 'elearning', 'module' => 'elearning', 'role' => $role]);
            return redirect()->to('/elearning/course');
        }
    }
}
