<?php

namespace App\Http\Controllers\Front;

use App\Helpers\EmailNotificationHelper;
use App\Models\ConversationMessage;
use App\Models\Stint;
use function foo\func;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Conversation;
use App\Events\SendMessage;
use Auth;

class ConversationController extends Controller {

    public function startConversation (Request $request, $token) {
        $stint = Stint::where('id', $request->get('stint'))->with('user')->first();
        $participant_1 = Auth::user()->id;
        $message = $request->get('message');

        //check if a conversation is already started or not;
        $conversation = Conversation::where(function($q) use ($participant_1, $stint) {

            $q->where('participant_1', $participant_1);
            $q->where('participant_2', $stint->user_id);
            $q->where('stint_id', $stint->id);
            })
            ->orWhere(function($q) use ($participant_1, $stint) {
                $q->where('participant_1', $stint->user_id);
                $q->where('participant_2', $participant_1);
                $q->where('stint_id', $stint->id);
            })
            ->first();
        if(!$conversation) {
            $conversation = new Conversation();
            $conversation->fill([
                'participant_1' => $participant_1,
                'participant_2' => $stint->user_id,
                'stint_id' => $stint->id,
            ])
            ->save();

        }
        ConversationMessage::createMessage($participant_1, $stint->user_id, $message, 1, 'out', $conversation->id);
        ConversationMessage::createMessage($participant_1, $stint->user_id, $message, 0, 'in', $conversation->id);
        event(new SendMessage($message, md5($stint->user_id)));
        //EmailNotificationHelper::sendMessageNotification($stint->user, Auth::user(), $stint, $message);
        return response()->json(['message' => 'Message has sent Successfully!', 'room'=> $conversation->id ], 200);
    }

    public function rooms(Request $request, $conversation=false) {
        $conversations = Conversation::where('participant_1', Auth::user()->id);
        $conversations->orWhere('participant_2', Auth::user()->id);
        $conversations->with('user_two');
        $conversations->with('user_one');

        $rooms = $conversations->paginate(10);
        $room=[];
        if($conversation) {
            $conversation = Conversation::where('id', $conversation);
            $room = $conversation->with('user_two')->with('user_one')->with(['messages'=> function($query) {
                return $query
                    ->where(function ($q){ return $q->where('direction', 'in')->where('receiver_id', Auth::user()->id); })
                    ->orWhere(function ($q){ return $q->where('direction', 'out')->where('sender_id', Auth::user()->id); });
            }])->first();
        }

        return view('front.conversations.index', ['rooms' => $rooms, 'openRoom' => $room]);

    }
}