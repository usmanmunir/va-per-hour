<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Chat;
use App\Models\Conversation;
use Auth;

class ChatController extends Controller
{
    //
    public function getUserChat($userName = false, $userId=false)
    {
        $userConversation = conversation::with('receiver')->with('sender')
            ->where('receiver_id', '=', Auth::user()->id)
            ->orwhere('sender_id', '=', Auth::user()->id)
            ->get();
        $userChat = Chat::with('user')->where('receiver_id', '=', Auth::user()->id);
        $userId and $userChat->Where('sender_id', '=', $userId);
        $userChat->orwhere('sender_id', '=', Auth::user()->id);
        $userId and $userChat->Where('receiver_id', '=', $userId);
        $userChat = $userChat->get();
        return view('front.chat.index')
            ->with('userChat', $userChat)
            ->with('userConversation', $userConversation)
            ->with('reciverId', $userId);
    }

    public function uploadFile(Request $request)
    {
        $folder = '/uploads/chat/files/';

        if (!file_exists($folder)) {
            mkdir($folder, 0777, true);
        }
        $images = $request->file('file');
        $extensionName = $request->file('file')->extension();
        $imageName = uniqid() . '_' . trim($images->getClientOriginalName());
        $filePath = $folder . $imageName;
        $images->move(public_path() . '/uploads/chat/files/', $imageName);
        return response()->json([
            'path' => $filePath,
            'file_type' => $extensionName,
            'original_name' => $images->getClientOriginalName(),
        ]);
    }

    public function sendMessage(Request $request)
    {

//        dd($request->all());

        $chatObj = new Chat();
        $conversation = $this->saveConversation($request->all());
//        dd($conversation);
        $chatObj->sender_id = $request->get('sender');
        $chatObj->receiver_id = $request->get('receiver');
        $chatObj->message = $request->get('message');
        $chatObj->file = $request->get('uploadfiles');
        $chatObj->file_type = $request->get('file_type');
        $chatObj->conversation_id = $conversation['id'];
        $messageSend = $chatObj->save();
//        dd($messageSend);

        if ($messageSend) {
            return response()->json([
                'data' => $messageSend,
                'message' => 'Message Sent'
            ], 203);
        } else {
            return response()->json([
                'message' => 'Message Sending faild'
            ], 200);
        }
    }

    public function saveConversation($data)
    {
//        dd($data);
        $conversationObj = new Conversation();
        $existinConversation = $conversationObj::where('sender_id', '=', $data['sender'])
            ->where('receiver_id', '=', $data['receiver'])
            ->orwhere('receiver_id', '=', $data['sender'])
            ->where('sender_id', '=', $data['receiver'])->first();
        if (empty($existinConversation)) {
//            exit('inside if');
            $conversationObj->sender_id = $data['sender'];
            $conversationObj->receiver_id = $data['receiver'];
            $conversationObj->message = $data['message'];
            $conversation = $conversationObj->save();
//            dd($conversation);
        } else {
//            exit('here');
            $conversationObj = $conversationObj::find($existinConversation->id);
//            $conversationObj->sender_id = $data['sender'];
//            $conversationObj->receiver_id = $data['receiver'];
            $conversationObj->message = $data['message'];
            $conversation = $conversationObj->save();
        }
        return $conversationObj;

    }
}
