<?php

namespace App\Http\Controllers\Front;

use App\Models\City;
use App\Models\Country;
use App\Models\Offer;
use App\Models\State;
use App\Models\Stint;
use App\Models\StintOrder;
use App\Models\Transaction;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Validator;

class CheckoutController extends Controller {

    public function index (Request $request) {
        $order = $request->all();
        $notes = 'Order Details';
        if(!empty($order['stint_id'])) {
            $stint = Stint::where('id', $order['stint_id'])->first();
            $notes = $stint->title;
        }
        $countries = Country::all();
        $states = State::where('country_id', Auth::user()->country_id)->get();
        $cities = City::where('state_id', Auth::user()->state_id)->get();
        return view('front.checkout.form', [
            'notes' => $notes,
            'order' => $order,
            'countries' => $countries,
            'states' => $states,
            'cities' => $cities,
        ]);
    }

    public function processPayment (Request $request) {
        $transaction = new Transaction();
        $data = $request->all();
        $data['user_id'] = $data['buyer_id'];
        $transaction->fill($data)->save();
        if($transaction) {
            $data['transaction_id'] = $transaction->id;
            $stintOrder = new StintOrder();
            $stintOrder->fill($data)->save();
            return redirect()->to('/user/orders')
                ->with('message', 'Order has been placed')
                ->with('alert-type', 'success');
        }
        return redirect()->back()
            ->with('message', 'transaction has failed please try again!')
            ->with('alert-type', 'error');
    }

    public function loadStates (Request $request, $country_id) {
        $states = [];
        $states = State::where('country_id', $country_id)->get();
        return response()->json($states, 200);
    }

    public function loadCities (Request $request, $state_id) {
        $cities = City::where('state_id', $state_id)->get();
        return response()->json($cities, 200);
    }
}