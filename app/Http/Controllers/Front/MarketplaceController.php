<?php

namespace App\Http\Controllers\Front;

use Auth;
use App\Models\Category;
use App\Models\Course;
use App\Models\Marketplaceproduct;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MarketplaceController extends Controller
{
    //
    public function home()
    {
        $userMarketplaceProducts = Marketplaceproduct::with('category')->where('user_id', '=', Auth::user()->id)->paginate(6);
        return view('front.marketplace.home')->with('userMarketplaceProducts', $userMarketplaceProducts);
    }

    public function uploadProduct()
    {
        $marketPlaceCategories = Category::where('category_type', '=', 'marketplace')->get();
        return view('front.marketplace.product-upload')->with('marketPlaceCategories', $marketPlaceCategories);
    }

    public function saveProduct(Request $request)
    {
        $marketePlaceProduct = new Marketplaceproduct();
        $marketePlaceProduct->category_id = $request->get('category_id');
        $marketePlaceProduct->user_id = Auth::user()->id;
        $marketePlaceProduct->name = $request->get('name');
        $marketePlaceProduct->description = $request->get('description');
        $marketePlaceProduct->thumbnail_image = $request->get('thumbnailimage');
        $marketePlaceProduct->mainfile = $request->get('zipfile');
        $marketePlaceProduct->regular_license = $request->get('regular_license');
        $marketePlaceProduct->extended_license = $request->get('extended_license');
        $marketePlaceProduct->status = 'publish';
        $MarketePlaceProductSave = $marketePlaceProduct->save();
        if ($MarketePlaceProductSave) {
            return response()->json([
                'data' => $MarketePlaceProductSave,
                'message' => 'Your Product has been Saved!',
                'alert-type' => 'success'
            ], 200);
        } else {
            return response()->json([
                'message' => 'Something went Wrong',
                'alert-type' => 'error'
            ], 403);
        }
    }

    public function editProduct(Request $request){
        if (Auth::guest()) {
            return redirect('/signin')->with('warning', 'Your session has expired, please login again');
        }
        $marketePlaceProductId = $request->get('marketplaceproductId');
        $marketePlaceProductRecord = Marketplaceproduct::find($marketePlaceProductId);
        $marketePlaceProductRecord->category_id = $request->get('category_id');
        $marketePlaceProductRecord->user_id = Auth::user()->id;
        $marketePlaceProductRecord->name = $request->get('name');
        $marketePlaceProductRecord->description = $request->get('description');
        $marketePlaceProductRecord->thumbnail_image = $request->get('thumbnailimage');
        $marketePlaceProductRecord->mainfile = $request->get('zipfile');
        $marketePlaceProductRecord->regular_license = $request->get('regular_license');
        $marketePlaceProductRecord->extended_license = $request->get('extended_license');
        $marketePlaceProductRecord->status = 'publish';
        $MarketePlaceProductSave = $marketePlaceProductRecord->save();
        if ($MarketePlaceProductSave) {
            return response()->json([
                'data' => $MarketePlaceProductSave,
                'message' => 'Your Product has been Update!',
                'alert-type' => 'success'
            ], 200);
        } else {
            return response()->json([
                'message' => 'Something went Wrong',
                'alert-type' => 'error'
            ], 403);
        }

    }

    public function saveThumbnailImage(Request $request)
    {
        $thumbnailImage = $request->file('file');
        $thumbnailImageName = str_slug('vaperhourMarketeplace') . '_' . time();
        $folder = '/uploads/Marketeplace/Thumbnail-images/';
        $imagePath = $folder . $thumbnailImageName . '.' . $thumbnailImage->getClientOriginalExtension();
        $thumbnailImage->move(public_path() . '/uploads/Marketeplace/Thumbnail-images/', $thumbnailImageName . '.' . $thumbnailImage->getClientOriginalExtension());
        return $imagePath;
    }

    public function saveZipfile(Request $request)
    {
        $zipFile = $request->file('file');
        $zipFileName = str_slug('vaperhourMarketeplaceProduct') . '_' . time();
        $folder = '/uploads/Marketeplace/Zip-FIles/';
        $zipFilePath = $folder . $zipFileName . '.' . $zipFile->getClientOriginalExtension();
        $zipFile->move(public_path() . '/uploads/Marketeplace/Zip-FIles/', $zipFileName . '.' . $zipFile->getClientOriginalExtension());
        return $zipFilePath;
    }

    public function productInfo($marketplaceProductId)
    {
        $singleMarketplaceProduct = Marketplaceproduct::with('user')->where('id', '=', $marketplaceProductId)->first();
        return view('front.marketplace.product-info')->with('singleMarketplaceProduct', $singleMarketplaceProduct);
    }

    public function manageUserProduct()
    {
        if (Auth::guest()) {
            return redirect('/signin')->with('warning', 'Your session has expired, please login again');
        }
        $userId = Auth::user()->id;
        $userMarketPlaceProducts = Marketplaceproduct::with('user')
            ->where('user_id', '=', $userId)
            ->paginate(5);
        return view('front.marketplace.user-products')->with('userMarketPlaceProducts', $userMarketPlaceProducts);
    }

    public function deleteUserMarketplaceProduct($userMarketplaceProductId)
    {
        if (Auth::guest()) {
            return redirect('/signin')->with('warning', 'Your session has expired, please login again');
        }
        $userMarketplaceProductRecord = Marketplaceproduct::find($userMarketplaceProductId);
        if ($userMarketplaceProductRecord->delete()) {
            return response()->json([
                'message' => 'Product Delete Successfully',
                'alert-type' => 'success'
            ]);
        } else {
            return response()->json([
                'message' => 'Something went wrong',
                'alert-type' => 'error'
            ]);
        }
    }

    public function editUserMarketplaceProduct($userMarketplaceProductId)
    {
        if (Auth::guest()) {
            return redirect('/signin')->with('warning', 'Your session has expired, please login again');
        }
        $marketPlaceCategories = Category::where('category_type', '=', 'marketplace')->get();
        $marketplaceUserProduct = Marketplaceproduct::with('category')->where('id', '=', $userMarketplaceProductId)->first();
        return view('front.marketplace.edit-user-product')->with('marketplaceUserProduct', $marketplaceUserProduct)->with('marketPlaceCategories', $marketPlaceCategories);
    }
}
