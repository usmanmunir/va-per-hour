<?php

namespace App\Http\Controllers\Front;

use App\Models\Offer;
use App\Models\Stint;
use App\Models\StintOrder;
use Auth;
use http\Env\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Validator;

class OrderController extends Controller {

    public function order(Request $request, $offer_id) {
        $offer_id = str_replace(csrf_token(), "", $offer_id);
        $check = Offer::where('id', $offer_id)->first();
        if($check) {
            //check if order already exists or not;
            $orderExists = StintOrder::where('buyer_id', Auth::user()->id)
                ->where('seller_id', $check->user_id)
                ->where('stint_id', $check->stint_id)
                ->where('status', 1)
                ->first();
            if($orderExists) {
                return response()->json(['message'=> 'an order is already in placed for selected stint'], 403);
            }
            $formData = [
                'buyer_id' => Auth::user()->id,
                'seller_id' => $check->user_id,
                'stint_id' => $check->stint_id,
                'offer_id' => $check->id,
                'status' => 1
            ];
            $stintOrder = new StintOrder();
            $stintOrder->fill($formData)->save();
            Offer::where('id', $offer_id)->update(['status' => 2]);
            return response()->json(['message'=> 'Offer has been started'], 200);
        }
        return response()->json(['message'=> 'problem while creating order'], 403);
    }

    public function userOrders() {
        if(session('role') == 'buyer') {
            $orders = StintOrder::where('buyer_id', Auth::user()->id)->orderBy('id', 'Desc')->paginate(10);
            return view('front.buyer.orders', ['orders' => $orders]);
        } else {
            return view('front.seller.orders');
        }
    }
}