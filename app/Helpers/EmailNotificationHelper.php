<?php
namespace App\Helpers;

use Illuminate\Support\Facades\Mail;

class EmailNotificationHelper {

    public static function sendOrderNotification ($buyer, $stint) {
        Mail::send('emails.new-order', ['buyer' => $buyer, 'stint' => $stint], function ($message) use ($stint) {
            $message->from('noreply@vaperhour.com', 'VAPH');
            $message->to($stint->user->email);
        });
    }

    public static function sendMessageNotification ($recipient, $sender, $stint, $message) {
        Mail::send('emails.new-message', [
            'recipient' => $recipient,
            'sender' => $sender,
            'messageBody' => $message,
            'stint' => $stint
        ], function ($message) use ($stint, $recipient) {
            $message->from('noreply@vaperhour.com', 'VAPH');
            $message->subject('You have new Message for '. $stint->title);
            $message->to($recipient->email);
        });
    }
}