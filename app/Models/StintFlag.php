<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StintFlag extends Model
{
    protected $fillable = [
        'user_id',
        'stint_id',
        'reason'
    ];
}
