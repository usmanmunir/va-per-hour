<?php

namespace App\Models;

use App\Models\MasterUservarify;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Database\Eloquent\Model;

class MasterUser extends Authenticatable
{
    use Notifiable;
    //
    public $timestamps = false;
    public $table = "user_master";
    protected $fillable = [
        'u_fname', 'u_lname', 'u_email', 'u_password', 'u_avator', 'u_seller', 'u_active', 'verified', 'u_website','created_at','updated_at'
    ];

    public function MasterUservarify()
    {
        return $this->hasOne(MasterUservarify::class, 'id', 'user_id' );
    }
}
