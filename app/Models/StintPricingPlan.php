<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StintPricingPlan extends Model
{
    protected $fillable = ['stint_id', 'plan', 'title', 'description', 'revisions', 'price', 'status'];

    public function stint() {
        return $this->belongsTo(Stint::class, 'id', 'stint_id');
    }

}
