<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JobRequest extends Model
{
    protected $fillable = ['user_id', 'job_id', 'cover_letter', 'budget', 'status'];

    public static $rules = [
        'cover_letter'=>'required',
        'budget'=>'required',
    ];

    public static function findRequested () {
        $query = JobRequest::query();
        // search results based on user input
        \Request::input('id') and $query->where('id', \Request::input('id'));
        // sort results
        \Request::input("sort") and $query->orderBy(\Request::input("sort"), \Request::input("sortType", "asc"));

        // paginate results
        return $query->paginate(15);
    }

    public function job() {
        return $this->belongsTo(Job::class);
    }
}
