<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Instructor extends Model
{
    //
    public $table = "instructors";
    protected $fillable = [
        'user_id', 'category_id', 'course_skills', 'expertise_level', 'professional_experience', 'comments'];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }
}
