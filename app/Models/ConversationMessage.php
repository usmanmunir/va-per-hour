<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConversationMessage extends Model
{
    protected $fillable = [
        'sender_id',
        'receiver_id',
        'message',
        'attachment',
        'file_type',
        'direction',
        'is_read',
        'offer_id',
        'conversation_id'
    ];

    public static function createMessage($sender, $receiver, $message, $isRead, $direction, $conversation_id, $offer_id=null) {
        ConversationMessage::create([
            'sender_id' => $sender,
            'receiver_id'=>$receiver,
            'message' => $message,
            'is_read' => $isRead,
            'direction' => $direction,
            'offer_id' => $offer_id,
            'conversation_id' => $conversation_id
        ]);
    }

    Public static function renderMessageList ( $messages ) {
        $html = '';
        if($messages) {
            foreach ($messages as $message) {
                $html .= '<a href="/messages/rooms/' . $message->conversation_id . '" class="message recent">
                        <div class="message__actions_avatar">
                            <div class="avatar">
                                <img src="' . $message->sender->avatar . '" alt="">
                            </div>
                        </div>
                        <!-- end /.actions -->

                        <div class="message_data">
                            <div class="name_time">
                                <div class="name">
                                    <p>' . $message->sender->first_name . ' ' . $message->sender->last_name . '</p>
                                    <span class="lnr lnr-envelope"></span>
                                </div>

                                <span class="time">' . date("H:i M d, Y", strtotime($message->created_at)) . '</span>
                                <p>' . substr($message->message, 0, 50) . '</p>
                            </div>
                        </div>
                        <!-- end /.message_data -->
                    </a>';
            }
        } else {
            $html .= "<p>No Message found</p>";
        }
        return $html;
    }

    public function conversation()
    {
        return $this->belongsTo(Conversation::class,'conversation_id','id');
    }

    public function sender() {
        return $this->hasOne(\App\User::class, 'id', 'sender_id');
    }
}
