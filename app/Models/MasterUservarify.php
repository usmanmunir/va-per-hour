<?php

namespace App\Models;
use App\Models\MasterUser;
use App\User;

use Illuminate\Database\Eloquent\Model;

class MasterUservarify extends Model
{
    //
    protected $guarded = [];
    public $table = "user_master_varify";

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
