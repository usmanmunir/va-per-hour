<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserMembership extends Model
{
    protected $fillable = [
        'user_id',
        'membership_id',
        'next_due_date',
    ];

    public function membership () {
        return $this->belongsTo(Membership::class);
    }

}
