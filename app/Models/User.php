<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class user extends Model
{

    protected $fillable = [
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function location () {
        return $this->hasOne(UserLocation::class);
    }
    public function conversation () {
        return $this->hasMany(Conversation::class);
    }

    public function social_accounts () {
        return $this->hasOne(UserLinkAccount::class);
    }

}
