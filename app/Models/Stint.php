<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Stint extends Model
{
    protected $fillable = ['title', 'status', 'category_id', 'sub_category_id', 'status', 'user_id', 'description'];

    public static $rules = [
        'category_id' => 'required',
        'sub_category_id' => 'required',
        'title' => 'required|max:80',
    ];

    public static function findRequested()
    {
        $query = Stint::with('category')->with('user');
        // search results based on user input
        \Request::input('category_id') and $query->where('category_id', \Request::input('category_id'));
        \Request::input('status') and $query->where('status', \Request::input('status'));
        \Request::input('created_at') and $query->where('created_at', \Request::input('created_at'));
        \Request::input('search') and $query->where(function ($q) {
            $q->where('title', 'like', '%' . \Request::input('stintname') . '%');
            $q->orWhere('user_id', 'like', '%' . \Request::input('username') . '%');
        });

        // sort results
        \Request::input("sort") and $query->orderBy(\Request::input("sort"), \Request::input("sortType", "asc"));

        // paginate results
        return $query->paginate(15);
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function category()
    {
        return $this->hasOne(Category::class, 'id', 'category_id');
    }

    public function gallery()
    {
        return $this->hasMany(StintGallery::class);
    }

    public function price()
    {
        return $this->hasMany(StintPricingPlan::class, 'stint_id', 'id');
    }

    public function faq()
    {
        return $this->hasMany(StintFaq::class, 'stint_id', 'id');
    }

    public function orders(){
        return $this->hasMany(StintOrder::class);
    }

    public function extras(){
        return $this->hasMany(StintExtra::class);
    }

    public function requirement()
    {
        return $this->hasMany(StintRequirement::class, 'stint_id', 'id');
    }

}
