<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Offer extends Model
{
    use SoftDeletes;
    protected $fillable = ['user_id', 'request_id', 'stint_id', 'amount', 'delivery_time', 'revisions', 'status'];

    public static function findRequested () {
        $query = Offer::query();
        // search results based on user input
        \Request::input('id') and $query->where('id', \Request::input('id'));
        \Request::input('status') and $query->where('status',  \Request::input('status') );

        // sort results
        \Request::input("sort") and $query->orderBy(\Request::input("sort"), \Request::input("sortType", "asc"));

        // paginate results
        return $query->paginate(30);
    }

    public function request() {
        return $this->belongsTo(Request::class);
    }

    public function offer_scope() {
        return $this->hasOne(OfferScope::class);
    }

    public function user() {
        return $this->belongsTo(\App\User::class);
    }

    public function stint() {
        return $this->belongsTo(Stint::class);
    }
}
