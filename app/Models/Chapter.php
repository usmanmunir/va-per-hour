<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chapter extends Model
{
    //
    public $table = "chapters";
    protected $fillable = [
        'course_id',
        'chapter_name',
        'chapter_description',
        'status',
        'chapter_type'
    ];

    public function lectures () {
        return $this->hasMany(Lecture::class);
    }
}
