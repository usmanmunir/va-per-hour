<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StintFaq extends Model
{
    public $timestamps = true;
    protected $fillable = ['stint_id', 'question', 'answer'];

    public function stint()
    {
        return $this->belongsTo(Stint::class);
    }

}
