<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lecture extends Model
{
    protected $fillable = ['title', 'description','video','course_id', 'chapter_id', 'status'];

    public function chapter () {
        return $this->belongsTo(Chapter::class);
    }

    public function course () {
        return $this->belongsTo(Course::class);
    }
}
