<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Userlanguage extends Model
{
    //
    public $table = "user_language";
    protected $fillable = [
        'user_id', 'language_id','proficiency'];

    public function language()
    {
        return $this->belongsTo(Language::class, 'language_id');
    }
}
