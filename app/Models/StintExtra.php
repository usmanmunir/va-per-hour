<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StintExtra extends Model
{
    protected $fillable = ['stint_id', 'plan', 'delivery_time', 'price', 'status'];

    public function stint() {
        return $this->belongsTo(Stint::class, 'id', 'stint_id');
    }

}
