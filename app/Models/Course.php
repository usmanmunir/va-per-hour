<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    public $table = "user_cources";
    protected $fillable = [
        'user_id', 'category_id', 'name', 'slug', 'price', 'image', 'description', 'status'];

    public static function findRequested()
    {
        $query = Course::with('user')
            ->with('category');
        // search results based on user input
        \Request::input('id') and $query->where('id', \Request::input('id'));
        \Request::input('user') and $query->where('user_id', \Request::input('user'));
        \Request::input('category_id') and $query->where('category_id', \Request::input('category_id'));
        \Request::input('status') and $query->where('status', \Request::input('status'));
        \Request::input('created_at') and $query->where('created_at', \Request::input('created_at'));
        \Request::input('search') and $query->where(function ($q) {
            $q->where('name', 'like', '%' . \Request::input('search') . '%');
            $q->orWhere('description', 'like', '%' . \Request::input('search') . '%');
        });

        // sort results
        \Request::input("sort") and $query->orderBy(\Request::input("sort"), \Request::input("sortType", "asc"));

        // paginate results
        return $query->paginate(15);
    }


    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function chapters () {
        return $this->hasMany(Chapter::class);
    }
    public function lectures(){
        return $this->hasMany(Lecture::class);
    }

}
