<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Membership extends Model
{
    protected $fillable = [
        'membership_for',
        'title',
        'user_type',
        'description',
        'price',
        'month',
        'can_view_course_details',
        'can_view_product_details',
        'can_invite_friends',
        'visible_in_seller_search',
        'number_of_stints_allowed_single_user',
        'number_of_stints_allowed_agency_user',
        'can_contact_seller',
        'can_submit_proposals',
        'can_register_to_course',
        'can_add_product_to_market_place',
        'eligible_for_cash_reward',
        'number_of_proposal_allowed',
        'can_purchase_product',
        'can_submit_bid',
        'can_order_stint',
        'seller_contact_limit_single_user',
        'seller_contact_limit_agency_user',
        'status'
    ];

    public static $rules = [
        'title' => 'required|unique:skills',
        'slug' => 'required|unique:skills',
    ];

    public static function findRequested () {
        $query = Membership::query();
        // search results based on user input
        \Request::input('id') and $query->where('id', \Request::input('id'));
        \Request::input('status') and $query->where('status',  \Request::input('status') );
        \Request::input('search') and $query->where(function ($q){
           $q->where('title', 'like', '%'.\Request::input('search').'%');
           $q->orWhere('description', 'like', '%'.\Request::input('search').'%');
        });

        // sort results
        \Request::input("sort") and $query->orderBy(\Request::input("sort"), \Request::input("sortType", "asc"));

        // paginate results
        return $query->paginate(30);
    }

    public function variations () {
        return $this->hasMany(MembershipVariation::class);
    }

    public function users_membership () {
        return $this->hasMany(UserMembership::class);
    }

}
