<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserLinkAccount extends Model
{
    public $table = "user_link_account";
}
