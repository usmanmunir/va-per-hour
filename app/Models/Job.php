<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $fillable = ['user_id', 'job_box', 'description', 'category_id', 'delivery_time', 'budget', 'status'];

    public static $rules = [
        'category_id' => 'required',
        'job_box' => 'required',
        'description' => 'required',
        'delivery_time' => 'required',
        'budget' => 'required',
    ];

    public static function findRequested () {
        $query = Job::query();
        // search results based on user input
        \Request::input('id') and $query->where('id', \Request::input('id'));
        \Request::input('status') and $query->where('status',  \Request::input('role') );
        \Request::input('category_id') and $query->where('category_type', \Request::input('category_type'));
        \Request::input('job_box') and $query->where('job_box', 'like', '%'.\Request::input('search').'%');

        // sort results
        \Request::input("sort") and $query->orderBy(\Request::input("sort"), \Request::input("sortType", "asc"));

        // paginate results
        return $query->paginate(15);
    }

    public function user()
    {
//        return $this->belongsTo(\App\User::class, 'id', 'user_id');
        return $this->belongsTo(\App\User::class, 'user_id', 'id');
    }
    public function category()
    {
        return $this->belongsTo(\App\Models\Category::class, 'category_id', 'id');
    }

    public function  job_requests() {
        return $this->hasMany(JobRequest::class, 'id', 'job_id');
    }
    public function  job_proposals() {
        return $this->hasMany(Proposal::class, 'id', 'job_id');
    }
}
