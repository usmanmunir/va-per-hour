<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    protected $fillable = [
      'user_id',
      'first_name',
      'last_name',
      'company_name',
      'email',
      'country_id',
      'state_id',
      'city_id',
      'address1',
      'address2',
      'zip_code',
      'summary'
    ];
}
