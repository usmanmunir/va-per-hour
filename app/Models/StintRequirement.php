<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StintRequirement extends Model
{
    //
    public $table = "stint_requirement";
    protected $fillable = ['stint_id', 'description', 'is_mandatory'];

    public function stint()
    {
        return $this->belongsTo(Stint::class, 'id', 'stint_id');
    }
}
