<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
    protected $fillable = ['participant_1', 'participant_2', 'stint_id'];

    public function messages()
    {
        return $this->hasMany(ConversationMessage::class);
    }

    public function receiver()
    {
        return $this->hasOne(User::class,'id','receiver_id');
    }

    public function sender()
    {
        return $this->hasOne(User::class,'id','sender_id');
    }

    public function user_two()
    {
        return $this->hasOne(User::class,'id','participant_2');
    }

    public function user_one()
    {
        return $this->hasOne(User::class,'id','participant_1');
    }

    public function stint() {
        return $this->belongsTo(Stint::class);
    }
}
