<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StintGalleryExtra extends Model
{
    //
    public $table = "stint_gallery_extra";
    protected $fillable = ['video', 'pdf'];


}
