<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Proposal extends Model
{
    //
    protected $fillable = ['title', 'user_id', 'job_id', 'cover_letter', 'bid_price','file','estimate_time'];
    public function job() {
        return $this->belongsTo(Job::class);
    }
    public function user()
    {
        return $this->hasOne(\App\User::class,'id','user_id');
    }
}
