<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Userskill extends Model
{
    //
    public $table = "user_skills";
    protected $fillable = [
        'user_id', 'skill_id', 'skill_level','created_at','updated_at'
    ];
    public function skill()
    {
        return $this->belongsTo(Skill::class, 'skill_id');
    }
}
