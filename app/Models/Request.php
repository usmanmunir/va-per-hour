<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Request extends Model
{
    protected $fillable = ['id','user_id', 'description', 'category_id', 'sub_category_id', 'duration', 'budget', 'type', 'status'];

    public static $rules = [
        'title' => 'required|unique:skills',
        'slug' => 'required|unique:skills',
    ];

    public static function findRequested () {
        $query = Request::query();
        // search results based on user input
        \Request::input('id') and $query->where('id', \Request::input('id'));
        \Request::input('status') and $query->where('status',  \Request::input('status') );
        \Request::input('search') and $query->where(function ($q){
            $q->where('description', 'like', '%'.\Request::input('search').'%');
        });

        // sort results
        \Request::input("sort") and $query->orderBy(\Request::input("sort"), \Request::input("sortType", "asc"));

        // paginate results
        return $query->paginate(30);
    }

    public function offers () {
        return $this->hasMany(Offer::class);
    }

    public function category() {
        return $this->hasOne(Category::class, 'category_id', 'id');
    }

    public function sub_category() {
        return $this->hasOne(Category::class, 'sub_category_id', 'id');
    }

    public function user() {
        return $this->belongsTo(\App\User::class);
    }
}
