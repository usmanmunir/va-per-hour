<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Userlanguage;

class Language extends Model
{
    //
    public $table = "language";
    protected $fillable = [
        'name', 'code'];
    public function userLanguage()
    {
        return $this->hasMany(Userlanguage::class, 'id', 'language_id' );
    }
}
