<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    //
    public $table = "chat";
    protected $fillable = ['sender_id', 'receiver_id', 'message', 'message_type', 'file', 'file_type', 'chat_type', 'is_read'
    ,'conversation_id'
    ];
    public function user()
    {
        return $this->belongsTo(User::class,'receiver_id','id');
    }
    public function conversation()
    {
        return $this->belongsTo(Conversation::class,'conversation_id','id');
    }
}
