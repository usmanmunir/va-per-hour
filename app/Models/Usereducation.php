<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Usereducation extends Model
{
    //
    public $table = "user_education";
    protected $fillable = [
        'user_id', 'institute_name','degree_name','year'
    ];
}
