<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FavoriteStint extends Model
{
    protected $fillable = [
        'user_id',
        'stint_id'
    ];
}
