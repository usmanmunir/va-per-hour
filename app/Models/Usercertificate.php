<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Usercertificate extends Model
{
    //
    public $table = "user_certificate";
    protected $fillable = [
        'user_id', 'certificate_name','certificate_type','year'
    ];
}
