<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StintOrder extends Model
{
    //
    public $table = "stint_orders";
    protected $fillable = [
        'stint_id',
        'buyer_id',
        'seller_id',
        'offer_id',
        'status',
        'payment_status',
        'stint_extra_id',
        'quantity',
        'transaction_id',
        'order_type',
        'extra_price',
        'unit_price'
        ];

    public function stint() {
        return $this->belongsTo(Stint::class);
    }

    public function buyer () {
        return $this->hasOne(\App\User::class, 'id', 'buyer_id');
    }
}
