<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OfferScope extends Model
{
    protected $fillable = [
        'offer_id',
        'design_customization',
        'content_upload',
        'responsive_design',
        'include_source_code'
    ];

    public static $rules = [
        'title' => 'required|unique:skills',
        'slug' => 'required|unique:skills',
    ];

    public function offer() {
        return $this->belongsTo(Offer::class);
    }
}
