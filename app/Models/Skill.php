<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Skill extends Model
{
    protected $fillable = ['title', 'slug', 'status'];

    public static $rules = [
        'title' => 'required|unique:skills',
        'slug' => 'required|unique:skills',
    ];

    public static function findRequested () {
        $query = Skill::query();
        // search results based on user input
        \Request::input('id') and $query->where('id', \Request::input('id'));
        \Request::input('status') and $query->where('status',  \Request::input('status') );
        \Request::input('search') and $query->where(function ($q){
           $q->where('title', 'like', '%'.\Request::input('search').'%');
           $q->orWhere('slug', 'like', '%'.\Request::input('search').'%');
        });

        // sort results
        \Request::input("sort") and $query->orderBy(\Request::input("sort"), \Request::input("sortType", "asc"));

        // paginate results
        return $query->paginate(30);
    }

    public function parent() {
        return $this->hasOne(Category::class, 'id', 'parent_id');
    }
    public function userSkill()
    {
        return $this->hasMany(Userskill::class, 'id', 'skill_id' );
    }
}
