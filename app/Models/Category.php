<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name', 'parent', 'slug', 'category_type', 'description', 'image', 'is_featured', 'put_in_menu', 'put_in_home'];

    public static $rules = [
        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        'name' => 'required|unique:categories,category_type',
        'slug' => 'required|unique:categories,category_type',
        'category_type' => 'required',
    ];

    public static function findRequested () {
        $query = Category::query();
        // search results based on user input
        \Request::input('id') and $query->where('id', \Request::input('id'));
        \Request::input('status') and $query->where('status',  \Request::input('role') );
        \Request::input('category_type') and $query->where('category_type', \Request::input('category_type'));
        \Request::input('search') and $query->where(function ($q){
           $q->where('name', 'like', '%'.\Request::input('search').'%');
           $q->orWhere('description', 'like', '%'.\Request::input('search').'%');
        });

        // sort results
        \Request::input("sort") and $query->orderBy(\Request::input("sort"), \Request::input("sortType", "asc"));

        // paginate results
        return $query->paginate(15);
    }

    public function parent() {
        $this->hasOne(Category::class, 'id', 'parent_id');
    }
    public function instructor()
    {
        return $this->hasone(Instructor::class, 'id', 'category_id' );
    }
    public function course()
    {
        return $this->hasone(Course::class, 'id', 'category_id' );
    }
}
