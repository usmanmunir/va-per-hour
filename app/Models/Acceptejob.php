<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Acceptejob extends Model
{
    //
    public $table = "accepted_jobs";
    protected $fillable = ['client_id', 'freelance_id', 'job_id', 'proposal_id'];
}
