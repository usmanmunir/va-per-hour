<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Marketplaceproduct extends Model
{
    //
    public $table = "marketplace_products";
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'category_id', 'user_id', 'name', 'description', 'thumbnail_image', 'mainfile', 'regular_price', 'extended_price', 'status'];

    public static function findRequested()
    {
        $query = Marketplaceproduct::with('user')
            ->with('category');
        // search results based on user input
        \Request::input('id') and $query->where('id', \Request::input('id'));
        \Request::input('user') and $query->where('user_id', \Request::input('user'));
        \Request::input('category_id') and $query->where('category_id', \Request::input('category_id'));
        \Request::input('status') and $query->where('status', \Request::input('status'));
        \Request::input('search') and $query->where(function ($q) {
            $q->where('name', 'like', '%' . \Request::input('search') . '%');
            $q->orWhere('description', 'like', '%' . \Request::input('search') . '%');
        });

        // sort results
        \Request::input("sort") and $query->orderBy(\Request::input("sort"), \Request::input("sortType", "asc"));

        // paginate results
        return $query->paginate(15);
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function user()
    {
        return $this->hasMany(user::class, 'id', 'user_id');
    }
}
