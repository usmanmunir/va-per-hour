<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StintGallery extends Model
{
    //
    public $table = "stint_gallery";
    protected $fillable = ['stint_id', 'image', 'stint_gallery_extra_id'];

    public function stint() {
        return $this->belongsTo(Stint::class, 'id', 'stint_id');
    }
    public function stintGallery() {
        return $this->belongsTo(StintGalleryExtra::class, 'stint_gallery_extra_id', 'id');
    }
}
