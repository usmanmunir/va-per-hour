<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Country;

class UserLocation extends Model
{
    //
    public $table = "user_location";
    protected $fillable = [
        'user_id', 'country_id', 'city','address','state', 'phone','created_at','updated_at', 'address2', 'zipcode'
    ];
    public function country()
    {
        return $this->hasOne(Country::class, 'id', 'country_id' );
    }

    public function state()
    {
        return $this->hasOne(State::class, 'id', 'state' );
    }

    public function city()
    {
        return $this->hasOne(City::class, 'id', 'city' );
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}
