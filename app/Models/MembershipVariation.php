<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MembershipVariation extends Model
{
    protected $fillable = [
        'membership_id','name', 'description', 'price', 'month', 'user_type'
    ];

    public static $rules = [
        'name',
        'description',
        'price',
        'month',
        'user_type'
    ];

    public static function findRequested () {
        $query = MembershipVariation::query();
        // search results based on user input
        \Request::input('id') and $query->where('id', \Request::input('id'));
        \Request::input('status') and $query->where('status',  \Request::input('status') );
        \Request::input('search') and $query->where(function ($q){
            $q->where('name', 'like', '%'.\Request::input('search').'%');
            $q->orWhere('description', 'like', '%'.\Request::input('search').'%');
        });

        // sort results
        \Request::input("sort") and $query->orderBy(\Request::input("sort"), \Request::input("sortType", "asc"));

        // paginate results
        return $query->paginate(30);
    }

    public function membership() {
        return $this->belongsTo(Membership::class);
    }
}
