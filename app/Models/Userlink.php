<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Userlink extends Model
{
    //
    public $table = "user_link_account";
    protected $fillable = [
        'user_id', 'linked_in', 'google','facebook','twitter', 'github','stack_overflow','behance','created_at','updated_at'
    ];
}
