<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = ['name', 'type', 'status'];

    public static $rules = [
        'name' => 'required',
        'type' => 'required',
    ];

    public static function findRequested () {
        $query = Tag::query();
        // search results based on user input
        \Request::input('id') and $query->where('id', \Request::input('id'));
        \Request::input('type') and $query->where('type',  \Request::input('type') );
        \Request::input('search') and $query->where(function ($q){
            $q->where('name', 'like', '%'.\Request::input('search').'%');
            $q->orWhere('type', 'like', '%'.\Request::input('search').'%');
        });

        // sort results
        \Request::input("sort") and $query->orderBy(\Request::input("sort"), \Request::input("sortType", "asc"));

        // paginate results
        return $query->paginate(30);
    }
}
