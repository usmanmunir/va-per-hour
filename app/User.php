<?php

namespace App;

use App\Models\Job;
use App\Models\Skill;
use App\Models\Stint;
use App\Models\Usercertificate;
use App\Models\Usereducation;
use App\Models\UserLinkAccount;
use App\Models\UserMembership;
use App\Models\Userskill;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Models\MasterUservarify;
use App\Models\UserLocation;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'user_type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function MasterUservarify()
    {
        return $this->hasOne(MasterUservarify::class, 'id', 'user_id' );
    }

    public function userLocation()
    {
        return $this->hasMany(UserLocation::class, 'id', 'user_id' );
    }

    public function location () {
        return $this->hasOne(UserLocation::class);
    }

    public function memberships() {
        return $this->hasMany(UserMembership::class, 'id', 'user_id');
    }

    public function stints () {
        return $this->hasMany(Stint::class);
    }

    public function social_links () {
        return $this->hasOne(UserLinkAccount::class);
    }

    public function skills () {
        return $this->hasMany(Userskill::class);
    }

    public function certification () {
        return $this->hasMany(Usercertificate::class);
    }

    public function education () {
        return $this->hasMany(Usereducation::class);
    }

    public function jobs () {
        return $this->hasMany(Job::class, 'id', 'user_id');
    }
}
