$('.edit-profile-user-details').click(function () {

    $('.profile-user-details .form-groups').toggleClass('hide1');
    $('.profile-user-details .sub').toggleClass('hide1');
    $('.profile-user-details .submit_box').toggleClass('hide1');
    $('.profile-user-details .cm-error').empty();

    return false;
});
$('.edit-location-icon').click(function () {

    $('#location-profile-update .form-groups').toggleClass('hide1');
    $('#location-profile-update .sub').toggleClass('hide1');
    $('#location-profile-update .submit_box').toggleClass('hide1');
    $('#location-profile-update .cm-error').empty();

    return false;
});
$('.edit-social-link-icon').click(function () {

    $('.linked-accounts .form-groups').toggleClass('hide1');
    $('.linked-accounts .sub').toggleClass('hide1');
    $('.linked-accounts .submit_box').toggleClass('hide1');
    $('.linked-accounts .cm-error').empty();

    return false;
});

$( '.edit-instructor' ).click( function() {

    $( '.user-instructor .cvform-box' ).toggleClass( 'hide1' );
    $( '.user-instructor .sub' ).toggleClass( 'hide1' );
    $( '.user-instructor .submit_box' ).toggleClass( 'hide1' );

    return false;
});
//Language forms scroll open
$(document).ready(function() {
    loadConversations(10);
    $(".languageadd").click(function() {
        $(this).nextAll('.lang-addbox').slideToggle();
    });
    $(".searchHide").click(function() {
        $('.serachShow').slideToggle();
    });

    $('.your-class').slick({
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 2,
        autoplay: true,
        autoplaySpeed: 2000,
        default:true,
        responsive: [
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                    centerMode: false
                }
            },
            {
                breakpoint: 550,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    centerMode: false
                }
            }
        ]
    });
});

function loadConversations(limit) {
    $.ajax({
        url:'/messages/load/'+limit,
        type: 'GET',
        success:function (res) {
            $(".dropdowns.messaging--dropdown .messages").html(res.html);
        },
        error: function (err) {
            toastr.error(err.responseJSON)
        }
    })
}