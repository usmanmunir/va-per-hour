<?php

return [
    'membership_for' => [
        "0" => "Free Plan",
        "1" => "Seller",
        "2" => "Buyer",
        "3" => "Marketplace",
        "4" => "Author",
        "5" => "E-Learning",
        "6" => "Instructor",
        "7" => "All",
    ],
    'user_type' => [
        'single-user'=> 'Single User',
        'agency-user'=> 'Agency',
    ],
    'chapter_type' => [
        'course'=> 'Course',
        'quiz'=> 'Quiz',
    ],
    'course_status' => [
        'draft'=> 'draft',
        'publish'=> 'Publish',
    ],
    'job_request_status'=>[
        1 => 'active',
        2 => 'paused',
        3 => 'pending',
        4 => 'unapproved',
    ],
    'stint_status' => [
        0 => 'draft',
        1 => 'active',
        2 => 'paused',
        3 => 'pending',
        4 => 'unapproved',
    ],
    'stint_plans' => [
        1 => 'BASIC',
        2 => 'STANDARD',
        3 => 'PREMIUM',
    ],
    'order_status' => [
        'active' => 1,
        'delivered' => 2,
        'completed'=> 3,
        'cancelled' => 4,
    ]

];