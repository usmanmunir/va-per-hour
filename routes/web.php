<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('test-event', function (){
    //\App\Helpers\PusherHelper::sendNotification('10', 'messages', ['message'=> 'test-message', 'recipient' => 1, 'conversation' => '1']);
    $check = event(new App\Events\SendMessage(['message'=> 'test-message', 'recipient' => 1, 'conversation' => '1'], 10));
    dd($check);
    return "Event has been sent!";
});

Route::namespace('Front')->group(function () {

    Route::get("/load-languages", function () {
        $list = \App\Models\Language::where('name', 'like', '%' . request('term') . '%')
            ->select(\DB::raw('name as label'), \DB::raw('id as value'))
            ->get();
        return response()->json($list, 200);
    });
    //    Load skills
    Route::get("/load-skills", function () {
        $skills = \App\Models\Skill::where('title', 'like', '%' . request('term') . '%')
            ->select(\DB::raw('title as label'), \DB::raw('id as value'))
            ->get();
        return response()->json($skills, 200);
    });

    Route::get('/', 'HomeController@index');
    Route::get('switch/{mode?}/{module?}/{role?}', 'HomeController@switchModule');
    Route::get('registeruser', 'Auth\RegisterController@showRegisterform');
    Route::post('/register', 'Auth\RegisterController@create');
    Route::get('/user/verify/{token}', 'Auth\RegisterController@verifyUser');
    //    LOGIN USER
    Route::get('signin', 'Auth\LoginController@showLogin')->name('user_signin');
    Route::post('signin', 'Auth\LoginController@doLogin')->name('signin');
    Route::get('signout', 'Auth\LoginController@signout');
    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset.token');
    Route::post('password/reset', 'Auth\ResetPasswordController@changePassword');

    Route::get('/user-profile/{user_id?}', 'MasterUserController@userProfile');

    Route::group(['middleware' => ['auth']], function () {
        Route::get('/edit-profile', 'MasterUserController@editProfile');
        Route::get('/user/membership', 'MasterUserController@userMembership');
        Route::get('/user/profile-setting', 'MasterUserController@profileSetting');
        Route::get('/user/get-paid', 'MasterUserController@getPaid');
        Route::get('/user/password-security', 'MasterUserController@PasswordSecurity');
        Route::get('/user/seller-profile', 'MasterUserController@sellerProfile');
        Route::get('/user/get-user-language', 'MasterUserController@getUserLanguage');
        Route::get('/user/get-user-skill', 'MasterUserController@getUserSkill');
        Route::get('/user/get-user-education', 'MasterUserController@getUserEducation');
        Route::get('/user/get-user-certificate', 'MasterUserController@getUserCertificate');
        Route::get('/user/instructor', 'InstructorController@userInstructor');
        Route::get('/getlanguages', 'MasterUserController@getLanguages');
        //    Front User Avatar change
        Route::post('image/update', 'MasterUserController@updateAvatar');
        //    Front user details update
        Route::post('user-detail-update', 'MasterUserController@updateUserDetail');
        Route::post('user-location-update', 'MasterUserController@updateUserLocation');
        Route::post('user-link-update', 'MasterUserController@linkdAccounts');

        Route::post('user/save-user-language', 'MasterUserController@saveUserLanguage');
        Route::post('user/delete-user-language/{userLanguageId}', 'MasterUserController@deleteUserLanguage');

        Route::post('user/save-user-skill', 'MasterUserController@saveUserSkill');
        Route::post('user/delete-user-skill/{userSkillId}', 'MasterUserController@deleteUserSkill');

        Route::post('user/save-user-education', 'MasterUserController@saveUserEducation');
        Route::post('user/delete-user-education/{userEducationId}', 'MasterUserController@deleteUserEducation');

        Route::post('user/save-user-certificate', 'MasterUserController@saveUserCertificate');
        Route::post('user/delete-user-certificate/{userCertificateId}', 'MasterUserController@deleteUserCertificate');

        Route::post('user/save-instructor', 'InstructorController@saveInstructorRecord');
        Route::post('user/save-website', 'MasterUserController@saveUserWebsite');

        /********  Routes related to User Membership*******/
        Route::get('/user/pricing-plan', 'UserMembershipsController@index');
        Route::post('/user/buy-membership/{membership}', 'UserMembershipsController@buyMembership');
        Route::post('/user/buy-membership/{membership}/confirm', 'UserMembershipsController@confirmMembership');

        /***** End User Membership ******/

        Route::get('/dashboard', 'HomeController@dashboard')->middleware('auth');
        Route::get('/home', 'HomeController@index')->name('home');
        Route::get('/analytics', 'SellerController@analytics');
        Route::get('/earnings', 'SellerController@earnings');
        Route::get('load-states/{country_id}', 'CheckoutController@loadStates');
        Route::get('load-cities/{state_id}', 'CheckoutController@loadCities');

        Route::post('/checkout', 'CheckoutController@index');
        Route::post('/process/payment', 'CheckoutController@processPayment');

        Route::get('/user/orders', 'OrderController@userOrders');
        Route::get('/users/manage-orders/{order_status?}', 'StintsController@myOrders');
        //    Courses Routes
        Route::prefix('/elearning')->group(function () {
            Route::prefix('/course')->group(function () {
                Route::get('/my-course', 'CourseController@courseLists');
                Route::get('/add-course', 'CourseController@addCourse');
                Route::get('/edit/{course}', 'CourseController@editCourse');
                Route::get('/{course}/chapters', 'ChapterController@index');
                Route::get('/{course}/{chapter}/lectures', 'LecturesController@index');
                Route::post('/save-course', 'CourseController@saveCourse');
                Route::post('/update-course', 'CourseController@updateCourse');
                Route::post('/delete-user-course/{courseId}', 'CourseController@deleteUserCourse');
                //    Chapter Routes
                Route::post('/save-chapter', 'ChapterController@saveChapter');
                Route::post('/delete-chapter/{chapterId}', 'ChapterController@deleteChapter');
                //Lecture Routes
                Route::post('/save-lecture', 'LecturesController@saveLecture');
                Route::post('/delete-lecture/{lectureId}', 'LecturesController@deleteLecture');
                Route::post('/save-lecture-video', 'LecturesController@saveVideo');

                //Elearning course routes for front user to see cources
                Route::get('/', 'CourseController@elearningCourse');
                Route::get('category/{categorySlug}/{categoryId}', 'CourseController@getCategroyCourse');
                Route::get('category', 'CourseController@diplayCategories');
                Route::get('course-syllabus/{courseId}', 'CourseController@getCourseSyllabus');
            });
        });

        // Marketplace Routes
        Route::prefix('/marketplace')->group(function () {
            Route::get('/home', 'MarketplaceController@home');
            Route::get('/upload-product', 'MarketplaceController@uploadProduct');
            Route::post('/save-marketplace-product', 'MarketplaceController@saveProduct');
            Route::post('/edit-marketplace-product', 'MarketplaceController@editProduct');
            Route::get('/product-info/{marketplaceProductId}', 'MarketplaceController@productInfo');
            Route::post('/user/delete-product/{userMarketplaceProductId}', 'MarketplaceController@deleteUserMarketplaceProduct');
            Route::get('/user/edit-product/{userMarketplaceProductId}', 'MarketplaceController@editUserMarketplaceProduct');
            Route::get('/user/manage-product', 'MarketplaceController@manageUserProduct');
            Route::post('/save-marketplace-product-thumbnail-image', 'MarketplaceController@saveThumbnailImage');
            Route::post('/save-marketplace-product-zipfile', 'MarketplaceController@saveZipfile');

        });


        Route::prefix('/job')->group(function () {
            /*Route::get('/', 'JobsController@index');
            Route::get('/category/{job_id}/{job_name}', 'JobsController@getJobByCategory');
            Route::get('/category/all', 'JobsController@getJobByCategory');
            Route::get('/manage-requests/{status?}', 'JobsController@manageRequests');
            Route::get('/post-request', 'JobsController@create');
            Route::post('/post-request', 'JobsController@store');
            Route::get('/job/{job}/pause', 'JobsController@pause');
            Route::get('/job/{job}/delete', 'JobsController@delete');
            Route::get('/job/{job}/resume', 'JobsController@resume');
            Route::get('/description/{jobId}', 'JobsController@jobDetail');
            Route::get('/proposals/{jobId}', 'ProposalController@index');
            Route::post('/upload-proposal-doc','ProposalController@uploadProposalDoc');
            Route::post('/submit-proposal','ProposalController@submitProposal');
            Route::get('/proposal-requests/{jobId?}','ProposalController@getProposalRequest');
            //Hire Freelance
            Route::post('/hirefreelance','JobsController@acceptJob');
            //Route::get('/{CategorySlug}/{categoryId}', 'JobsController@getJobByCategory');
            Route::get('/chat/{userName?}/{userId?}', 'ChatController@getUserChat');*/
            Route::get('/manage-requests/{type?}', 'BuyerController@manageRequests');
            Route::get('/manage-offers/{request_id}', 'BuyerController@manageOffers');
            Route::get('/post-request', 'BuyerController@postRequest');
            Route::get('/edit-request/{job_request}', 'BuyerController@editRequest');
            Route::get('/pause-request/{job_request}', 'BuyerController@pauseRequest');
            Route::get('/activate-request/{job_request}', 'BuyerController@activateRequest');
            Route::get('/delete-request/{job_request}', 'BuyerController@deleteRequest');
            Route::get('/request/{job_request}/view/offer', 'BuyerController@manageOffers');
            Route::group(['prefix' => 'offer'], function () {
                Route::delete('/{job_id}/remove', 'OfferController@removeOffer');
                Route::post('/{job_id}/order', 'OrderController@order');
            });
            Route::get('/buyer-requests', 'SellerController@buyerRequests');
            Route::get('/sent-offers', 'SellerController@sentOffers');
            Route::post('/post-request', 'BuyerController@saveRequest');
            Route::post('/make/offer/{request_id?}', 'OfferController@saveOffer');


        });

        Route::prefix('/stint')->group(function () {
            Route::get('/', 'StintsController@index');
            Route::get('/category/{category}/{category_name}', 'StintsController@getByCategories');
            Route::get('/category/all', 'StintsController@getByCategories');
            Route::get('/my-stints/{status?}', 'StintsController@myStints');
            Route::get('/create-stint', 'StintsController@create');
            Route::get('/{stint}/edit', 'StintsController@editStint');
            Route::get('/{stint}/delete', 'StintsController@deleteStint');
            Route::get('/my-stint/{stint}/pricing-plan', 'StintsController@pricing');
            Route::get('/my-stint/{stint}/details', 'StintsController@descriptions');
            Route::get('/my-stint/{stint}/requirement', 'StintsController@requirment');
            Route::get('/my-stint/{stint}/gallery', 'StintsController@gallery');
            Route::get('/my-stint/{stint}/publish', 'StintsController@publish');

            Route::post('/save_package', 'StintsController@store');
            Route::post('/my-stint/{stint}/pricing-plan', 'StintsController@savePricing');
            Route::post('/my-stint/{stint}/details', 'StintsController@saveDescriptions');
            Route::post('/my-stint/{stint}/requirement', 'StintsController@saveRequirement');
            Route::post('/my-stint/{stint}/upload-gallery', 'StintsController@uploadGallery');
            Route::post('/my-stint/{stint}/upload-video', 'StintsController@uploadVideo');
            Route::post('/my-stint/{stint}/upload-pdf', 'StintsController@uploadPdf');
            Route::post('/my-stint/{stint}/gallery', 'StintsController@saveStintGallery');
            Route::post('/my-stint/{stint}/publish', 'StintsController@publishStint');

            Route::post('/mark-favorite/{stint}', 'StintsController@markFavorite');
            Route::post('/report/{stint}', 'StintsController@markReport');

            // Routes for the frontend stint listing
            Route::get('/stint-list/{user?}', 'StintsController@stintLists');
            Route::get('/view/{stint_id}/{stintname}', 'StintsController@stintDetail');
            Route::post('/purchase/{stint}/{stint_title?}','StintsController@purchaseStint');

            //Chat Routes
            Route::get('chat/{userName?}/{userId?}', 'ChatController@getUserChat');
            Route::post('chat/upload-file', 'ChatController@uploadFile');
            Route::post('chat/send-message', 'ChatController@sendMessage');
        });
        Route::prefix('/category')->group(function () {
            Route::get('/find-subcategory/{category}', 'CategoriesController@getSubCategory');
        });

        Route::prefix('/messages')->group(function (){
            Route::get('load/{limit?}', 'MessagesController@loadMessages');
            Route::post('/send-message/{token}', 'ConversationController@startConversation');
            Route::get('rooms/{conversation?}', 'ConversationController@rooms');
            Route::post('rooms/{conversation}/send', 'MessagesController@send');
        });
    });
});


//Admin routes
Route::namespace('Admin')->prefix('superaccess')->group(function () {
    Route::get('/', 'AuthController@login')->middleware('sentry.guest');

    Route::group(['middleware' => ['sentry.admin']], function () {
        Route::get('dashboard', 'DashboardController@index')->name('admin.dashboard');

        /*ResourceFull routing*/
        Route::resource('permissions', 'PermissionsController');
        Route::resource('tags', 'TagsController');
        Route::resource('categories', 'CategoriesController');
        Route::resource('skills', 'SkillsController');
        Route::resource('memberships', 'MembershipController');

//        Marketplace Prefix for admin side
        Route::prefix('marketplace')->group(function () {
            Route::get('dashboard-marketplace', 'MarketplaceController@dashboard');
            Route::get('/', 'MarketplaceController@marketplaceProducts');
            Route::get('orders', 'MarketplaceController@marketplaceOrders');
            Route::get('view/{marketplaceProductId}', 'MarketplaceController@marketplaceProductDetail');
        });

//        Course Prefix for admin side
        Route::prefix('course')->group(function () {
            Route::get('dashboard-course', 'CourseController@dashboard');
            Route::get('/', 'CourseController@getAllCources');
            Route::get('orders', 'CourseController@courseOrders');
            Route::get('instructor-user-request', 'CourseController@instructorUsers');
            Route::get('course-edit/{courseId}', 'CourseController@courseDetail');
        });
//        Stints Prefix for admin side

        Route::prefix('stints')->group(function () {
            Route::get('dashboard', 'StintsController@dashboard');
            Route::get('/', 'StintsController@getAllStints');
            Route::get('stint-detail/{stintId}', 'StintsController@stintDetail');
            Route::post('stint-status/{stintId}', 'StintsController@stintStatus');


        });
    });
});

Route::group(['namespace' => '\\Sentinel\Controllers', 'middleware' => ['web']], function () {
    Route::prefix('superaccess')->group(function () {
        // Sentinel Session Routes
        Route::get('login', ['as' => 'sentinel.login', 'uses' => 'SessionController@create']);
        Route::get('logout', ['as' => 'sentinel.logout', 'uses' => 'SessionController@destroy']);
        Route::get('sessions/create', ['as' => 'sentinel.session.create', 'uses' => 'SessionController@create']);
        Route::post('sessions/store', ['as' => 'sentinel.session.store', 'uses' => 'SessionController@store']);
        Route::delete('sessions/destroy', ['as' => 'sentinel.session.destroy', 'uses' => 'SessionController@destroy']);

        // Sentinel Registration
        Route::get('register', ['as' => 'sentinel.register.form', 'uses' => 'RegistrationController@registration']);
        Route::post('register', ['as' => 'sentinel.register.user', 'uses' => 'RegistrationController@register']);
        Route::get('users/activate/{hash}/{code}', ['as' => 'sentinel.activate', 'uses' => 'RegistrationController@activate']);
        Route::get('reactivate', ['as' => 'sentinel.reactivate.form', 'uses' => 'RegistrationController@resendActivationForm']);
        Route::post('reactivate', ['as' => 'sentinel.reactivate.send', 'uses' => 'RegistrationController@resendActivation']);
        Route::get('forgot', ['as' => 'sentinel.forgot.form', 'uses' => 'RegistrationController@forgotPasswordForm']);
        Route::post('forgot', ['as' => 'sentinel.reset.request', 'uses' => 'RegistrationController@sendResetPasswordEmail']);
        Route::get('reset/{hash}/{code}', ['as' => 'sentinel.reset.form', 'uses' => 'RegistrationController@passwordResetForm']);
        Route::post('reset/{hash}/{code}', ['as' => 'sentinel.reset.password', 'uses' => 'RegistrationController@resetPassword']);

        // Sentinel Profile
        Route::get('profile', ['as' => 'sentinel.profile.show', 'uses' => 'ProfileController@show']);
        Route::get('profile/edit', ['as' => 'sentinel.profile.edit', 'uses' => 'ProfileController@edit']);
        Route::put('profile', ['as' => 'sentinel.profile.update', 'uses' => 'ProfileController@update']);
        Route::post('profile/password', ['as' => 'sentinel.profile.password', 'uses' => 'ProfileController@changePassword']);

        // Sentinel Users
        Route::get('admin/users', ['as' => 'sentinel.users.index', 'uses' => 'UserController@index']);
        Route::get('admin/users/create', ['as' => 'sentinel.users.create', 'uses' => 'UserController@create']);
        Route::post('admin/users', ['as' => 'sentinel.users.store', 'uses' => 'UserController@store']);
        Route::get('admin/users/{hash}', ['as' => 'sentinel.users.show', 'uses' => 'UserController@show']);
        Route::get('admin/users/{hash}/edit', ['as' => 'sentinel.users.edit', 'uses' => 'UserController@edit']);
        Route::post('admin/users/{hash}/password', ['as' => 'sentinel.password.change', 'uses' => 'UserController@changePassword']);
        Route::post('admin/users/{hash}/memberships', ['as' => 'sentinel.users.memberships', 'uses' => 'UserController@updateGroupMemberships']);
        Route::put('admin/users/{hash}', ['as' => 'sentinel.users.update', 'uses' => 'UserController@update']);
        Route::delete('admin/users/{hash}', ['as' => 'sentinel.users.destroy', 'uses' => 'UserController@destroy']);
        Route::get('admin/users/{hash}/suspend', ['as' => 'sentinel.users.suspend', 'uses' => 'UserController@suspend']);
        Route::get('admin/users/{hash}/unsuspend', ['as' => 'sentinel.users.unsuspend', 'uses' => 'UserController@unsuspend']);
        Route::get('admin/users/{hash}/ban', ['as' => 'sentinel.users.ban', 'uses' => 'UserController@ban']);
        Route::get('admin/users/{hash}/unban', ['as' => 'sentinel.users.unban', 'uses' => 'UserController@unban']);

        // Sentinel Groups
        Route::get('groups', ['as' => 'sentinel.groups.index', 'uses' => 'GroupController@index']);
        Route::get('groups/create', ['as' => 'sentinel.groups.create', 'uses' => 'GroupController@create']);
        Route::post('groups', ['as' => 'sentinel.groups.store', 'uses' => 'GroupController@store']);
        Route::get('groups/{hash}', ['as' => 'sentinel.groups.show', 'uses' => 'GroupController@show']);
        Route::get('groups/{hash}/edit', ['as' => 'sentinel.groups.edit', 'uses' => 'GroupController@edit']);
        Route::put('groups/{hash}', ['as' => 'sentinel.groups.update', 'uses' => 'GroupController@update']);
        Route::delete('groups/{hash}', ['as' => 'sentinel.groups.destroy', 'uses' => 'GroupController@destroy']);
    });

});
