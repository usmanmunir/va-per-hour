<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">

    <title>VAPerHour</title>
    <meta name="Title" content="Vaper Hour"/>

    <meta name="description" content="Vaper Hour"/>

    <meta name="keywords" content="Vaper Hour"/>

    <link rel="shortcut icon" href=/assets/images/logo/favicon.png"/>


    <link rel="stylesheet" href="/assets/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="/assets/css/imageuploadify.min.css"/>
    <link rel="stylesheet" href="/assets/css/dropzone.min.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/css/slick.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/css/slick-theme.css"/>
    <link rel="stylesheet" href="/assets/css/selectize.default.css"/>
    <link rel="stylesheet" href="/assets/css/jquery.fancybox.min.css"/>
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="/assets/css/dashboard.css"/>
    <script type="text/javascript">
        var vaph = {
            base_url: 'https://www.vaperhour.com/'
        }
    </script>


    <link rel="stylesheet" href="/assets/css/style.css"/>
    <link rel="stylesheet" href="/assets/css/course.css"/>
    <link rel="stylesheet" href="/assets/css/checkout.css"/>
    <link rel="stylesheet" href="/assets/css/stint.css"/>
    <link rel="stylesheet" href="/assets/css/custom.css"/>
    <link rel="stylesheet" href="/assets/css/responsive.css"/>

    <link rel="stylesheet" href="/assets/css/nomalization/token-input.css"/>
    <link rel="stylesheet" href="/assets/css/nomalization/token-input-facebook.css"/>
    <!--<link rel="stylesheet" href="css/animate.css">-->
    <script src="/assets/js/jquery-3.3.1.min.js"></script>
    <!-- -->
    <!--<link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />-->
    <!-- Start of vaph1552074656 Zendesk Widget script -->
    <script id="ze-snippet"
            src="https://static.zdassets.com/ekr/snippet.js?key=31516029-3d67-4d44-8c23-b50a512e7751"></script>
    <!-- End of vaph1552074656 Zendesk Widget script -->

</head>
<body class="home product-listing" spellcheck="true">


<header class="header">
    <div class="full-width top-menu">
        <!--<div class="langauge-change">
            <select class="form-control form-control-sm">
                <option>English</option>
                <option>France</option>
            </select>
        </div>-->
        <div class="container">
            <div class="logo animated swing">
                <a href="https://www.vaperhour.com/">
                    <img class="" src="/assets/images/logo/logo-1.png">
                </a>
            </div>
            <div class="right-menu">
                <div class="btn-seller">
                    <a class="ctm-btn mright20" href="#" data-toggle="modal" data-target=".signin-model">Signup /
                        Login</a>
                </div>
            </div>
        </div>
    </div>

    <section id="top-search" class="hide1 serachShow">
        <div class="topsearch_back">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="search_box">
                            <form id="search_form" onsubmit="return false;">
                                <input type="text" class="text_field" placeholder="Search your products..."
                                       name="search">
                                <div class="search__select select-wrap">
                                    <select name="search_type" class="select--field" id="cat">
                                        <option value="" selected>Select option</option>
                                        <option value="stint">Stint</option>
                                        <option value="freelancer">Freelancer</option>
                                        <option value="course">Course</option>
                                        <option value="products">Products</option>
                                    </select>
                                    <i class="fa fa-angle-down"></i>
                                </div>
                                <button type="button" onclick="return searching();" class="search-btn btn--lg">Search
                                    Now
                                </button>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="full-widht bottom-menu">
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <!-- <a class="navbar-brand" href="#">Navbar</a> -->
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item dropdown responsive-proifile">
                            <a href="#" class="dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false">
                                <div class="home-profile-pic">
                                    <img src="/assets/images/default/user.png">
                                </div>
                                <div class="name-and-balance">
                                    <h2 class="name"></h2>
                                    <span class="balance">$10.00</span>
                                </div>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                <a class="dropdown-item active" href="#"><i class="fa fa-user" aria-hidden="true"></i>Profile</a>
                                <a class="dropdown-item" href="#"><i class="fa fa-home" aria-hidden="true"></i>Dashboard</a>
                                <a class="dropdown-item" href="#"><i class="fa fa-cog"
                                                                     aria-hidden="true"></i>Setting</a>
                                <a class="dropdown-item" href="#"><i class="fa fa-heart-o" aria-hidden="true"></i>Favourite</a>
                                <a class="dropdown-item" href="#"><i class="fa fa-briefcase" aria-hidden="true"></i>Withdrawl</a>
                                <a class="dropdown-item" href="#"><i class="fa fa-sign-out" aria-hidden="true"></i>Logout</a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="https://www.vaperhour.com/stint/category/programming-tech">Programming
                                & Tech</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="https://www.vaperhour.com/stint/category/digital-marketing">Digital
                                Marketing</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="https://www.vaperhour.com/stint/category/graphic-design">Graphic
                                Design</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link"
                               href="https://www.vaperhour.com/stint/category/E-Marketing">E-Marketing</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="https://www.vaperhour.com/stint/category/logo-design">Logo
                                Design</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="https://www.vaperhour.com/stint/category/">View All</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</header>
{{--End header--}}
@yield('main_container')

{{--Footer start--}}
<footer>
    <div class="fotter-top">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="logo">
                        <img src="/assets/images/fotter-logo.png">
                    </div>
                </div>
                <!--            <div class="col-xs-12 col-sm-6 col-md-3 col-xl-3 f-1 quick-info">-->
                <!--               <h3 class="title">Quick Info</h3>-->
                <!--               <a href="#"><i class="fa fa-phone" aria-hidden="true"></i><span> +88 25151 51515</span></a>-->
                <!--               <a href="#"><i class="fa fa-envelope" aria-hidden="true"></i><span> info@vaperhour.com<br>info@vaperhour.com</span></a>-->
                <!--               <a href="#"><i class="fa fa-phone" aria-hidden="true"></i><span> +88 25151 51515</span></a>-->
                <!--               <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i><span> Khan jahan Ali Road , <br> India</span></a>-->
                <!--            </div>-->
                <div class="col-xs-12 col-sm-6 col-md-2 col-xl-2 f-1 quick-info">
                    <h3 class="title">Useful Links</h3>
                    <a href="https://www.vaperhour.com/user/pricing-plan" data-toggle="modal"
                       data-target=".signin-model"><span>Become a Free User</span></a>
                    <a href="https://www.vaperhour.com/stint/become-seller"><span>Become a Seller</span></a>
                    <a href="https://www.vaperhour.com/user/pricing-plan" data-toggle="modal"
                       data-target=".signin-model"><span>Become a Buyer</span></a>
                    <a href="https://www.vaperhour.com/marketplace/marketplace/become-merchant"><span>Become A Merchant</span></a>
                    <a href="https://www.vaperhour.com/user/become-instructor-home"><span>Become Instructor</span></a>
                    <a href="https://www.vaperhour.com/user/pricing-plan" data-toggle="modal"
                       data-target=".signin-model"><span>Become a Premium</span></a>

                </div>
                <div class="col-xs-12 col-sm-6 col-md-2 col-xl-2 f-2 quick-info">
                    <h3 class="title">Company</h3>
                    <a href="https://www.vaperhour.com/about"><span>About us</span></a>
                    <a href="https://www.vaperhour.com/partnership"><span>Partnership</span></a>
                    <a href="https://www.vaperhour.com/careers"><span>Careers</span></a>
                    <a href="https://www.vaperhour.com/features"><span>Features</span></a>
                    <a href="https://www.vaperhour.com/privacy-policy"><span>Privacy Policy</span></a>
                    <a href="https://www.vaperhour.com/terms"><span>Terms & Conditions</span></a>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-2 col-xl-2 f-3 quick-info">
                    <h3 class="title">Support</h3>

                    <a href="https://www.vaperhour.com/faqs"><span>Faq's</span></a>

                    <a href="https://www.vaperhour.com/#"><span> Help & Support </span></a>

                    <a href="https://www.vaperhour.com/trust-and-safety"><span>Trust & Safety</span></a>

                </div>

                <div class="col-xs-12 col-sm-6 col-md-2 col-xl-2 f-3 quick-info">
                    <h3 class="title">Community</h3>

                    <a href="https://www.vaperhour.com/#"><span>Blog</span></a>

                    <a href="https://www.vaperhour.com/#"><span>Forum</span></a>

                    <a href="https://www.vaperhour.com/affiliates"><span>Affiliates</span></a>

                    <a href="https://www.vaperhour.com/invite-friend"><span>Invite a Friend</span></a>

                </div>

                <div class="col-xs-12 col-sm-6 col-md-4 col-xl-4 f-5 quick-info">
                    <h3 class="title">Get the Vaper Hour App</h3>
                    <p class="discription">Download app and enjoy the freelancer, E-learning, & Marketplace on your
                        mobile</p>
                    <!--               <div class="input-group">-->
                    <!--                  <input type="text" class="form-control" name="x" placeholder="Search term...">-->
                    <!--                  <span class="input-group-btn">-->
                    <!--                  <button class="btn btn-default" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>-->
                    <!--                  </span>-->
                    <!--               </div>-->
                    <div class="footer-app-image">
                        <div class="iphone-image">
                            <a href=""> <img src="/assets/images/applestore.png"> </a>
                        </div>
                        <div class="gplay-image">
                            <a href=""> <img src="/assets/images/googleplay.png"></a>
                        </div>
                    </div>
                    <div class="socail-meadia">
                        <a href="https://www.facebook.com/vaphtech"><img src="/assets/images/icons/facebook.png"></a>
                        <a href=""><img src="/assets/images/icons/twitter.png"></a>
                        <a href=""><img src="/assets/images/icons/google-plus.png"></a>
                        <a href="https://in.pinterest.com/vaperhour/"><img src="/assets/images/icons/pinterest.png"></a>
                        <a href=""><img src="/assets/images/icons/linkedin.png"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="fotter-bottom">
        <div class="rights">2018 &copy; All Right Reserved By <span>VAPH Technologies Co. Ltd</span></div>
    </div>
</footer>


<div class="modal fade bd-example-modal-lg login-model signin-model" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <!-- <img src="images/logo/logo-2.png"> -->
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Login</h5>
            </div>
            <div class="modal-body">
                <form method="POST" id="login-form" action="{{ route('sentinel.session.store') }}" accept-charset="UTF-8">
                {{--<form id="login-" action="{{ route('sentinel.session.store') }}" method="POST">--}}
                    <div class="form-group row">
                        <div class="col-sm-12 ctm-input">
                            <input name="email" type="email" class="form-control {{ ($errors->has('email')) ? 'has-error' : '' }}" placeholder="Your Email">
                            {{ ($errors->has('email') ? $errors->first('email') : '') }}
                        </div>
                        <div class="col-sm-12 ctm-input">
                            <input name="password" type="password" class="form-control {{ ($errors->has('password')) ? 'has-error' : '' }}" placeholder="Password">
                            {{ ($errors->has('password') ?  $errors->first('password') : '') }}
                        </div>
                        <!--						<div class="col-sm-12 ctm-input">-->
                        <!--							<select class="form-control">-->
                        <!--								<option>Select Role</option>-->
                        <!--							</select>-->
                        <!--						</div>-->
                        <div class="col-sm-12 cm-error"></div>
                        <div class="col-sm-12 text-danger" id="login-error"></div>
                        <div class="col-sm-12 ctm-login-btn">
                            <input name="_token" value="{{ csrf_token() }}" type="hidden">
                            <button type="submit" id="login-button" data-type="login" class="ctm-btn">Login</button>
                            <span class="ragister-now"><span>Not a Member Yet? &nbsp; </span><a href="#"
                                                                                                data-toggle="modal"
                                                                                                data-target=".signup-model"
                                                                                                data-dismiss="modal"> Register</a></span>
                            <span class="forgot-pass"><a href="{{ route('sentinel.forgot.form') }}">Forget Password?</a></span>

                        </div>
                        <div class="col-sm-12 socail-btn">
                            <a href="https://www.vaperhour.com/user/facebookAuth" class="facebook ctm-btn">Register With
                                Facebook</a>
                            <a href="https://www.vaperhour.com/user/twitterAuth" class="tw ctm-btn">Register With
                                Twitter</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg login-model signup-model" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Register</h5>
            </div>
            <div class="modal-body">
                <form id="registration-form" action="https://www.vaperhour.com/user/register" method="post">
                    <div class="form-group row">
                        <div class="col-sm-12 ctm-input full-name">
                            <input type="text" name="first_name" class="form-control" placeholder="First Name">
                            <input type="text" name="last_name" class="form-control" placeholder="Last Name">
                        </div>
                        <!--                  <div class="col-sm-12 ctm-input">-->
                        <!--                     <select class="form-control" name="type">-->
                        <!--                        <option value="buyer">Buyer</option>-->
                        <!--                        <option value="seller">Seller</option>-->
                        <!--                     </select>-->
                        <!--                  </div>-->
                        <div class="col-sm-12 ctm-input">
                            <input type="text" name="username" class="form-control" placeholder="Enter Username">
                        </div>
                        <div class="col-sm-12 ctm-input">
                            <input type="email" name="email" class="form-control" placeholder="Your Email">
                        </div>
                        <div class="col-sm-12 ctm-input">
                            <input type="password" name="password" class="form-control" id="inputPassword"
                                   placeholder="Password">
                        </div>
                        <div class="col-sm-12 cm-error"></div>
                        <div class="col-sm-12 cm-success"></div>
                        <div class="col-sm-12 ctm-login-btn">
                            <button type="submit" class="ctm-btn" id="register-button" data-type="register">Register
                                Now
                            </button>
                            <span class="ragister-now"><span>Already a Member </span><a href="#" data-toggle="modal"
                                                                                        data-target=".login-model"
                                                                                        data-dismiss="modal">Login in Here </a></span>
                        </div>
                        <div class="col-sm-12 socail-btn">
                            <a href="https://www.vaperhour.com/user/facebookAuth" class="facebook ctm-btn">Register With
                                Facebook</a>
                            <a href="https://www.vaperhour.com/user/twitterAuth" class="tw ctm-btn">Register With
                                Twitter</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg thankyou-model" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body customPadingModel">
                <div class="row">
                    <div class="col-md-12">
                        <div class="thankyou-content mauto">
                            <img src="https://www.vaperhour.com/assets/images/svg/check.svg" alt="" class="svg"
                                 height="65">
                            <h3>Thank you</h3>
                            <p>Email is sending<br>Please check your inbox</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="/assets/js/jquery-3.3.1.min.js"></script>
<script src="/assets/js/jquery-migrate-1.2.1.min.js"></script>
<script src="/assets/js/bootstrap.bundle.js"></script>
<script src="/assets/js/selectize.js"></script>
<script src="/assets/js/jquery.fancybox.min.js"></script>
<script src="/assets/js/jquery.counterup.min.js"></script>
<script src="/assets/js/imageuploadify.js"></script>
<script src="/assets/js/dropzone.min.js"></script>
<script src="/assets/js/checkout.js"></script>
<script src="/assets/js/slick.min.js"></script>
<script src="/assets/js/invoice.js"></script>
<script src="/assets/js/chart.bundle.min.js"></script>
<script src="/assets/js/dashboard.js"></script>
<script src="/assets/js/waypoint.js"></script>
<script src="/assets/js/pricing.js"></script>
<script src="/assets/js/jquery.validate.min.js"></script>
<script src="/assets/js/main.js"></script>


<script type="text/javascript">


    // $('.signin-model').modal('hide');

    // $('.signup-model').on('hidden', function () {
    //   $('.signin-model').modal('show')
    // });

</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.sponsor-logos').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 1000,
            arrows: false,
            dots: false,
            pauseOnHover: false,
            responsive: [{
                breakpoint: 768,
                settings: {
                    slidesToShow: 4
                }
            }, {
                breakpoint: 520,
                settings: {
                    slidesToShow: 3
                }
            }]
        });
    });
</script>
</body>
</html>