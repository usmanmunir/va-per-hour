<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 7/1/19
 * Time: 3:57 PM
 */
?>
@extends('layouts.admin.default')

{{-- Web site Title --}}
@section('page_title', 'Course Detail')
{{--@section('sub_title', 'Dashboard')--}}
{{-- Content --}}
@section('content')
    @include('admin.shared.flash-messages')
    <section class="content">

        <div class="row">
            <div class="col-xs-12">
                <div class="cbox">
                    <div class="main-title mbot30">
                        <h2 class="page-header font26">{{!empty($singleStint) ? $singleStint->title : ''}}</h2>
                    </div>

                    <div class="medium-title mbot30">
                        <h2 class="font23">1. Basic Details</h2>
                    </div>

                    <div class="form-box borderbottom pbot50">
                        <form action="/superaccess/stints/stint-status/{{$singleStint->id}}" method="post"
                              id="stintStatus">
                            @csrf
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Stint Title :</label>
                                        <input type="text" name="ad_stint_title"
                                               value="{{!empty($singleStint) ? $singleStint->title : ''}}"
                                               class="form-control input-lg" disabled="disabled">
                                    </div>
                                    <div class="form-group">
                                        <label>Stint Slug :</label>
                                        <input type="text" name="ad_stint_slug"
                                               value="{{!empty($singleStint) ? $singleStint->slug : ''}}"
                                               class="form-control input-lg" disabled="disabled">
                                    </div>

                                    <div class="form-group">
                                        <label>Stint Category Name :</label>
                                        <select class="form-control input-lg" name="ad_stint_category"
                                                disabled="disabled">
                                            <option value="{{!empty($singleStint) ? $singleStint->category->name : ''}}">{{!empty($singleStint) ? $singleStint->category->name : ''}}</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Stint Created Data :</label>
                                        <input type="text" name="ad_stint_createdate"
                                               value=" {{!empty($singleStint) ? date("F j, Y, g:i a", strtotime($singleStint->created_at)) : ''}}"
                                               class="form-control input-lg" disabled="disabled">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Stint Created by : <a href="#">(View
                                                user details)</a></label>
                                        <input type="text" name="ad_stint_createby"
                                               value="{{!empty($singleStint) ? $singleStint->user->first_name:''}}{{!empty($singleStint)? $singleStint->user->last_name : ''}}"
                                               class="form-control input-lg" disabled="disabled">
                                    </div>

                                    <div class="form-group">
                                        <label>Stint Status :</label>
                                        <input type="text" name="ad_stint_status"
                                               value="{{$singleStint->status  == '1' ?  'Active' : 'Draft' }}"
                                               class="form-control input-lg" disabled="disabled">
                                    </div>

                                    <div class="form-group">
                                        <label>Stint Description :</label>
                                        <textarea name="ad_stint_desc" id="ad_stint_desc" rows="3"
                                                  class="form-control input-lg"
                                                  placeholder="Enter stint description"
                                                  disabled="disabled">{!! !empty($singleStint) ? $singleStint->description : '' !!}</textarea>
                                    </div>

                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <input type="hidden" value="{{$singleStint->id}}" name="stintId">
                                    <div class="input-group-btn1 pull-left">
                                        <select name="status" class="form-control">
                                            <option value="1" selected="">Active</option>
                                            <option value="0">Draft</option>
                                            <option value="2">Delete</option>
                                            <option value="3">is_home</option>

                                        </select>
                                    </div>
                                    <div class="input-group-btn1 pull-right">
                                        <button type="submit" class="btn bg-orange btn-flat margin btn-lg">Update
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>


                    <div class="medium-title mbot30 ptop50">
                        <h2 class="font23">2. Stint Packages</h2>
                    </div>


                    <div class="row">
                        <div class="border">
                            <div class="col-md-3 pd0 stint-form">
                                <div class="form-group borderbottom input-lg bradius0 height6035">
                                    <label>Package Name</label>
                                </div>
                                <div class="form-group ad_package_desc_box borderbottom input-lg bradius0">
                                    <label>Package Description</label>
                                </div>

                                <div class="form-group borderbottom input-lg bradius0 height6035">
                                    <label>Package Delivery Time</label>
                                </div>

                                <div class="form-group borderbottom input-lg bradius0 height6035">
                                    <label>Package Revisions</label>
                                </div>

                                <div class="form-group borderbottom input-lg bradius0 height6035">
                                    <label>Has Extra Fast Option</label>
                                </div>

                                <div class="form-group borderbottom input-lg bradius0 height6035">
                                    <label>Extra Fast Day</label>
                                </div>

                                <div class="form-group borderbottom input-lg bradius0 height6035">
                                    <label>Extra Fast Amount</label>
                                </div>

                                <div class="form-group borderbottom input-lg bradius0 height6035 btbottom0">
                                    <label>Package Status</label>
                                </div>
                            </div>
                            <?php for($p = 0 ; $p < count($singleStint['price']); $p++){ ?>
                            @if(!empty($singleStint['price'][$p]->price) && !empty($singleStint['price'][$p]->title)  )
                                <div class="col-md-3 pd0 stint-form">
                                    <div class="form-group">
                                        <input type="text" name="stint_pack_name" id="stint_pack_name"
                                               class="form-control input-lg botopleft height60"
                                               value="{{$singleStint->price[$p]->title}}"
                                               placeholder="Enter stint package name" disabled="disabled">
                                    </div>
                                    <div class="form-group">
                                        <textarea name="stint_pack_desc" id="stint_pack_desc" rows="5"
                                                  class="form-control input-lg autonone botopleft"
                                                  placeholder="Enter stint package name"
                                                  disabled="disabled">{{$singleStint->price[$p]->description}}</textarea>
                                    </div>

                                    <div class="form-group">

                                        <input type="text" class="form-control input-lg botopleft height60"
                                               value="{{$singleStint->price[$p]->delivery_time}}"
                                               placeholder="Enter stint package name" disabled="disabled">
                                    </div>

                                    <div class="form-group">
                                        <input type="text" class="form-control input-lg botopleft height60"
                                               value="{{$singleStint->price[$p]->revisions}}"
                                               placeholder="Enter stint package name"
                                               disabled="disabled">

                                    </div>

                                    <div class="form-group text-center custcheck">
                                        <div class="icheckbox_square-blue checked disabled" aria-checked="false"
                                             aria-disabled="true" style="position: relative;"><input type="checkbox"
                                                                                                     checked=""
                                                                                                     class="minimal input-lg botopleft"
                                                                                                     name="option_extra"
                                                                                                     id="option_extra"
                                                                                                     disabled="disabled"
                                                                                                     style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;">
                                            <ins class="iCheck-helper"
                                                 style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <input type="text" class="form-control input-lg botopleft height60" value="2"
                                               placeholder="Enter stint package name" disabled="disabled">
                                    </div>

                                    <div class="form-group">
                                        <input type="text" class="form-control input-lg botopleft height60" value="30"
                                               placeholder="Enter stint package name" disabled="disabled">
                                    </div>


                                    <div class="form-group">
                                        <input type="text" class="form-control input-lg botopleft height60"
                                               value="{{$singleStint->price[$p]->status}}"
                                               placeholder="Enter stint package name"
                                               disabled="disabled">
                                    </div>
                                </div>
                            @endif
                            <?php } ?>
                            <div class="clearfix"></div>
                        </div>
                    </div>


                    <div class="medium-title mbot30  ptop50">
                        <h2 class="font23">3. Stint Requirements</h2>
                    </div>

                    <div class="faq-box greybg">
                        @if(count($singleStint['requirement']) > 0)
                            <?php for($r = 0 ; $r < count($singleStint['requirement']); $r++){ ?>
                            <div class="form-group faqtext">
                                <label>Requirement : {{$r == 0 ? '1' : $i || $r == 1 ? '1' : $i}} </label>
                                <textarea name="stint_requiement" id="stint_requiement" rows="3"
                                          class="form-control input-lg" placeholder="Enter stint package name"
                                          disabled="disabled">{{$singleStint['requirement'][$r]->description}}</textarea>
                            </div>
                            <?php } ?>
                        @else
                            <p>No Reuirement added by user</p>
                        @endif
                    </div>

                    <div class="medium-title mbot30 ptop50">
                        <h2 class="font23">4. Stint FAQs</h2>
                    </div>
                    @if(count($singleStint['faq']) > 0)
                        <?php for($f = 0 ; $f < count($singleStint['faq']); $f++){ ?>
                        <div class="faq-box greybg">
                            <div class="form-group faqtext">

                                <input type="text" name="stint_faq_que" id="stint_faq_que"
                                       class="form-control input-lg height60 btbottom0 fmedium"
                                       placeholder="Enter stint package name"
                                       value="{{$singleStint['faq'][$f]->question}}"
                                       disabled="disabled">

                                <textarea name="stint_faq_ans" id="stint_faq_ans" rows="3" class="form-control input-lg"
                                          placeholder="Enter stint package name"
                                          disabled="disabled">{{$singleStint['faq'][$f]->answer}}</textarea>
                            </div>
                            <?php } ?>
                            @else
                                <p>No Question is added by user</p>
                            @endif
                        </div>


                </div>
            </div>
        </div>

    </section>


@endsection
@push('scripts')
    @include('admin.stints.stint-js')
@endpush