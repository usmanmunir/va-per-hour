<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 6/25/19
 * Time: 6:33 PM
 */
?>
<script>
    $(function () {
        $("#name").blur(function () {
            $("#slug").val(($(this).val().split(' ').join('-')).toLowerCase());
        });
        $("#category-toggle").click(function () {
            $(".box-add-category").slideToggle();
        });

        $("#hidefilter").click(function () {
            $("#showfilter").slideToggle();
        });
        @if(!empty($categoryToEdit->id) || !empty($errors->toArray()))
        $("#category-toggle").trigger('click');
        @endif
        $('input[type="checkbox"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        })
    });
    var frm = $('#stintStatus');

    frm.submit(function (e) {

        e.preventDefault();

        $.ajax({
            type: frm.attr('method'),
            url: frm.attr('action'),
            data: frm.serialize(),
            success: function (res) {
                $('.alert alert-success').show();
                $('.alert alert-success').html(res.message);
            },
            error: function (data) {
                //alert("inside error");

            },
        });
    });
</script>

