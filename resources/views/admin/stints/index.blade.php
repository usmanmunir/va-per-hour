<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 6/28/19
 * Time: 3:50 PM
 */
?>
@extends('layouts.admin.default')

{{-- Web site Title --}}
@section('page_title', 'Stint')
@section('sub_title', 'Listing')
{{-- Content --}}
@section('filters_and_form')
    @include('admin.stints.filter_and_forms')
@endsection
{{-- Content --}}
@section('content')
    @include('admin.shared.flash-messages')
    <div class="row">
        <div class="col-sm-12">
            <div class="table-responsive">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th width="2%">
                            <input type="checkbox" id="checked_all_checkbox">
                        </th>
                        <th width="10%">Name</th>
                        <th width="30%">Title</th>
                        <th width="10%">Categories</th>
                        <th width="8%">Date</th>
                        <th width="6%">Is Home</th>
                        <th width="4%">Status</th>
                        <th width="7%">Action</th>

                    </tr>

                    </thead>
                    <tbody>
                    @if(count($stints) > 0)
                        @foreach($stints as $stint)
                            {{--                            {{$stint->user->first_name}}--}}
                            <?php //exit(); ?>
                            <tr>
                                <td>
                                    <input type="checkbox" class="category_ids" value="65" name="stint_ids[]">
                                </td>
                                <td>
                                    <a href="http://vaperhour.local/superaccess/user/detail/13">{{$stint->user->first_name}}{{$stint->user->last_name}}</a>
                                </td>
                                <td class="font17">
                                    <a href="/superaccess/stints/stint-detail/{{$stint->id}}">{{$stint->title}}</a>

                                    <br><a href="/stint/stint-detail/{{$stint->title}}/{{$stint->user->first_name}}/{{$stint->user->id}}"
                                           class="font14"
                                           target="_blank">View</a>
                                </td>
                                <td>{{$stint->category->name}}</td>
                                <td>Created<br>
                                    {{date("F j, Y, g:i a", strtotime($stint->created_at))}}
                                </td>
                                <td></td>
                                <td>
                                    <a href="#" class="publish staus">{{$stint->status == 1 ? 'Active' : 'Delete'}}</a>
                                </td>
                                <td>
                                    &nbsp;
                                    <div class="delete_action1">
                                        <a href="#"
                                           onclick="return confirm( 'Are you sure to delete?' )"><i
                                                    class="fa fa-trash"></i></a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr class="alert alert-warning">
                            <td colspan="12">
                                Record not found
                            </td>
                        </tr>
                    @endif
                    </tbody>
                </table>

            </div>
        </div>
    </div>
    <div class="row">
        {{ $stints->render() }}
    </div>
@endsection

@push('scripts')
    @include('admin.stints.stint-js')
@endpush
