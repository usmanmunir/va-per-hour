<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 6/25/19
 * Time: 6:29 PM
 */
?>
<div class="row mb20">
    <div class="col-md-12">
        <button class="btn btn-primary btn-lg pull-left" type="button" id="hidefilter">Filters</button>
    </div>
</div>
<div class="whitebg hide1" id="showfilter" style="display: none;">
    <div class="row">
        <form action="" method="get">
            <div class="col-md-2 col-sm-2 col-xs-2">
                <div class="form-group">
                    <input type="text" name="stintname" class="form-control" placeholder="Stint Name" value="">
                </div>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-2">
                <div class="form-group">
                    <input type="text" name="username" class="form-control" placeholder="User Name" value="">
                </div>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-2">
                <div class="form-group">
                    <select class="form-control select2" style="width: 100%;" id="category_id" name="category_id"
                            tabindex="-1" aria-hidden="true">
                        <option selected="selected" value="">Select Category</option>
                        @foreach($stintCategories as $stintCategory)
                            <option value="{{$stintCategory->id}}">{{$stintCategory->name}}</option>
                        @endforeach

                    </select>
                </div>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-2">
                <div class="form-group">
                    <select name="status" id="status" class="form-control">
                        <option value="">Status</option>
                        <option value="Publish">Publish</option>
                        <option value="Draft">Draft</option>
                    </select>
                </div>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-2">
                <div class="form-group">
                    <input type="date" name="created_at" id="created_at" class="form-control" value="">
                </div>
            </div>

            <div class="col-md-2 col-sm-2 col-xs-2">
                <div class="input-group-btn1 pull-right filter_btn">
                    <button type="reset" class="btn bg-navy btn-flat margin btn-md w90">Clear</button>
                </div>
                <div class="input-group-btn1 pull-right filter_btn">
                    <button type="submit" name="filter" value="filter" class="btn bg-orange btn-flat margin btn-md w90">
                        Search
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
