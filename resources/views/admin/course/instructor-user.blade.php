<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 6/28/19
 * Time: 5:50 PM
 */
?>
@extends('layouts.admin.default')

{{-- Web site Title --}}
@section('page_title', 'Course')
@section('sub_title', 'Dashboard')
@section('filters_and_form')
    {{--@include('admin.marketplace.filter_and_forms')--}}
@endsection
{{-- Content --}}
@section('content')
    @include('admin.shared.flash-messages')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th width="2%">
                                <input type="checkbox" id="checked_all_checkbox">
                            </th>

                            <th width="25%">Name</th>
                            <th width="10%">Email</th>
                            <th width="10%">Date</th>
                            <th width="10%">Action</th>
                        </tr>

                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="12"> Record Not found</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    {{--    @include('admin.marketplace.marketplace-js')--}}
@endpush

