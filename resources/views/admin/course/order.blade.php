<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 6/28/19
 * Time: 5:38 PM
 */
?>
@extends('layouts.admin.default')

{{-- Web site Title --}}
@section('page_title', 'Course')
@section('sub_title', 'Dashboard')
@section('filters_and_form')
    {{--@include('admin.marketplace.filter_and_forms')--}}
@endsection
{{-- Content --}}
@section('content')
    @include('admin.shared.flash-messages')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th width="5%">
                                <input type="checkbox" id="checked_all_checkbox">
                            </th>
                            <th width="30%">Name</th>
                            <th width="15%">Type</th>
                            <th width="10%">Total</th>
                            <th width="15%">User Name</th>
                            <th width="10%">Created Date</th>
                            <th width="7%">Status</th>
                            <th width="8%">Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        <tr>
                            <td>
                                <input type="checkbox" class="category_ids" value="85" name="o_ids[]">
                            </td>
                            <td>
                                <b>
                                    Online Marketing </b>
                                <br>
                                <a href="http://vaperhour.local/superaccess/order/order-view/85"
                                   target="_blank">View</a>
                            </td>
                            <td>
                                <span>course</span>
                            </td>
                            <td class="font17">
                                <span>$195</span>
                            </td>
                            <td>
                                Bhavesh Jadav
                            </td>

                            <td>
                                17th April 2019
                            </td>

                            <td class="text-center">
                                <a href="#" class="draft staus">Paid</a>
                            </td>

                            <td>
                                <div class="edit_action1">
                                    <a href="http://vaperhour.local/superaccess/order-list/delete-order/85"><i
                                                class="fa fa-trash"
                                                onclick="return confirm('are you sure to delete ?');"></i></a>
                                </div>
                                &nbsp;
                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    {{--    @include('admin.marketplace.marketplace-js')--}}
@endpush

