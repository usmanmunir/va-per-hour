<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 6/28/19
 * Time: 3:50 PM
 */
?>
@extends('layouts.admin.default')

{{-- Web site Title --}}
@section('page_title', 'Course')
@section('sub_title', 'Dashboard')
{{-- Content --}}
@section('filters_and_form')
    @include('admin.course.filter_and_forms')
@endsection
{{-- Content --}}
@section('content')
    @include('admin.shared.flash-messages')
    <div class="row">
        <div class="col-sm-12">
            <div class="table-responsive">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th width="2%">
                            <input type="checkbox" id="checked_all_checkbox">
                        </th>
                        <th width="8%"><img src="http://vaperhour.local/assets/admin/dist/img/image.png"
                                            class="img img-responsive center-img"></th>
                        <th width="25%">Title</th>
                        <th width="10%">Instructor</th>
                        <th width="15%">Categories</th>
                        <th width="5%">Price</th>
                        <th width="10%">Date</th>
                        <th width="10%">Views</th>
                        <th width="7%">Status</th>
                        <th width="10%">Action</th>
                    </tr>

                    </thead>
                    <tbody>
                    @if(count($cources) > 0)
                        @foreach($cources as $cource)
                            <tr>
                                <td>
                                    <input type="checkbox" class="category_ids" value="14" name="course_ids[]">

                                </td>
                                <td><a href="#"><img
                                                src="{{$cource->image}}"
                                                class="img img-responsive center-img" width="100" heigh="100px"></a>
                                </td>
                                <td class="font17">

                                    <a href="#">{{$cource->name}}</a>
                                    <br><a href="course/course-edit/{{$cource->id}}"
                                           class="font14">View</a>
                                </td>
                                <td><a href="#">{{$cource['user']->first_name}}&nbsp;{{$cource['user']->last_name}}</a>
                                </td>
                                <td><a href="#">{{$cource['category']->name}}</a></td>
                                <td><a href="#">{{$cource->price}}</a></td>
                                <td>Published<br>{{$cource->created_at}}</td>
                                <td><b>0</b> &nbsp;Views</td>
                                <td class="text-center">
                                    <a href="#" class="btn btn-warning">{{$cource->status}}</a>
                                </td>
                                <td>
                                    <!--						<div class="edit_action1">-->
                                    <!--						 <a href="-->
                                    <!--"><i class="fa fa-edit"></i></a>-->
                                    <!--						</div>-->
                                    &nbsp;
                                    <div class="delete_action1">
                                        <a href="http://vaperhour.local/superaccess/course/course_delete/14"
                                           onclick="return confirm('are you sure to delete ?');"><i
                                                    class="fa fa-trash"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr class="alert alert-warning">
                            <td colspan="12">
                                Record not found
                            </td>
                        </tr>
                    @endif
                    </tbody>
                </table>

            </div>
        </div>
    </div>
    <div class="row">
        {!! $cources->render() !!}
    </div>
@endsection

@push('scripts')
    @include('admin.course.course-js')
@endpush
