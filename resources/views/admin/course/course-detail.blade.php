<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 7/1/19
 * Time: 3:57 PM
 */
?>
@extends('layouts.admin.default')

{{-- Web site Title --}}
@section('page_title', 'Course Detail')
{{--@section('sub_title', 'Dashboard')--}}
{{-- Content --}}
@section('content')
    @include('admin.shared.flash-messages')
    <section class="content">
        <div class="row adcourseside">
            <div class="col-xs-12">
                <div class="cbox">
                    <div class="main-title mbot30">
                        <h2 class="page-header font26">
                            {{$singleCourse->name}} </h2>
                    </div>

                    <form method="post" action="#"
                          enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="box box-solid bxshaodw">
                                    <div class="box-body">
                                        <img src="{{$singleCourse->image}}"
                                             width="100%">

                                        <!--<video width="100%" controls>
                                             <source src="mov_bbb.mp4" type="video/mp4">
                                         </video>-->
                                    </div>
                                </div>

                                <!-- /.box -->

                                <div class="features-theme mtop50">
                                    <!-- Custom Tabs -->
                                    <div class="nav-tabs-custom">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="#tab_1" data-toggle="tab">Item Details</a></li>
                                            <!--                                    <li><a href="#tab_2" data-toggle="tab">Tab 2</a></li>-->
                                            <!--                                    <li><a href="#tab_3" data-toggle="tab">Tab 3</a></li>-->
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab_1">
                                                {!! $singleCourse->description !!}
                                            </div>
                                            <!-- /.tab-pane -->
                                            <!--                                    <div class="tab-pane" id="tab_2">-->
                                            <!---->
                                            <!--                                    </div>-->
                                            <!--                                    <div class="tab-pane" id="tab_3">-->
                                            <!---->
                                            <!--                                    </div>-->
                                            <!-- /.tab-pane -->
                                        </div>
                                        <!-- /.tab-content -->
                                    </div>
                                    <!-- nav-tabs-custom -->
                                </div>

                            </div>

                            <div class="col-md-4">

                                <div class="course_sidebar box box-primary">
                                    <div class="chapter-box">
                                        <h2 style="color: #000;">chapter 1 </h2>

                                        <a href="#">

                                            <h3>Quiz</h3>

                                        </a>

                                    </div>
                                </div>


                                <div class="gray-box">
                                    <div class="meta-attributes">
                                        <table>
                                            <tr>
                                                <td class="meta-attributes__attr-name">Price</td>
                                                <td class="meta-attributes__attr-detail">
                                                    {{$singleCourse->price}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="meta-attributes__attr-name">Total Review</td>
                                                <td class="meta-attributes__attr-detail">
                                                    0
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="meta-attributes__attr-name">Average Review</td>
                                                <td class="meta-attributes__attr-detail">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="meta-attributes__attr-name">Upload Date</td>
                                                <td class="meta-attributes__attr-detail">
                                                    {{$singleCourse->created_at}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="meta-attributes__attr-name">Category</td>
                                                <td class="meta-attributes__attr-detail">
                                                    {{$singleCourse['category']->name}}
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>

                                <div class="box box-primary mtop30">
                                    <div class="box-body box-profile">


                                        <img class="profile-user-img img-responsive img-circle"
                                             src="{{$singleCourse['user']->avatar}}"
                                             alt="User profile picture">

                                        <h3 class="profile-username text-center">
                                            {{$singleCourse['user']->first_name}}{{$singleCourse['user']->last_name}} </h3>

                                        <a href="#" class="btn btn-primary btn-block w150auto"><b>View Profile</b></a>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Change Status of item :</label>
                                    <select class="form-control input-lg" name="status">
                                        <option value="draft" selected>draft</option>

                                        <option value="publish">publish</option>

                                    </select>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="input-group-btn1 pull-right">
                                            <button type="reset" class="btn bg-navy btn-flat margin btn-lg">Clear
                                            </button>
                                        </div>

                                        <div class="input-group-btn1 pull-right">
                                            <button type="submit" class="btn bg-orange btn-flat margin btn-lg">Update
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

