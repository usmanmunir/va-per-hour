<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 6/28/19
 * Time: 3:24 PM
 */
?>
@extends('layouts.admin.default')

{{-- Web site Title --}}
@section('page_title', 'Course')
@section('sub_title', 'Dashboard')
{{-- Content --}}
@section('content')
    @include('admin.shared.flash-messages')
    <section class="content">
        <!-- Info boxes -->
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-empire"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Course</span>
                        <span class="info-box-number">13</span>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="fa fa-user-o"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Auther</span>
                        <span class="info-box-number">6</span>
                    </div>
                </div>
            </div>

            <div class="clearfix visible-sm-block"></div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Sales</span>
                        <span class="info-box-number">15</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">New Members</span>
                        <span class="info-box-number">17</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->


        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <div class="col-md-8">

                <!-- TABLE: LATEST ORDERS -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Latest Orders</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                        class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table no-margin">
                                <thead>
                                <tr>
                                    <th>Order ID</th>
                                    <th>Item</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td><a href="#">#vaph85</a></td>
                                    <td>Online Marketing</td>
                                    <td>
                                        <span class="label label-success">Paid</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="#">#vaph84</a></td>
                                    <td>CSS3 - Tutorial</td>
                                    <td>
                                        <span class="label label-success">Paid</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="#">#vaph63</a></td>
                                    <td>Online Marketing</td>
                                    <td>
                                        <span class="label label-success">Paid</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="#">#vaph59</a></td>
                                    <td>Online Marketing</td>
                                    <td>
                                        <span class="label label-success">Paid</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="#">#vaph48</a></td>
                                    <td>HTML5</td>
                                    <td>
                                        <span class="label label-success">Paid</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="#">#vaph43</a></td>
                                    <td>HTML</td>
                                    <td>
                                        <span class="label label-success">Paid</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="#">#vaph38</a></td>
                                    <td>Digital Marketing</td>
                                    <td>
                                        <span class="label label-success">Paid</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="#">#vaph24</a></td>
                                    <td>CSS3 - Tutorial</td>
                                    <td>
                                        <span class="label label-success">Paid</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="#">#vaph23</a></td>
                                    <td>HTML5</td>
                                    <td>
                                        <span class="label label-success">Paid</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="#">#vaph16</a></td>
                                    <td>Online Marketing</td>
                                    <td>
                                        <span class="label label-success">Unknown</span>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix">
                        <a href="#" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a>
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->

            <div class="col-md-4">

                <!-- USERS LIST -->
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Latest Members</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                        class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <ul class="users-list clearfix">

                        </ul>
                        <!-- /.users-list -->
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-center">
                        <a href="#" class="uppercase">View All Users</a>
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!--/.box -->

                <!-- PRODUCT LIST -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Recently Added Products</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                        class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">

                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-center">
                        <a href="#" class="uppercase">View All Products</a>
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
    </div>

@endsection