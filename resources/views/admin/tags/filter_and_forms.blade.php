<div class="row mb20">
    <div class="col-md-12">
        <button class="btn btn-primary btn-lg pull-left" type="button" id="hidefilter">Filters</button>
        <button class="btn btn-primary btn-lg pull-right" type="button" id="tag-toggle">Add New Tag</button>
    </div>
</div>
<div class="whitebg hide1" id="showfilter" style="display: none;">
    <div class="row">
        <form action="" method="get">
            <div class="col-md-2 col-sm-2 col-xs-2">
                <div class="form-group">
                    <input type="text" name="search" class="form-control" placeholder="Search" value="">
                </div>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-2">
                <div class="form-group">
                    <select name="type" id="type" class="form-control" placeholder="Tag Type">
                        <option value="">Select Tag Type</option>
                        <option value="stint">Stint</option>
                        <option value="course">Course</option>
                        <option value="marketplace">Marketplace</option>
                    </select>
                </div>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-2">
                <div class="input-group-btn1 pull-right filter_btn">
                    <button type="reset" class="btn bg-navy btn-flat margin btn-md w90" onclick="window.location='/superaccess/tags'">Clear</button>
                </div>
                <div class="input-group-btn1 pull-right filter_btn">
                    <button type="submit" name="filter" value="filter" class="btn bg-orange btn-flat margin btn-md w90">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>