<script>
    $(function () {
        $("#name").blur(function(){
           $("#slug").val(($(this).val().split(' ').join('-')).toLowerCase());
        });
        $("#tag-toggle").click(function () {
            $(".box-add-tag").slideToggle();
        });

        $("#hidefilter").click(function () {
            $("#showfilter").slideToggle();
        });
        @if(!empty($tagToEdit->id) || !empty($errors->toArray()))
                $("#tag-toggle").trigger('click');
        @endif
        $('input[type="checkbox"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass   : 'iradio_minimal-blue'
        })
    });
</script>
