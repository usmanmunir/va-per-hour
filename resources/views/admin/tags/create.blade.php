<div class="row ">
    <div class="col-md-12">
        <div class="admin_error_box">
            <?php
            if($errors) {
                $errors = $errors->toArray();
            }
            ?>
        </div>

        <div class="box box-primary hide1 box-add-tag mbot50">
            <form method="post" action="{{ empty($tagToEdit) ? action('Admin\TagsController@store') : action('Admin\TagsController@update',[$tagToEdit->id])}}" enctype="multipart/form-data">

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tag Title</label>
                                <input type="text" class="form-control" name="name" id="name" placeholder="Enter Title" value="{{!empty($tagToEdit->name) ? $tagToEdit->name : '' }}">

                                <p id="name-error" class="normal_error">{{!empty($errors['name']) ? $errors['name'][0] : ''}}</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tag Type</label>
                                <select name="type" id="type" class="form-control">
                                    <option value="stint">Stint</option>
                                    <option value="course">Course</option>
                                    <option value="marketplace">Marketplace</option>
                                </select>
                                <p id="slug-error" class="normal_error">{{!empty($errors['type']) ? $errors['type'][0] : ''}}</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1" style="display: block;">&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                @if(!empty($tagToEdit))
                                    <input type="hidden" name="_method" value="PUT">
                                @endif
                                <button type="submit" class="btn btn-primary" id="addcatform-submit">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>