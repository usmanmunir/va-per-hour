@extends('layouts.admin.default')

{{-- Web site Title --}}
@section('page_title', 'Tags')
@section('sub_title', 'list')

@section('filters_and_form')
    @include('admin.tags.filter_and_forms')
@endsection

{{-- Content --}}
@section('content')
    @include('admin.shared.flash-messages')
    @include('admin.tags.create')
    <div class="row">
        <div class="col-sm-12">
            <div class="table-responsive">
                <table class="table table-striped table-hover">
                    <thead>
                    <th><input type="checkbox" id="checkall" class="minimal" /></th>
                    <th>Tag</th>
                    <th>Type</th>
                    <th>Status</th>
                    <th>Action</th>
                    </thead>
                    <tbody>
                    @foreach ($tags as $tag)
                        <tr>
                            <td><input type="checkbox" class="minimal" class="select-item" /></td>
                            <td>{{$tag->name}}</td>
                            <td>{{$tag->type}}</td>
                            <td>
                                <span class="btn btn-{{$tag->status == 1 ? 'success' : 'danger'}}">{{$tag->status == 1 ? 'active' : 'Disable'}}</span>
                            </td>
                            <td>
                                <a href="{{action('Admin\TagsController@edit', [$tag->id])}}" class="btn btn-primary btn-success"><i class="fa fa-pencil"></i></a>
                                <form style="display: inline;" action="{{action('Admin\TagsController@destroy', [$tag->id])}}" method="post" onsubmit="return confirm('Are you sure to delete this record')">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button class="btn btn-primary btn-danger"><i class="fa fa-trash"></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        {!! $tags->render() !!}
    </div>
@endsection

@push('scripts')
    @include('admin.tags.tag-js')
@endpush
