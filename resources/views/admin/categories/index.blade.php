@extends('layouts.admin.default')

{{-- Web site Title --}}
@section('page_title', 'Categories')
@section('sub_title', 'list')

@section('filters_and_form')
    @include('admin.categories.filter_and_forms')
@endsection

{{-- Content --}}
@section('content')
    @include('admin.shared.flash-messages')
    @include('admin.categories.create')
    <div class="row">
        <div class="col-sm-12">
            <div class="table-responsive">
                <table class="table table-striped table-hover">
                    <thead>
                    <th><input type="checkbox" id="checkall" class="minimal" /></th>
                    <th>Thumbnail</th>
                    <th>Category title</th>
                    <th>Parent</th>
                    <th>Is featured | Menu | Home</th>
                    <th>Category Slug</th>
                    <th>Category Type</th>
                    <th>Status</th>
                    <th>Action</th>
                    </thead>
                    <tbody>
                    @foreach ($categories as $category)
                        <tr>
                            <td><input type="checkbox" class="minimal" class="select-item" /></td>
                            <td><img src="/{{$category->image}}" width="100"></td>
                            <td>{{$category->name}}</td>
                            <td>{{$category->parent}}</td>
                            <td class="options">
                                <span>{{$category->is_featured == 1 ? 'Featured |' : ''}}</span>
                                <span>{{$category->put_in_menu == 1 ? 'In Menu |' : ''}}</span>
                                <span>{{$category->put_in_home == 1 ? 'Is Home' : ''}}</span>
                            </td>
                            <td>
                                {{$category->slug}}
                            </td>
                            <td>
                                {{$category->category_type}}
                            </td>
                            <td>
                                <span class="btn btn-{{$category->status == 1 ? 'success' : 'danger'}}">{{$category->status == 1 ? 'active' : 'Disable'}}</span>
                            </td>
                            <td>
                                <a href="{{action('Admin\CategoriesController@edit', [$category->id])}}" class="btn btn-primary btn-success"><i class="fa fa-pencil"></i></a>
                                <form style="display: inline;" action="{{action('Admin\CategoriesController@destroy', [$category->id])}}" method="post" onsubmit="return confirm('Are you sure to delete this record')">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button class="btn btn-primary btn-danger"><i class="fa fa-trash"></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        {!! $categories->render() !!}
    </div>
@endsection

@push('scripts')
    @include('admin.categories.categories-js')
@endpush
