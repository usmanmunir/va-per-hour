<div class="row ">
    <div class="col-md-12">
        <div class="admin_error_box">
            <?php
            if($errors) {
                $errors = $errors->toArray();
            }
            ?>
        </div>

        <div class="box box-primary hide1 box-add-category mbot50">
            <form method="post" action="{{ empty($categoryToEdit) ? action('Admin\CategoriesController@store') : action('Admin\CategoriesController@update',[$categoryToEdit->id])}}" enctype="multipart/form-data">

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Category Name</label>
                                <input type="text" class="form-control" name="name" id="name" placeholder="Enter Category" value="{{!empty($categoryToEdit->name) ? $categoryToEdit->name : '' }}">

                                <p id="name-error" class="normal_error">{{!empty($errors['name']) ? $errors['name'][0] : ''}}</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Category Slug</label>
                                <input type="text" class="form-control" name="slug" id="slug" placeholder="Enter Category Slug" value="{{!empty($categoryToEdit->slug) ? $categoryToEdit->slug : '' }}">
                                <p id="slug-error" class="normal_error">{{!empty($errors['slug']) ? $errors['slug'][0] : ''}}</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Select Parent Category</label>
                                <select class="form-control" name="parent" id="parent">
                                    <option value="0">Select Category</option>
                                    @foreach($cats as $cat)
                                        <option value="{{$cat->id}}" {{!empty($categoryToEdit->parent) && $categoryToEdit->parent == $cat->id ? 'selected="selected"' : '' }}>{{$cat->name}}</option>
                                    @endforeach
                                </select>
                                <p class="normal_error">{{!empty($errors['parent']) ? $errors['parent'][0] : ''}}</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Category Type</label>
                                <select class="form-control" name="category_type" id="category_type">
                                    <option value="" selected="selected">Category Type</option>
                                    <option value="stint" {{!empty($categoryToEdit->category_type) && $categoryToEdit->category_type == 'stint'? 'selected="selected"' : '' }}>stint</option>
                                    <option value="course" {{!empty($categoryToEdit->category_type) && $categoryToEdit->category_type == 'course' ? 'selected="selected"' : '' }}>course</option>
                                    <option value="marketplace" {{!empty($categoryToEdit->category_type) && $categoryToEdit->category_type == 'marketplace' ? 'selected="selected"' : '' }}>marketplace</option>
                                    <option value="settings" {{!empty($categoryToEdit->category_type) && $categoryToEdit->category_type == 'settings' ? 'selected="selected"' : '' }}>Settings</option>
                                </select>
                                <p id="category_type-error" class="normal_error">{{!empty($errors['category_type']) ? $errors['category_type'][0] : ''}}</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Category Image</label>
                                <input type="file" class="form-control" name="image" id="image" placeholder="Enter Category Slug">
                                @if(!empty($categoryToEdit->image))
                                <p>
                                    <img src="{{!empty($categoryToEdit->image) ? '/'.$categoryToEdit->image : ''}}" width="60"/>
                                </p>
                                @endif
                                <p id="image-error" class="normal_error">{{!empty($errors['image']) ? $errors['image'][0] : ''}}</p>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Category Short Description</label>
                                <textarea class="form-control" rows="3" name="description" id="description"> {{!empty($categoryToEdit->description) ? $categoryToEdit->description : '' }}</textarea>
                            </div>
                            <p id="description-error" class="normal_error">{{!empty($errors['description']) ? $errors['description'][0] : ''}}</p>
                        </div>


                        <div class="col-md-12">

                            <div class="form-group">
                                <label> <input type="checkbox" class="minimal" name="is_featured" id="is_featured" value="1" {{!empty($categoryToEdit->is_featured) ? 'checked' : '' }} /> Featured category </label>
                            </div>

                            <div class="form-group">
                                <label> <input type="checkbox" class="minimal" name="put_in_menu" id="put_in_menu" value="1" {{!empty($categoryToEdit->put_in_menu) ? 'checked' : '' }}/> Put it in menu </label>
                            </div>

                            <div class="form-group">
                                <label> <input type="checkbox" class="minimal" name="put_in_home" id="put_in_home" value="1" {{!empty($categoryToEdit->put_in_home) ? 'checked' : '' }}/> Put it in Home </label>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="box-footer">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    @if(!empty($categoryToEdit))
                        <input type="hidden" name="_method" value="PUT">
                    @endif
                    <button type="submit" class="btn btn-primary" id="addcatform-submit">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>