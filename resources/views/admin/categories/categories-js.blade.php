<script>
    $(function () {
        $("#name").blur(function(){
           $("#slug").val(($(this).val().split(' ').join('-')).toLowerCase());
        });
        $("#category-toggle").click(function () {
            $(".box-add-category").slideToggle();
        });

        $("#hidefilter").click(function () {
            $("#showfilter").slideToggle();
        });
        @if(!empty($categoryToEdit->id) || !empty($errors->toArray()))
                $("#category-toggle").trigger('click');
        @endif
        $('input[type="checkbox"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass   : 'iradio_minimal-blue'
        })
    });
</script>
