<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 6/25/19
 * Time: 8:04 PM
 */
?>
@extends('layouts.admin.default')

{{-- Web site Title --}}
@section('page_title', 'Marketplace Prdouct Details')
{{--@section('sub_title', 'Dashboard')--}}
{{-- Content --}}
@section('content')
    @include('admin.shared.flash-messages')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="cbox">
                    <div class="main-title mbot30">
                        <h2 class="page-header font26">{{$singleMarketplaceProduct->name}}</h2>
                    </div>

                    <form action="#" method="post">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="box box-solid bxshaodw">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                            <div class="carousel-inner">
                                                <div class="item active">
                                                    <img
                                                            src="{{$singleMarketplaceProduct->thumbnail_image}}"
                                                            alt="First slide">

                                                    <div class="carousel-caption">
                                                        First Slide
                                                    </div>
                                                </div>
                                            </div>
                                            <a class="left carousel-control"
                                               href=""
                                               data-slide="prev">
                                                <span class="fa fa-angle-left"></span>
                                            </a>
                                            <a class="right carousel-control"
                                               href=""
                                               data-slide="next">
                                                <span class="fa fa-angle-right"></span>
                                            </a>
                                        </div>
                                    </div>
                                    <!-- /.box-body -->
                                </div>

                                <div class="features-theme mtop50">
                                    <div class="nav-tabs-custom">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a
                                                        href="http://vaperhour.local/superaccess/marketplace/view/23#tab_1"
                                                        data-toggle="tab">Item Details</a></li>

                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab_1">
                                                {!! $singleMarketplaceProduct->description !!}
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="col-md-4">

                                <div class="zip-download">
                                    <a href="{{$singleMarketplaceProduct->mainfile}}"
                                       class="download btn btn-block btn-success btn-lg" download="">Download Zip
                                        File</a>
                                </div>

                                <div class="adpriceing-info">
                                    <label class="price-label">{{$singleMarketplaceProduct->regular_license}}
                                        - {{$singleMarketplaceProduct->extended_license}}</label>
                                </div>

                                <div class="price-list">
                                    <ul>
                                        <li>Regular price - $
                                            {{$singleMarketplaceProduct->regular_license}}
                                        </li>
                                        <li>
                                            single user license - $
                                            5
                                        </li>
                                        <li>
                                            two user license - $
                                            5
                                        </li>
                                        <li>
                                            multi user license - $
                                            5
                                        </li>
                                    </ul>
                                </div>

                                <div class="gray-box mtop30">
                                    <div class="meta-attributes">
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td class="meta-attributes__attr-name">Upload Date</td>
                                                <td class="meta-attributes__attr-detail">
                                                    {{$singleMarketplaceProduct->created_at}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="meta-attributes__attr-name">Category</td>
                                                <td class="meta-attributes__attr-detail">
                                                    {{$singleMarketplaceProduct['category']->name}}
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- Profile Image -->
                                <div class="box box-primary mtop30">
                                    <div class="box-body box-profile bgf1">
                                        <img class="profile-user-img img-responsive img-circle"
                                             src="{{$singleMarketplaceProduct['user'][0]->avatar}}"
                                             alt="User profile picture">

                                        <h3 class="profile-username text-center">{{$singleMarketplaceProduct['user'][0]->first_name}}
                                            &nbsp;
                                            {{$singleMarketplaceProduct['user'][0]->last_name}}
                                        </h3>

                                        <a href="#"
                                           class="btn btn-primary btn-block w150auto"><b>View Profile</b></a>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                                <!-- /.box -->

                                <div class="form-group">
                                    <label>Change Status of item :</label>
                                    <input type="hidden" name="mp_id" value="23">
                                    <select class="form-control input-lg" name="action">
                                        <option value="draft" selected="">draft</option>
                                        <option value="publish">publish</option>
                                    </select>
                                </div>

                                <div class="submit-wrap">
                                    <div class="input-group-btn1 pull-right w100">
                                        <button type="submit" class="download1 btn btn-block bg-orange btn-lg">Save
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </form>

                </div>
            </div>
        </div>
    </section>
@endsection
