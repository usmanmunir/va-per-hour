<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 6/25/19
 * Time: 6:29 PM
 */
?>
<div class="row mb20">
    <div class="col-md-12">
        <button class="btn btn-primary btn-lg pull-left" type="button" id="hidefilter">Filters</button>
    </div>
</div>
<div class="whitebg hide1" id="showfilter" style="display: none;">
    <div class="row">
        <form action="" method="get">
            <div class="col-md-2 col-sm-2 col-xs-2">
                <div class="form-group">
                    <input type="text" name="search" class="form-control" placeholder="Search" value="">
                </div>
            </div>

            <div class="col-md-3 col-sm-3 col-xs-3">
                <div class="form-group">
                    <select class="form-control select2" style="width: 100%;" id="category_id" name="category_id"
                            tabindex="-1" aria-hidden="true">
                        <option selected="selected" value="">Select Category</option>
                        @foreach($marketplaceCategories as $marketplaceCategory)
                            <option value="{{$marketplaceCategory->id}}">{{$marketplaceCategory->name}}</option>
                        @endforeach

                    </select>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-3">
                <div class="form-group">
                    <select class="form-control select2" style="width: 100%;" id="user_id" name="user_id"
                            tabindex="-1" aria-hidden="true">
                        <option selected="selected" value="">Select User</option>
                        @foreach($users as $user)
                            <option value="{{$user->id}}">{{$user->first_name}}&nbsp;{{$user->last_name}}</option>
                        @endforeach

                    </select>
                </div>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-2">
                <div class="form-group">
                    <select name="status" id="status" class="form-control">
                        <option value="">Status</option>
                        <option value="1">Publish</option>
                        <option value="0">Draft</option>
                    </select>
                </div>
            </div>


            <div class="col-md-2 col-sm-2 col-xs-2">
                <div class="input-group-btn1 pull-right filter_btn">
                    <button type="reset" class="btn bg-navy btn-flat margin btn-md w90">Clear</button>
                </div>
                <div class="input-group-btn1 pull-right filter_btn">
                    <button type="submit" name="filter" value="filter" class="btn bg-orange btn-flat margin btn-md w90">
                        Search
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
