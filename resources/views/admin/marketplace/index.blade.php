<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 6/25/19
 * Time: 4:33 PM
 */
?>
@extends('layouts.admin.default')

{{-- Web site Title --}}
@section('page_title', 'Marketplace')
@section('sub_title', 'Dashboard')
@section('filters_and_form')
    @include('admin.marketplace.filter_and_forms')
@endsection
{{-- Content --}}
@section('content')
    @include('admin.shared.flash-messages')
    <div class="row">
        <div class="col-sm-12">
            <div class="table-responsive">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>
                            <input type="checkbox" id="checked_all_checkbox">
                        </th>
                        <th width="8%">
                            <img src="http://vaperhour.local/assets/admin/dist/img/image.png"
                                 class="img img-responsive center-img">
                        </th>
                        <th width="30%">Name</th>
                        <th width="15%">User Name</th>
                        <th width="10%">Categories</th>
                        <th width="8%">Regular Price</th>
                        <th width="8%">Extended Price</th>
                        <th width="8%">Is Home</th>
                        <th width="7%">Status</th>
                        <th width="8%">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($marketplaceProducts as $marketplaceProduct)
                        <tr>
                            <td>
                                <input type="checkbox" class="category_ids" value="19" name="mp_ids[]">
                            </td>
                            <td>
                                <a href=""><img
                                            src="{{$marketplaceProduct->thumbnail_image}}"
                                            height="60"></a>

                            </td>
                            <td class="font17">
                                <a href="">{{$marketplaceProduct->name}}</a>
                                <br><a href="/superaccess/marketplace/view/{{$marketplaceProduct->id}}" class="font14">View</a>
                            </td>

                            <td>{{$marketplaceProduct['user'][0]->first_name}}</td>

                            <td>{{$marketplaceProduct['category']->name}}</td>

                            <td>{{$marketplaceProduct->regular_license}}</td>
                            <td>{{$marketplaceProduct->extended_license}}</td>
                            <td></td>

                            <td class="text-center">
                                <a href="#" class="publish staus">{{$marketplaceProduct->status}}</a>

                            </td>
                            <td class="action_center">
                                <div class="delete_action1">
                                    <a href=""><i class="fa fa-trash"
                                                  onclick="return confirm('are you sure to delete ?');"></i></a>
                                </div>
                                &nbsp;
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="row">
            {!! $marketplaceProducts->render() !!}
        </div>
    </div>
@endsection

@push('scripts')
    @include('admin.marketplace.marketplace-js')
@endpush

