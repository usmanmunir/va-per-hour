<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 6/25/19
 * Time: 3:16 PM
 */

?>
@extends('layouts.admin.default')

{{-- Web site Title --}}
@section('page_title', 'Marketplace')
@section('sub_title', 'Dashboard')
{{-- Content --}}
@section('content')
    @include('admin.shared.flash-messages')
    <section class="content">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-empire"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Marketplace Products</span>
                        <span class="info-box-number">{{$totalMarketplaceProducts}}</span>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="fa fa-user-o"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Merchants</span>
                        <span class="info-box-number">8</span>
                    </div>
                </div>
            </div>

            <div class="clearfix visible-sm-block"></div>

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Sales</span>
                        <span class="info-box-number">21</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">New Members</span>
                        <span class="info-box-number">17</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- Main row -->
        <div class="row">

            <div class="col-md-8">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Latest Orders</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                        class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table no-margin">
                                <thead>
                                <tr>
                                    <th>Order ID</th>
                                    <th>Item</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td><a href="#">#VAPH87</a>
                                    </td>
                                    <td>PET - Responsive Email Template</td>
                                    <td>
                                        <span class="label label-success">Paid</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="#">#VAPH73</a>
                                    </td>
                                    <td>Siena - Marketing Landing Page Template</td>
                                    <td>
                                        <span class="label label-success">Paid</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="#">#VAPH73</a>
                                    </td>
                                    <td>Arrosa - Creative Multipurpose WordPress Theme</td>
                                    <td>
                                        <span class="label label-success">Paid</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="#">#VAPH72</a>
                                    </td>
                                    <td>PET - Responsive Email Template</td>
                                    <td>
                                        <span class="label label-success">Paid</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="#">#VAPH71</a>
                                    </td>
                                    <td>Nexos - Real Estate Agency Directory</td>
                                    <td>
                                        <span class="label label-success">Paid</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="#">#VAPH60</a>
                                    </td>
                                    <td>casaRoyal - Real Estate WordPress Theme</td>
                                    <td>
                                        <span class="label label-success">Paid</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="#">#VAPH45</a>
                                    </td>
                                    <td>PET - Responsive Email Template</td>
                                    <td>
                                        <span class="label label-success">Paid</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="#">#VAPH44</a>
                                    </td>
                                    <td>Siena - Marketing Landing Page Template</td>
                                    <td>
                                        <span class="label label-success">Paid</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="#">#VAPH44</a>
                                    </td>
                                    <td>Homey - Booking and Rentals WordPress Theme</td>
                                    <td>
                                        <span class="label label-success">Paid</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="#">#VAPH42</a>
                                    </td>
                                    <td>Siena - Marketing Landing Page Template</td>
                                    <td>
                                        <span class="label label-success">Paid</span>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="box-footer clearfix">
                        <a href="#"
                           class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a>
                    </div>
                </div>
            </div>

            <div class="col-md-4">

                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Latest Members</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                        class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body no-padding">
                        <ul class="users-list clearfix">
                            @foreach($latestMembers as $latestMember)
                                <li>
                                    <img
                                            src="{{$latestMember->avatar}}"
                                            alt="User Image">
                                    <a class="users-list-name"
                                       href="#">{{$latestMember->first_name}}{{$latestMember->last_name}}</a>
                                    <span class="users-list-date">{{$latestMember->created_at}}</span>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="box-footer text-center">
                        <a href="#" class="uppercase">View All
                            Users</a>
                    </div>
                </div>


                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Recently Added Products</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                        class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <ul class="products-list product-list-in-box">
                            @foreach($latestMarketplaceProducts as $latestMarketplaceProduct)
                                <li class="item">
                                    <div class="product-img">
                                        <img
                                                src="{{$latestMarketplaceProduct->thumbnail_image}}"
                                                alt="Product Image">
                                    </div>
                                    <div class="product-info">
                                        <a href="javascript:void(0)"
                                           class="product-title">{{$latestMarketplaceProduct->name}} </a>
                                        <span class="label label-warning pull-right">{{$latestMarketplaceProduct->regular_license}}</span>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-center">
                        <a href="#" class="uppercase">View All Products</a>
                    </div>
                    <!-- /.box-footer -->
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection

