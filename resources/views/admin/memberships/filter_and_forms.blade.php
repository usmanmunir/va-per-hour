<div class="row mb20">
    <div class="col-md-12">
        <button class="btn btn-primary btn-lg pull-left" type="button" id="hidefilter">Filters</button>
        <button class="btn btn-primary btn-lg pull-right" type="button" id="" onclick="window.location='{{action('Admin\MembershipController@create')}}'">Add New Membership</button>
    </div>
</div>
<div class="whitebg hide1" id="showfilter" style="display: none;">
    <div class="row">
        <form action="" method="get">
            <div class="col-md-2 col-sm-2 col-xs-2">
                <div class="form-group">
                    <input type="text" name="search" class="form-control" placeholder="Search" value="">
                </div>
            </div>

            <div class="col-md-2 col-sm-2 col-xs-2">
                <div class="form-group">
                    <select class="form-control select2" style="width: 100%;" id="category_id" name="id" tabindex="-1" aria-hidden="true">
                        <option value="">Member for</option>
                        @foreach($membershipFor as $key => $value)
                            <option value="{{$key}}}">{{$value}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-2">
                <div class="form-group">
                    <select name="user_type" id="" class="form-control">
                        <option value="">User Type</option>
                        @foreach($userTypes as $key => $value)
                            <option value="{{$key}}}">{{$value}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-2">
                <div class="form-group">
                    <select name="status" id="status" class="form-control">
                        <option value="">Status</option>
                        <option value="1">Publish</option>
                        <option value="0">Disable</option>
                    </select>
                </div>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-2">
                <div class="input-group-btn1 pull-right filter_btn">
                    <button type="reset" class="btn bg-navy btn-flat margin btn-md w90">Clear</button>
                </div>
                <div class="input-group-btn1 pull-right filter_btn">
                    <button type="submit" name="filter" value="filter" class="btn bg-orange btn-flat margin btn-md w90">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>