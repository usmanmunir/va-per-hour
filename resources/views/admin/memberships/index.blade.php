@extends('layouts.admin.default')

{{-- Web site Title --}}
@section('page_title', 'Memberships')
@section('sub_title', 'list')
<?php
    $membershipFor=config('constants.membership_for');
    $userTypes = \Config::get('constants.user_type');
?>
@section('filters_and_form')
    @include('admin.memberships.filter_and_forms', ['membershipFor' => $membershipFor, 'userTypes' => $userTypes])
@endsection

{{-- Content --}}
@section('content')
    @include('admin.shared.flash-messages')
    <div class="row">
        <div class="col-sm-12">
            <div class="table-responsive">
                <table class="table table-striped table-hover">
                    <thead>
                    <th><input type="checkbox" id="checkall" class="minimal" /></th>
                    <th>Name</th>
                    <th>Month</th>
                    <th>Price</th>
                    <th>User Type</th>
                    <th>For User</th>
                    <th>Status</th>
                    <th>Action</th>
                    </thead>
                    <tbody>
                    @if($memberships)
                        @foreach ($memberships as $membership)
                        <tr>
                            <td><input type="checkbox" class="minimal" class="select-item" /></td>
                            <td>{{$membership->title}}</td>
                            <td>{{$membership->month}}</td>
                            <td>{{$membership->price}}</td>
                            <td>{{ !empty($userTypes[$membership->user_type]) ? $userTypes[$membership->user_type] : '' }}</td>
                            <td>{{!empty($membershipFor[$membership->membership_for]) ? $membershipFor[$membership->membership_for] : ''}}</td>
                            <td>{{$membership->status}}</td>
                            <td>
                                <a href="{{action('Admin\MembershipController@edit', [$membership->id])}}" class="btn btn-primary btn-success"><i class="fa fa-pencil"></i></a>
                                <form style="display: inline;" action="{{action('Admin\MembershipController@destroy', [$membership->id])}}" method="post" onsubmit="return confirm('Are you sure to delete this record')">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button class="btn btn-primary btn-danger"><i class="fa fa-trash"></i></button>
                                </form>
                            </td>

                        </tr>
                    @endforeach
                    @else
                        <tr>
                            <td colspan="8" style="text-align:left;">No Record Found</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        {!! $memberships->render() !!}
    </div>
@endsection

@push('scripts')
    @include('admin.memberships.membership-js')
@endpush
