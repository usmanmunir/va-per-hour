<script src="/admin/dist/js/jquery.validate.min.js" type="application/javascript"></script>
<script>
    $(function () {
        $("#hidefilter").click(function () {
            $("#showfilter").slideToggle();
        });
        $('input[type="checkbox"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass   : 'iradio_minimal-blue'
        });
        //bootstrap WYSIHTML5 - text editor
        $('.textarea').wysihtml5();

        $( document ).on( 'click', '.variation-toggle',  function(e){
            e.preventDefault();
            $(this).parents( '.variation-container:first' ).find( '.variation-elements' ).slideToggle();
        });

        $( '#add_variation' ).click( function(e){
            e.preventDefault();
            $( '.membership-variations' ).append( $( '#variation-html' ).html() );
            $('.membership-variations .textarea-desc:last').wysihtml5();
            return false;
        } );

        $(document).on( 'click', '.remove-variation',  function(e){
            update = $(this).attr('data-id');
            if(update=='update'){
                if(confirm ('Are you sure remove Membership Variations ?')){
                    var m_id = $(this).attr('id');
                    _this = $(this);
                    $.ajax({
                        type: "POST",
                        url: '/remove_variations',
                        data: {'m_id':m_id},
                        success: function( response ) {
                            if(response=='false'){
                                alert('!opps something is wrong');
                                return false;
                            }else{
                                $(_this).parents( '.variation-container:first' ).remove();
                            }
                        }
                    });
                }
                return;
            }
            $(this).parents( '.variation-container:first' ).remove();
        } );

        $("#membership-form").validate({
            rules: {
                membership_for: {required: true},
                title: {required: true},
                user_type: {required: true},
                price: {required: true, number: true},
                description: {required: true},
                month: {required: true},
                //number_of_stints_allowed_single_user:{required:true, number:true},
                //number_of_stints_allowed_agency_user:{required:true, number:true},
                //number_of_proposal_allowed:{required:true, number:true},
                //seller_contact_limit_agency_user:{required:true, number:true},
            },
            submitHandler: function(form) {
                form.submit();
            }
        })
    });
</script>
