@extends('layouts.admin.default')

{{-- Web site Title --}}
@section('page_title', 'Memberships')
@section('sub_title', 'Create')
@push('styles')
    <style>
        label.error {
            color: red;
            font-family: normal;
        }
    </style>
@endpush
<?php
$membershipFor=config('constants.membership_for');
$userTypes = \Config::get('constants.user_type');
?>
{{-- Content --}}
@section('content')
    @include('admin.shared.flash-messages')
    <div class="row">
        <div class="col-xs-12">
            <div class="cbox">
                <form id="membership-form"  method="post" action="{{ empty($membership) ? action('Admin\MembershipController@store') : action('Admin\MembershipController@update',[$membership->id])}}">
                    <div class="form-box pbot50">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Membership for  :</label>
                                    <select class="form-control input-lg" id="membership_for" name="membership_for">
                                        <option value="">Select option</option>
                                        @foreach($membershipFor as $key => $value)
                                            <option value="{{$key}}" {{!empty($membership->membership_for) && $membership->membership_for == $key ? 'selected="selected"' : ''}}>{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Name :</label>
                                    <input type="text" name="title" id="title" value="{{!empty($membership->title) ? $membership->title : ''}}" class="form-control input-lg">
                                </div>

                                <div class="form-group">
                                    <label>User type  :</label>
                                    <select class="form-control input-lg" name="user_type" id="user_type">
                                        <option value="" selected>Select option</option>
                                        @foreach($userTypes as $key => $value)
                                            <option value="{{$key}}" {{!empty($membership->user_type) &&  $membership->user_type == $key ? 'selected="selected"' : ''}}>{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group" style="margin-top: 31px;">
                                    <label>Price :</label>
                                    <input type="text" name="price" id="price" value="{{!empty($membership->price) ? $membership->price : ''}}" class="form-control input-lg" >
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group hidesome_textarea">
                                    <label>Description :</label>
                                    <textarea class="textarea-desc textarea" name="description" placeholder="Enter description" style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{!empty($membership->description) ? $membership->description : ''}}</textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Month :</label>
                                    <input type="text" name="month" id="month" value="{{!empty($membership->month) ? $membership->month : ''}}" class="form-control input-lg" >
                                </div>
                            </div>


                        </div>

                        <div class="membership-variations">
                            @if(!empty($membership->variations))
                                @foreach($membership->variations as $variation)
                                    <div class="variation-container">
                                            <div class="variation_title">
                                                <h2>Membership Variations</h2>
                                                <span class="variation-toggle">Hide/Show</span>
                                            </div>
                                            <div class="variation-elements">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Name :</label>
                                                        <input type="text" name="variations[name][]" value="{{!empty($variation->name) ? $variation->name : ''}}" class="form-control input-lg">
                                                    </div>

                                                    <div class="form-group">
                                                        <label>User type :</label>
                                                        <select class="form-control input-lg" name="variations[user_type][]" >
                                                            <option value="" selected>Select option</option>
                                                            @foreach($userTypes as $key => $value)
                                                                <option value="{{$key}}" {{!empty($variation->user_type) &&  $variation->user_type == $key ? 'selected="selected"' : ''}}>{{$value}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group hidesome_textarea">
                                                        <label>Description :</label>
                                                        <textarea class="textarea-desc textarea" name="variations[description][]" placeholder="Enter description" style="">{{!empty($variation->description) ? $variation->description : ''}}</textarea>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Month :</label>
                                                    <input type="text" name="variations[month][]"  value="{{!empty($variation->month) ? $variation->month : ''}}" class="form-control input-lg">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Price :</label>
                                                    <input type="text" name="variations[price][]"  value="{{!empty($variation->price) ? $variation->price : ''}}" class="form-control input-lg">
                                                </div>
                                            </div>
                                            <div class="form-group mright15">
                                                <button  type="button" class="btn btn-danger pull-right remove-variation mleft25" data-id="{{$variation->id}}">Remove Variation</button>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                @endforeach
                            @endif
                        </div>
                        <div class="form-group ">
                            <button type="button" class="btn btn-info pull-right" id="add_variation">Add New Variation</button>
                        </div>
                    </div>

                    <div class="main-title mbot30">
                        <h2 class="page-header font26">Setting options</h2>
                    </div>

                    <div class="row">
                        <div class="form-box borderbottom pbot50">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>View Course Details :</label>
                                        <input type="checkbox" name="can_view_course_details" value="1" {{!empty($membership->can_view_course_details) && $membership->can_view_course_details == 1 ? 'checked="checked"' : ''}} class="minimal input-lg" />
                                    </div>
                                    <div class="form-group">
                                        <label>View Product Details :</label>
                                        <input type="checkbox" name="can_view_product_details" value="1" {{!empty($membership->title) ? 'checked="checked"' : ''}} class="minimal input-lg" />
                                    </div>
                                    <div class="form-group">
                                        <label>Invite Friends :</label>
                                        <input type="checkbox" name="can_invite_friends" value="1" {{!empty($membership->can_invite_friends) && $membership->can_invite_friends == 1 ? 'checked="checked"' : ''}} class="minimal input-lg" />
                                    </div>
                                    <div class="form-group">
                                        <label>Visible In Seller Search Result :</label>
                                        <input type="checkbox" name="visible_in_seller_search" value="1" {{!empty($membership->visible_in_seller_search) && $membership->visible_in_seller_search == 1 ? 'checked="checked"' : ''}} class="minimal input-lg" />
                                    </div>
                                    <div class="form-group">
                                        <label>How many stints can be created ( Single User ) :</label>
                                        <input type="text" name="number_of_stints_allowed_single_user" value="{{!empty($membership->number_of_stints_allowed_single_user) ? $membership->number_of_stints_allowed_single_user : ''}}" class="form-control input-lg" />
                                    </div>
                                    <div class="form-group">
                                        <label>How many stints can be created ( Agency ) :</label>
                                        <input type="text" name="number_of_stints_allowed_agency_user" value="{{!empty($membership->number_of_stints_allowed_agency_user) ? $membership->number_of_stints_allowed_agency_user : ''}}" class="form-control input-lg" />
                                    </div>
                                    <div class="form-group">
                                        <label>Can contact Seller :</label>
                                        <input type="checkbox" name="can_contact_seller" value="1" {{!empty($membership->can_contact_seller) && $membership->can_contact_seller == 1 ? 'checked="checked"' : ''}} class="minimal input-lg" />
                                    </div>
                                    <div class="form-group">
                                        <label>Can Submit Proposal for bid :</label>
                                        <input type="checkbox" name="can_submit_proposals" value="1" {{!empty($membership->can_submit_proposals) && $membership->can_submit_proposals == 1 ? 'checked="checked"' : ''}} class="minimal input-lg" />
                                    </div>
                                    <div class="form-group">
                                        <label>Can Register to courses :</label>
                                        <input type="checkbox" name="can_register_to_course" value="1" {{!empty($membership->can_register_to_course) && $membership->can_register_to_course == 1 ? 'checked="checked"' : ''}} class="minimal input-lg" />
                                    </div>
                                    <div class="form-group">
                                        <label>Add Product to Market place :</label>
                                        <input type="checkbox" name="can_add_product_to_market_place" value="1" {{!empty($membership->can_add_product_to_market_place) && $membership->can_add_product_to_market_place == 1 ? 'checked="checked"' : ''}} class="minimal input-lg" />
                                    </div>
                                    <div class="form-group">
                                        <label>Eligible for cash rewards ( Through Affiliate system ) :</label>
                                        <input type="checkbox" name="eligible_for_cash_reward" value="1" {{!empty($membership->eligible_for_cash_reward) && $membership->eligible_for_cash_reward == 1 ? 'checked="checked"' : ''}} class="minimal input-lg" />
                                    </div>
                                    <div class="form-group">
                                        <label>How many proposal can be sumited per week :</label>
                                        <input type="text" name="number_of_proposal_allowed" value="{{!empty($membership->number_of_proposal_allowed) ? $membership->number_of_proposal_allowed : ''}}" class="form-control input-lg" />
                                    </div>
                                    <div class="form-group">
                                        <label>Can Purchase marketplace product :</label>
                                        <input type="checkbox" name="can_purchase_product" value="1" {{!empty($membership->title) && $membership->can_purchase_product == 1 ? 'checked="checked"' : ''}} class="minimal input-lg" />
                                    </div>
                                    <div class="form-group">
                                        <label>Can submit bid project request :</label>
                                        <input type="checkbox" name="can_submit_bid" value="1" {{!empty($membership->can_submit_bid) && $membership->can_submit_bid == 1 ? 'checked="checked"' : ''}} class="minimal input-lg" />
                                    </div>
                                    <div class="form-group">
                                        <label>Can Create Course :</label>
                                        <input type="checkbox" name="can_create_course" value="1" {{!empty($membership->can_create_course) && $membership->can_create_course == 1 ? 'checked="checked"' : ''}} class="minimal input-lg" />
                                    </div>
                                    <div class="form-group">
                                        <label>Can Order Stint :</label>
                                        <input type="checkbox" name="can_order_stint" value="1" {{!empty($membership->can_order_stint) && $membership->can_order_stint == 1 ? 'checked="checked"' : ''}} class="minimal input-lg" />
                                    </div>
                                    <div class="form-group">
                                        <label>Seller contact limit through stint proposal ( per day - Single User ) :</label>
                                        <input type="text" name="seller_contact_limit_single_user" value="{{!empty($membership->seller_contact_limit_single_user) ? $membership->seller_contact_limit_single_user : ''}}" class="form-control input-lg" />
                                    </div>
                                    <div class="form-group">
                                        <label>Seller contact limit through stint proposal ( per day - Agency ) :</label>
                                        <input type="text" name="seller_contact_limit_agency_user" value="{{!empty($membership->seller_contact_limit_agency_user) ? $membership->seller_contact_limit_agency_user : ''}}" class="form-control input-lg" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 pd0">
                            <div class="input-group-btn1 pull-left mtop20">
                                <input type="hidden" name="_token" value="{{csrf_token()}}" />
                                @if(!empty($membership))
                                    <input type="hidden" name="_method" value="PUT" />
                                @endif
                                <button type="submit" class="btn bg-navy btn-flat margin btn-lg">Save Membership</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-sm-12" style="display: none" id="variation-html">
        <div class="variation-container">
            <div class="variation_title">
                <h2>Membership Variations</h2>
                <span class="variation-toggle">Hide/Show</span>
            </div>
            <div class="variation-elements">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Name :</label>
                        <input type="text" name="variations[name][]" value="" class="form-control input-lg">
                    </div>

                    <div class="form-group">
                        <label>User type :</label>
                        <select class="form-control input-lg" name="variations[user_type][]" >
                            <option value="" selected>Select option</option>
                            @foreach($userTypes as $key => $value)
                                <option value="{{$key}}" {{!empty($membership->user_type) &&  $membership->user_type == $key ? 'selected="selected"' : ''}}>{{$value}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group hidesome_textarea">
                        <label>Description :</label>
                        <textarea class="textarea-desc textarea" name="variations[description][]" placeholder="Enter description" style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                    </div>
                </div>

            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Month :</label>
                    <input type="text" name="variations[month][]"  value="" class="form-control input-lg">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Price :</label>
                    <input type="text" name="variations[price][]"  value="" class="form-control input-lg">
                </div>
            </div>
            <div class="form-group mright15">
                <button  type="button" class="btn btn-danger pull-right remove-variation mleft25" >Remove Variation</button>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

@endsection

@push('scripts')
    @include('admin.memberships.membership-js')
@endpush