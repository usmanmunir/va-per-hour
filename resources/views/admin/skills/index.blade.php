@extends('layouts.admin.default')

{{-- Web site Title --}}
@section('page_title', 'Skills')
@section('sub_title', 'list')

@section('filters_and_form')
    @include('admin.skills.filter_and_forms')
@endsection

{{-- Content --}}
@section('content')
    @include('admin.shared.flash-messages')
    @include('admin.skills.create')
    <div class="row">
        <div class="col-sm-12">
            <div class="table-responsive">
                <table class="table table-striped table-hover">
                    <thead>
                    <th><input type="checkbox" id="checkall" class="minimal" /></th>
                    <th>Title</th>
                    <th>slug</th>
                    <th>Status</th>
                    <th>Action</th>
                    </thead>
                    <tbody>
                    @foreach ($skills as $skill)
                        <tr>
                            <td><input type="checkbox" class="minimal" class="select-item" /></td>
                            <td>{{$skill->title}}</td>
                            <td>{{$skill->slug}}</td>
                            <td>
                                <span class="btn btn-{{$skill->status == 1 ? 'success' : 'danger'}}">{{$skill->status == 1 ? 'active' : 'Disable'}}</span>
                            </td>
                            <td>
                                <a href="{{action('Admin\SkillsController@edit', [$skill->id])}}" class="btn btn-primary btn-success"><i class="fa fa-pencil"></i></a>
                                <form style="display: inline;" action="{{action('Admin\SkillsController@destroy', [$skill->id])}}" method="post" onsubmit="return confirm('Are you sure to delete this record')">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button class="btn btn-primary btn-danger"><i class="fa fa-trash"></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        {!! $skills->render() !!}
    </div>
@endsection

@push('scripts')
    @include('admin.skills.skills-js')
@endpush
