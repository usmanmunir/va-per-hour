<div class="row ">
    <div class="col-md-12">
        <div class="admin_error_box">
            <?php
            if($errors) {
                $errors = $errors->toArray();
            }
            ?>
        </div>

        <div class="box box-primary hide1 box-add-skill mbot50">
            <form method="post" action="{{ empty($skillToEdit) ? action('Admin\SkillsController@store') : action('Admin\SkillsController@update',[$skillToEdit->id])}}" enctype="multipart/form-data">

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Skill Title</label>
                                <input type="text" class="form-control" name="title" id="title" placeholder="Enter Title" value="{{!empty($skillToEdit->title) ? $skillToEdit->title : '' }}">

                                <p id="name-error" class="normal_error">{{!empty($errors['title']) ? $errors['title'][0] : ''}}</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Category Slug</label>
                                <input type="text" class="form-control" name="slug" id="slug" placeholder="Enter Skill Slug" value="{{!empty($skillToEdit->slug) ? $skillToEdit->slug : '' }}">
                                <p id="slug-error" class="normal_error">{{!empty($errors['slug']) ? $errors['slug'][0] : ''}}</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1" style="display: block;">&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                @if(!empty($skillToEdit))
                                    <input type="hidden" name="_method" value="put">
                                @endif
                                <button type="submit" class="btn btn-primary" id="addcatform-submit">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>