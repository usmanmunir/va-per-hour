@extends('layouts.admin.default')

{{-- Web site Title --}}
@section('page_title', 'Permissions')
@section('sub_title', 'list')

{{-- Content --}}
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="table-responsive">
                <table class="table table-striped table-hover">
                    <thead>
                    <th>Id</th>
                    <th>Group</th>
                    <th>Title</th>
                    <th>Slug</th>
                    <th>Created At</th>
                    </thead>
                    <tbody>
                    <?php $groups = [
                        'users'=> 'User',
                        'groups'=> 'Group/Role',
                        ];
                    ?>
                    @foreach ($permissions as $permission)

                        <tr>
                            <td>{{$permission->id}}</td>
                            <td>{{$permission->title}}</td>
                            <td>{{$permission->title}}</td>
                            <td>{{$permission->slug}}</td>
                            <td>{{$permission->created_at}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        {!! $permissions->render() !!}
    </div>
@stop

