<html>
    <body>
        <div>
            <center><img src="/vaperhour.com/front/images/logo/logo-1.png" alt="VAPH"/></center>
        </div>
        <div>
            <p>
                Dear {{$recipient->first_name}} {{$recipient->first_name}},<br>
            </p>
            <p>
                {{$sender->first_name}} {{$sender->first_name}} sent you new message for <strong>{{$stint->title}}</strong>,
            </p>
            <p>
                {!! $messageBody !!}
            </p>
            <p>
            Thanks,<br>
            The VAPH Team
            </p>
        </div>
    </body>
</html>