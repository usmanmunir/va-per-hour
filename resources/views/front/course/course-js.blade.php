<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 5/23/19
 * Time: 5:07 AM
 */
?>
<script>
    $(function () {
        $("#fileUploadForm").validate({
            rules: {
                name: {required: true},
                category_id: {required: true},
                price: {required: true},
                file: {required: true},
                description: {required: true},
            },
            submitHandler: function (form, e) {
                e.preventDefault();
                for (instance in CKEDITOR.instances) {
                    CKEDITOR.instances.description.updateElement();
                }
                //then get the form data
                var fd = new FormData(form);
                $.ajax({
                    type: "post",
                    url: "{{ url('/elearning/course/save-course') }}",
                    dataType: "json",
                    "headers": {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
                    data: fd,
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function () {
                        //$("#preview").fadeOut();
                        $("#err").fadeOut();
                    },
                    success: function (data) {
                        var stringified = JSON.stringify(data);
                        var parsedObj = JSON.parse(stringified);
                        // alert(data.id);
                        toastr.success(data.message);
                        var courseId = parsedObj.data["id"];
                        window.location= '/elearning/course/'+courseId+'/chapters';
                    },
                    error: function (data) {
                        toastr.error("Something went wrong");
                    }
                });
            }
        });
        //Edit form Request
        $("#editfileUploadForm").validate({
            rules: {
                name: {required: true},
                category_id: {required: true},
                price: {required: true},
                description: {required: true},

            },
            submitHandler: function (form, e) {
                e.preventDefault();
                for (instance in CKEDITOR.instances) {
                    CKEDITOR.instances[instance].updateElement();
                }
                //then get the form data
                var fd = new FormData(form);
                $.ajax({
                    type: "post",
                    url: "{{ url('/elearning/course/update-course') }}",
                    dataType: "json",
                    "headers": {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
                    data: fd,
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function () {
                        //$("#preview").fadeOut();
                        $("#err").fadeOut();
                    },
                    success: function (data) {
                        toastr.success(data.message);
                    },
                    error: function (data) {
                        toastr.error("Something went wrong");
                    }
                });
            }
        });

        $(document).on("click", ".removeUserCourse", function (e) {

            e.preventDefault();

            var userCourseId = $(this).attr("coueseId");

            if (confirm("Are you want to sure?")) {

                deleteUserCourse(userCourseId);
            }

        });
        $(document).on("click", ".editUserCourse", function (e) {
            e.preventDefault();
            var currentRecord = $(this).closest('tr.course-row');
            var CourseId = currentRecord.data('record');
            window.location.href = "/elearning/course/edit/" + CourseId
        });

    });

    function deleteUserCourse(userCourseId) {
        $.ajax({
            type: "post",
            url: "/elearning/course/delete-user-course/" + userCourseId,
            dataType: "json",
            "headers": {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
            success: function (data) {
                toastr.success(data.message);
                window.location='';
            },
            error: function (data) {
                toastr.error("Something Went Wrong");
            }
        });
    }

    function getUserCourse() {
        var tableData = "";
        $.ajax({
            url: '/elearning/course/get-user-course',
            type: 'get',
            dataType: 'json',
            success: function (data) {
                console.log(data.data);
                if (data.length) {
                    $.each(data, function (key, val) {
                        console.log(val);
                        tableData += '<tr class="course-row" data-record="' + val.id + '">';
                        tableData += '<td data-id="' + val.id + '">' + val.id + '</td>';
                        tableData += '<td data-price="' + val.price + '">' + val.price + '</td>';
                        tableData += '<td data-name="' + val.name + '">' + val.name + '</td>';
                        tableData += '<td data-created="' + val.created_at + '">' + val.created_at + '</td>';
                        // tableData += '<td data-langid="' + val.language.id + '">' + val.language.id + '</td>';
                        tableData += '<td>';
                        tableData += '<a class="types course-model-open1" href="javascript:;">';
                        tableData += '<i class="fa fa-edit editUserCourse" style="color: #0e73e6 !important;"></i></a>';
                        tableData += '<a class="types" href="javascript:;"> ';
                        tableData += ' <i class="fa fa-trash removeUserCourse" style="color: #0e73e6 !important;" coueseId=' + val.id + '></i></a></td>';
                        tableData += '<td data-status="' + val.status + '">' + val.status + '</td>';
                        tableData += '</tr>';

                    })
                } else {
                    tableData += '<tr colspan=3>'
                    tableData += '<td>No Data Avaliable</td>';
                    tableData += '<tr>'
                }
                $("#userCourseTable tbody").html(tableData);
            }
        });
    }

    function getUserCourse() {
        var tableData = "";
        $.ajax({
            url: '/elearning/course/get-user-course',
            type: 'get',
            dataType: 'json',
            success: function (data) {
                console.log(data.data);
                if (data.length) {
                    $.each(data, function (key, val) {
                        console.log(val);
                        tableData += '<tr class="course-row" data-record="' + val.id + '">';
                        tableData += '<td data-id="' + val.id + '">' + val.id + '</td>';
                        tableData += '<td data-price="' + val.price + '">' + val.price + '</td>';
                        tableData += '<td data-name="' + val.name + '">' + val.name + '</td>';
                        tableData += '<td data-created="' + val.created_at + '">' + val.created_at + '</td>';
                        // tableData += '<td data-langid="' + val.language.id + '">' + val.language.id + '</td>';
                        tableData += '<td>';
                        tableData += '<a class="types course-model-open1" href="javascript:;">';
                        tableData += '<i class="fa fa-edit editUserCourse" style="color: #0e73e6 !important;"></i></a>';
                        tableData += '<a class="types" href="javascript:;"> ';
                        tableData += ' <i class="fa fa-trash removeUserCourse" style="color: #0e73e6 !important;" coueseId=' + val.id + '></i></a></td>';
                        tableData += '<td data-status="' + val.status + '">' + val.status + '</td>';
                        tableData += '</tr>';

                    })
                } else {
                    tableData += '<tr colspan=3>'
                    tableData += '<td>No Data Avaliable</td>';
                    tableData += '<tr>'
                }
                $("#userCourseTable tbody").html(tableData);
            }
        });
    }

    $('textarea.editor, textarea.ckeditor, #editor1').ckeditor();
    $('.add_course_button').click(function(){
        $("#course-box").append( $( '.course-new-chapter-html' ).html());
        $( 'textarea.editor, textarea.ckeditor' ).ckeditor();
    });
    $( '.course-step' ).click( function() {

        $('.course_tabs a[id="title-tab"]').tab('show');
        scroll();
        return false;
    } );
</script>
