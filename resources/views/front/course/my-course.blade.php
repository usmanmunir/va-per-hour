<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 5/22/19
 * Time: 3:06 PM
 */
?>
@extends('front.layouts.default')
@section('main_container')
    @push('style')
        <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
        @endpush
    <section class="ctn banner">
        <div class="main-banner">
            <div class="banner-img">
                <!-- <div class="overlay-img"></div> -->
                <img src="/front/images/post-offer.png">
            </div>
            <div class="banner-contant">
                <h2 class="banner-title-2"> My Courses </h2>
                <h5 class="banner-title-3">Courses being offered by me</h5>
            </div>
        </div>
    </section>
    <section class="offer-post section-1">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12 post-all-offers">
                    <div class="tab-content post-oofer-contant" id="myTabContent">
                        <div class="priority-contant tab-pane fade show active" id="priority" role="tabpanel"
                             aria-labelledby="priority-tab">
                            <div class="orders-format priority-orders" >
                                <div class="text-right">
                                    <a href="{{url('/elearning/course/add-course')}}" class="cmbtn cmbtn-sm mtop20imp mbot20imp">Create
                                        New</a>
                                </div>

                                <div class="table-responsive mtop20">
                                    <table class="table table-bordred" id="userCourseTable">
                                        <thead>
                                            <tr>
                                                <th width="5%">#</th>
                                                <th width="10%">Course Price</th>
                                                <th width="30%">Course Name</th>
                                                <th width="20%">Created</th>
                                                <th width="10%">Action</th>
                                                <th width="10%">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if($courses)
                                                @foreach($courses as $course)
                                                    <tr class="course-row" data-record="{{$course->id}}">
                                                        <td data-id="{{$course->id}}">{{$course->id}}</td>
                                                        <td data-price="{{$course->price}}">{{$course->price}}</td>
                                                        <td data-name="{{$course->name}}">{{$course->name}}</td>
                                                        <td data-created="{{$course->created_at}}">{{$course->created_at}}</td>
                                                        <td>
                                                            <a class="types course-model-open1" title="edit" href="/elearning/course/edit/{{$course->id}}">
                                                                <i class="fa fa-edit editUserCourse" style="color: #0e73e6 !important;"></i>
                                                            </a>
                                                            <a class="types" href="javascript:;" title="Remove it">
                                                                <i class="fa fa-trash removeUserCourse" style="color: #0e73e6 !important;" coueseId='{{$course->id}}'></i>
                                                            </a>
                                                            <a class="types" href="/elearning/course/{{$course->id}}/chapters" title="View Content">
                                                                <i class="fa fa-eye" style="color: #0e73e6 !important;" coueseId='{{$course->id}}'></i>
                                                            </a>
                                                        </td>
                                                        <td data-status="{{$course->status}}">{{$course->status}}</td>
                                                    </tr>
                                                @endforeach
                                             @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @push('scripts')
        @include('front.course.course-js')
    @endpush
@endsection
