@extends('front.layouts.default')
@section('main_container')
    @push('styles')
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css">
    <style>
        .dropzone {
            background: white;
            border-radius: 5px;
            border: 2px dashed rgb(0, 135, 247);
            border-image: none;
            max-width: 500px;
            margin-left: auto;
            margin-right: auto;
        }
        #fade {
            display: none;
            position: fixed;
            top: 0%;
            left: 0%;
            width: 100%;
            height: 100%;
            background-color: black;
            z-index: 1001;
            -moz-opacity: 0.8;
            opacity: .80;
            filter: alpha(opacity=80);
        }

        #light {
            display: none;
            position: absolute;
            top: 50%;
            left: 50%;
            max-width: 600px;
            max-height: 360px;
            margin-left: -300px;
            margin-top: -180px;
            border: 2px solid #FFF;
            background: #FFF;
            z-index: 1002;
            overflow: visible;
        }

        #boxclose {
            float: right;
            cursor: pointer;
            color: #fff;
            border: 1px solid #AEAEAE;
            border-radius: 3px;
            background: #222222;
            font-size: 31px;
            font-weight: bold;
            display: inline-block;
            line-height: 0px;
            padding: 11px 3px;
            position: absolute;
            right: 2px;
            top: 2px;
            z-index: 1002;
            opacity: 0.9;
        }

        .boxclose:before {
            content: "×";
        }
    </style>
    @endpush
    <style>
        .offer-post .post-all-offers .nav {
            position: static !important;
            top: 0px !important;
        }

        .offer-post .post-all-offers .nav .nav-item .nav-link {
            padding: 0px 15px 0px 10px !important;
        }
    </style>
    <?php
    $chapterType = config('constants.chapter_type');
    $chapterStatus = config('constants.course_status');
    ?>
    <section class="ctn banner">
        <div class="main-banner">
            <div class="banner-img">
                <img src="/front/images/post-offer.png">
            </div>
            <div class="banner-contant">
                <h2 class="banner-title-2"> Manage {{$chapter->chapter_name}} lectures</h2>
                <h5 class="banner-title-3">Manage Content of chapters</h5>
            </div>
        </div>
    </section>
    <section class="offer-post section-1 course_upload">

        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12 post-all-offers">
                    <ul class="nav nav-tabs course_tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link disabled" id="title-tab" data-toggle="tab" href="#title" role="tab"
                               aria-controls="title" aria-selected="true"><span class="badge badge-secondary">1</span>Course
                                Overview</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" id="pricing-tab" data-toggle="tab" href="#pricing" role="tab"
                               aria-controls="pricing" aria-selected="false"><span
                                        class="badge badge-secondary">2</span>Course content</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link disabled" id="requirment-tab" data-toggle="tab" href="#requirment"
                               role="tab" aria-controls="requirment" aria-selected="false"><span
                                        class="badge badge-secondary">3</span>Publish</a>
                        </li>
                    </ul>
                    <div class="tab-content post-oofer-contant" id="myTabContent">
                        <div class="stint-contant tab-pane fade show active pbot0" id="add-course" role="tabpanel"
                             aria-labelledby="title-tab">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="text-right">
                                        <a href="/elearning/course/my-course" class="cmbtn cmbtn-sm mtop20imp mbot20imp"><i class="fa fa-caret-left"></i> Back to Courses</a>
                                        <a href="/elearning/course/{{$chapter->course_id}}/chapters" class="cmbtn cmbtn-sm mtop20imp mbot20imp"><i class="fa fa-caret-left"></i> Back to Chapters</a>
                                        <a href="javascript:;" class="cmbtn cmbtn-sm mtop20imp mbot20imp" onclick="showLectureForm()"><i class="fa fa-plus"></i> Create New</a>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <table class="table table-striped">
                                        <tr>
                                            <td>id</td>
                                            <td>Chapter</td>
                                            <td>title</td>
                                            <td>video</td>
                                            <td>Action</td>
                                            <td>Status</td>
                                        </tr>
                                    @if(count($lectures) > 0 )
                                        @foreach($lectures as $lecture)
                                            <tr>
                                                <td>{{$lecture->id}}</td>
                                                <td>{{$chapter->chapter_name}}</td>
                                                <td>{{$lecture->title}}</td>
                                                <td>
                                                    <video width="320" height="240" controls onclick="lightbox_open();">
                                                        <source src="{{$lecture->video}}" type="video/mp4">
                                                    </video>
                                                    </td>
                                                <td>
                                                    <a href="javascript:;" class="anchor-primary" onclick="editLecture('{{json_encode($lecture)}}')"><i class="fa fa-2x fa-pencil"></i> </a>
                                                    <a href="javascript:;" class="anchor-primary" onclick="deleteLecture('{{$lecture->id}}')"><i class="fa fa-2x fa-trash"></i> </a>
                                                </td>
                                                <td>
                                                    {{$lecture->status}}
                                                </td>
                                            </tr>
                                                <div id="light">
                                                    <a class="boxclose" id="boxclose" onclick="lightbox_close();"></a>
                                                    <video id="lectureVideo" width="600" controls>
                                                        <source src="{{$lecture->video}}" type="video/mp4">
                                                        <!--Browser does not support <video> tag -->
                                                    </video>
                                                </div>

                                                <div id="fade" onClick="lightbox_close();"></div>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="5">
                                                <div class="alert alert-warning">No Lecture added so far</div>
                                            </td>
                                        </tr>
                                    @endif
                                    </table>
                                </div>
                            </div>
                            <div class="row display-none" id="add_lecture_form">
                                <div class="col-sm-12">
                                    <form method="POST" id="addLecutreForm" enctype="multipart/form-data" class="step2Form">
                                        @csrf
                                        <div class="summary1">
                                            <div class="course_error_step2 alert alert-danger" style="display: none;">
                                                <strong></strong></div>

                                            <div class="course_success_step2 alert alert-success" style="display: none;">
                                                <strong></strong></div>

                                            <div class="course_error_validation_step2 alert alert-danger"
                                                 style="display: none;"></div>

                                            <div class="head-title">
                                                <div id="course-box">
                                                    <div class="course-box">
                                                        <div class="textbox-title">
                                                            <h3>Add Lecture</h3>
                                                        </div>

                                                        <div class="cv_form_overview chapter-box">

                                                            <div class="row pbot20">
                                                                <div class="col-md-4 col-sm-4 col-xs-12 pd0">
                                                                    <div class="form-label">
                                                                        <span>Lecture Title: </span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-8 col-sm-8 col-xs-12">
                                                                    <div class="cvform-box">
                                                                        <input type="text" class="cv_text course_name" name="title" id="title" placeholder="Lecture Title" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row pbot20">
                                                                <div class="col-md-4 col-sm-4 col-xs-12">
                                                                    <div class="form-label">
                                                                        <span>Description: </span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-8 col-sm-8 col-xs-12">
                                                                    <div class="editor">
                                                                        <textarea rows="3" name="description" id="description"
                                                                          placeholder="Write Description here..."
                                                                          class="ckeditor"></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row pbot20 image-box">
                                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                                <div class="form-label">
                                                                    <span>Image/Video upload:</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                                <div class="dropzone" id="dropzone">
                                                                </div>
                                                            </div>
                                                                <input type="hidden" name="video" id="video">
                                                        </div>
                                                            <div class="row pbot20">
                                                                <div class="col-md-4 col-sm-4 col-xs-12 pd0">
                                                                    <div class="form-label">
                                                                        <span>Status: </span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-8 col-sm-8 col-xs-12">
                                                                    <div class="cvform-box">
                                                                        <select class="cv_text" name="status"
                                                                                id="status">
                                                                            @foreach($chapterStatus as $key => $value)
                                                                                <option value="{{$key}}">{{$value}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row pbot20">
                                                                <div class="col-sm-12">
                                                                    <input type="hidden" value="{{$course->id}}" name="course_id" id="course_id" />
                                                                    <input type="hidden" value="{{$chapter->id}}" name="chapter_id" id="chapter_id" />
                                                                    <input type="hidden" value="" name="id" id="id" />
                                                                    <span class="course-save-status"></span>
                                                                    <button type="submit"
                                                                            class="ctm-btn submit pull-right course_step2">Save
                                                                    </button>
                                                                </div>
                                                            </div>
                                                            <div class="chapter-quiz" style="display:none;">
                                                                <div class="row pbot20">
                                                                    <div class="col-md-4 col-sm-4 col-xs-12 pd0">
                                                                        <div class="form-label">
                                                                            <span>Add Quiz: </span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                                        <div class="cvform-box backnull">
                                                                            <a class="ctm-btn chapter-add-question"
                                                                               data-toggle="modal"
                                                                               data-target="#chapter-quiz-model">Click here
                                                                                to add Quiz question!</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>
    <script type="text/javascript">
        Dropzone.autoDiscover = false;

        $(document).ready(function () {
            $("#dropzone").dropzone({
                maxFiles: 2000,
                method:"POST",
                url:"{{ url('elearning/course/save-lecture-video') }}",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
                success: function (file, response) {
                    console.log(response);
                    $('#video').val(response);
                }
            });
        })
        Dropzone.options.dropzone =
            {
                maxFilesize: 500000000,
                renameFile: function(file) {
                    var dt = new Date();
                    var time = dt.getTime();
                    return time+file.name;
                },
                acceptedFiles: ".mp4,.gif",
                addRemoveLinks: true,
                timeout: 5000,
                success: function(file, response)
                {
                    console.log(response);
                },
                error: function(file, response)
                {
                    return false;
                }
            };
        window.document.onkeydown = function(e) {
            if (!e) {
                e = event;
            }
            if (e.keyCode == 27) {
                lightbox_close();
            }
        }

        function lightbox_open() {
            var lightBoxVideo = document.getElementById("lectureVideo");
            window.scrollTo(0, 0);
            document.getElementById('light').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
            lightBoxVideo.play();
        }

        function lightbox_close() {
            var lightBoxVideo = document.getElementById("lectureVideo");
            document.getElementById('light').style.display = 'none';
            document.getElementById('fade').style.display = 'none';
            lightBoxVideo.pause();
        }
    </script>
    @include('front.course.lecture.lecture-js')
@endpush


