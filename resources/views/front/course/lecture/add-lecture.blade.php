<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 5/30/19
 * Time: 4:14 PM
 */
?>

<script type="text" class="lecture-html">
<div class="lecture-box">
	<div class="textbox-title">
   <div class="textbox_wrap">
         <div class="form-label">
          <span>Add Lecture</span>
                    </div>
                  </div>
                   <div class="clearfix"></div>
                      </div>
 <form id="addLecutreForm" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="cv_form_overview chapter-box">

        <div class="row pbot20">
            <div class="col-md-4 col-sm-4 col-xs-12 pd0">
                <div class="form-label">
                    <span>Chapter: </span>
                </div>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-12">
                <div class="cvform-box">
                 <div class="cvform-box">
                    <input type="text" class="cv_text" name="chapter_name" value=""
                              placeholder="Insert course name" readonly id="chapter_name">
                      </div>
                </div>
            </div>
        </div>
        <div class="row pbot20">
            <div class="col-md-4 col-sm-4 col-xs-12 pd0">
                <div class="form-label">
                    <span>Lecture Name </span>
                </div>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-12">
             <div class="cvform-box">
                 <input type="text" class="cv_text" name="lecture_name" value=""
                       placeholder="Insert course name" id="lecture_name">
                     </div>

            </div>
        </div>


       <div class="row pbot20 image-box">
			<div class="col-md-4 col-sm-4 col-xs-12">
				<div class="form-label">
					<span>Image/Video upload:</span>
				</div>
			</div>
			<div class="col-md-8 col-sm-8 col-xs-12">
				<div class="cvform-box">
					<div class="chapter-dropzone dropzone needsclick dropzone-uploader">
						<div class="dz-message ">
							<i class="fa fa-picture-o" aria-hidden="true"></i>
							Drop files here or click to upload.<br>
						</div>
					</div>
				</div>
			</div>
		</div>
        <div class="row pbot20">
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="form-label">
                    <span>Description: </span>
                </div>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-12">
                <div class="editor">
                    <textarea rows="3"  name="lecture_description" class="ckeditor lecture_description" id="lecture_description" placeholder="Write Description here..."></textarea>
                </div>
            </div>
        </div>

        <span class="course-save-status" ></span>
        <button type="button" class="ctm-btn submit pull-right course_step2">Save</button>

        <div class="chapter-quiz" style="display:none;">
            <div class="row pbot20">
                <div class="col-md-4 col-sm-4 col-xs-12 pd0">
                    <div class="form-label">
                        <span>Add Quiz: </span>
                    </div>
                </div>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <div class="cvform-box backnull">
                        <a class="ctm-btn chapter-add-question" data-toggle="modal" data-target="#chapter-quiz-model">Click here to add Quiz question!</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="sub-chapter-container" style="display: none" >

            <div class="sub-chapter-box" style="margin-top:50px;"></div>
            <div class="col-md-12 submit-cancle-btn sub-chaper-display-button">
                <button type="button" class="ctm-btn submit pull-left add-sub-chapter">Add New Sub chapter</button>
            </div>

        </div>
    </div>
</form>
</div>
</script>

