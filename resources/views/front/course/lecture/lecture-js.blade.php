<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 5/30/19
 * Time: 5:17 PM
 */
?>
<script>
    $(function () {
        $("#addLecutreForm").validate({
            rules: {
                title: {required: true},
                description: {required: true},
            },
            submitHandler: function (form, e) {
                e.preventDefault();
                for (instance in CKEDITOR.instances) {
                    CKEDITOR.instances.description.updateElement();
                }
                var bla = $('#lecture_description').val();
                //then get the form data
                var fd = new FormData(form);
                fd.append('chapter_description', bla);
                $.ajax({
                    type: "post",
                    url: "{{ url('elearning/course/save-lecture') }}",
                    dataType: "json",
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
                    data: fd,
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function () {
                        //$("#preview").fadeOut();
                        $("#err").fadeOut();
                    },
                    success: function (data) {

                        toastr.success(data.message);
                        hideLectureForm();
                        window.location= '';
                    },
                    error: function (data) {
                        toastr.error("Something went wrong");
                    }
                });
            }
        });

        if ($('.dropzone-uploader').length) {

            console.log($('.dropzone-uploader').length);

            $('.dropzone-uploader').each(function () {
                console.log($(this).attr('id'));
                var sub_chapter_box = $(this).parents('.sub-chapter-box:first-child');
                var chapter_id = '';
                if (sub_chapter_box.length === 0) {
                    var chapter_box = $(this).parents('.course-box:first-child');
                    chapter_id = chapter_box.find('.chapter_id').val();
                } else {
                    chapter_id = sub_chapter_box.find('.sub_chapter_id').val();
                }

                var id = 1;
                if ('undefined' !== typeof chapter_id) {
                    load_dropzone('#' + $(this).attr('id'), id[1]);
                }
            });
        }
    });

    function load_dropzone(dropzon_id, chapter_id) {

        if ($(dropzon_id).length == 0) {
            return;
        }

        console.log(dropzon_id);
        var dropzone = new Dropzone(dropzon_id, {
            url: '/add-course-video/'+chapter_id,
            // previewTemplate: document.querySelector('#preview-template').innerHTML,
            addRemoveLinks: true,
            parallelUploads: 1,
            thumbnailHeight: 80,
            thumbnailWidth: 100,
            maxFilesize: 8,
            filesizeBase: 1000,
            dictResponseError: 'Server not Configured',
            maxFiles: 1,
            init: function () {
                var self = this;
                // config
                self.options.addRemoveLinks = true;
                self.options.dictRemoveFile = "Delete";
                //New file added
                self.on("addedfile", function (file) {
                    console.log('new file added ', file);
                });
                // Send file starts
                self.on("sending", function (file) {
                    console.log('upload started', file);
                    $('.meter').show();
                });

                // File upload Progress
                self.on("totaluploadprogress", function (progress) {
                    console.log("progress ", progress);
                    $('.roller').width(progress + '%');
                });

                self.on("queuecomplete", function (progress) {
                    $('.meter').delay(999).slideUp(999);
                });

                // On removing file
                self.on("removedfile", function (file) {
                    console.log(file);
                });

                self.on('error', function (file, message, xhr) {
                    var header = xhr.status + ": " + xhr.statusText;

                    console.log('erors and stuff');
                    console.log(message);
                    console.log(header);
                });
            },
            thumbnail: function (file, dataUrl) {
                if (file.previewElement) {
                    file.previewElement.classList.remove("dz-file-preview");
                    var images = file.previewElement.querySelectorAll("[data-dz-thumbnail]");
                    for (var i = 0; i < images.length; i++) {
                        var thumbnailElement = images[i];
                        thumbnailElement.alt = file.name;
                        thumbnailElement.src = dataUrl;
                    }
                    setTimeout(function () {
                        file.previewElement.classList.add("dz-image-preview");
                    }, 1);
                }
            }

        });
    }

    function showLectureForm() {
        $("#add_lecture_form").show();
        window.location='#add_chapter_form';
    }

    function hideLectureForm () {
        $("#add_chapter_form").hide();
        resetLectureForm();
    }

    function resetLectureForm () {
        var obj=['status', 'title', 'description', 'id'];
        for (var i=0; i<obj.length; i++){
            $("#"+obj[i]).val('');
        }
    }

    function editLecture(dataString) {
        var obj = JSON.parse(dataString);
        // console.log(obj);
        for (var key in obj) {
            $("#"+key).val(obj[key]);
            if(key == 'description') {
                CKEDITOR.instances[key].setData(obj[key]);
            }
        }
        showLectureForm();
    }

    function deleteLecture(lectureId) {
        $.ajax({
            type: "post",
            url: "/elearning/course/delete-lecture/" + lectureId,
            dataType: "json",
            "headers": {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
            success: function (data) {
                toastr.success(data.message);
                window.location='';
            },
            error: function (data) {
                toastr.error("Something Went Wrong");
            }
        });
    }


</script>
