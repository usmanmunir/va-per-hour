<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 5/22/19
 * Time: 2:28 PM
 */
?>
@extends('front.layouts.default')
@section('main_container')
    <style>
        .offer-post .post-all-offers .nav {
            position: static !important;
            top: 0px !important;
        }

        .offer-post .post-all-offers .nav .nav-item .nav-link {
            padding: 0px 15px 0px 10px !important;
        }
    </style>
    <?php
//    $chapterType = config('constants.chapter_type');
    $chapterStatus = config('constants.course_status');
    ?>
    <section class="ctn banner">
        <div class="main-banner">
            <div class="banner-img">
                <img src="http://dev.vaperhour.coddrule.com/assets/images/post-offer.png">
            </div>
            <div class="banner-contant">
                <h2 class="banner-title-2">Upload Course</h2>
                <!--      <h5 class="banner-title-3">It is a long established fact that a reader will be distracted by the readable content of a  page when looking.</h5>
                 -->  </div>
        </div>
    </section>
    <section class="offer-post section-1 course_upload">

        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12 post-all-offers">
                    <ul class="nav nav-tabs course_tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="title-tab" data-toggle="tab" href="#title" role="tab"
                               aria-controls="title" aria-selected="true"><span class="badge badge-secondary">1</span>Course
                                Overview</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link disabled" id="pricing-tab" data-toggle="tab" href="#pricing" role="tab"
                               aria-controls="pricing" aria-selected="false"><span
                                        class="badge badge-secondary">2</span>Course content</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link disabled" id="requirment-tab" data-toggle="tab" href="#requirment"
                               role="tab" aria-controls="requirment" aria-selected="false"><span
                                        class="badge badge-secondary">3</span>Publish</a>
                        </li>
                    </ul>
                    <div class="tab-content post-oofer-contant" id="myTabContent">
                        <div class="stint-contant tab-pane fade show active pbot0" id="title" role="tabpanel"
                             aria-labelledby="title-tab">
                            <form method="post" action="#" id="editfileUploadForm" enctype="multipart/form-data">
                                @csrf
                                <div class="membership-contant">
                                    <div class="head-title">
                                        <h5>Course Overview</h5>
                                    </div>
                                    <div class="summary1">
                                        <div class="head-title1">
                                            <div class="course-box">
                                                <div class="textbox-title">
                                                    <div class="textbox_wrap">
                                                        <div class="form-label">
                                                            <span>Course Overview: </span>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="alert alert-danger course_error" style="display: none;">
                                                    <strong></strong>
                                                </div>
                                                <div class="alert alert-success course_success" style="display: none;">
                                                    <strong></strong>
                                                </div>
                                                <div class="course_validation_error alert alert-danger"
                                                     style="display: none;">
                                                </div>

                                                <div class="cv_form_overview">
                                                    <div class="row pbot20">
                                                        <div class="col-md-4 col-sm-4 col-xs-12 pd0">
                                                            <div class="form-label">
                                                                <span>Course Name: </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-8 col-sm-8 col-xs-12 ">
                                                            <div class="cvform-box">
                                                                <input type="text" class="cv_text" name="name"
                                                                       value="{{$userCourseRecord->name}}"
                                                                       placeholder="Insert course name">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row pbot20">
                                                        <div class="col-md-4 col-sm-4 col-xs-12 pd0">
                                                            <div class="form-label">
                                                                <span>Course Category: </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <div class="cvform-box">
                                                                <select class="cv_text" name="category_id"
                                                                        id="category_id">

                                                                    <option value="">Select Category</option>
                                                                    @foreach($categories as $category)
                                                                        <option value="{{$category->id}}"
                                                                                @if($userCourseRecord['category']->id == $category->id) selected @endif>
                                                                            {{$category->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row pbot20">
                                                        <div class="col-md-4 col-sm-4 col-xs-12 pd0">
                                                            <div class="form-label">
                                                                <span>Course Price: </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <div class="cvform-box">
                                                                <input type="text" class="cv_text" name="price"
                                                                       value="{{$userCourseRecord->price}}"
                                                                       placeholder="Enter price">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row pbot20">
                                                        <div class="col-md-4 col-sm-4 col-xs-12 pd0">
                                                            <div class="form-label">
                                                                <span>Course Feature Image: </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <div class="cvform-box">
                                                                <input id="uploadFile" class="f-input" readonly=""
                                                                       placeholder="Insert image"/>
                                                                <div class="fileUpload btn btn--browse">
                                                                    <span>Browse</span>
                                                                    <input id="uploadBtn" type="file" class="upload"
                                                                           name="file"/>
                                                                </div>
                                                            </div>
                                                            <img src="{{$userCourseRecord->image}}"
                                                                 class="img img-responsive center-img" height="300px">
                                                        </div>
                                                    </div>
                                                    <div class="row pbot20">
                                                        <div class="col-md-4 col-sm-4 col-xs-12 pd0">
                                                            <div class="form-label">
                                                                <span>Course Description: </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <div class="editor">
                                                                <textarea rows="3" name="description" id="description"
                                                                          placeholder="Write Description here..."
                                                                          class="ckeditor">{{$userCourseRecord->description}}</textarea>
                                                            </div>
                                                        </div>
                                                        {{--Hidden Field--}}
                                                        <input type="hidden" name="course_id" id="course_id"
                                                               value="{{$userCourseRecord->id}}">
                                                    </div>
                                                    <div class="row pbot20">
                                                        <div class="col-md-4 col-sm-4 col-xs-12 pd0">
                                                            <div class="form-label">
                                                                <span>Course Status: </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <div class="cvform-box">
                                                                <select class="cv_text" name="status"
                                                                        id="status">
                                                                    @foreach($chapterStatus as $key => $value)
                                                                        <option value="{{$key}}" @if($userCourseRecord->status == $key) selected @endif>{{$value}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 submit-cancle-btn mtop50">
                                        <button type="submit" class="ctm-btn submit pull-right course_step1"
                                                data-type="course_step1">Next
                                        </button>

                                    </div>

                                    <!--<div class="col-md-12 submit-cancle-btn mtop50">

                                        <button  type="button" class="ctm-btn submit pull-right course_step1 next-step1-button1" data-type="course_step1" >Next</button>

                                    </div> -->
                                </div>
                            </form>
                        </div>
                        <div class="pricing-contant tab-pane fade pbot0" id="pricing" role="tabpanel"
                             aria-labelledby="pricing-tab">
                            <div class="membership-contant">
                                <div class="head-title">
                                    <h5>Course Content</h5>
                                </div>
                                <form action="javascript;:" method="post" enctype="multipart/form-data"
                                      class="step2Form">
                                    <div class="summary1">
                                        <div class="course_error_step2 alert alert-danger" style="display: none;">
                                            <strong></strong></div>

                                        <div class="course_success_step2 alert alert-success" style="display: none;">
                                            <strong></strong></div>

                                        <div class="course_error_validation_step2 alert alert-danger"
                                             style="display: none;"></div>

                                        <div class="head-title1">
                                            <div id="course-box">

                                                <div class="course-box">
                                                    <div class="textbox-title">
                                                        <div class="sidebox_element">
                                                            <div class="delete-icon dblock delete-button">
                                                                <a href="javascript:;"><i class="fa fa-trash"></i></a>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>

                                                    <div class="cv_form_overview chapter-box">

                                                        <div class="row pbot20">
                                                            <div class="col-md-4 col-sm-4 col-xs-12 pd0">
                                                                <div class="form-label">
                                                                    <span>Chapter Type: </span>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                                <div class="cvform-box">
                                                                    <select class="cv_text chapter_type"
                                                                            name="chapter_type" id="chapter_type">
                                                                        <option value="">Select Type</option>
                                                                        <option value="course">course</option>
                                                                        <option value="quiz">Quiz</option>
                                                                        <option value="sub-chapter">Sub Chapter</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row pbot20">
                                                            <div class="col-md-4 col-sm-4 col-xs-12 pd0">
                                                                <div class="form-label">
                                                                    <span>Chapter: </span>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                                <div class="cvform-box">
                                                                    <textarea class="cv_text course_name"
                                                                              name="chapter_title" rows="3"
                                                                              placeholder="Enter Chapter Title"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row pbot20 image-box ctm-hide">
                                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                                <div class="form-label">
                                                                    <span>Image/Video upload:</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                                <div class="cvform-box">
                                                                    <div class="chapter-dropzone dropzone needsclick dropzone-uploader">
                                                                        <div class="dz-message ">
                                                                            <i class="fa fa-picture-o"
                                                                               aria-hidden="true"></i>
                                                                            Drop files here or click to upload.<br>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row pbot20">
                                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                                <div class="form-label">
                                                                    <span>Description: </span>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                                <div class="editor">
                                                                    <textarea rows="3" name="chapter_ckeditor"
                                                                              class="ckeditor chapter_description"
                                                                              placeholder="Write Description here..."
                                                                              id="myTextarea"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <span class="course-save-status"></span>
                                                        <button type="button"
                                                                class="ctm-btn submit pull-right course_step2">Save
                                                        </button>

                                                        <div class="chapter-quiz" style="display:none;">
                                                            <div class="row pbot20">
                                                                <div class="col-md-4 col-sm-4 col-xs-12 pd0">
                                                                    <div class="form-label">
                                                                        <span>Add Quiz: </span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-8 col-sm-8 col-xs-12">
                                                                    <div class="cvform-box backnull">
                                                                        <a class="ctm-btn chapter-add-question"
                                                                           data-toggle="modal"
                                                                           data-target="#chapter-quiz-model">Click here
                                                                            to add Quiz question!</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="sub-chapter-container" style="display: none">

                                                            <div class="sub-chapter-box" style="margin-top:50px;"></div>
                                                            <div class="col-md-12 submit-cancle-btn sub-chaper-display-button">
                                                                <button type="button"
                                                                        class="ctm-btn submit pull-left add-sub-chapter">
                                                                    Add New Sub chapter
                                                                </button>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="add_course_box add_course_button  ptop50">
                                                <h3><a href="javascript:;">+ Add Another Course</a></h3>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 submit-cancle-btn mtop50">
                                        <a href="#" class="ctm-btn cancle pull-left course-step">Previous</a>
                                        <a href="javascript:;"
                                           class="ctm-btn submit pull-right next-step2-button">Next</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="publish-contant course_publish tab-pane fade pbot0" id="requirment" role="tabpanel"
                             aria-labelledby="requirment-tab">
                            <div class="publish-almost ">
                                <div class="pub-details">
                                    <span class="title">It's Under Review!</span>
                                    <span class="contant">Your course is under review our team send you Email ASAP.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script type="text" class="sub-chaper-html">
<div class="cv_form_overview sub-chapter">
<div class="row pbot20">
  <div class="col-md-4 col-sm-4 col-xs-12 pd0">
     <div class="form-label">
        <span>Select Type: </span>
     </div>
  </div>
  <div class="col-md-8 col-sm-8 col-xs-12">
     <div class="cvform-box">
        <select class="cv_text sub_chapter_type" name="lesson_type[]">
           <option value="" selected="" >Select type</option>
           <option value="course" >Course</option>
           <option value="quiz">Quiz</option>
        </select>
     </div>
  </div>
</div>
<div class="quiz" style="display:none;">
 <div class="row pbot20">
  <div class="col-md-4 col-sm-4 col-xs-12 pd0">
   <div class="form-label">
      <span>Add Quiz: </span>
  </div>
 </div>
<div class="col-md-8 col-sm-8 col-xs-12">
   <div class="cvform-box backnull">
      <a class="ctm-btn dblock chapter-add-question" data-toggle="modal" data-target="#chapter-quiz-model" href="javascript:;">Click here to add Quiz question!</a>
   </div>
</div>
</div>
</div>

<div class="course" style="display:none;">
  <div class="row pbot20">
     <div class="col-md-4 col-sm-4 col-xs-12 pd0">
        <div class="form-label">
           <span>Lesson 1: </span>
        </div>
     </div>
     <div class="col-md-8 col-sm-8 col-xs-12">
        <div class="cvform-box">
           <textarea class="sub_chapter_title cv_text" name="lesson_title" rows="3">Insert the title lesson here...</textarea>
        </div>
     </div>
  </div>
  <div class="row pbot20 sub-chapter-image-box ctm-hide">
     <div class="col-md-4 col-sm-4 col-xs-12 pd0">
        <div class="form-label">
           <span>Image/Video upload:</span>
        </div>
     </div>
     <div class="col-md-8 col-sm-8 col-xs-12">
        <div class="cvform-box">

           <div class="sub-chapter-dropzone dropzone needsclick dropzone-uploader">
				<div class="dz-message ">
					<i class="fa fa-picture-o" aria-hidden="true"></i>
					Drop files here or click to upload.<br>
				</div>
			</div>
        </div>
     </div>
  </div>
  <div class="row pbot20">
     <div class="col-md-4 col-sm-4 col-xs-12">
        <div class="form-label">
           <span>Description: </span>
        </div>
     </div>
     <div class="col-md-8 col-sm-8 col-xs-12">
        <div class="editor">
           <textarea rows="3" class="ckeditor sub_chapter_description" name="lesson_ckeditor" placeholder="Write Description here..."></textarea>
        </div>
     </div>
  </div>
</div>
<span class="sub-course-save-status" ></span>
<button  type="button" class="ctm-btn submit pull-right save-sub-chapter">Save Sub chapter</button>
</div>






    </script>

    <!-- course new question -->
    <script type="text" class="course-new-question-html">
    </script>
    <!-- course new question -->

    <!-- course new chapter -->
    <script type="text" class="course-new-chapter-html">
<div class="course-box">
	<div class="textbox-title">
		<div class="sidebox_element">
			<div class="delete-icon dblock delete-button">
				<a href="javascript:;"><i class="fa fa-trash"></i></a>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>

	<div class="cv_form_overview chapter-box">

		<div class="row pbot20">
			<div class="col-md-4 col-sm-4 col-xs-12 pd0">
				<div class="form-label">
					<span>Chapter Type: </span>
				</div>
			</div>
			<div class="col-md-8 col-sm-8 col-xs-12">
				<div class="cvform-box">
					<select class="cv_text chapter_type"  name="chapter_type" id="chapter_type">
						<option value="">Select Type</option>
						<option value="course">course</option>
						<option value="quiz">Quiz</option>
						<option value="sub-chapter">Sub Chapter</option>
					</select>
				</div>
			</div>
		</div>

		<div class="row pbot20">
			<div class="col-md-4 col-sm-4 col-xs-12 pd0">
				<div class="form-label">
					<span>Chapter: </span>
				</div>
			</div>
			<div class="col-md-8 col-sm-8 col-xs-12">
				<div class="cvform-box">
					<textarea class="cv_text course_name" name="chapter_title" rows="3" placeholder="Enter Chapter Title"></textarea>
				</div>
			</div>
		</div>
		<div class="row pbot20 image-box ctm-hide">
			<div class="col-md-4 col-sm-4 col-xs-12">
				<div class="form-label">
					<span>Image/Video upload:</span>
				</div>
			</div>
			<div class="col-md-8 col-sm-8 col-xs-12">
				<div class="cvform-box">
					<div class="chapter-dropzone dropzone needsclick dropzone-uploader">
						<div class="dz-message ">
							<i class="fa fa-picture-o" aria-hidden="true"></i>
							Drop files here or click to upload.<br>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row pbot20">
			<div class="col-md-4 col-sm-4 col-xs-12">
				<div class="form-label">
					<span>Description: </span>
				</div>
			</div>
			<div class="col-md-8 col-sm-8 col-xs-12">
				<div class="editor">
					<textarea rows="3"  name="chapter_ckeditor" class="ckeditor chapter_description" placeholder="Write Description here..."></textarea>
				</div>
			</div>
		</div>

		<span class="course-save-status" ></span>
		<button type="button" class="ctm-btn submit pull-right course_step2">Save</button>

		<div class="chapter-quiz" style="display:none;">
			<div class="row pbot20">
				<div class="col-md-4 col-sm-4 col-xs-12 pd0">
					<div class="form-label">
						<span>Add Quiz: </span>
					</div>
				</div>
				<div class="col-md-8 col-sm-8 col-xs-12">
					<div class="cvform-box backnull">
						<a class="ctm-btn chapter-add-question" data-toggle="modal" data-target="#chapter-quiz-model">Click here to add Quiz question!</a>
					</div>
				</div>
			</div>
		</div>

		<div class="sub-chapter-container" style="display: none" >

			<div class="sub-chapter-box" style="margin-top:50px;"></div>
			<div class="col-md-12 submit-cancle-btn sub-chaper-display-button">
				<button type="button" class="ctm-btn submit pull-left add-sub-chapter">Add New Sub chapter</button>
			</div>

		</div>
	</div>
</div>




    </script>
    <!-- course new chpater -->

    <!-- Model -->
    <div class="modal fade bd-example-modal-lg chapter-quiz-model" id="chapter-quiz-model" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Quiz</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="cv_form_overview">
                        <div class="quiz-box"></div>
                        <div class="col-md-12 submit-cancle-btn mtop50">
                            <a href="javascript:;" class="ctm-btn submit pull-left add-question">Add New Question</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @push('scripts')
        @include('front.course.course-js')
    @endpush
@endsection
