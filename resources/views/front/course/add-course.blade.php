<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 5/22/19
 * Time: 2:28 PM
 */
?>
@extends('front.layouts.default')
@section('main_container')
    <style>
        .offer-post .post-all-offers .nav {
            position: static !important;
            top: 0px !important;
        }

        .offer-post .post-all-offers .nav .nav-item .nav-link {
            padding: 0px 15px 0px 10px !important;
        }
    </style>
    <?php
    $chapterType = config('constants.chapter_type');
    $chapterStatus = config('constants.course_status');
    ?>
    <section class="ctn banner">
        <div class="main-banner">
            <div class="banner-img">
                <img src="/front/images/post-offer.png">
            </div>
            <div class="banner-contant">
                <h2 class="banner-title-2">Upload Course</h2>
                <h5 class="banner-title-3">Add course details you want to offer.</h5>
             </div>
        </div>
    </section>
    <section class="offer-post section-1 course_upload">

        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12 post-all-offers">
                    <ul class="nav nav-tabs course_tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="title-tab" data-toggle="tab" href="#title" role="tab"
                               aria-controls="title" aria-selected="true"><span class="badge badge-secondary">1</span>Course
                                Overview</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link disabled" id="pricing-tab" data-toggle="tab" href="#pricing" role="tab"
                               aria-controls="pricing" aria-selected="false"><span
                                        class="badge badge-secondary">2</span>Course content</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link disabled" id="requirment-tab" data-toggle="tab" href="#requirment"
                               role="tab" aria-controls="requirment" aria-selected="false"><span
                                        class="badge badge-secondary">3</span>Publish</a>
                        </li>
                    </ul>
                    <div class="tab-content post-oofer-contant" id="myTabContent">
                        <div class="stint-contant tab-pane fade show active pbot0" id="add-course" role="tabpanel"
                             aria-labelledby="title-tab">
                            <form id="fileUploadForm" action="#" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="membership-contant">
                                    <div class="head-title">
                                        <h5>Course Overview</h5>
                                    </div>
                                    <div class="summary1">
                                        <div class="head-title1">
                                            <div class="course-box">
                                                <div class="textbox-title">
                                                    <div class="textbox_wrap">
                                                        <div class="form-label">
                                                            <span>Course Overview: </span>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="alert alert-danger course_error" style="display: none;">
                                                    <strong></strong>
                                                </div>
                                                <div class="alert alert-success course_success" style="display: none;">
                                                    <strong></strong>
                                                </div>
                                                <div class="course_validation_error alert alert-danger"
                                                     style="display: none;">
                                                </div>

                                                <div class="cv_form_overview">
                                                    <div class="row pbot20">
                                                        <div class="col-md-4 col-sm-4 col-xs-12 pd0">
                                                            <div class="form-label">
                                                                <span>Course Name: </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-8 col-sm-8 col-xs-12 ">
                                                            <div class="cvform-box">
                                                                <input type="text" class="cv_text" name="name" value=""
                                                                       placeholder="Insert course name">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row pbot20">
                                                        <div class="col-md-4 col-sm-4 col-xs-12 pd0">
                                                            <div class="form-label">
                                                                <span>Course Category: </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <div class="cvform-box">
                                                                <select class="cv_text" name="category_id"
                                                                        id="category_id">
                                                                    <option value="" selected>Select Category</option>
                                                                    @foreach($categories as $category)
                                                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row pbot20">
                                                        <div class="col-md-4 col-sm-4 col-xs-12 pd0">
                                                            <div class="form-label">
                                                                <span>Course Price: </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <div class="cvform-box">
                                                                <input type="text" class="cv_text" name="price" value=""
                                                                       placeholder="Enter price">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row pbot20">
                                                        <div class="col-md-4 col-sm-4 col-xs-12 pd0">
                                                            <div class="form-label">
                                                                <span>Course Feature Image: </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <div class="cvform-box">
                                                                <input id="uploadFile" class="f-input" readonly=""
                                                                       placeholder="Insert image"/>
                                                                <div class="fileUpload btn btn--browse">
                                                                    <span>Browse</span>
                                                                    <input id="uploadBtn" type="file" class="upload"
                                                                           name="file"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row pbot20">
                                                        <div class="col-md-4 col-sm-4 col-xs-12 pd0">
                                                            <div class="form-label">
                                                                <span>Course Description: </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <div class="editor">
                                                                <textarea rows="3" name="description" id="description"
                                                                          placeholder="Write Description here..."
                                                                          class="ckeditor"></textarea>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="row pbot20">
                                                        <div class="col-md-4 col-sm-4 col-xs-12 pd0">
                                                            <div class="form-label">
                                                                <span>Course Status: </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <div class="cvform-box">
                                                                <select class="cv_text" name="status"
                                                                        id="status">
                                                                    @foreach($chapterStatus as $key => $value)
                                                                        <option value="{{$key}}">{{$value}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 submit-cancle-btn mtop50">
                                        <button type="submit" class="ctm-btn submit pull-right course_step1"
                                                data-type="course_step1">Next
                                        </button>

                                    </div>

                                    <!--<div class="col-md-12 submit-cancle-btn mtop50">

                                        <button  type="button" class="ctm-btn submit pull-right course_step1 next-step1-button1" data-type="course_step1" >Next</button>

                                    </div> -->
                                </div>
                            </form>
                        </div>
                        <div class="publish-contant course_publish tab-pane fade pbot0" id="requirment" role="tabpanel"
                             aria-labelledby="requirment-tab">
                            <div class="publish-almost ">
                                <div class="pub-details">
                                    <span class="title">It's Under Review!</span>
                                    <span class="contant">Your course is under review our team send you Email ASAP.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
    @include('front.course.course-js')

@endpush
