@extends('front.layouts.default')
@section('main_container')
    <style>
        .offer-post .post-all-offers .nav {
            position: static !important;
            top: 0px !important;
        }

        .offer-post .post-all-offers .nav .nav-item .nav-link {
            padding: 0px 15px 0px 10px !important;
        }
    </style>
    <?php
    $chapterType = config('constants.chapter_type');
    $chapterStatus = config('constants.course_status');
    ?>
    <section class="ctn banner">
        <div class="main-banner">
            <div class="banner-img">
                <img src="/front/images/post-offer.png">
            </div>
            <div class="banner-contant">
                <h2 class="banner-title-2">Course Content (Chapters)</h2>
                <h5 class="banner-title-3">Manage Content of the course</h5>
            </div>
        </div>
    </section>
    <section class="offer-post section-1 course_upload">

        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12 post-all-offers">
                    <ul class="nav nav-tabs course_tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link disabled" id="title-tab" data-toggle="tab" href="#title" role="tab"
                               aria-controls="title" aria-selected="true"><span class="badge badge-secondary">1</span>Course
                                Overview</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" id="pricing-tab" data-toggle="tab" href="#pricing" role="tab"
                               aria-controls="pricing" aria-selected="false"><span
                                        class="badge badge-secondary">2</span>Course content</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link disabled" id="requirment-tab" data-toggle="tab" href="#requirment"
                               role="tab" aria-controls="requirment" aria-selected="false"><span
                                        class="badge badge-secondary">3</span>Publish</a>
                        </li>
                    </ul>
                    <div class="tab-content post-oofer-contant" id="myTabContent">
                        <div class="stint-contant tab-pane fade show active pbot0" id="add-course" role="tabpanel"
                             aria-labelledby="title-tab">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="text-right">
                                        <a href="/elearning/course/my-course" class="cmbtn cmbtn-sm mtop20imp mbot20imp"><i class="fa fa-caret-left"></i> Back to Courses</a>
                                        <a href="javascript:;" class="cmbtn cmbtn-sm mtop20imp mbot20imp" onclick="showChapterForm()"><i class="fa fa-plus"></i> Create New</a>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <table class="table table-striped">
                                        <tr>
                                            <th>id</th>
                                            <th>Chapter Title</th>
                                            <th>Type</th>
                                            <th>Lectures Added</th>
                                            <th>action</th>
                                        </tr>
                                        @if(count($chapters) > 0 )
                                            @foreach($chapters as $chapter)
                                                <tr>
                                                    <td>{{$chapter->id}}</td>
                                                    <td>{{$chapter->chapter_name}}</td>
                                                    <td>{{$chapter->chapter_type}}</td>
                                                    <td>{{count($chapter->lectures)}}</td>
                                                    <td>
                                                        <a href="javascript:;" class="anchor-primary" onclick="editChapter('{{json_encode($chapter)}}')"><i class="fa fa-2x fa-pencil" ></i></a>
                                                        <a href="javascript:;" class="anchor-primary" onclick="deleteChapter('{{$chapter->id}}')"><i class="fa fa-2x fa-trash" ></i></a>
                                                        <a href="/elearning/course/{{$chapter->course_id}}/{{$chapter->id}}/lectures" class="anchor-primary" title="lectures"><i class="fa fa-2x fa-eye" ></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="4"><div class="alert alert-warning">No Chapter added so far</div></td>
                                            </tr>
                                        @endif

                                    </table>
                                </div>
                            </div>
                            <div class="row display-none" id="add_chapter_form">
                                <div class="col-sm-12">
                                    <form method="POST" id="coursecontent" enctype="multipart/form-data" class="step2Form">
                                        @csrf
                                        <div class="summary1">
                                            <div class="course_error_step2 alert alert-danger" style="display: none;">
                                                <strong></strong></div>

                                            <div class="course_success_step2 alert alert-success" style="display: none;">
                                                <strong></strong></div>

                                            <div class="course_error_validation_step2 alert alert-danger"
                                                 style="display: none;"></div>

                                            <div class="head-title">
                                                <div id="course-box">
                                                    <div class="course-box">
                                                        <div class="textbox-title">
                                                            <h3>Add New Chapter</h3>
                                                        </div>

                                                        <div class="cv_form_overview chapter-box">

                                                            <div class="row pbot20">
                                                                <div class="col-md-4 col-sm-4 col-xs-12 pd0">
                                                                    <div class="form-label">
                                                                        <span>Chapter Type: </span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-8 col-sm-8 col-xs-12">
                                                                    <div class="cvform-box">
                                                                        <select class="cv_text chapter_type"
                                                                                name="chapter_type" id="chapter_type">
                                                                            <option value="">Select Type</option>
                                                                            @foreach($chapterType as $key => $value)
                                                                                <option value="{{$key}}">{{$value}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="row pbot20">
                                                                <div class="col-md-4 col-sm-4 col-xs-12 pd0">
                                                                    <div class="form-label">
                                                                        <span>Chapter Title: </span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-8 col-sm-8 col-xs-12">
                                                                    <div class="cvform-box">
                                                                <textarea class="cv_text course_name" name="chapter_name" id="chapter_name" rows="3"
                                                                          placeholder="Enter Chapter Title"></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row pbot20">
                                                                <div class="col-md-4 col-sm-4 col-xs-12">
                                                                    <div class="form-label">
                                                                        <span>Description: </span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-8 col-sm-8 col-xs-12">
                                                                    <div class="editor">
                                                                <textarea rows="3" name="chapter_description" id="chapter_description"
                                                                          placeholder="Write Description here..."
                                                                          class="ckeditor"></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row pbot20">
                                                                <div class="col-md-4 col-sm-4 col-xs-12 pd0">
                                                                    <div class="form-label">
                                                                        <span>Status: </span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-8 col-sm-8 col-xs-12">
                                                                    <div class="cvform-box">
                                                                        <select class="cv_text" name="status"
                                                                                id="status">
                                                                            @foreach($chapterStatus as $key => $value)
                                                                                <option value="{{$key}}">{{$value}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row pbot20">
                                                                <div class="col-sm-12">
                                                                    <input type="hidden" value="{{$course->id}}" name="course_id" id="course_id" />
                                                                    <input type="hidden" value="" name="id" id="id" />
                                                                    <span class="course-save-status"></span>
                                                                    <button type="submit"
                                                                            class="ctm-btn submit pull-right course_step2">Save
                                                                    </button>
                                                                </div>
                                                            </div>
                                                            <div class="chapter-quiz" style="display:none;">
                                                                <div class="row pbot20">
                                                                    <div class="col-md-4 col-sm-4 col-xs-12 pd0">
                                                                        <div class="form-label">
                                                                            <span>Add Quiz: </span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                                        <div class="cvform-box backnull">
                                                                            <a class="ctm-btn chapter-add-question"
                                                                               data-toggle="modal"
                                                                               data-target="#chapter-quiz-model">Click here
                                                                                to add Quiz question!</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
    @include('front.course.chapter.chapter-js')
    @include('front.course.lecture.lecture-js')

@endpush


