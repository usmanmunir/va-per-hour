<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 5/27/19
 * Time: 1:08 PM
 */
?>
<script>
    $(function () {
       // var validator =  $("#coursecontent").validate();
        $("#coursecontent").validate({
            rules: {
                chapter_type: {required: true},
                chapter_name: {required: true},
                description: {required: true},
            },
            submitHandler: function (form, e) {
                e.preventDefault();
                for (instance in CKEDITOR.instances) {
                    CKEDITOR.instances[instance].updateElement();
                }
                var bla = $('#chapter_description').val();
                //then get the form data
                var fd = new FormData(form);
                /*fd.append('chapter_description',bla);*/
                $.ajax({
                    type: "post",
                    url: "{{ url('elearning/course/save-chapter') }}",
                    dataType: "json",
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
                    data: fd,
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function () {
                        $("#err").fadeOut();
                    },
                    success: function (data) {
                        toastr.success(data.message);
                        //loadLectures(data.data.id);
                        $( 'textarea.editor, textarea.ckeditor' ).ckeditor();
                        hideChapterForm();
                    },
                    error: function (data) {
                        toastr.error("Something went wrong");
                    }
                });
            }
        });
    });
    function showChapterForm() {
        $("#add_chapter_form").show();
        window.location='#add_chapter_form';
    }

    function hideChapterForm () {
        $("#add_chapter_form").hide();
    }

    function resetChapterForm () {
            var obj=['chapter_type', 'chapter_name', 'description', 'id'];
            for (var i=0; i<obj.length; i++){
                $("#"+obj[i]).val('');
            }
    }

    function editChapter(dataString) {
        var obj = JSON.parse(dataString);
        for (var key in obj) {
            $("#"+key).val(obj[key]);
            if(key == 'chapter_description') {
                CKEDITOR.instances[key].setData(obj[key]);
            }
        }
        showChapterForm();
    }

    function deleteChapter(chapterId) {
        $.ajax({
            type: "post",
            url: "/elearning/course/delete-chapter/" + chapterId,
            dataType: "json",
            "headers": {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
            success: function (data) {
                toastr.success(data.message);
                window.location='';
            },
            error: function (data) {
                toastr.error("Something Went Wrong");
            }
        });
    }
</script>
