<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 5/11/19
 * Time: 1:59 AM
 */
?>
@extends('front.layouts.default')
@section('main_container')
    <section class="ctn banner">
        <div class="main-banner">
            <div class="banner-img">
                <!-- <div class="overlay-img"></div> -->
                <img src="/front/images/post-offer.png">
            </div>
            <div class="banner-contant">
                <h2 class="banner-title-2">Create Stint</h2>
            </div>
        </div>
    </section>
    <section class="offer-post section-1 stintCreate-wrap">

        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12 post-all-offers">
                    @include('front.stints.menu')
                    <form enctype="multipart/form-data" class="stint-form" method="post" id="pricingplan_form" action="/my-stint/{{$stint->id}}/pricing-plan">
                        <div class="tab-content post-oofer-contant">
                            <div class="pricing-contant tab-pane active show" id="pricing" role="tabpanel" aria-labelledby="pricing-tab">
                            <div class="scope-pricing col-md-12">
                                <span class="pricing-title">Scope &amp; Pricing</span>
                                <span class="packages-on-off">
                                    <label>3 Packages</label>
                                    <label class="switch">
                                        <input type="checkbox" class="three-packages"
                                        @if(count($pricingPlans) > 0)
                                            checked @endif >
                                        <span class="slider round"></span>
                                    </label>
                                </span>
                            </div>
                            <div class="packegaes col-md-12 one-package-only">
                                <label class="title">Packages</label>
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th scope="col"></th>
                                        <th scope="col">BASIC</th>
                                        <th scope="col">STANDARD</th>
                                        <th scope="col">PREMIUM</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr class="pack-name">
                                        <th scope="row"></th>
                                        <td>
                                            <input type="hidden" name="plan[]" id="plan1" value="1" />
                                            <input type="text" name="package_name[]" id="stingprice_packname1" value="" class="pack_text" placeholder="Name your packages">
                                        </td>
                                        <td>
                                            <input type="hidden" name="plan[]" id="plan1" value="2" />
                                            <input type="text" name="package_name[]" id="stingprice_packname2" value="" class="pack_text" placeholder="Name your packages">
                                        </td>
                                        <td>
                                            <input type="hidden" name="plan[]" id="plan2" value="3" />
                                            <input type="text" name="package_name[]" id="stingprice_packname3" value="" class="pack_text" placeholder="Name your packages">
                                        </td>
                                    </tr>
                                    <tr class="offer-detail">
                                        <th scope="row"></th>
                                        <td>
                                            <textarea placeholder="describe the details of your offering" id="stingprice_packdesc1" name="description[]"></textarea>
                                        </td>
                                        <td>
                                            <textarea placeholder="describe the details of your offering" id="stingprice_packdesc2" name="description[]"></textarea>
                                        </td>
                                        <td>
                                            <textarea placeholder="describe the details of your offering" id="stingprice_packdesc3" name="description[]"></textarea>
                                        </td>
                                    </tr>
                                    <tr class="del-time">
                                        <th scope="row"></th>
                                        <td>
                                            <div class="form-group">
                                                <select class="form-control package-delivery-time" id="stingprice_packdelivery1" name="delivery_time[]">
                                                    <option value="">Delivery Time</option>
                                                    @for($i=1; $i<30; $i++)
                                                        <option value="{{$i}}">{{$i}} Day{{$i == 1 ? '' : 's'}}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <select class="form-control package-delivery-time" id="stingprice_packdelivery2" name="delivery_time[]">
                                                    <option value="">Delivery Time</option>
                                                    @for($i=1; $i<30; $i++)
                                                        <option value="{{$i}}">{{$i}} Day{{$i == 1 ? '' : 's'}}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <select class="form-control package-delivery-time" id="stingprice_packdelivery3" name="delivery_time[]">
                                                    <option value="">Delivery Time</option>
                                                    @for($i=1; $i<30; $i++)
                                                        <option value="{{$i}}">{{$i}} Day{{$i == 1 ? '' : 's'}}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="revisions">
                                        <th scope="row">Revisions</th>
                                        <td>
                                            <div class="form-group">
                                                <select class="form-control" name="revisions[]">
                                                    @for($i=1; $i<=10; $i++)
                                                    <option value="{{$i}}">{{$i}}</option>
                                                    @endfor
                                                    <option value="0">Unlimited Revisions</option>
                                                </select>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <select class="form-control" name="revisions[]">
                                                    @for($i=1; $i<=10; $i++)
                                                        <option value="{{$i}}">{{$i}}</option>
                                                    @endfor
                                                    <option value="0">Unlimited Revisions</option>
                                                </select>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <select class="form-control" name="revisions[]">
                                                    @for($i=1; $i<=10; $i++)
                                                        <option value="{{$i}}">{{$i}}</option>
                                                    @endfor
                                                    <option value="0">Unlimited Revisions</option>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="price">
                                        <th scope="row">Price</th>
                                        <td>
                                            <div class="form-group">
                                                <select class="form-control package-price" name="price[]" id="stingprice_packprice">
                                                    <option value="">$5 - $595</option>
                                                    @for($i=5; $i<1000; $i=$i+5)
                                                        <option value="{{$i}}">${{$i}}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <select class="form-control package-price" name="price[]" id="stingprice_packprice">
                                                    <option value="">$5 - $595</option>
                                                    @for($i=5; $i<1000; $i=$i+5)
                                                        <option value="{{$i}}">${{$i}}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <select class="form-control package-price" name="price[]" id="stingprice_packprice">
                                                    <option value="">$5 - $595</option>
                                                    @for($i=5; $i<1000; $i=$i+5)
                                                        <option value="{{$i}}">${{$i}}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="stint-extras col-md-12">
                                <label class="title">My Stint Extras</label>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th colspan="5" scope="col">
                                                <input type="checkbox" name="has_extra_fast_delivery" value="1"> Extra Fast Delivery
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="pack-name extra-pack-name">
                                        <td colspan="5">
                                            <div class="col-md-12">

                                                <div class="pk-name basic-pk">
                                                    <label class="pk-1">Basic</label>
                                                    <label class="deli-only">I 'll deliver only</label>
                                                    <div class="form-group only-drop">
                                                        <input type="hidden" name="extra_plan[]" value="1" />
                                                        <select class="form-control" name="extra_delivery_time[]">
                                                            @for($i=1; $i<30; $i++)
                                                                <option value="{{$i}}">{{$i}} Day{{$i == 1 ? '' : 's'}}</option>
                                                            @endfor
                                                        </select>
                                                    </div>
                                                    <label class="extra-deli">for an extra</label>
                                                    <div class="form-group extra-dil-drop">
                                                        <select class="form-control" name="extra_price[]">
                                                            <option value="">$5 - $595</option>
                                                            @for($i=5; $i<1000; $i=$i+5)
                                                                <option value="{{$i}}">${{$i}}</option>
                                                            @endfor
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="pk-name standard-pk" style="display:none">
                                                    <label class="pk-1">Standard</label>
                                                    <label class="deli-only">I 'll deliver only</label>
                                                    <div class="form-group only-drop">
                                                        <input type="hidden" name="extra_plan[]" value="2" />
                                                        <select class="form-control" name="extra_delivery_time[]">
                                                            @for($i=1; $i<30; $i++)
                                                                <option value="{{$i}}">{{$i}} Day{{$i == 1 ? '' : 's'}}</option>
                                                            @endfor
                                                        </select>
                                                    </div>
                                                    <label class="extra-deli">for an extra</label>
                                                    <div class="form-group extra-dil-drop">
                                                        <select class="form-control" name="extra_price[]">
                                                            <option value="">$5 - $595</option>
                                                            @for($i=5; $i<1000; $i=$i+5)
                                                                <option value="{{$i}}">${{$i}}</option>
                                                            @endfor
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="pk-name premium-pk" style="display:none">
                                                    <label class="pk-1">Premium</label>
                                                    <label class="deli-only">I 'll deliver only</label>
                                                    <div class="form-group only-drop">
                                                        <input type="hidden" name="extra_plan[]" value="3" />
                                                        <select class="form-control" name="extra_delivery_time[]">
                                                            @for($i=1; $i<30; $i++)
                                                                <option value="{{$i}}">{{$i}} Day{{$i == 1 ? '' : 's'}}</option>
                                                            @endfor
                                                        </select>
                                                    </div>
                                                    <label class="extra-deli">for an extra</label>
                                                    <div class="form-group extra-dil-drop">
                                                        <select class="form-control" name="extra_price[]">
                                                            <option value="">$5 - $595</option>
                                                            @for($i=5; $i<1000; $i=$i+5)
                                                                <option value="{{$i}}">${{$i}}</option>
                                                            @endfor
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>

                                <div class="error-box step-3 hide1">
                                    <ul>
                                    </ul>
                                </div>

                                <div class="col-md-12 submit-cancle-btn">
                                    <a href="{{!empty($stint->id) ? '/stint/my-stint/'.$stint->id.'/edit' : '#' }}" class="ctm-btn cancle previous-step" data-step="1">Back</a>
                                    <button type="submit" class="ctm-btn submit" id="price-step">Next Step</button>
                                </div>
                            </div>
                        </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('styles')
    <style>
        label.error {
            color: red;
            margin-top: 5px;
            font-size: 12px;
        }
        .offer-post .post-all-offers .nav{
            width: 100%;
            background: #fff;
            position: unset;
        }
        .offer-post .post-all-offers .nav .nav-item .nav-link {
            padding: 0px 15px;
        }
        .offer-post .post-all-offers .nav .nav-item {
            border: none;
        }
        ul.nav-tabs li a:before {
            background: none;
        }
        .offer-post .post-all-offers .post-oofer-contant .stint-contant .form-group textarea,
        .offer-post .post-all-offers .post-oofer-contant .stint-contant .form-group select,
        .offer-post .post-all-offers .post-oofer-contant .stint-contant .form-group input {
            border: 1px solid #e5e5e5;
        }
        tr td textarea {
            position: unset;
            border: 1px solid #e5e5e5;
        }
    </style>
@endpush
@push('scripts')
    @include('front.stints.stints-js')
@endpush