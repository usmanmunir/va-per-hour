<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 5/11/19
 * Time: 1:59 AM
 */
?>
@extends('front.layouts.default')
@section('main_container')
    <section class="ctn banner">
        <div class="main-banner">
            <div class="banner-img">
                <!-- <div class="overlay-img"></div> -->
                <img src="/front/images/post-offer.png">
            </div>
            <div class="banner-contant">
                <h2 class="banner-title-2">Manage Sales</h2>
                <!--                 <h5 class="banner-title-3">It is a long established fact that a reader will be distracted by the readable content of a  page when looking.</h5>
                 -->			</div>
        </div>
    </section>
    <section class="joblist-wrap dashboard-area">
        <div class="container">
            <div class="m50">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12">
                        <ul class="nav nav-tabs joblistTabs">
                            <li class="nav-item">
                                <a class="nav-link {{$status == 'priority' ? 'active' : ''}}" id="activejob-tab" href="/users/manage-orders/priority" >Priority</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{$status == 'new' ? 'active' : ''}}" id="activejob-tab" href="/users/manage-orders/new" >New</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{$status == 'active' ? 'active' : ''}}" id="activejob-tab" href="/users/manage-orders/active" >Active</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{$status == 'delivered' ? 'active' : ''}}" id="activejob-tab" href="/users/manage-orders/delivered" >Delivered</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{$status == 'completed' ? 'active' : ''}}" id="activejob-tab" href="/users/manage-orders/completed" >Completed</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{$status == 'cancelled' ? 'active' : ''}}" id="activejob-tab" href="/users/manage-orders/cancelled" >Cancelled</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{$status == 'starred' ? 'active' : ''}}" id="activejob-tab" href="/users/manage-orders/starred" >Starred</a>
                            </li>
                        </ul>
                        <div class="tab-content jobliststeptabs" id="myTabContent">
                            <div class="activejob-contant tab-pane fade active show pbot0 m50" id="activejob" role="tabpanel" aria-labelledby="activejob-tab">
                                <div class="job-list-box">
                                    <div class="db-new-main-table align-top js-db-table">
                                        <table>
                                            <thead>
                                                <tr class="header-filter">
                                                    <td colspan="9" class="header-filter">{{$status}} Orders</td>
                                                </tr>
                                                <tr class="js-header-titles">
                                                    <td></td>
                                                    <td colspan="2">Buyer</td>
                                                    <td>Stint</td>
                                                    <td></td>
                                                    <td>Due on</td>
                                                    <td>Total</td>
                                                    <td class="t-a-center">Note</td>
                                                    <td class="t-a-center" colspan="1">Status</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @if(!empty($orders))
                                                <?php $statuses = config('constants.order_status') ?>
                                                @foreach($orders as $order)
                                                    <tr class="">
                                                        <td>
                                                           <i class="fa fa-2x fa-star-o"></i>
                                                        </td>
                                                        <td colspan="2">
                                                            {{$order->buyer->first_name.' '.$order->buyer->last_name}}
                                                        </td>
                                                        <td colspan="2">
                                                            {{$order->stint->title}}
                                                        </td>
                                                        <td>
                                                            {{date("Y-m-d", strtotime($order->due_on))}}
                                                        </td>
                                                        <td>
                                                            {{($order->unit_price*$order->quantity)+ $order->extra_price}}
                                                        </td>
                                                        <td>
                                                            <i class="fa fa-2x fa-sticky-note-o"></i>
                                                        </td>
                                                        <td>
                                                            {{$statuses[$order->status]}}
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="9">No buyer request posted yet</td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
    @include('front.stints.stints-js')
@endpush