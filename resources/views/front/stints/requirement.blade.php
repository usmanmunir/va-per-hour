<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 7/3/19
 * Time: 2:01 AM
 */
?>
@extends('front.layouts.default')
@section('main_container')
    <section class="ctn banner">
        <div class="main-banner">
            <div class="banner-img">
                <!-- <div class="overlay-img"></div> -->
                <img src="/front/images/post-offer.png">
            </div>
            <div class="banner-contant">
                <h2 class="banner-title-2">Create Stint</h2>
            </div>
        </div>
    </section>
    <section class="offer-post section-1 stintCreate-wrap">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12 post-all-offers">
                    @include('front.stints.menu')
                    <form enctype="multipart/form-data" class="stint-form" id="requirementForm" method="post"
                          action="http://vaperhour.local/stint/save_package">
                        <div class="tab-content post-oofer-contant" id="myTabContent">
                            <div class="requirment-contant tab-pane fade active show" id="requirment" role="tabpanel"
                                 aria-labelledby="requirment-tab">
                                <div class="req-box">
                                    <div class="box">
                                        <div class="re-title">
                                            <div class="icon">
                                                <i class="fa fa-file" aria-hidden="true"></i>
                                            </div>
                                            <div class="text">
                                                <p class="main-title">Tell your buyer what you need to get started.</p>
                                                <p class="sub-title">Structure your Buyer Instructions as a free text, a
                                                    multiple choice question or file upload. </p>
                                            </div>
                                        </div>
                                        @if(count($stintRequirments) > 0)
                                            <?php $i = 1; ?>
                                            @foreach($stintRequirments as $stintRequirment)
                                                <div class="requir-box">
                                                    <div class="requirements">
                                                        <div class="requirement">
                                                            <h6 class="title">REQUIREMENT # <?php echo $i; ?></h6>
                                                            <div class="form-group">
                                                                <input type="hidden" name="requirmentId[]"
                                                                       value="{{$stintRequirment->id}}">
                                                                <textarea name="requirement[]" class="form-control"
                                                                          placeholder="For example specifications, dimensions, brand guidelines, or background materials."
                                                                          rows="3">{{ $stintRequirment->description}}</textarea>
                                                            </div>
                                                            <div class="custom-control1 custom-checkbox1">
                                                                <input type="checkbox" id="is_mandatory"
                                                                       name="is_mandatory[]"
                                                                       class="custom-control-input1 mandatory requirement-require"
                                                                       value="1"
                                                                       @if($stintRequirment->is_mandatory == 1) checked @endif>
                                                                <label class="custom-control-label1" for="">Answers is
                                                                    mandatory</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="requirement-add-btn">
                                                        <button type="submit" class="ctm-btn add-requirement">Add
                                                        </button>
                                                    </div>
                                                </div>
                                                <?php $i++; ?>
                                            @endforeach
                                        @else
                                            <div class="requir-box">
                                                <div class="requirements">
                                                    <div class="requirement">
                                                        <h6 class="title">REQUIREMENT #1</h6>
                                                        <div class="form-group">
                                                            <input type="hidden" name="requirmentId[]"
                                                                   value="">
                                                        <textarea name="requirement[]" class="form-control"
                                                                  placeholder="For example specifications, dimensions, brand guidelines, or background materials."
                                                                  rows="3"></textarea>
                                                        </div>
                                                        <div class="custom-control1 custom-checkbox1">
                                                            <input type="checkbox" id="is_mandatory"
                                                                   name="is_mandatory[]"
                                                                   class="custom-control-input1 mandatory requirement-require"
                                                                   value="1">
                                                            <label class="custom-control-label1" for="">Answers is
                                                                mandatory</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="requirement-add-btn">
                                                    <button type="submit" class="ctm-btn add-requirement">Add</button>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="col-md-12 submit-cancle-btn">
                                        <a href="{{!empty($stint->id) ? '/stint/my-stint/'.$stint->id.'/details' : '#' }}" class="ctm-btn cancle previous-step" data-step="3">Back</a>
                                        <button type="submit" class="ctm-btn submit stint-next" data-step="5">Next
                                            Step
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

@endsection

@push('styles')
    <style>
        label.error {
            color: red;
            margin-top: 5px;
            font-size: 12px;
        }

        .offer-post .post-all-offers .nav {
            width: 100%;
            background: #fff;
            position: unset;
        }

        .offer-post .post-all-offers .nav .nav-item .nav-link {
            padding: 0px 15px;
        }

        .offer-post .post-all-offers .nav .nav-item {
            border: none;
        }

        ul.nav-tabs li a:before {
            background: none;
        }

        .offer-post .post-all-offers .post-oofer-contant .stint-contant .form-group textarea,
        .offer-post .post-all-offers .post-oofer-contant .stint-contant .form-group select,
        .offer-post .post-all-offers .post-oofer-contant .stint-contant .form-group input {
            border: 1px solid #e5e5e5;
        }

        tr td textarea {
            position: unset;
            border: 1px solid #e5e5e5;
        }
    </style>
@endpush
@push('scripts')
    @include('front.stints.stints-js')
    <script>
        $('#stint_description').ckeditor();

    </script>
@endpush
