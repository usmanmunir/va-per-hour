<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 7/4/19
 * Time: 7:56 PM
 */
?>
@extends('front.layouts.default')
@section('main_container')
    <section class="ctn banner">
        <div class="main-banner">
            <div class="banner-img">
                <!-- <div class="overlay-img"></div> -->
                <img src="/front/images/post-offer.png">
            </div>
            <div class="banner-contant">
                <h2 class="banner-title-2">Create Stint</h2>
            </div>
        </div>
    </section>
    <section class="offer-post section-1 stintCreate-wrap">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12 post-all-offers">
                    @include('front.stints.menu')
                    <form enctype="multipart/form-data" class="stint-form" id="publish_form" method="post">
                        <div class="tab-content post-oofer-contant" id="myTabContent">
                            <div class="publish-contant tab-pane fade active show" id="publish" role="tabpanel"
                                 aria-labelledby="publish-tab">
                                <div class="publish-almost">
                                    <div class="pub-details">
                                        <span class="title">It's Alive!</span>
                                        <span class="contant">Let's publish your Stint and get some buyers rolling in.</span>
                                    </div>
                                </div>
                                <div class="publish-almost">
                                    <div class="pub-details">
                                        <span class="title">Almost There...</span>
                                        <span class="contant">Let's publish your Stint and get some buyers rolling in.</span>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-link"
                                                                                                    aria-hidden="true"></i></span>
                                            </div>
                                            <input type="text" id="stint-link" class="form-control"
                                                   placeholder="http://www.vaperhour.com" aria-label="Username"
                                                   aria-describedby="basic-addon1" value="{{$link}}">
                                        </div>
                                        <div class="social-icon">
                                            <a class="fb"
                                               href="http://www.facebook.com/sharer.php?u={{$link}}"
                                               target="_blank"><i class="fa fa-facebook-official"
                                                                  aria-hidden="true"></i></a>
                                            <a class="tweet"
                                               href="http://twitter.com/share?text=[TITLE]&amp;url={{$link}}"
                                               target="_blank"><i class="fa fa-twitter-square"
                                                                  aria-hidden="true"></i></a>
                                            <a class="link"
                                               href="http://www.linkedin.com/shareArticle?mini=true&amp;url={{$link}}"
                                               target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 submit-cancle-btn">
                                    <a href="/stint/my-stint/{{$stint->id}}/gallery"
                                       class="ctm-btn cancle previous-step" data-step="4">Back</a>
                                    @csrf
                                    <button type="submit"
                                       class="ctm-btn submit publish-stint">Publish</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('styles')
    <style>
        label.error {
            color: red;
            margin-top: 5px;
            font-size: 12px;
        }

        .offer-post .post-all-offers .nav {
            width: 100%;
            background: #fff;
            position: unset;
        }

        .offer-post .post-all-offers .nav .nav-item .nav-link {
            padding: 0px 15px;
        }

        .offer-post .post-all-offers .nav .nav-item {
            border: none;
        }

        ul.nav-tabs li a:before {
            background: none;
        }

        .offer-post .post-all-offers .post-oofer-contant .stint-contant .form-group textarea,
        .offer-post .post-all-offers .post-oofer-contant .stint-contant .form-group select,
        .offer-post .post-all-offers .post-oofer-contant .stint-contant .form-group input {
            border: 1px solid #e5e5e5;
        }

        tr td textarea {
            position: unset;
            border: 1px solid #e5e5e5;
        }

        .up-img {
            width: 100% !important;
        }

        .dropzone {
            max-width: 700px !important;
        }
    </style>
@endpush
@push('scripts')
    @include('front.stints.stints-js')
@endpush
