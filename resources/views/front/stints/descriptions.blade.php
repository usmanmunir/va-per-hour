<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 5/11/19
 * Time: 1:59 AM
 */
?>
@extends('front.layouts.default')
@section('main_container')
    <section class="ctn banner">
        <div class="main-banner">
            <div class="banner-img">
                <!-- <div class="overlay-img"></div> -->
                <img src="/front/images/post-offer.png">
            </div>
            <div class="banner-contant">
                <h2 class="banner-title-2">Create Stint</h2>
                <h5 class="banner-sub-title">Descriptions</h5>
            </div>
        </div>
    </section>
    <section class="offer-post section-1 stintCreate-wrap">

        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12 post-all-offers">
                    @include('front.stints.menu')
                    <form id="descriptionForm" enctype="multipart/form-data" class="stint-form" method="POST">
                        @csrf
                        <div class="tab-content post-oofer-contant" id="myTabContent">
                            <div class="description-contant tab-pane show active">
                                <div class="scope-pricing col-md-12">
                                    <span class="pricing-title">Description</span>
                                    <span class="title">Briefly Describe your Stint</span>
                                </div>
                                <div class="editor">
                                    <textarea cols="80" id="stint_description" name="stint_description" rows="10"
                                              style="visibility: hidden; display: none;">{!! !empty($stintDescription) ? $stintDescription->description : ''  !!}</textarea>
                                    <p class="editor-counter">
                                        <span class="limit">min. 120</span>
                                        <span class="counters">918 / 1200 Max. characters</span>
                                    </p>

                                    <p id="editor1-error" class="custome_error"></p>
                                </div>
                                <div class="asked-que">
                                    <div class="fre-ask-que">
                                        <span class="title">Frequently Asked Questions</span>
                                        <a class="add-stint-faq" href="#"><i class="fa fa-plus" aria-hidden="true"></i>
                                            Add FAQ</a>
                                    </div>
                                    <div class="que-buy">
                                        <span>Add Questions &amp; Answers for your Buyers</span>
                                        <a href="#" class="add-stint-faq"><i class="fa fa-plus" aria-hidden="true"></i>
                                            Add FAQ</a>
                                    </div>
                                    @if(count($stintFaqs) > 0)
                                        @foreach($stintFaqs as $stintFaq)
                                            <div class="faq-question">
                                                <input type="hidden" name="faqId[]" value="{{$stintFaq->id}}">
                                                <input class="form-control" type="text" name="faq_question[]"
                                                       value="{{$stintFaq->question}}"
                                                       placeholder="Question"/><br/>
                                                <textarea name="faq_answer[]" class="form-control"
                                                          placeholder="Answer">{{ $stintFaq->answer }}</textarea>
                                            </div>
                                        @endforeach
                                    @endif

                                </div>
                                <div class="col-md-12 submit-cancle-btn">
                                    <a href="{{!empty($stint->id) ? '/stint/my-stint/'.$stint->id.'/pricing-plan' : '#' }}" class="ctm-btn cancle previous-step" data-step="2">Back</a>
                                    <button type="submit" class="ctm-btn submit stint-next" data-step="4" id="step2">
                                        Next Step
                                    </button>

                                    {{--<a href="#" class="ctm-btn submit stint-next" data-step="4" id="step2">Next Step</a>--}}
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('styles')
    <style>
        label.error {
            color: red;
            margin-top: 5px;
            font-size: 12px;
        }

        .offer-post .post-all-offers .nav {
            width: 100%;
            background: #fff;
            position: unset;
        }

        .offer-post .post-all-offers .nav .nav-item .nav-link {
            padding: 0px 15px;
        }

        .offer-post .post-all-offers .nav .nav-item {
            border: none;
        }

        ul.nav-tabs li a:before {
            background: none;
        }

        .offer-post .post-all-offers .post-oofer-contant .stint-contant .form-group textarea,
        .offer-post .post-all-offers .post-oofer-contant .stint-contant .form-group select,
        .offer-post .post-all-offers .post-oofer-contant .stint-contant .form-group input {
            border: 1px solid #e5e5e5;
        }

        tr td textarea {
            position: unset;
            border: 1px solid #e5e5e5;
        }
    </style>
@endpush
@push('scripts')
    @include('front.stints.stints-js')
    <script>
        $('#stint_description').ckeditor();

    </script>
@endpush