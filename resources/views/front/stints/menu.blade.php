<ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item">
        <a class="nav-link" id="title-tab" href="{{!empty($stint->id) ? '/stint/my-stint/'.$stint->id.'/edit' : '#' }}">
            <span class="badge badge-secondary">1</span>
            Stint Title
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{!empty($stint->id) ? '/stint/my-stint/'.$stint->id.'/pricing-plan' : '#' }}">
            <span class="badge badge-secondary">2</span>
            Pricing
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{!empty($stint->id) ? '/stint/my-stint/'.$stint->id.'/details' : '#' }}">
            <span class="badge badge-secondary">3</span>
            Description
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="{{!empty($stint->id) ? '/stint/my-stint/'.$stint->id.'/requirement' : '#' }}">
            <span class="badge badge-secondary">4</span>
            Requirement
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{!empty($stint->id) ? '/stint/my-stint/'.$stint->id.'/gallery' : '#' }}">
            <span class="badge badge-secondary">5</span>
            Gallery
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{!empty($stint->id) ? '/stint/my-stint/'.$stint->id.'/publish' : '#' }}">
            <span class="badge badge-secondary">6</span>
            Publish
        </a>
    </li>
</ul>