<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 7/8/19
 * Time: 7:39 AM
 */
?>
@extends('front.layouts.default')
@section('main_container')
    <section id="new-stints" class="ptop50 pbot50">
        <div class="new-wrap">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="title-with-viewall">
                            <div class="stint-left-side">
                                <h2>Stints</h2>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 item-manag">
                        <div class="item-list">
                            <?php $i = 0; ?>
                            @foreach($stintRecords as $stintRecord)
                                <div class="list">
                                    <div class="box">
                                        <div class="item-img">
                                            <img src="{{$stintRecord['gallery'][$i]->image}}">
                                        </div>
                                        <div class="item-contant">
                                            <h5>
                                                <a href="/stint/view/{{$stintRecord->id}}/{{str_replace(" ", "-", strtolower($stintRecord->title))}}">
                                                    {{$stintRecord->title}}</a>
                                            </h5>
                                            <div class="user-info">
                                                <div class="img">
                                                    <img src="{{$stintRecord['user']->avatar}}">
                                                </div>
                                                <a href="/user-profile/{{csrf_token().$stintRecord->user->id}}">
                                                    <h6>{{$stintRecord['user']->first_name}}{{$stintRecord['user']->last_name}}</h6>
                                                </a>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="name-cate"><i class="fa fa-book"
                                                                      aria-hidden="true"></i> {{$stintRecord['category']->name}}
                                            </div>
                                        </div>
                                        <div class="item-pricing">

                                            <div class="price">
                                                <a href="#">
                                                    <?php for($j = 0;$j < count($stintRecord['price']);$j++){ ?>
                                                    {{$stintRecord['price'][$j]->price}}
                                                    <?php } ?>
                                                </a>
                                            </div>

                                            <div class="like"><i class="fa fa-heart-o" aria-hidden="true"></i> 0</div>

                                            <div class="cart"><i class="fa fa-shopping-cart" aria-hidden="true"></i> 0
                                            </div>

                                            <div class="rating-star"><i class="fa fa-star" aria-hidden="true"></i> <h4>
                                                    0 </h4><span> (0)</span>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <?php  //$i++;  ?>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@push('scripts')
    {{--    @include('front.stints.stints-js')--}}
@endpush
