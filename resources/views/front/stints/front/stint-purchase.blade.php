<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 8/6/19
 * Time: 4:05 AM
 */ ?>
@extends('front.layouts.default')
@push('styles')
    <link rel="stylesheet" href="/front/css/flexslider.css" type="text/css" media="screen"/>
@endpush
@section('main_container')
    <section class="banner">
        <div class="main-banner">
            <div class="banner-img">
                <div class="overlay-img"></div>
                <img src="/front/images/confirm-order-banner.png">
            </div>
            <div class="banner-contant">
                <h2 class="banner-title-2">Confirm Your Order</h2>
                <!--  <h5 class="banner-title-3">It is a long established fact that a reader will be distracted by the readable content of a
                         page when looking.</h5> -->
            </div>
        </div>
    </section>
    <section class="con-oreder section-1">
        <div class="container">
            <form method="post" action="/checkout" id="purchaseFrom">
                @csrf
                <input type="hidden" name="stint_id" value="{{$stintRecord->id}}"/>
                <input type="hidden" name="order_type" value="stint"/>
                <input type="hidden" name="buyer_id" value="{{Auth::user()->id}}"/>
                <input type="hidden" name="seller_id" value="{{$stintRecord->user_id}}"/>
                <input type="hidden" id="summary" name="summary" value="{{$stintRecord->price[0]->price}}"/>
                <div class="row">
                    <div class="col-xs-12 col-sm-8 col-md-8 col-xl-8 order-details">
                        <div class="padding">
                            <div class="order-img">
                                <img src="{{$stintRecord->gallery[0]->image}}">
                            </div>
                            <div class="order-info">
                                <h3>{{$stintRecord->title}}</h3>
                                <span class="other-nm">{{$stintRecord->user->first_name}}</span>
                                <div class="rate-and-reviws">
                                    <div class="stars">
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                    </div>
                                    <div class="reviw-counter">
                                        <a href="#">(26 reviews)</a>
                                    </div>
                                </div>
                                <div class="qty-price">
                                    <div class="qty">
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1">Qty:</label>
                                            <select class="form-control" id="quantity" name="quantity">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                            </select>
                                        </div>
                                    </div>
                                    <input type="hidden" id="unit_price" name="unit_price" value="{{$stintRecord->price[0]->price}}">
                                    <div class="price" id="price">
                                        <h3>$<span class="package-price" id="packageprice">{{$stintRecord->price[0]->price}}</span></h3>
                                    </div>
                                    @if(!empty($stintRecord->extras) && count($stintRecord->extras) > 0)
                                    <div class="">
                                        <label>
                                            <input type="hidden" class="fast-delivery-price" name="extra_price" value="{{$stintRecord->extras[0]->price}}">
                                            <!--					                    <input type="checkbox" class="extra_fast_delivery" name="extra_fast_delivery"> Add -->
                                            <!-- day(s) delivery - $<span>--><!--</span>-->

                                            <div class="custom-checkbox fast-delivey-checkbox">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox"
                                                           class="custom-control-input extra_fast_delivery"
                                                           id="customControlInline" name="extra_fast_delivery">
                                                    <label class="custom-control-label extra_fast_delivery"
                                                           for="customControlInline">Add {{$stintRecord->extras[0]->delivery_time}} day(s) delivery -
                                                        $<span>{{$stintRecord->extras[0]->price}}</label>
                                                </div>
                                            </div>

                                        </label>
                                    </div>
                                    @endif
                                </div>
                            </div>

                            <div class="order-discription">
                                <p class="title">discription</p>
                                <p class="o-dic">
                               {!! $stintRecord->description !!}</p>
                            </div>

                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-xl-4 order-payment">
                        <div class="col-md-12 summery">
                            <div class="summ summ-title">Summary</div>
                            <div class="summ summ-list">
                                <div class="list">Subtototal</div>
                                <div class="list" >$<span class="order-sub-total" id="ordersubtotal">{{$stintRecord->price[0]->price}}</span></div>
                            </div>
                            <div class="summ-total">
                                <div class="list total">Total</div>
                                <div class="list total" >$<span class="order-total" id="ordertotal">{{$stintRecord->price[0]->price}}</span></div>
                                <div class="list">Delivery Time</div>
                                <div class="list">{{$stintRecord->price[0]->delivery_time}} Days</div>
                            </div>
                            <div class="payment-btn">
                                <button type="submit" class="ctm-btn">Order Now</button>
                            </div>
                            <div class="payment-way">
                                <p>We Accept All this Card</p>
                                <img src="/front/images/payment-card.png">
                            </div>
                        </div>
                        <div class="col-md-12 checkout-sequrity">
                            <p><i class="fa fa-lock" aria-hidden="true"></i><span>SSL Secured Checkout</span></p>
                            <p><i class="fa fa-check-circle" aria-hidden="true"></i><span>100% Risk Free Payment</span>
                            </p>
                            <p><i class="fa fa-user" aria-hidden="true"></i><span>24/7 Customer Support</span></p>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>

@endsection
@push('scripts')
    <script type="text/javascript">
        $(function () {
            if ('undefined' != typeof SyntaxHighlighter) {
                SyntaxHighlighter.all();
            }
            $("#quantity").change(function(){
                var stintQty = ($(this).find(':selected').text());//this will give the selected option's text
                var packagePrice = $("#unit_price").val();
                var total = stintQty * packagePrice;
                packageprice.innerText = total;
                ordersubtotal.innerText = total;
                ordertotal.innerText = total;
                $("#summary").val(total)
            });
        });
    </script>

@endpush
