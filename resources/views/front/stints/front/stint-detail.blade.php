<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 7/9/19
 * Time: 4:15 AM
 */
?>
@extends('front.layouts.default')
@push('styles')
    <link rel="stylesheet" href="/front/css/flexslider.css" type="text/css" media="screen"/>
@endpush
@section('main_container')
    <div class="stint-view-page">
        <section id="banner-title" class="ptop30">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="breadcrumbsNew">
                            <a href="/stint/">Stint</a>
                            <a href="/stint/category/{{$stintRecord->category->id}}/{{str_replace(" ", "-",strtolower($stintRecord->category->name))}}">{{$stintRecord->category->name}}</a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="stint-head-box">
                            <h2 class="normal-title">{{$stintRecord->title}}</h2>
                        </div>
                        <div class="star blackWord">
                            <i class="fa fa-star-o" aria-hidden="true"></i>
                            <i class="fa fa-star-o" aria-hidden="true"></i>
                            <i class="fa fa-star-o" aria-hidden="true"></i>
                            <i class="fa fa-star-o" aria-hidden="true"></i>
                            <i class="fa fa-star-o" aria-hidden="true"></i>
                            <span><b>0.0</b></span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="product-info-contant pbot100">
            <div class="container">
                <div class="row">
                    <div class="product-in product-priceing">
                        <form method="post" action="/stint/purchase/{{$stintRecord->id}}/{{str_replace(" ", "-", $stintRecord->title)}}" id="purchase_stint" method="post">
                            @csrf
                            <div class="box">
                                <div class="priceing-info">

                                    <label class="price-label">$ {{!empty($stintRecord['price']) && count($stintRecord['price']) > 0  ? $stintRecord['price'][0]->price : 0}}</label>
                                    <div class="custom-control custom-radio">
                                        @if(!empty($stintRecord['price']) && count($stintRecord['price']) > 0 )
                                            @for($k = 0;$k < count($stintRecord['price']);$k++)
                                                @if(!empty($stintRecord['price'][$k]->price))
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" name="package" id="customRadio_{{$k}}"
                                                               class="custom-control-input"
                                                               value="{{$stintRecord['price'][$k]->id}}">
                                                        <label class="custom-control-label"
                                                               for="customRadio_{{$k}}">{{$stintRecord['price'][$k]->title}}
                                                            - ${{$stintRecord['price'][$k]->price}}</label>
                                                    </div>
                                                @endif
                                            @endfor
                                        @endif
                                    </div>

                                    <div class="purches-item margin-padding0">
                                        <a href="javascript:;" onclick="$('#purchase_stint').submit();"
                                           class="ctm-btn bigctm-btn">Purchase Now
                                        </a>
                                    </div>
                                </div>
                                <div class="total-sales">
                                    <div class="list">
                                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                        <span>Total Sales</span>
                                        <span class="counter">{{$stintRecord->orders_count}}</span>
                                    </div>
                                    <div class="list">
                                        <i class="fa fa-heart-o" aria-hidden="true"></i>
                                        <span>Favourites</span>
                                        <span class="counter">0</span>
                                    </div>

                                    <div class="action-list-wrap mtop20">
                                        <div class="save-stint common-fav" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Mark as favorite">
                                            <a href="javascript:markFavorite('{{$stintRecord->id}}')" data-action="favourite" data-type="stint" class="add-to-favorites">
                                                <i class="fa fa-heart-o"></i>
                                                <span class="heart-text"> Favorite </span>
                                            </a>
                                        </div>

                                        <div class="save-stint common-fav">
                                            <a href="javascript:;" data-toggle="modal" data-toggle="tooltip" data-placement="bottom"  data-target=".flagmodel"  data-original-title="Report this stint"><i class="fa fa-flag"></i></a>
                                        </div>

                                        <div class="save-stint common-fav">
                                            <a href="javascript:;" data-toggle="modal"  data-toggle="tooltip" data-placement="bottom"  data-target=".share-model" data-original-title="share this gig"><i class="fa fa-share-alt"></i> <span class="heart-text">share</span></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-inforamtion">
                                    <h2 class="title">Seller Information</h2>
                                    <div class="profile-info">
                                        <div class="pro-img">
                                            <img src="{{!empty($stintRecord->user->avatar) ? $stintRecord->user->avatar : '/uploads/images/default.svg'}}">

                                        </div>
                                        <p class="name">{{$stintRecord->user->first_name.' '.$stintRecord->user->last_name}}</p>
                                        <p class="sign-up-time">Signed Up: {{date("d M, Y", strtotime($stintRecord->user->created_at))}}</p>
                                        <div class="social">

                                            <a href="{{$stintRecord->user->social_accounts->facebook ?? '#'}}" target="_blank" class="facebook"><i class="fa fa-facebook-official" aria-hidden="true"></i></a>

                                            <a href="{{$stintRecord->user->social_accounts->twitter ?? '#'}}" target="_blank" class="twiter"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>

                                            <a href="{{$stintRecord->user->social_accounts->linked_in ?? '#'}}" target="_blank" class="dribble"><span><i class="fa fa-linkedin" aria-hidden="true"></i></span></a>

                                        </div>
                                        <div class="profile-info-btn">
                                            <a href="/user-profile/{{$stintRecord->user->id}}" class="ctm-btn">View Profile</a>
                                            <a href="#" data-toggle="modal" data-placement="bottom" data-target=".send-message-modal" class="ctm-btn">Send Message</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="product-in product-details">
                        <div class="box">
                            @if(!empty($stintRecord->gallery) && count($stintRecord->gallery) > 0)
                            <div class="product-img">
                                <div class="img-slider" style="">
                                    <div id="slider" class="flexslider">
                                        <ul class="slides">
                                            @for($j = 0;$j < count($stintRecord->gallery);$j++)
                                            <li>
                                                <img src="{{$stintRecord->gallery[$j]->image}}"/>
                                            </li>
                                            @endfor
                                        </ul>
                                    </div>
                                    <div id="carousel" class="flexslider bootom-slider">
                                        <ul class="slides ">
                                            @for($j = 0;$j < count($stintRecord->gallery);$j++)
                                                <li>
                                                    <img src="{{$stintRecord->gallery[$j]->image}}"/>
                                                </li>
                                            @endfor
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <div class="product-info-tab">
                                <ul class="nav nav-tabs ctm-tabs" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Item Details</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#review-tab" role="tabpanel" aria-controls="review-tab" aria-selected="false">Reviews</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#faq-tab" role="tab" aria-controls="faq-tab" aria-selected="false" style="border-right: 0;">Item FAQ</a>
                                    </li>
                                </ul>
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade show  ctm-item-detail active" id="home" role="tabpanel" aria-labelledby="item-tab">
                                        <div class="box">
                                            <h1 class="title">{{$stintRecord->title}}</h1>
                                            <p>
                                                {!! $stintRecord->description !!}
                                            </p>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade  ctm-item-detail" id="review-tab" role="tabpanel" aria-labelledby="comment-tab">
                                        <div class="box">
                                            <div class="feed-contant">
                                                <div class="review-feed">
                                                    <div class="title-view-all">
                                                        <a href="#" class="all-feed-reviews">Review (0)</a>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="tab-pane fade ctm-item-detail" id="faq-tab" role="tabpanel" aria-labelledby="faq-tab">
                                        <div class="box">
                                            <h1 class="title">Stint FAQ:</h1>
                                            @if(!empty($stintRecord->faq))
                                                @foreach($stintRecord->faq as $faq)
                                                <div class="form-group">
                                                    <p>{{$faq->question}}</p>
                                                    <span>{{$faq->answer}}</span>
                                                </div>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @include('front.partials.modal.report-stint', ['stint' => $stintRecord])
    @include('front.partials.modal.share-stint', ['stint' => $stintRecord])
    @include('front.partials.modal.start-conversation', ['stint' => $stintRecord])

@endsection
@push('scripts')
    <script defer src="/front/js/jquery.flexslider.js"></script>
    <script type="text/javascript">


        $(function () {
            if ('undefined' != typeof SyntaxHighlighter) {
                SyntaxHighlighter.all();
            }
        });
        $(document).ready(function () {

            $('#carousel').flexslider({
                animation: "slide",
                controlNav: false,
                animationLoop: false,
                slideshow: false,
                itemWidth: 100,
                itemMargin: 5,
                asNavFor: '#slider'
            });

            $('#slider').flexslider({
                animation: "slide",
                controlNav: false,
                animationLoop: false,
                slideshow: false,
                sync: "#carousel",
                start: function (slider) {
                    $('body').removeClass('loading');
                }
            });

            $("#purchase_stint").validate({
                rules: {
                    package: { required: true}
                },
                submitHandler: function (form, e) {
                    form.submit();
                }
            });

        });
        function report(stint) {
            $.ajax({
                'url': '/stint/report/'+stint,
                'type': 'POST',
                'data': {_token: '{{csrf_token()}}', 'report_reason': $("input[name=report_reason]:checked").val()},
                beforeSend: function () {
                    $(".flagmodel").find('.submit-btn').html('<i class="fa fa-spin fa-spinner"></i>')
                },
                success:function (res) {
                    toastr.success(res);
                    $(".flagmodel").find('.submit-btn').html('Request Report');
                    $(".flagmodel").find('.close').trigger('click');
                },
                error: function (err) {
                    if(err.status !== 403 ) {
                        toastr.error('Something went wrong, please try again!');
                    }
                    toastr.error(err.responseJSON);
                    $(".flagmodel").find('.submit-btn').html('Request Report')
                }
            })
        }

        function markFavorite(stint) {
            $.ajax({
                'url': '/stint/mark-favorite/'+stint,
                'type': 'POST',
                'data': {_token: '{{csrf_token()}}'},
                beforeSend: function () {

                },
                success:function (res) {
                    toastr.success('Stint marked as favorite');
                },
                error: function (err) {
                    toastr.error('Something went wrong, please try again!');
                }
            });
        }

        function send_message(stint) {
            $.ajax({
                'url': '/messages/send-message/{{csrf_token()}}',
                data: {stint: stint, message: $("#message").val(), _token: '{{csrf_token()}}'},
                'type': 'POST',
                beforeSend: function () {
                    $(".send-message-modal").find('.submit-btn').html('<i class="fa fa-spin fa-spinner"></i>')
                },
                success:function (res) {
                    toastr.success('Message has sent successfully');
                    $(".send-message-modal").find('.submit-btn').html('send Message');
                    $(".send-message-modal").find('.close').trigger('click')
                },
                error: function (err) {
                    toastr.error('Something went wrong, please try again!');
                    $(".send-message-modal").find('.submit-btn').html('send Message');
                }
            });
        }
    </script>

@endpush