<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 7/3/19
 * Time: 2:43 PM
 */
?>
@extends('front.layouts.default')
@section('main_container')
    <section class="ctn banner">
        <div class="main-banner">
            <div class="banner-img">
                <!-- <div class="overlay-img"></div> -->
                <img src="/front/images/post-offer.png">
            </div>
            <div class="banner-contant">
                <h2 class="banner-title-2">Create Stint</h2>
                <h5 class="banner-sub-title-2">Gallery</h5>
            </div>
        </div>
    </section>
    <section class="offer-post section-1 stintCreate-wrap">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12 post-all-offers">
                    @include('front.stints.menu')
                    <form enctype="multipart/form-data" class="stint-form" method="post" id="galleryForm">
                        @csrf
                        <div class="tab-content post-oofer-contant" id="myTabContent">
                            <div class="gallery-contant tab-pane fade active show" id="gallery" role="tabpanel"
                                 aria-labelledby="gallery-tab">
                                <div class="gallery-title">
                                    <h3 class="title">Build Your Stint Gallery</h3>
                                    <p class="sub-title">Add memoreble content to gallery to set yourself apart from
                                        competitors</p>
                                </div>
                                <div class="gigs-contents">
                                    <div class="gigs gig-photo">
                                        <div class="gig-tiitle">
                                            <span class="title">Gig Photos</span>
                                            <span class="sub-title">Upload photos that describe or are related to your stint.</span>
                                            <span class="img-counter">(0/3)</span>
                                        </div>
                                        <div class="upload-img">
                                            <div class="up-img img-1">
                                                <div class="dropzone" id="dropzone">
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="gigs gig-video">
                                        <div class="gig-tiitle">
                                            <span class="title">Gig Video</span>
                                            <span class="sub-title">Add a relevant, high quality video that best showcases your Stint offering</span>
                                            <span class="img-counter">(0/1)</span>
                                        </div>
                                        <div class="upload-img">
                                            <div class="up-img">
                                                <div class="dropzone" id="uploadVideo">
                                                    <div class="dz-message ">
                                                        <i class="fa fa-picture-o" aria-hidden="true"></i>
                                                        Drop files here or click to upload.<br>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="up-img img-2">
                                                <h6>Please choose a video shorter than 75<br>seconds and smaller than
                                                    50mb</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="gigs gig-pdf">
                                        <div class="gig-tiitle">
                                            <span class="title">Gig PDFs</span>
                                            <span class="sub-title">We only recommend adding a PDF file if it furter clerifies the services you will be providing</span>
                                            <span class="img-counter">(0/2)</span>
                                        </div>
                                        <div class="upload-img">
                                            <div class="up-img">
                                                <div class="dropzone" id="uploadPdf">
                                                    <div class="dz-message ">
                                                        <i class="fa fa-picture-o" aria-hidden="true"></i>
                                                        Drop files here or click to upload.<br>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-md-12 submit-cancle-btn">
                                        <a href="{{!empty($stint->id) ? '/stint/my-stint/'.$stint->id.'/requirement' : '#' }}"
                                           class="ctm-btn cancle">Back</a>
                                        <button type="submit"
                                                class="ctm-btn submit stint-next" data-step="6">Next Step
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('styles')
    <style>
        label.error {
            color: red;
            margin-top: 5px;
            font-size: 12px;
        }

        .offer-post .post-all-offers .nav {
            width: 100%;
            background: #fff;
            position: unset;
        }

        .offer-post .post-all-offers .nav .nav-item .nav-link {
            padding: 0px 15px;
        }

        .offer-post .post-all-offers .nav .nav-item {
            border: none;
        }

        ul.nav-tabs li a:before {
            background: none;
        }

        .offer-post .post-all-offers .post-oofer-contant .stint-contant .form-group textarea,
        .offer-post .post-all-offers .post-oofer-contant .stint-contant .form-group select,
        .offer-post .post-all-offers .post-oofer-contant .stint-contant .form-group input {
            border: 1px solid #e5e5e5;
        }

        tr td textarea {
            position: unset;
            border: 1px solid #e5e5e5;
        }

        .up-img {
            width: 100% !important;
        }

        .dropzone {
            max-width: 700px !important;
        }
    </style>
@endpush
@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>

    <script type="text/javascript">
        Dropzone.autoDiscover = false;
        var uploadedDocumentMap = {}
        $(document).ready(function () {
            $("#dropzone").dropzone
            ({
                maxFiles: 3,
                addRemoveLinks: true,
                method: "POST",
                url: "{{ url('/stint/my-stint/'.$stint->id.'/upload-gallery') }}",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
                success: function (file, response) {
                     console.log(response);
                    $('form').append('<input type="hidden" name="galleryimages[]" value="' + response.path + '">')
                    uploadedDocumentMap[file.path] = response.path
                }
            });


            //    Upload Video

            $("#uploadVideo").dropzone({
                maxFiles: 1,
                maxFilesize: 500000000,
                acceptedFiles: ".mp4,.gif",
                addRemoveLinks: true,
                timeout: 5000,
                method: "POST",
                url: "{{ url('/stint/my-stint/'.$stint->id.'/upload-video') }}",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
                success: function (file, response) {
                    // console.log(response);
                    $('form').append('<input type="hidden" name="video" value="' + response + '">')
                }
            });

            //    Upload PDF
            $("#uploadPdf").dropzone({
                maxFiles: 1,
                maxFilesize: 500000000,
                acceptedFiles: ".pdf",
                addRemoveLinks: true,
                timeout: 5000,
                method: "POST",
                url: "{{ url('/stint/my-stint/'.$stint->id.'/upload-pdf') }}",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
                success: function (file, response) {
                    // console.log(response);
                    $('form').append('<input type="hidden" name="pdf" value="' + response + '">')
                }
            });
        })

        function lightbox_open() {
            var lightBoxVideo = document.getElementById("lectureVideo");
            window.scrollTo(0, 0);
            document.getElementById('light').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
            lightBoxVideo.play();
        }

        function lightbox_close() {
            var lightBoxVideo = document.getElementById("lectureVideo");
            document.getElementById('light').style.display = 'none';
            document.getElementById('fade').style.display = 'none';
            lightBoxVideo.pause();
        }
    </script>
    @include('front.stints.stints-js')
@endpush
