<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 5/11/19
 * Time: 1:59 AM
 */
?>
@extends('front.layouts.default')
@section('main_container')
    <section class="ctn banner">
        <div class="main-banner">
            <div class="banner-img">
                <!-- <div class="overlay-img"></div> -->
                <img src="/front/images/post-offer.png">
            </div>
            <div class="banner-contant">
                <h2 class="banner-title-2">Create Stint</h2>
            </div>
        </div>
    </section>
    <section class="offer-post section-1 stintCreate-wrap">

        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12 post-all-offers">
                    @include('front.stints.menu')
                    <form enctype="multipart/form-data" class="stint-form" method="post" id="create_stint_form"
                          action="/stint/save_package">
                        <div class="tab-content post-oofer-contant" id="myTabContent">
                            <div class="stint-contant tab-pane active show" id="title" role="tabpanel"
                                 aria-labelledby="title-tab">
                                <div class="form-group">
                                    <label for="exampleFormControlTextarea1">Stint Title:</label>
                                    <textarea name="title" class="form-control" id="c"
                                              rows="3"> {{(!empty($stintRecord)? $stintRecord->title : '')}}</textarea>
                                    <p id="exampleFormControlTextarea1-error" class="custome_error"></p>
                                    <p class="word-limit">0/80 Max</p>
                                    <input type="hidden" value="{{(!empty($stintRecord)? $stintRecord->id : '')}}"
                                           name="stintId">
                                </div>
                                <div class="form-group">
                                    <label for="main_category">Select Categories:</label>
                                    <select class="form-control" name="category_id" id="category_id">
                                        <option value="" selected="selected">Please select</option>
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}"
                                            @if(!empty($stintRecord))
                                                {{$stintRecord->category_id == $category->id ? 'selected':'' }}
                                                    @endif
                                            >{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                    <p id="main_category-error" class="custome_error"></p>
                                </div>

                                <div class="form-group sub_category-container" style="">
                                    <label for="sting_subcat">Select Sub Categories:</label>
                                    <select class="form-control" name="sub_category_id" id="sub_category_id">
                                        {{--<option value="" selected="selected">Please select</option>--}}
                                        @foreach($subcategories as $subcategory)
                                            <option value="{{$subcategory->id}}" {{ $stintRecord->sub_category_id == $subcategory->id ? 'selected':'' }}>{{$subcategory->name}}</option>
                                        @endforeach
                                    </select>
                                    <p id="sting_subcat-error" class="custome_error"></p>
                                </div>

                                <div class="form-group">
                                    <label for="exampleFormControlTextarea1">Search Tags:</label>
                                    <!--<textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>-->
                                    <select id="select-tags" name="tags[]" multiple="multiple"
                                            class="demo-default selectized" style="width: 100%; display: none;"
                                            placeholder="Enter Tags..." tabindex="-1"></select>
                                    <div class="selectize-control demo-default multi" style="width: 100%;">
                                        <div class="selectize-input items not-full"><input type="select-multiple"
                                                                                           autocomplete="off"
                                                                                           tabindex=""
                                                                                           id="select-tags-selectized"
                                                                                           placeholder="Enter Tags..."
                                                                                           style="width: 74.5px;"></div>
                                        <div class="selectize-dropdown multi demo-default"
                                             style="display: none; width: 1110px; top: 54px; left: 0px;">
                                            <div class="selectize-dropdown-content"></div>
                                        </div>
                                    </div>
                                    <p id="select-tags-error" class="custome_error"></p>
                                    <p class="word-limit">Upto 5 items</p>
                                </div>
                                <div class="error-box step-2 hide1">
                                    <ul>
                                    </ul>
                                </div>
                                <div class="col-md-12 submit-cancle-btn">
                                    <a href="/stint/my-stints" class="ctm-btn cancle">Cancle</a>
                                    <button type="submit" class="ctm-btn submit stint-next" data-step="2" id="step1">
                                        Next Step
                                    </button>
                                    @csrf
                                    <input type="hidden" class="stint_slug" id="slug" name="slug" value="">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('styles')
    <style>
        label.error {
            color: red;
            margin-top: 5px;
            font-size: 12px;
        }

        .offer-post .post-all-offers .nav {
            width: 100%;
            background: #fff;
            position: unset;
        }

        .offer-post .post-all-offers .nav .nav-item .nav-link {
            padding: 0px 15px;
        }

        .offer-post .post-all-offers .nav .nav-item {
            border: none;
        }

        ul.nav-tabs li a:before {
            background: none;
        }

        .offer-post .post-all-offers .post-oofer-contant .stint-contant .form-group textarea,
        .offer-post .post-all-offers .post-oofer-contant .stint-contant .form-group select,
        .offer-post .post-all-offers .post-oofer-contant .stint-contant .form-group input {
            border: 1px solid #e5e5e5;
        }
    </style>
@endpush
@push('scripts')
    @include('front.stints.stints-js')
@endpush