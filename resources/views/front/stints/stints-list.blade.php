<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 5/11/19
 * Time: 1:59 AM
 */
?>
@extends('front.layouts.default')
@section('main_container')
    <section class="ctn banner">
        <div class="main-banner">
            <div class="banner-img">
                <!-- <div class="overlay-img"></div> -->
                <img src="/front/images/post-offer.png">
            </div>
            <div class="banner-contant">
                <h2 class="banner-title-2">Stint's List</h2>
                <!--                 <h5 class="banner-title-3">It is a long established fact that a reader will be distracted by the readable content of a  page when looking.</h5>
                 -->			</div>
        </div>
    </section>
    <section class="offer-post section-1">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12 post-all-offers">
                    <div class="tab-content post-oofer-contant" id="myTabContent">
                        <div class="priority-contant tab-pane fade show active" id="priority" role="tabpanel" aria-labelledby="priority-tab">
                            <div class="orders-format priority-orders">

                                <div class="text-right">
                                    <a href="/stint/create-stint" class="cmbtn cmbtn-sm mtop20imp mbot20imp">Create New</a>
                                </div>

                                <div class="table-responsive">
                                    <table id="mytable" class="table table-bordred">
                                        <thead>
                                            <tr>
                                                <th width="5%">#</th>
                                                <th width="15%">Category</th>
                                                <th width="50%">Stint</th>
                                                <th width="15%">Created</th>
                                                <th width="5%">Status</th>
                                                <th width="10%">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if($stints)
                                                <?php $index = 1; ?>
                                                @foreach($stints as $stint)
                                                    <tr>
                                                        <td>{{$index}}</td>
                                                        <td>{{$stint->category->name}}</td>
                                                        <td>
                                                            <a class="types" href="/stint/view/{{$stint->id}}/{{str_replace(' ', '-', strtolower($stint->title))}}" target="_blank"> {{$stint->title}}</a>
                                                        </td>
                                                        <td>{{date("F j, Y, g:i a", strtotime($stint->created_at))}}</td>
                                                        <td>{{$stint->status == 1 ? 'Active' : 'Draft'}}</td>
                                                        <td>
                                                            <a class="types" href="/stint/{{$stint->id}}/edit">
                                                                <i class="fa fa-edit" style="font-size:30px"></i>
                                                            </a>
                                                            <a class="types" href="/stint/{{$stint->id}}/delete" onclick="return confirm('are you sure to delete ? ');">
                                                                <i class="fa fa-trash" style="font-size:30px"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <?php $index++; ?>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="6"> <div class="alert alert-warning text-center"> No Stint Found, Create New by <a href="/stint/create-stint">Clicking Here</a></div></td>
                                                </tr>
                                            @endif

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
    @include('front.stints.stints-js')
@endpush