<script>
    $(function () {

        $("#title").blur(function () {
            $("#slug").val($this.val().split(' ').join('-'));
        });

        $("#category_id").change(function () {
            $.get('/category/find-subcategory/' + $(this).val(), function (res) {
                var html = '<option value="">Sub category</option>';
                for (key in res) {
                    html += '<option value="' + key + '">' + res[key] + '</option>';
                }
                $("#sub_category_id").html(html);
            });
        });

        $('.add-stint-faq').click(function () {
            $('.asked-que').append('<div class="faq-question" > \n' +
                '<input class="form-control" type="hidden" name="faqId[]" value="" placeholder="Question" /><br />\n ' +
                '<input class="form-control" type="text" name="faq_question[]" value="" placeholder="Question" /><br />\n ' +
                '<textarea name="faq_answer[]" class="form-control" placeholder="Answer" ></textarea> \n ' +
                '</div>');
            return false;
        });

        //Add requirement form
        $('.add-requirement').click(function () {
            $('.requirements').append('<div class="requirement">\n' +
                '<h6 class="title">REQUIREMENT #1</h6>\n' +
                ' <div class="form-group">\n' +
                ' <div class="form-group">\n' +
                '<input class="form-control" type="hidden" name="requirmentId[]" value="" placeholder="Question" />\n ' +
                ' <textarea name="requirement[]" class="form-control" placeholder="For example specifications, dimensions,' +
                ' brand guidelines, or background materials." rows="3"></textarea>\n' +
                ' </div>' +
                '<div class="custom-control1 custom-checkbox1">\n' +
                '<input type="checkbox" id="is_mandatory" name="is_mandatory[]" ' +
                'class="custom-control-input1 requirement-require" value ="1" ' +
                '>\n' +
                '<label class="custom-control-label1" for="">Answers is mandatory</label>\n' +
                ' </div>' +
                '</div>');
            return false;
        });

        $('.three-packages').click(function () {
            if ($(this).is(":checked")) {
                $('.packegaes').removeClass('one-package-only');
                $('.standard-pk, .premium-pk').slideDown();
            } else {
                $('.packegaes').addClass('one-package-only');
                $('.standard-pk, .premium-pk').slideUp();
            }
        });

        $("#create_stint_form").validate({
            rules: {
                title: {
                    required: true,
                    maxlength: 80,
                },
                category_id: {required: true},
                sub_category_id: {required: true},
            },
            submitHandler: function (form, e) {
                e.preventDefault();

                var fd = new FormData(form);
                $.ajax({
                    type: "post",
                    url: "{{ url('/stint/save_package') }}",
                    dataType: "json",
                    "headers": {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
                    data: fd,
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function () {
                        //$("#preview").fadeOut();
                        $("#err").fadeOut();
                        $(".submit").attr('disabled', true).html('<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function (res) {
                        toastr.success('success', 'Success!!', res.message);
                        window.location = '/stint/my-stint/' + res.data + '/pricing-plan';
                    },
                    error: function (data) {
                        toastr.error("Something went wrong");
                        $(".submit").attr('disabled', false).html('Next');
                    }
                });
            }
        });

        $("#pricingplan_form").validate({
            rules: {},
            submitHandler: function (form, e) {
                e.preventDefault();
                var fd = new FormData(form);
                <?php $stintId = empty($stint->id) ? '0' : $stint->id;?>
                $.ajax({
                    type: "post",
                    url: "{{ url('/stint/my-stint/'.$stintId.'/pricing-plan') }}",
                    dataType: "json",
                    "headers": {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
                    data: fd,
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function () {
                        //$("#preview").fadeOut();
                        $("#err").fadeOut();
                        $(".submit").attr('disabled', true).html('<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function (res) {
                        toastr.success('success', 'Success!!', res.message);
                        window.location = '/stint/my-stint/{{$stintId}}/details';
                    },
                    error: function (data) {
                        //alert("inside error");
                        toastr.error("Something went wrong");
                        $(".submit").attr('disabled', false).html('Next');
                    }
                });
            }
        });

        $("#descriptionForm").validate({
            ignore: [],
            rules: {
                cktext: {
                    description: true
                }
            },
            submitHandler: function (form, e) {
                e.preventDefault();
                var bla = $('#stint_description').val();
                //then get the form data
                <?php $stintId = empty($stint->id) ? '0' : $stint->id;?>
                var fd = new FormData(form);
                fd.append('stint_description', bla);
                $.ajax({
                    type: "post",
                    url: "{{ url('/stint/my-stint/'.$stintId.'/details') }}",
                    dataType: "json",
                    "headers": {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
                    data: fd,
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function () {
                        //$("#preview").fadeOut();
                        $("#err").fadeOut();
                        $(".submit").attr('disabled', true).html('<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function (res) {
                        toastr.success('success', 'Success!!', res.message);
                        window.location = '/stint/my-stint/{{$stintId}}/requirement';
                    },
                    error: function (data) {
                        //alert("inside error");
                        toastr.error("Something went wrong");
                        $(".submit").attr('disabled', false).html('Next');
                    }
                });
            }
        });

        $("#requirementForm").validate({
            rules: {
                requirement: {
                    required: true,
                },
            },
            submitHandler: function (form, e) {
                e.preventDefault();
                var fd = new FormData(form);
                // fd.append('is_mandatory', is_mandatoryValue);
                <?php $stintId = empty($stint->id) ? '0' : $stint->id;?>
                $.ajax({
                    type: "post",
                    url: "{{ url('/stint/my-stint/'.$stintId.'/requirement') }}",
                    dataType: "json",
                    "headers": {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
                    data: fd,
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function () {
                        //$("#preview").fadeOut();
                        $("#err").fadeOut();
                        $(".submit").attr('disabled', true).html('<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function (res) {
                        toastr.success('success', 'Success!!', res.message);
                        window.location = '/stint/my-stint/{{empty($stint->id) ? '0' : $stint->id}}/gallery';
                    },
                    error: function (data) {
                        //alert("inside error");
                        toastr.error("Something went wrong");
                        $(".submit").attr('disabled', false).html('Next');
                    }
                });
            }
        });

        $("#galleryForm").validate({
            rules: {
                requirement: {
                    required: true,
                },
            },
            submitHandler: function (form, e) {
                e.preventDefault();
                var fd = new FormData(form);
                // fd.append('is_mandatory', is_mandatoryValue);
                <?php $stintId = empty($stint->id) ? '0' : $stint->id ?>
                $.ajax({
                    type: "post",
                    url: "{{ url('/stint/my-stint/'.$stintId.'/gallery') }}",
                    dataType: "json",
                    "headers": {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
                    data: fd,
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function () {
                        //$("#preview").fadeOut();
                        $("#err").fadeOut();
                        $(".submit").attr('disabled', true).html('<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function (res) {
                        toastr.success('success', 'Success!!', res.message);
                        window.location = '/stint/my-stint/{{$stintId}}/publish';
                    },
                    error: function (data) {
                        //alert("inside error");
                        toastr.error("Something went wrong");
                        $(".submit").attr('disabled', false).html('Next');
                    }
                });
            }
        });

        $("#publish_form").validate({
            rules: {
            },
            submitHandler: function (form, e) {
                e.preventDefault();
                var fd = new FormData(form);
                // fd.append('is_mandatory', is_mandatoryValue);
                <?php $stintId = empty($stint->id) ? '0' : $stint->id ?>
                $.ajax({
                    type: "post",
                    url: "{{ url('/stint/my-stint/'.$stintId.'/publish') }}",
                    dataType: "json",
                    "headers": {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                    data: fd,
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function () {
                        //$("#preview").fadeOut();
                        $("#err").fadeOut();
                        $(".submit").attr('disabled', true).html('<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function (res) {
                        toastr.success('success', 'Success!!', res.message);
                        window.location = '/stint/my-stints';
                    },
                    error: function (data) {
                        //alert("inside error");
                        toastr.error("Something went wrong");
                        $(".submit").attr('disabled', false).html('Publish');
                    }
                });
            }
        });
    });

</script>