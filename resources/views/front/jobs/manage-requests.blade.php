<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 5/11/19
 * Time: 1:59 AM
 */
?>
@extends('front.layouts.default')
@section('main_container')
    <section class="dashboard-area">
        <div class="dashboard_contents pbot0">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="dashboard_title_area">
                            <div class="dashboard__title">
                                <h3 class="dblock">Manage Requests</h3>
                                <a href="/job/post-request" class="btn btn--sm seeall">Post A Request</a>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="joblist-wrap">
        <div class="container">
            <div class="m50">
                <div class="row">
                    <div class="error_message"></div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12">
                        <ul class="nav nav-tabs joblistTabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link {{$status*1 == 1 ? 'active' : ''}}" id="activejob-tab" data-toggle="tab" href="/job/manage-requests">
                                    Active <span class="badge badge-secondary">{{$active_job}}</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{$status*1 == 2 ? 'active' : ''}}" id="pause-tab" href="/job/manage-requests/2">
                                    Paused <span class="badge badge-secondary">{{$paused_job}}</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{$status*1 == 3 ? 'active' : ''}}" id="Pending-tab" href="/job/manage-requests/3">
                                    Pending <span class="badge badge-secondary">{{$pending_job}}</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{$status*1 == 4 ? 'active' : ''}}" id="Unapproved-tab" href="/job/manage-requests/4">
                                    Unapproved <span class="badge badge-secondary">{{$unapproved_job}}</span>
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content jobliststeptabs" id="myTabContent">
                            <div class="activejob-contant tab-pane fade active show pbot0 m50" id="activejob" role="tabpanel" aria-labelledby="activejob-tab">
                                <div class="job-list-box">
                                    <div class="db-new-main-table align-top js-db-table pdextratable">
                                        <table id="" class="table-striped">
                                            <thead>
                                                <tr class="header-filter">
                                                    <td colspan="6" class="js-filter-title">
                                                        @if($status == 1)
                                                            Active Jobs
                                                        @elseif($status == 2)
                                                            Paused Jobs
                                                        @elseif($status == 2)
                                                            Pending Jobs
                                                        @elseif($status == 4)
                                                            Unapproved Jobs
                                                        @endif
                                                    </td>
                                                </tr>
                                                <tr class="js-header-titles">
                                                    <td width="20%">Date</td>
                                                    <td width="40%">Request</td>
                                                    <td width="10%">Price</td>
                                                    <td width="10%">Offers</td>
                                                    <td width="10%">Action</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($job_requests as $job)
                                                <tr>
                                                    <td>{{$job->created_at}}</td>
                                                    <td>{{$job->job_box}}</td>
                                                    <td>{{$job->budget}}</td>
                                                    <td>{{count($job->job_proposals)}}</td>
                                                    <td>
                                                        <a href="/job/proposal-requests/{{$job->id}}" class="ctm-btn1 offer-btn">Job requests</a>
                                                        <select class="select--field h38" onchange="performAction('{{$job->id}}',this.value)">
                                                            <option value="">Select Action</option>
                                                            @if($status*1 == 1)
                                                                <option value="1">Pause</option>
                                                            @endif
                                                            @if($status*1 == 2)
                                                                <option value="3">Resume</option>
                                                            @endif
                                                            <option value="2">Delete</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
    @include('front.jobs.jobs-js')
@endpush