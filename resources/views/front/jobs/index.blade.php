@extends('front.layouts.default')
@section('main_container')
    <section class="section-1">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="dashboard_module single_item_visitor">
                        <div class="dashboard__title small-common-title">
                            <h4 class="mtop10">
                                My Feeds
                            </h4>
                            <div class="pull-right">
                                <div class="select-wrap">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                @foreach($jobs as $job)
                <div class="col-md-12 item-manag">
                    <div class="item-list">
                            <div class="item-contant">
                                <h4><a href="/job/description/{{$job->id}}" style="color: #0e73e6; font-weight: 900;">
                                      {{$job->job_box}} </a></h4>
                                <p>{{$job->description}}</p>
                                <div class="user-info">
                                    <div class="img">
                                        <img src="{{$job->user->avatar}}">
                                    </div>
                                    <a href="#"><h6>{{$job->user->first_name}}{{$job->user->last_name}}</h6></a>
                                </div>
                                <div class="clearfix"></div>
                                <div class="name-cate"><i class="fa fa-book" aria-hidden="true"></i>
                                {{$job->category->name}}</div>
                            </div>
                    </div>
                </div>
                    @endforeach
            </div>

            <div class="pagination-area pagination-area2 text-center">
                {{$jobs->links()}}
            </div>

        </div>

        </div>
    </section>
@endsection

@push('scripts')
    @include('front.jobs.jobs-js')
@endpush