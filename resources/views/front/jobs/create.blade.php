<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 5/11/19
 * Time: 1:59 AM
 */
?>
@extends('front.layouts.default')
@section('main_container')
        <?php
            $errors = $errors->getMessages();
        ?>
    <section class="offer-post section-1">
        <div class="container">
            <div class="m50">
                <div class="row">
                    <div class="error_message" style="display: none"></div>
                    <div class="col-md-10">
                        <div class="job-title">
                            <h2>Post A Request To The Seller Community</h2>
                        </div>
                        <form id="job-post" method="post">
                            <div class="job-common-white">
                                <div class="job-inner1">
                                    <div class="job-inTitle">
                                        <h3>Describe the service you're looking to purchase</h3>
                                    </div>
                                    <div class="job-form">
                                        <textarea class="job_box" id="job_box" placeholder="I'm looking for...." rows="5" name="job_box">{{old('job_box', '')}}</textarea>
                                        <span class="error">{{!empty($errors['job_box']) ? $errors['job_box'][0] : ''}}</span>
                                    </div>
                                </div>
                                <div class="job-inner1">
                                    <div class="job-inTitle">
                                        <h3>provide details as much as possible for your request:</h3>
                                    </div>
                                    <div class="job-form">
                                        <textarea class="job_box" id="description" placeholder="Some details about your job" rows="8" name="description">{{old('description', '')}}</textarea>
                                        <span class="error">{{!empty($errors['description']) ? $errors['description'][0] : ''}}</span>
                                    </div>
                                </div>
                                <div class="job-inner1">
                                    <div class="job-inTitle">
                                        <h3>Choose a category:</h3>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="job-form">
                                                <select class="job_box" id="category_id" name="category_id">
                                                    <option value="">Select Category</option>
                                                    @foreach($categories as $category)
                                                        <option {{old('job_box', '') == $category->id ? 'selected="selected"' : ''}} value="{{$category->id}}">{{$category->name}}</option>
                                                    @endforeach
                                                </select>
                                                <span class="error">{{!empty($errors['category_id']) ? $errors['category_id'][0] : ''}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="job-inner1">
                                    <div class="job-inTitle">
                                        <h3>Once you place your order, when would you like your service delivered?</h3>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-sm-12">
                                            <div class="job-form">
                                                <input type="text" placeholder="1 - 30days" id="delivery_time" class="job_box" name="delivery_time" value="{{old('delivery_time', '')}}">
                                                <span class="error">{{!empty($errors['delivery_time']) ? $errors['delivery_time'][0] : ''}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="job-inner1">
                                    <div class="job-inTitle">
                                        <h3>What is your budget for this service?</h3>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-sm-12">
                                            <div class="job-form">
                                                <small class="js-currency-symbol">$</small>
                                                <input type="text" placeholder="Enter budget" class="job_box pleft25" id="budget" name="budget" value="{{old('budget', '')}}">
                                                <span class="error">{{!empty($errors['budget']) ? $errors['budget'][0] : ''}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mtop30">
                                        <div class="col-md-12">
                                            <input type="hidden" name="user_id" id="user_id" value="{{Auth::user()->id}}" />
                                            @csrf
                                            <button type="submit" class="green-btn pull-right job-post-btn">Post</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection