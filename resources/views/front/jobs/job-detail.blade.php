<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 7/31/19
 * Time: 12:07 AM
 */
?>
@extends('front.layouts.default')
@section('main_container')
    <div class="course-syallbus-page">
        <section id="syallbus_area" class="ptop20 bgwhite mtop50">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                        <div class="syallbus_content_wrap">
                            <div class="course_desc_wrap ptop30">
                                <div class="course-title two-part">
                                    <div class="course_title dblock">
                                        <h2>{{$jobDetail->job_box}}</h2>
                                    </div>
                                    <div class="course_count dblock pull-right">
                                        <div class="co_video_count dblock">
                                            <h3><i class="fa fa-book"></i> {{$jobDetail->category->name}}
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="course_desc">
                                    <p>{{$jobDetail->description}}</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-bordered">
                                        <thead>
                                        <th>
                                            Estimate Time :
                                        </th>
                                        <th>
                                            Budget :
                                        </th>
                                        <th>
                                            job posted :
                                        </th>
                                        </thead>
                                        <tbody>
                                        <td>
                                            {{$jobDetail->delivery_time}}
                                        </td>
                                        <td>
                                            {{$jobDetail->budget}}
                                        </td>
                                        <td>
                                            {{$jobDetail->created_at->diffForHumans()}}
                                        </td>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <hr>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                        <div class="syallbus_sidebar">
                        </div>
                        <div class="product-in product-priceing" style="width: 100% !important;">
                            <div class="box">
                                <div class="total-sales">
                                    @if(count($proposalStatusForUser) == 0)
                                    <div class="purches-item">
                                        <a href="/job/proposals/{{$jobDetail->id}}" class="ctm-btn"> <i
                                                    class="fa fa-bell-slash" aria-hidden="true"></i>
                                            Submit a
                                            Proposal</a>
                                    </div>
                                    @endif
                                    <div class="product-inforamtion">
                                        <h2 class="title">Client Information</h2>
                                        <div class="profile-info">
                                            <div class="pro-img">
                                                <img src="{{$jobDetail->user->avatar}}">
                                            </div>
                                            <p class="name">
                                                <a href="/user-profile/{{$jobDetail->user->first_name}}"
                                                   target="_blank">
                                                    {{$jobDetail->user->first_name}}{{$jobDetail->user->last_name}} </a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
        </section>
    </div>
@endsection
@push('styles')
    <style>
        .purches-item {
            margin: auto !important;
            float: none;
            width: 85% !important;
            padding: 15px;
        }
    </style>

@endpush

