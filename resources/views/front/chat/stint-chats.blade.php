<div class="mesgs" id="mesgs" style="">
    <div class="msg_history" id="msg_history">
        @if(!empty($userChat))
            @foreach($userChat as $userChat)
                <div @if($userChat->sender_id == Auth::user()->id) class="outgoing_msg"
                     @else class="incoming_msg" @endif>

                    @if($userChat->sender_id == Auth::user()->id)
                        <div class="outgoing_msg">
                            <div class="sent_msg">
                                @if(($userChat->file_type == 'png' || $userChat->file_type == 'jpg' ||
                                 $userChat->file_type == 'svg' || $userChat->file_type == 'jpeg' && (!empty($userChat->file_type))
                                 &&(!empty($userChat->message))
                                 ))
                                    <img src="{{$userChat->file}}">
                                    {{$userChat->message}}
                                @elseif(($userChat->file_type == 'pdf' || $userChat->file_type == 'docx' ||
                                 $userChat->file_type == 'xls' && (!empty($userChat->file_type))
                                 && (!empty($userChat->message))
                                 ))
                                    <iframe src="{{$userChat->file}}"></iframe>
                                    {{$userChat->message}}

                                @elseif(($userChat->file_type == 'png' || $userChat->file_type == 'jpg' ||
                                 $userChat->file_type == 'svg' || $userChat->file_type == 'jpeg' && (!empty($userChat->file_type))
                                 &&(empty($userChat->message))
                                 ))
                                    <img src="{{$userChat->file}}">
                                @elseif(($userChat->file_type == 'pdf' || $userChat->file_type == 'docx' ||
                               $userChat->file_type == 'xls' && (!empty($userChat->file_type))
                               && (empty($userChat->message))
                               ))
                                    <iframe src="{{$userChat->file}}"></iframe>
                                @else
                                    <p>{{$userChat->message}}</p>
                                @endif
                                <span class="time_date"> {{date( 'M d Y, h:i A',strtotime($userChat->created_at))}}</span>
                            </div>
                        </div>
                    @else
                        <div class="incoming_msg_img">
                            <?php
                            $senderAvatar = \App\User::where('id', '=', $userChat->sender_id)->select('avatar', 'first_name')->first();
                            //                            dd($senderAvatar);
                            ?>
                            @if($senderAvatar->avatar == 0)
                                <img src="/front/default.png" alt="{{$senderAvatar->first_name}}">
                            @else
                                <img src="{{$senderAvatar->avatar}}" alt="{{$senderAvatar->first_name}}">
                            @endif
                        </div>
                        <div class="received_msg">
                            <div class="received_withd_msg">
                                @if(($userChat->file_type == 'png' || $userChat->file_type == 'jpg' ||
                                $userChat->file_type == 'svg' || $userChat->file_type == 'jpeg' && (!empty($userChat->file_type))
                                &&(!empty($userChat->message))
                                ))
                                    <img src="{{$userChat->file}}">
                                    {{$userChat->message}}
                                @elseif(($userChat->file_type == 'pdf' || $userChat->file_type == 'docx' ||
                                 $userChat->file_type == 'xls' && (!empty($userChat->file_type))
                                 && (!empty($userChat->message))
                                 ))
                                    <iframe src="{{$userChat->file}}"></iframe>
                                    {{$userChat->message}}
                                @elseif(($userChat->file_type == 'png' || $userChat->file_type == 'jpg' ||
                             $userChat->file_type == 'svg' || $userChat->file_type == 'jpeg' && (!empty($userChat->file_type))
                             &&(empty($userChat->message))
                             ))
                                    <img src="{{$userChat->file}}">
                                @elseif(($userChat->file_type == 'pdf' || $userChat->file_type == 'docx' ||
                               $userChat->file_type == 'xls' && (!empty($userChat->file_type))
                               && (empty($userChat->message))
                               ))
                                    <iframe src="{{$userChat->file}}"></iframe>
                                @else
                                    <p>{{$userChat->message}}</p>
                                @endif
                                <span class="time_date"> {{date( 'M d Y, h:i A',strtotime($userChat->created_at))}}</span>
                            </div>
                        </div>
                    @endif
                </div>
            @endforeach
        @endif

    </div>
    <div class="type_msg">
        <form action="/stint/chat/send-message" method="post" id="messageSend">
            @csrf
            <div class="input_msg_write">
                <input type="hidden" name="receiver" id="receiver" value="{{$reciverId}}">
                <input type="hidden" name="sender" id="sender" value="{{Auth::user()->id}}">
                <img src="/front/user-check-solid.svg" class="hireman" style="width: 24px;" data-toggle="modal" data-target="#myModal">
                <i class="fa fa-paperclip fa-2x imageupload" aria-hidden="true"></i>

                <input type="text" class="write_msg" name="message" placeholder="Type a message"/>

                <button class="msg_send_btn" type="submit"><i class="fa fa-paper-plane-o"
                                                              aria-hidden="true"></i></button>
                <div class="dropzone needsclick chat-file-upload hide1" id="chat-file-upload">
                    <div class="dz-message ">
                        <i class="fa fa-picture-o" aria-hidden="true"></i>
                        Drop files here or click to upload.<br>
                    </div>
                </div>
            </div>
        </form>

    </div>

    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Do you Want to Hire ?</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <form action="/job/hirefreelance" method="post" id="hirefreelance">
                    @csrf
                    <input type="hidden" name="freelance_id" id="freelance_id" value="{{$reciverId}}">
                    <input type="hidden" name="client_id" id="client_id" value="{{Auth::user()->id}}">
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" onclick="hireFreelance()" data-dismiss="modal">Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
            </div>

        </div>
    </div>
</div>