<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 7/15/19
 * Time: 1:31 PM
 */
?>

@extends('front.layouts.default')
@section('main_container')

    <div class="container">
        <h3 class=" text-center">Messaging</h3>
        <div class="messaging">
            <div class="inbox_msg" id="inbox_msg">
                <div class="inbox_people">
                    <div class="headind_srch">
                        <div class="recent_heading">
                            <h4>Recent</h4>
                        </div>
                        <div class="srch_bar">
                            <div class="stylish-input-group">
                                <input type="text" class="search-bar" placeholder="Search">
                                <span class="input-group-addon">
                <button type="button"> <i class="fa fa-search" aria-hidden="true"></i> </button>
                </span></div>
                        </div>
                    </div>
                    <div class="inbox_chat">
                        @foreach($userConversation as $userConversation)
                                @if(Auth::user()->id != $userConversation->receiver_id)
                            <a href="/stint/chat/{{$userConversation->receiver->first_name}}/{{$userConversation->receiver->id}}"> <div class="chat_list active_chat" id="chat_list">
                                <div class="chat_people">
                                    <div class="chat_img">
                                        @if($userConversation->receiver->avatar == 0)
                                            <img src="/front/default.png" alt="{{$userConversation->receiver->first_name}}">
                                        @else
                                            <img src="{{$userConversation->receiver->avatar}}" alt="{{$userConversation->receiver->first_name}}">
                                        @endif
                                    </div>
                                    <div class="chat_ib">
                                        <h5>{{$userConversation->receiver->first_name}} {{$userConversation->receiver->last_name}}
                                            <span class="chat_date">{{date("F j",strtotime($userConversation->updated_at))}}</span></h5>
                                        <p>{{$userConversation->message}}</p>
                                    </div>
                                </div>
                            </div>
                            </a>
                                    @elseif(Auth::user()->id != $userConversation->sender_id)
                                <a href="/stint/chat/{{$userConversation->sender->first_name}}/{{$userConversation->sender->id}}"> <div class="chat_list active_chat" id="chat_list">
                                        <div class="chat_people">
                                            <div class="chat_img"><img src="{{$userConversation->sender->avatar}}"
                                                                       alt="{{$userConversation->sender->first_name}}"></div>
                                            <div class="chat_ib">
                                                <h5>{{$userConversation->sender->first_name}}{{$userConversation->sender->last_name}}
                                                    <span class="chat_date">{{date("F j",strtotime($userConversation->updated_at))}}</span></h5>
                                                <p>{{$userConversation->message}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            @endif
                        @endforeach
                    </div>
                </div>
                @include('front.chat.stint-chats')
            </div>

        </div>
    </div>

@endsection
@push('styles')
    <style>
        textarea {
            min-height: 0px !important;
        }
        input.write_msg{
            /*position: relative;*/
        }
        i.fa.fa-paperclip {
            position: absolute;
            top: 18px;
            left: -10px;
        }
        .input_msg_write input{
            padding-left:40px
        }
        .hireman{
            position: absolute;
            top: 18px;
            left: 15px;
        }

    </style>
    <link rel="stylesheet" href="/front/css/chat.css">
@endpush
@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>
    <script>
        $(".imageupload").click(function(){
            // alert('here');
            $(".hide1").css( "display", "block" );
        });
        $(function () {

            $('#messageSend').on('submit', function (e) {
                $.ajaxSetup({
                    header: $('meta[name="csrf_token"]').attr('content')
                })
                e.preventDefault(e);

                $.ajax({

                    type: "POST",
                    url: '/stint/chat/send-message',
                    data: $(this).serialize(),
                    dataType: 'json',
                    success: function (res) {
                        toastr.success('success', res.message);
                        $('#msg_history').load(document.URL + ' #msg_history');
                        $("#messageSend").trigger('reset');
                        // alert(data.success);
                    },
                    error: function (res) {
                        //alert("inside error");
                        toastr.error("Message Sending Faild!");
                    }
                })
            });
        });
        Dropzone.autoDiscover = false;
        var uploadedDocumentMap = {}
        $(document).ready(function () {
            $("#chat-file-upload").dropzone
            ({
                maxFiles: 1,
                addRemoveLinks: true,
                method: "POST",
                url: "{{ url('/stint/chat/upload-file') }}",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
                success: function (file, response) {
                    // console.log(response);
                    $('form').append('<input type="hidden" name="uploadfiles" value="' + response.path + '">')
                    $('form').append('<input type="hidden" name="file_type" value="' + response.file_type + '">')
                    uploadedDocumentMap[file.path] = response.path
                }
            });
        });
        function hireFreelance()
        {
            var formData= $("#hirefreelance").serialize() // returns all the data in your form
            $.ajax({
                type: "POST",
                data: formData,
                url: "{{ url('/job/hirefreelance') }}",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
                success: function (res) {
                    toastr.success('success', 'Success!!', res.message);
                    window.location = '/job/proposal-requests';
                },
                error: function (data) {
                    toastr.error("Something went wrong");
                }
            });
        }
    </script>
    <script src="/front/js/chat.js"></script>
    {{--    @include('front.stints.stints-js')--}}
@endpush