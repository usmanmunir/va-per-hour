@extends('front.layouts.default')
@section('main_container')
    <section class="offer-post section-1 EditProfile">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="dashboard_module single_item_visitor">
                        <div class="dashboard__title pfull30">
                            <h4 class="mtop10">Pricing Plans</h4>
                            <div class="pull-right">
                                <div class="select-wrap">
                                    <select name="pricing_plans" class="period_selector" id="available_pricing_plans">
                                        <option value="">Select Pricing Plans</option>
                                        @foreach($memberships as $membership)
                                            <option value="{{$membership->id}}">{{$membership->title}}</option>
                                        @endforeach
                                    </select>
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @foreach($memberships as $membership)
                <div class="row bgtranspernt planes" id="row-{{$membership->id}}" style="display: none;">
                    <form action="/user/buy-membership/{{$membership->id}}" method="post">
                        <div class="top_pricing_box">
                            <div class="container flex-center">
                                <div class="col-md-12">
                                    <div class="pricing blue">
                                        <h4 class="pricing--title">{{$membership->title}}</h4>
                                        <div class="pricing--features">
                                            {!! $membership->description !!}
                                            @if(!empty($membership->variations) && empty($membership->users_membership->next_due_date))
                                                <div class="select-wrap mtop30">
                                                    <select name="pricing_plans" class="period_selector w100">
                                                        <option value="">Select Pricing plan</option>
                                                        @foreach($membership->variations as $variation)
                                                            <option value="{{$variation->id}}">${{$variation->price}} per {{$variation->month}} Month(s)</option>
                                                        @endforeach
                                                    </select>
                                                    <i class="fa fa-angle-down"></i>
                                                </div>
                                            @endif
                                            @if(empty($membership->users_membership->next_due_date))
                                                <input type="hidden" name="_token" value="{{csrf_token()}}" />
                                                <button type="submit" name="getplan" class="pricing--btn col-md-12">Get Plan</button>
                                            @else
                                                <p><strong>Next Due date: </strong> {{$membership->user_membership->next_due_date}}</p>
                                            @endif

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            @endforeach
        </div>
    </section>
@endsection

@push('styles')
    <style>
        .planes form { width: 100%;}
    </style>
@endpush

@push('scripts')
    <?php
    $data = ['memberships' => $memberships, 'userMemberships' => $user_memberships];
    ?>
    @include('front.memberships.membership-js', $data)
@endpush