@extends('front.layouts.default')
@section('main_container')
    <section class="offer-post section-1 EditProfile">
        <div class="container">
            <form action="/user/buy-membership/{{$membership->id}}/confirm" class="setting_form" method="post">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="information_module">
                            <div class="toggle_title">
                                <h4>Biling Information </h4>
                            </div>
                            <div class="information__set">
                                <div class="information_wrapper form--fields">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="first_name">First Name <sup>*</sup>
                                                </label>
                                                <input type="text" name="first_name" class="text_field" placeholder="John" value="{{Auth::user()->first_name}}">
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="last_name">last Name <sup>*</sup>
                                                </label>
                                                <input type="text" name="last_name" class="text_field" placeholder="Smith" value="{{Auth::user()->last_name}}">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end /.row -->

                                    <div class="form-group">
                                        <label for="email">Company Name <sup>*</sup>
                                        </label>
                                        <input type="text" name="company_name" class="text_field" placeholder="Company Name" value="{{Auth::user()->company}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="email1">Email Address <sup>*</sup>
                                        </label>
                                        <input type="email" name="email" class="text_field" placeholder="Email address" value="{{Auth::user()->email}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="country1">Country <sup>*</sup>
                                        </label>
                                        <div class="select-wrap select-wrap2">
                                            <select name="country" class="text_field" id="countryId">
                                                @foreach($countries as $country)
                                                    <option value="{{$country->id}}" {{ !empty($userLocation->country_id) && $country->id == $userLocation->country_id ? 'selected="selected"' : ''}}>{{$country->name}}</option>
                                                @endforeach
                                            </select>
                                            <span class="lnr lnr-chevron-down"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="country1">State <sup>*</sup>
                                        </label>
                                        <div class="select-wrap select-wrap2">
                                            <input type="state" name="state" class="text_field" placeholder="State" value="{{$userLocation ? $userLocation->state : ''}}">
                                            <span class="lnr lnr-chevron-down"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="country1">City <sup>*</sup>
                                        </label>
                                        <div class="select-wrap select-wrap2">
                                            <input type="state" name="city" class="text_field" placeholder="City Nanme" value="{{$userLocation ? $userLocation->city : ''}}">
                                            <span class="lnr lnr-chevron-down"></span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="address1">Address Line 1</label>
                                        <input type="text" name="address1" class="text_field" placeholder="Address line one" value="{{$userLocation ? $userLocation->address : ''}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="address2">Address Line 2</label>
                                        <input type="text" name="address2" class="text_field" placeholder="Address line two" value="{{$userLocation ? $userLocation->address2 : ''}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="zipcode">Zip / Postal Code
                                            <sup>*</sup>
                                        </label>
                                        <input type="text" name="zipcode" class="text_field" placeholder="zip/postal code" value="{{$userLocation ? $userLocation->zipcode : ''}}">
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="information_module order_summary mtop0">
                            <div class="toggle_title">
                                <h4>Order Summary</h4>
                            </div>
                            <ul>



                                <li class="item">
                                    <a href="#">{{$membership->title}}</a>
                                    <span>${{$variation ? $variation->price : $membership->price}}</span>
                                </li>

                                <li class="total_ammount">
                                    <p>Total</p>
                                    <span> ${{$variation ? $variation->price : $membership->price}} </span>
                                </li>

                            </ul>

                        </div>

                        <div class="information_module payment_options">
                            <div class="toggle_title">
                                <h4>Select Payment Method</h4>
                            </div>
                            <ul>
                                <li>
                                    <div class="custom-control custom-radio">
                                        <input type="radio" checked id="customRadio1" name="payment_method" class="custom-control-input" value="Upaycard" >

                                        <label class="custom-control-label" for="customRadio1">
                                            <img src="/uploads/images/icons/upaucard.png" width="100" alt="Visa Cards">
                                        </label>

                                        <label class="text-right"> &nbsp;&nbsp;&nbsp; if you don't have account yet!
                                            <a href="http://upaycardtracker.net//return/plainclick/campaigns_id/enc5cb754c603e8d/affiliate_id/976713/banner_id/1" target="_blank" class="btn-link">Register Now </a></label>
                                    </div>
                                </li>
                            </ul>

                            <div class="payment_info modules__content">
                                <div class="form-group">
                                    <label for="card_number">Username</label>
                                    <input name="upay_username" id="card_number" type="text" class="text_field" placeholder="Enter Upaycard username here...">
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="hidden" value="{{$variation ? $variation->id : 0}}" name="variation" />
                                        @csrf
                                        <button type="submit" class="btn btn-default btn-primary">Confirm Order</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
@endsection
