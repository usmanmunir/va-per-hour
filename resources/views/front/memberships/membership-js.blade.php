<script>
    var userMemberships = [];
    var memberships = [];
    @foreach($userMemberships as $membership)
            userMemberships.push({'user_id': '{{$membership->user_id}}', 'membership_id': '{{$membership->membership_id}}', 'next_due_date': '{{$membership->next_due_date}}' });
    @endforeach
    @foreach($memberships as $membership)
        memberships.push({'membership_id': '{{$membership->membership_id}}', 'title': '{{$membership->title}}' });
    @endforeach
    $(function () {
        selectFirstPlan();
        $("#available_pricing_plans").change(function () {
            $(".planes").hide();
            $("#row-"+$(this).val()).show();
        });

    });

    function selectFirstPlan() {
        var found =false;
        $(".period_selector").children('option').each(function () { $(this).removeAttr('selected'); });
        $(".period_selector").children('option').each(function () {
            for(var j=0; j<userMemberships.length; j++) {
                if(userMemberships[j].membership_id*1 === $(this).val()*1 && !found) {
                    found = true;
                    $(this).attr('selected', true);
                    //console.log("free plan");
                    $(".planes").hide();
                    $("#row-"+$(this).val()).show();
                    break;
                }
            }
        });
        if(!found) {
            $(".period_selector").children('option').each(function () {
                var text = '____'+$(this).text();
                if(text.indexOf('Free Plan') >= 0) {
                    $(this).attr('selected', true);
                    //console.log("free plan");
                    $(".planes").hide();
                    $("#row-"+$(this).val()).show();
                }
            });
        }
    }

</script>
