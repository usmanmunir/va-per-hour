<ul class="nav" id="myTab" role="tablist">
    <li class="nav-item">
        <a class="nav-link" href="{{url('/edit-profile')}}">Contact Info</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{url('/user/membership')}}">Membership</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{url('/user/profile-setting')}}">Profile
            setting</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{url('/user/get-paid')}}">Get Paid</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{url('/user/password-security')}}">Password &
            Security</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{url('/user/seller-profile')}}">Seller</a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="{{url('/user/instructor')}}">Instructor</a>
    </li>
</ul>