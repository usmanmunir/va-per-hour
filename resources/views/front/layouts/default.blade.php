<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">

    <!-- viewport meta -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="MartPlace - Complete Online Multipurpose Marketplace HTML Template">
    <meta name="keywords" content="marketplace, easy digital download, digital product, digital, html5">
    <meta name="csrf_token" content="{{ csrf_token() }}"/>


    <title>@yield('page_title', 'VAPERHOUR')</title>

    <!-- inject:css -->

    <link rel="stylesheet" href="/front/css/animate.css">
    <link rel="stylesheet" href="/front/css/font-awesome.min.css">
    <link rel="stylesheet" href="/front/css/fontello.css">
    <link rel="stylesheet" href="/front/css/jquery-ui.css">
    <link rel="stylesheet" href="/front/css/lnr-icon.css">
    <link rel="stylesheet" href="/front/css/owl.carousel.css">
    <link rel="stylesheet" href="/front/css/slick.css">
    <link rel="stylesheet" href="/front/css/trumbowyg.min.css">
    <link rel="stylesheet" href="/front/css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="/front/css/dropzone.min.css"/>
    <link rel="stylesheet" href="/front/style.css">
    <link rel="stylesheet" href="/front/css/style.css">
    <link rel="stylesheet" href="/front/css/dashboard.css">
    <link rel="stylesheet" href="/front/css/custom.css">
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">
    <style>
        label.error{
            margin-top: 5px !important;
            color:#FF0000 !important;
            font-weight: normal !important;
        }
    </style>
    <!-- endinject -->
@stack('styles')
<!-- Favicon -->
    <link rel="icon" type="/front/image/logo/favicon.png" sizes="16x16" href="images/favicon.png">
</head>

<body class="preload home1 mutlti-vendor">

<!-- ================================
START MENU AREA
================================= -->
<!-- start menu-area -->
<div class="menu-area">
    <!-- start .top-menu-area -->
    <div class="top-menu-area">
        <!-- start .container -->
        <div class="container">
            <!-- start .row -->
            <div class="row">
                <!-- start .col-md-3 -->
                <div class="col-lg-3 col-md-3 col-6 v_middle">
                    <div class="logo">
                        <a href="{{url('/')}}">
                            <img src="/front/images/logo/logo-1.png" alt="logo image" class="img-fluid">
                        </a>
                    </div>
                </div>
                <!-- end /.col-md-3 -->
                <!-- start .col-md-5 -->
                <div class="col-lg-8 offset-lg-1 col-md-9 col-6 v_middle">
                @if (session('module') == 'marketplace')
                    <div class="btn-seller">
                        <a class="nav-gray-btn" href="#">Become A Merchant</a>
                    </div>
                @elseif(session('module') == 'stint')
                    @if(session('role') == 'seller')
                        <div class="btn-seller">
                            <a class="nav-gray-btn" href="/switch/freelancer/stint/buyer">Switch to Buyer</a>
                        </div>
                    @else
                        <div class="btn-seller">
                            <a class="nav-gray-btn" href="/switch/freelancer/stint/seller">Switch to Seller</a>
                        </div>
                    @endif
                @endif
                <!-- start .author-area -->
                    <div class="author-area">
                        @if (Auth::guest())
                            <a href="{{url('signin')}}" class="author-area__seller-btn inline">Login/Signup</a>
                        @else
                            {{--<a href="signup.html" class="author-area__seller-btn inline">Become a Seller</a>--}}
                            <div class="author__notification_area">
                                <ul>
                                    <li class="has_dropdown">
                                        <div class="icon_wrap" onclick="loadAllNotifications()">
                                            <span class="lnr lnr-alarm"></span>
                                            <span class="notification_count noti"></span>
                                        </div>

                                        <div class="dropdowns notification--dropdown">

                                            <div class="dropdown_module_header">
                                                <h4>My Notifications</h4>
                                                <a href="/notifications">View All</a>
                                            </div>

                                            <div class="notifications_module">
                                                <div class="notification">
                                                    <div class="notification__info">
                                                        <div class="info_avatar">
                                                            <img src="/front/images/notification_head.png" alt="">
                                                        </div>
                                                        <div class="info">
                                                            <p>
                                                                <span>Anderson</span> added to Favourite
                                                                <a href="#">Mccarther Coffee Shop</a>
                                                            </p>
                                                            <p class="time">Just now</p>
                                                        </div>
                                                    </div>
                                                    <!-- end /.notifications -->

                                                    <div class="notification__icons ">
                                                        <span class="lnr lnr-heart loved noti_icon"></span>
                                                    </div>
                                                    <!-- end /.notifications -->
                                                </div>
                                                <!-- end /.notifications -->
                                            </div>
                                            <!-- end /.dropdown -->
                                        </div>
                                    </li>
                                    <li class="has_dropdown">
                                        <div class="icon_wrap">
                                            <span class="lnr lnr-envelope"></span>
                                            <span class="notification_count msg"></span>
                                        </div>

                                        <div class="dropdowns messaging--dropdown">
                                            <div class="dropdown_module_header">
                                                <h4>My Messages</h4>
                                                <a href="/messages/rooms">View All</a>
                                            </div>
                                            <div class="messages">

                                            </div>
                                        </div>
                                    </li>

                                    @if(session('mode') == 'marketplace' || session('mode') == 'elearning')
                                    <li class="has_dropdown">
                                        <div class="icon_wrap">
                                            <span class="lnr lnr-cart"></span>
                                            <span class="notification_count purch"></span>
                                        </div>

                                        <div class="dropdowns dropdown--cart">
                                            <div class="cart_area">
                                                <div class="cart_product">
                                                    <div class="product__info">
                                                        <div class="thumbn">
                                                            <img src="images/capro1.jpg" alt="cart product thumbnail">
                                                        </div>

                                                        <div class="info">
                                                            <a class="title" href="single-product.html">Finance and
                                                                Consulting Business Theme</a>
                                                            <div class="cat">
                                                                <a href="#">
                                                                    <img src="images/catword.png" alt="">Wordpress</a>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="product__action">
                                                        <a href="#">
                                                            <span class="lnr lnr-trash"></span>
                                                        </a>
                                                        <p>$60</p>
                                                    </div>
                                                </div>
                                                <div class="total">
                                                    <p>
                                                        <span>Total :</span>$80</p>
                                                </div>
                                                <div class="cart_action">
                                                    <a class="go_cart" href="cart.html">View Cart</a>
                                                    <a class="go_checkout" href="checkout.html">Checkout</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    @endif
                                </ul>
                            </div>
                            <!--start .author__notification_area -->

                            <!--start .author-author__info-->
                            <div class="author-author__info inline has_dropdown">
                                <div class="author__avatar">

                                    @if(file_exists(Auth::user()->avatar) || !empty(Auth::user()->avatar))
                                        <img width="45" src="{{Auth::user()->avatar}}"
                                             alt="{{Auth::user()->first_name}} {{Auth::user()->last_name}}">
                                    @else
                                        <img width="45" src="/uploads/images/default.svg"
                                             alt="{{Auth::user()->first_name}} {{Auth::user()->last_name}}">
                                    @endif
                                </div>
                                <div class="autor__info">
                                    <p class="name">
                                        {{Auth::user()->first_name}} {{Auth::user()->last_name}}
                                    </p>
                                    <p class="ammount">${{Auth::user()->amount || '0.00'}}</p>
                                </div>

                                <div class="dropdowns dropdown--author">
                                    <ul>
                                        <li>
                                            <a href="{{url('/user-profile')}}">
                                                <span class="lnr lnr-user"></span>Profile</a>
                                        </li>
                                        @if(session('module') == 'stint')
                                            @if(session('role') == 'buyer')
                                                <li>
                                                    <a href="/stint/chat"><span class="lnr lnr-dice"></span>Messages</a>
                                                </li>
                                                <li>
                                                    <a href="/user/orders"><span class="lnr lnr-dice"></span>Orders</a>
                                                </li>
                                                <li>
                                                    <a href="/job/manage-requests"><span class="lnr lnr-dice"></span>Manage Requests</a>
                                                </li>
                                            @endif
                                        @endif
                                        @if(session('module') == 'elearning')
                                            <li>
                                                <a href="{{url('/elearning/course/my-course')}}">
                                                    <span class="lnr lnr-dice"></span>My Courses</a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <span class="lnr lnr-cart"></span>Purchases</a>
                                            </li>
                                        @endif
                                        @if(session('module') == 'marketplace')
                                            <li>
                                                <a href="{{url('/marketplace/catalogue')}}">
                                                    <span class="lnr lnr-dice"></span>My Products</a>
                                            </li>
                                        @endif
                                        <li>
                                            <a href="{{ url('signout') }}">
                                                <span class="lnr lnr-exit"></span>Logout</a>
                                        </li>
                                        <div class="menu_divider"></div>
                                        <li>
                                            <a href="/switch/freelancer">
                                                <span class="lnr lnr-dice"></span>Freelancer</a>
                                        </li>
                                        <li>
                                            <a href="/switch/elearning">
                                                <span class="lnr lnr-dice"></span>E-learning</a>
                                        </li>
                                        <li>
                                            <a href="/switch/marketplace">
                                                <span class="lnr lnr-dice"></span>MarketPlace</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                    @endif
                    <!--end /.author-author__info-->
                    </div>
                    <!-- end .author-area -->

                    <!-- author area restructured for mobile -->
                    <div class="mobile_content ">
                        @if (Auth::guest())

                            <a href="{{url('signin')}}"
                               class="author-area__seller-btn inline mobbutton">Login/Signup</a>
                        @else
                            <span class="lnr lnr-user menu_icon"></span>

                            <!-- offcanvas menu -->
                            <div class="offcanvas-menu closed">
                                <span class="lnr lnr-cross close_menu"></span>
                                <div class="author-author__info">
                                    <div class="author__avatar v_middle">
                                        <img src="/front/images/usr_avatar.png" alt="user avatar">
                                    </div>
                                    <div class="autor__info v_middle">
                                        <p class="name">
                                            Jhon Doe
                                        </p>
                                        <p class="ammount">$20.45</p>
                                    </div>
                                </div>
                                <!--end /.author-author__info-->

                                <div class="author__notification_area">
                                    <ul>
                                        <li>
                                            <a href="notification.html">
                                                <div class="icon_wrap">
                                                    <span class="lnr lnr-alarm"></span>
                                                    <span class="notification_count noti"></span>
                                                </div>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="message.html">
                                                <div class="icon_wrap">
                                                    <span class="lnr lnr-envelope"></span>
                                                    <span class="notification_count msg"></span>
                                                </div>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="cart.html">
                                                <div class="icon_wrap">
                                                    <span class="lnr lnr-cart"></span>
                                                    <span class="notification_count purch"></span>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <!--start .author__notification_area -->

                                <div class="dropdowns dropdown--author">
                                    <ul>
                                        <li>
                                            <a href="{{url('/user-profile')}}">
                                                <span class="lnr lnr-user"></span>Profile</a>
                                        </li>
                                        @if(session('module') == 'stint')
                                            @if(session('role') == 'buyer')
                                                <li>
                                                    <a href="/stint/chat"><span class="lnr lnr-dice"></span>Messages</a>
                                                </li>
                                                <li>
                                                    <a href="/stint/manage-orders"><span class="lnr lnr-dice"></span>Orders</a>
                                                </li>
                                                <li>
                                                    <a href="/stint/manage-requests"><span class="lnr lnr-dice"></span>Manage Requests</a>
                                                </li>
                                            @endif
                                        @endif
                                        @if(session('module') == 'elearning')
                                            <li>
                                                <a href="{{url('/elearning/course/my-course')}}">
                                                    <span class="lnr lnr-dice"></span>My Courses</a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <span class="lnr lnr-cart"></span>Purchases</a>
                                            </li>
                                        @endif
                                        @if(session('module') == 'marketplace')
                                            <li>
                                                <a href="{{url('/marketplace/catalogue')}}">
                                                    <span class="lnr lnr-dice"></span>My Products</a>
                                            </li>
                                        @endif
                                        <li>
                                            <a href="{{ url('signout') }}">
                                                <span class="lnr lnr-exit"></span>Logout</a>
                                        </li>
                                        <div class="menu_divider"></div>
                                        <li>
                                            <a href="/switch/freelancer">
                                                <span class="lnr lnr-dice"></span>Freelancer</a>
                                        </li>
                                        <li>
                                            <a href="/switch/elearning">
                                                <span class="lnr lnr-dice"></span>E-learning</a>
                                        </li>
                                        <li>
                                            <a href="/switch/marketplace">
                                                <span class="lnr lnr-dice"></span>MarketPlace</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        @endif
                    </div>
                    <!-- end /.mobile_content -->
                </div>
                <!-- end /.col-md-5 -->
            </div>
            <!-- end /.row -->
        </div>
        <!-- end /.container -->
    </div>
    <!-- end  -->

    <!-- start .mainmenu_area -->
    <div class="full-widht bottom-menu">
        <!-- start .container -->
        <div class="container">
            <!-- start .row-->
            <div class="row">
                <!-- start .col-md-12 -->
                <div class="col-md-12">
                    <nav class="navbar navbar-expand-lg navbar-light bg-light">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                                aria-controls="navbarNav" aria-expanded="false"
                                aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        @if (session('module') == 'marketplace')
                            <div class="collapse navbar-collapse" id="navbarNav">
                                <ul class="navbar-nav">
                                    @foreach(\App\Models\Category::where('category_type','=','marketplace')->get() as $category)
                                        <li class="nav-item">
                                            <a class="nav-link" href="/marketplace/category/{{$category->id}}/{{str_replace(" ", "-",$category->slug)}}">{{$category->name}}</a>
                                        </li>
                                    @endforeach
                                    <li class="nav-item">
                                        <a class="nav-link" href="/marketplace/category/all">View All</a>
                                    </li>
                                </ul>
                            </div>

                        @elseif(session('module') == 'elearning')
                            <div class="collapse navbar-collapse" id="navbarNav">
                                <ul class="navbar-nav">
                                    @foreach(\App\Models\Category::where('category_type','=','course')->get() as $category)
                                        <li class="nav-item">
                                        <a class="nav-link" href="/elearning/course/category/{{$category->id}}/{{str_replace(" ", "-",$category->slug)}}">{{$category->name}}</a>
                                        </li>
                                    @endforeach
                                    <li class="nav-item">
                                        <a class="nav-link" href="/elearning/course/category">View All</a>
                                    </li>
                                </ul>
                            </div>
                        @elseif(session('mode') == 'freelancer' && (session('role') == 'buyer'))
                        <!-- Collect the nav links, forms, and other content for toggling -->

                            <div class="collapse navbar-collapse" id="navbarNav">
                                <ul class="navbar-nav">
                                    @foreach(\App\Models\Category::where('category_type','=','stint')->get() as $category)
                                        @if(session('module') == 'stint')
                                            <li class="nav-item">
                                            <a class="nav-link" href="/stint/category/{{$category->id}}/{{str_replace(" ", "-",$category->slug)}}">{{$category->name}}</a>
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                            </div>
                        @elseif(session('mode') == 'freelancer' && (session('role') == 'seller'))
                        <!-- Collect the nav links, forms, and other content for toggling -->

                            <div class="collapse navbar-collapse" id="navbarNav">
                                <ul class="navbar-nav">
                                    @if(session('role') == 'seller')
                                        <li class="nav-item">
                                            <a class="nav-link" href="/stint/dashboard">Dashboard</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="/stint/chat">Messages</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="/users/manage-orders">Orders</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="/stint/my-stints">Stints</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="/stint/analytics">Analytics</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="/stint/analytics">Earnings</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="/job/buyer-requests">Buyer Requests</a>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        @else
                            <div class="collapse navbar-collapse" id="navbarNav">
                                <ul class="navbar-nav">
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">Programming &amp; Tech</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">WordPress</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">Web Programming</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">Digital Marketing</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">SEO</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">Web Anyaltics</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">Graphic Design</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">Logo Design</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">Test1</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">E-Marketing</a>
                                    </li>
                                </ul>
                            </div>
                            <!-- /.navbar-collapse -->
                        @endif
                    </nav>
                </div>
                <!-- end /.col-md-12 -->
            </div>
            <!-- end /.row-->
        </div>
        <!-- start .container -->
    </div>
    <!-- end /.mainmenu-->
</div>
<!-- end /.menu-area -->
<!--================================
END MENU AREA
=================================-->

@yield('main_container')
<!--================================
 START FOOTER AREA
=================================-->
<footer class="footer-area">
    <div class="footer-big section--padding">
        <!-- start .container -->
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="info-footer">
                        <div class="info__logo">
                            <img src="/front/images/logo/logo-2.png" alt="footer logo">
                        </div>
                        <p class="info--text">Nunc placerat mi id nisi interdum they mollis. Praesent pharetra, justo ut
                            scel erisque the mattis,
                            leo quam.</p>
                        <ul class="info-contact">
                            <li>
                                <span class="lnr lnr-phone info-icon"></span>
                                <span class="info">Phone: +6789-875-2235</span>
                            </li>
                            <li>
                                <span class="lnr lnr-envelope info-icon"></span>
                                <span class="info">support@aazztech.com</span>
                            </li>
                            <li>
                                <span class="lnr lnr-map-marker info-icon"></span>
                                <span class="info">202 New Hampshire Avenue Northwest #100, New York-2573</span>
                            </li>
                        </ul>
                    </div>
                    <!-- end /.info-footer -->
                </div>
                <!-- end /.col-md-3 -->

                <div class="col-lg-5 col-md-6">
                    <div class="footer-menu">
                        <h4 class="footer-widget-title text--white">Our Company</h4>
                        <ul>
                            <li>
                                <a href="#">How to Join Us</a>
                            </li>
                            <li>
                                <a href="#">How It Work</a>
                            </li>
                            <li>
                                <a href="#">Buying and Selling</a>
                            </li>
                            <li>
                                <a href="#">Testimonials</a>
                            </li>
                            <li>
                                <a href="#">Copyright Notice</a>
                            </li>
                            <li>
                                <a href="#">Refund Policy</a>
                            </li>
                            <li>
                                <a href="#">Affiliates</a>
                            </li>
                        </ul>
                    </div>
                    <!-- end /.footer-menu -->

                    <div class="footer-menu">
                        <h4 class="footer-widget-title text--white">Help and FAQs</h4>
                        <ul>
                            <li>
                                <a href="#">How to Join Us</a>
                            </li>
                            <li>
                                <a href="#">How It Work</a>
                            </li>
                            <li>
                                <a href="#">Buying and Selling</a>
                            </li>
                            <li>
                                <a href="#">Testimonials</a>
                            </li>
                            <li>
                                <a href="#">Copyright Notice</a>
                            </li>
                            <li>
                                <a href="#">Refund Policy</a>
                            </li>
                            <li>
                                <a href="#">Affiliates</a>
                            </li>
                        </ul>
                    </div>
                    <!-- end /.footer-menu -->
                </div>
                <!-- end /.col-md-5 -->

                <div class="col-lg-4 col-md-12">
                    <div class="newsletter">
                        <h4 class="footer-widget-title text--white">Newsletter</h4>
                        <p>Subscribe to get the latest news, update and offer information. Don't worry, we won't send
                            spam!</p>
                        <div class="newsletter__form">
                            <form action="#">
                                <div class="field-wrapper">
                                    <input class="relative-field rounded" type="text" placeholder="Enter email">
                                    <button class="btn btn--round" type="submit">Submit</button>
                                </div>
                            </form>
                        </div>

                        <!-- start .social -->
                        <div class="social social--color--filled">
                            <ul>
                                <li>
                                    <a href="#">
                                        <span class="fa fa-facebook"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="fa fa-twitter"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="fa fa-google-plus"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="fa fa-pinterest"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="fa fa-linkedin"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="fa fa-dribbble"></span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <!-- end /.social -->
                    </div>
                    <!-- end /.newsletter -->
                </div>
                <!-- end /.col-md-4 -->
            </div>
            <!-- end /.row -->
        </div>
        <!-- end /.container -->
    </div>
    <!-- end /.footer-big -->

    <div class="mini-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="copyright-text">
                        <p>&copy; 2019
                            <a href="#">MartPlace</a>. All rights reserved. Created by
                            <a href="#">AazzTech</a>
                        </p>
                    </div>

                    <div class="go_top">
                        <span class="lnr lnr-chevron-up"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<!--================================
END FOOTER AREA
=================================-->

<!--//////////////////// JS GOES HERE ////////////////-->

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA0C5etf1GVmL_ldVAichWwFFVcDfa1y_c"></script>
<!-- inject:js -->
<script src="/front/js/vendor/jquery/jquery-1.12.3.js"></script>
<script src="https://js.pusher.com/4.4/pusher.min.js"></script>
<script src="/front/js/vendor/jquery-ui.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script src="/front/js/vendor/jquery/popper.min.js"></script>
<script src="/front/js/vendor/jquery/uikit.min.js"></script>
<script src="/front/js/vendor/bootstrap.min.js"></script>
<script src="/front/js/vendor/chart.bundle.min.js"></script>
<script src="/front/js/vendor/grid.min.js"></script>

<script src="/front/js/vendor/jquery.barrating.min.js"></script>
<script src="/front/js/vendor/jquery.countdown.min.js"></script>
<script src="/front/js/vendor/jquery.counterup.min.js"></script>
<script src="/front/js/vendor/jquery.easing1.3.js"></script>
<script src="/front/js/vendor/owl.carousel.min.js"></script>
<script src="/front/js/vendor/slick.min.js"></script>
<script src="/front/js/vendor/tether.min.js"></script>
<script src="/front/js/vendor/trumbowyg.min.js"></script>
<script src="/front/js/vendor/waypoints.min.js"></script>
{{--<script src="/front/js/dropzone.min.js"></script>--}}
<script src="/front/js/dashboard.js"></script>
<script src="/front/js/main.js"></script>
<script src="/front/js/jquery.validate.min.js"></script>
<script src="/front/js/additional-methods.min.js"></script>
<script src="/front/js/custom.js"></script>
<script src="//cdn.ckeditor.com/4.11.2/standard/ckeditor.js"></script>
<script src="/front/js/multickeditor.js"></script>
<script src="/front/js/map.js"></script>
<script src="/front/imageupload/js/vendor/jquery.ui.widget.js"></script>
<script src="/front/imageupload/js/jquery.iframe-transport.js"></script>
<script src="/front/imageupload/js/jquery.fileupload.js"></script>

<!-- endinject -->
<script>@if(Session::has('message'))
    var type = "{{ Session::get('alert-type', 'info') }}";
    switch (type) {
        case 'info':
            toastr.info("{{ Session::get('message') }}");
            break;

        case 'warning':
            toastr.warning("{{ Session::get('message') }}");
            break;

        case 'success':
            toastr.success("{{ Session::get('message') }}");
            break;

        case 'error':
            toastr.error("{{ Session::get('message') }}");
            break;
    }
    @endif
    $(function () {
        $(".notification_count.msg, .notification_count.noti").hide();
        var current = location.pathname;
        $('#myTab li a').each(function () {
            var $this = $(this);
            // if the current path is like this link, make it active
            if ($this.attr('href').indexOf(current) !== -1) {
                $this.addClass('active');
            }
        });
    });

    var pusher = new Pusher('af4e4db553274d1ed5b8', {
        cluster: 'mt1'
    });
    @if(!Auth::guest())
    var channel = pusher.subscribe('{{md5(Auth::user()->id)}}');

    channel.bind('conversations', function(data) {
        $(".notification_count.msg").text(Number($(".notification_count.msg").text())+1).show();
        loadConversations(10);
    });
    channel.bind('notifications', function(data) {
        $(".notification_count.noti").text(Number($(".notification_count.noti").text())+1).show();
    });
    @endif
</script>
@stack('scripts')
</body>

</html>
