<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 5/11/19
 * Time: 1:59 AM
 */
?>
@extends('front.layouts.default')
@section('main_container')
    <section class="dashboard-area">

        <div class="dashboard_contents pbot0">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="dashboard_title_area">
                            <div class="dashboard__title">
                                <h3 class="dblock">View Offers ({{$request->offers_count}} offers )</h3>
                                <a href="http://dev.vaperhour.coddrule.com/job/manage_requests" class="btn btn--sm seeall">Back to Manage Requests</a>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <!-- end /.col-md-12 -->
                </div>
                <!-- end /.row -->
            </div>
        </div>
    </section>
    <section class="joblist-wrap">
        <div class="container">
            <div class="m50 mtop25imp">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12">
                        <div class="col-xs-12">
                            <div class="sfreeelancer-list offer_jobown">
                                <div class="sfl-box">
                                    <div class="sfl-image">
                                        <a href=""> <img src="http://dev.vaperhour.coddrule.com/assets/images/test1.jpg"></a>
                                    </div>
                                    <div class="sfi-desc-wrap">
                                        <div class="sfi-title">
                                            <h2><a href="">My Request</a></h2>
                                        </div>
                                        <div class="ojobo m10">
                                            <i class="fa fa-clock-o" aria-hidden="true"></i> {{$request->duration}} days &nbsp; | &nbsp; Budget : ${{$request->budget}}                                  </div>

                                        <div class="sfi-desc">
                                            <p>{{$request->description}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="main-jobofferbox">
                    @if($offers)
                        @foreach($offers as $offer)
                        <div class="row seller-offer" id="offer-{{csrf_token().$offer->id}}">
                            <div class="col-md-12">
                                <div class="offerlist-box">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="offerjb_stint">
                                                <a href="/stint/view/{{$offer->stint->id}}/{{str_replace(' ', '-', strtolower($offer->stint->title))}}" target="_blank">
                                                    <img src="{{$offer->stint->gallery[0]->image ?? '/uploads/images/default.png'}}" class="img img-responsive" alt="">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="offerjb-profile-wrap">
                                                <div class="offerjb-imagebox">
                                                    <div class="offerjb-image">
                                                        <a href="/user-profile/{{csrf_token().$offer->user->id}}" target="_blank"> <img src="{{!empty($offer->user->avatar) ? $offer->user->avatar : '/uploads/images/default.png'}}"> </a>
                                                    </div>
                                                    <div class="offerjb-content">
                                                        <h2><a href="/user-profile/{{csrf_token().$offer->user->id}}" target="_blank">{{$offer->user->first_name. ' '. $offer->user->last_name}}</a></h2>
                                                    </div>
                                                </div>
                                                <div class="offerjb-price">
                                                    <h2>${{$offer->amount}}</h2>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="offerjb-contentwrap mtop10">
                                                <p>{{$offer->proposal}}</p>
                                            </div>
                                            <div class="ojobo m10">
                                                <i class="fa fa-clock-o" aria-hidden="true"></i>  {{$offer->delivery_time}} days delivery
                                            </div>
                                            <div class="two-button-right pull-right">
                                                @if($offer->status == 1)
                                                    <a href="javascript:;" data-offer="{{csrf_token().$offer->id}}" data-url="/job/offer/{{csrf_token().$offer->id}}/remove" class="btn btn-danger remove-offer">Remove offer</a>
                                                    <a href="javascript:;" data-offer="{{csrf_token().$offer->id}}" data-url="/job/offer/{{csrf_token().$offer->id}}/order" class="btn btn-primary start-order">Order Now</a>
                                                @else
                                                    <a href="javascript:;" class="btn btn-primary btn-default">Order is in process</a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    @else
                        <div class="row">
                            <div class="col-sm-12">No Offer Found so far</div>
                        </div>
                    @endif
                </div>
            </div>

        </div>
    </section>
@endsection
@push('styles')
    <style>
        .offer-post .post-all-offers .nav {
            width: 100%;
            background: #fff;
            position: unset;
        }
        .offer-post .post-all-offers .nav .nav-item {
            border: none;
        }
        .offer-post .post-all-offers .nav .nav-item .nav-link {
            padding: 0px 15px;
        }
    </style>
@endpush
@push('scripts')
    <script>
        $(function(){
            $(".remove-offer").click(function(){
                var ele = $(this);
                $.ajax({
                    url: ele.attr('data-url'),
                    data: {'_token': '{{csrf_token()}}'},
                    method: 'DELETE',
                    success: function(result){
                        $('#offer-'+ele.attr('data-offer')).remove();
                        toastr.success(result.message)
                    },
                    error: function (err) {
                        console.log(err);
                        toastr.error( result.message)
                    }
                });
            });
            $(".start-order").click(function(){
                var ele = $(this);
                $.ajax({
                    url: ele.attr('data-url'),
                    data: {'_token': '{{csrf_token()}}'},
                    method: 'post',
                    success: function(result){
                        $('#offer-'+ele.attr('data-offer')).remove();
                        toastr.success(result.message)
                    },
                    error: function (err) {
                        console.log(err);
                        toastr.error( result.message)
                    }
                });
            });

        })
    </script>
@endpush