<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 5/11/19
 * Time: 1:59 AM
 */
?>
@extends('front.layouts.default')
@section('main_container')
    <section class="dashboard-area">
        <div class="dashboard_contents pbot0">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="dashboard_title_area">
                            <div class="dashboard__title">
                                <h3 class="dblock">Manage Requests</h3>
                                <a href="http://dev.vaperhour.coddrule.com/job/job-post" class="btn btn--sm seeall">Post A Request</a>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="joblist-wrap dashboard-area">
        <div class="container">
            <div class="m50">
                <div class="row">
                    <div class="error_message"></div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12">
                        <ul class="nav nav-tabs joblistTabs">
                            <li class="nav-item">
                                <a class="nav-link {{$status == 3 ? 'active' : ''}}" href="/job/manage-requests/3"  aria-controls="activejob" aria-selected="true"> Active</a>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link {{$status == 2 ? 'active' : ''}}" href="/job/manage-requests/2" aria-controls="pausejob" aria-selected="false">Paused</a>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link {{$status == 1 ? 'active' : ''}}" href="/job/manage-requests/1" aria-controls="Pending" aria-selected="false">Pending</a>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link {{$status == 4 ? 'active' : ''}}" href="/job/manage-requests/4" aria-controls="Unapproved" aria-selected="false">Unapproved</a>
                            </li>
                        </ul>
                        <div class="tab-content jobliststeptabs" id="myTabContent">
                            <div class="activejob-contant tab-pane fade pbot0 m50 active show" id="activejob" role="tabpanel" aria-labelledby="activejob-tab">
                                <div class="job-list-box">
                                    <div class="db-new-main-table align-top js-db-table pdextratable">
                                        <table id="mytable" class="table table-stripped">
                                            <thead>
                                            <tr class="header-filter">
                                                <td colspan="6" class="js-filter-title">
                                                    @if($status == 1)
                                                        Pending Jobs
                                                    @elseif($status == 2)
                                                        Paused Jobs
                                                    @elseif($status == 3)
                                                        Active Jobs
                                                    @elseif($status == 4)
                                                        Unapproved Jobs
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr class="js-header-titles">
                                                <td width="20%">Date</td>
                                                <td width="40%">Request</td>
                                                <td width="10%">Price</td>
                                                <td width="10%">Offers</td>
                                                <td width="10%">Action</td>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            @if(count($jobRequests) > 0)
                                                <?php $index = 1; ?>
                                                @foreach($jobRequests as $request)
                                                    <tr>
                                                        <td>{{$request->created_at}}</td>
                                                        <td>{{substr($request->description, 0, 250)}}</td>
                                                        <td>{{$request->budget}}</td>
                                                        <td>
                                                            <a class="ctm-btn dblock" href="/job/request/{{csrf_token().$request->id}}/view/offer">
                                                                {{$request->offers_count}} Offers
                                                            </a>
                                                        </td>
                                                        <td>
                                                            @if($status == 2 || $status == 3)
                                                                <a class="types" href="/stint/edit-request/{{csrf_token().$request->id}}"><i class="fa fa-pencil"></i> </a>
                                                                @if($request->status == 3)
                                                                    <a class="types" href="/stint/pause-request/{{csrf_token().$request->id}}"><i class="fa fa-pause"></i> </a>
                                                                @else
                                                                    <a class="types" href="/stint/activate-request/{{csrf_token().$request->id}}"><i class="fa fa-play"></i> </a>
                                                                @endif
                                                            @endif
                                                            <a class="types" href="/stint/delete-request/{{csrf_token().$request->id}}"><i class="fa fa-trash"></i> </a>
                                                        </td>
                                                    </tr>
                                                    <?php $index++; ?>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="6"> <div class="alert alert-warning text-center"> No requests found.</div></td>
                                                </tr>
                                            @endif

                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('styles')
    <style>
        .offer-post .post-all-offers .nav {
            width: 100%;
            background: #fff;
            position: unset;
        }
        .offer-post .post-all-offers .nav .nav-item {
            border: none;
        }
        .offer-post .post-all-offers .nav .nav-item .nav-link {
            padding: 0px 15px;
        }
    </style>
@endpush