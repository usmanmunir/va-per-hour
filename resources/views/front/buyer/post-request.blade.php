<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 5/11/19
 * Time: 1:59 AM
 */
?>
@extends('front.layouts.default')
@section('main_container')
    <section class="ctn banner">
        <div class="main-banner">
            <div class="banner-img">
                <!-- <div class="overlay-img"></div> -->
                <img src="/front/images/post-offer.png">
            </div>
            <div class="banner-contant">
                <h2 class="banner-title-2">POST NEW REQUESt</h2>
            </div>
        </div>
    </section>
    <section class="offer-post section-1 stintCreate-wrap">

        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12 post-all-offers">
                    <form enctype="multipart/form-data" class="stint-form" method="post" id="create_request_form"
                          action="/stint/post-request">
                        <div class="tab-content post-oofer-contant" id="myTabContent">
                            <div class="stint-contant tab-pane active show" id="title" role="tabpanel"
                                 aria-labelledby="title-tab">
                                <div class="form-group">
                                    <label for="exampleFormControlTextarea1">Describe the service you're looking to purchase - please be as detailed as possible:</label>
                                    <textarea name="description" class="form-control" id="c"
                                              rows="3"> {{(!empty($request)? $request->description : '')}}</textarea>
                                    <p id="exampleFormControlTextarea1-error" class="custome_error"></p>
                                    <p class="word-limit">5/1500 Max</p>
                                    <input type="hidden" value="{{(!empty($request->id)? $request->id : '')}}"
                                           name="id">
                                </div>
                                <div class="form-group">
                                    <label for="main_category">Choose a category:</label>
                                    <select class="form-control" name="category_id" id="category_id">
                                        <option value="">Choose Category</option>
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}"
                                            @if(!empty($request))
                                                {{$request->category_id == $category->id ? 'selected':'' }}
                                                    @endif
                                            >{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                    <p id="main_category-error" class="custome_error"></p>
                                </div>

                                <div class="form-group sub_category-container" style="">
                                    <label for="sting_subcat">Choose Sub Categories:</label>
                                    <select class="form-control" name="sub_category_id" id="sub_category_id">
                                        <option value="">Choose Sub Category</option>
                                        @if(!empty($subcategories))
                                            @foreach($subcategories as $subcategory)
                                                <option value="{{$subcategory->id}}" {{ $request->sub_category_id == $subcategory->id ? 'selected':'' }}>{{$subcategory->name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>

                                <div class="form-group sub_category-container" style="">
                                    <label for="sting_subcat">Choose Request Type:</label>
                                    <select class="form-control" name="type" id="type">
                                        <option value="1" {{!empty($request->type) && $request->type == 1 ? 'selected="selected"' : ''}}>Fixed</option>
                                        <option value="2" {{!empty($request->type) && $request->type == 2 ? 'selected="selected"' : ''}}>Hourly</option>
                                    </select>
                                </div>

                                <div class="form-group sub_category-container" style="">
                                    <label for="sting_subcat">Once you place your order, when would you like your service delivered?</label>
                                    <input name="duration" id="duration" class="form-control" placeholder="1-2 hours/days" value=" {{!empty($request->duration) ? $request->duration : ''}}"/>
                                </div>

                                <div class="form-group sub_category-container" style="">
                                    <label for="sting_subcat">What is your budget for this service?</label>
                                    <input name="budget" id="budget" class="form-control" placeholder="Enter Budget" value="{{!empty($request->budget) ? $request->budget : ''}}" />
                                </div>

                                <div class="col-md-12 submit-cancle-btn">
                                    <a href="/stint/manage-requests" class="ctm-btn cancle">Cancel</a>
                                    <button type="submit" class="ctm-btn submit stint-next" data-step="2" id="submit">
                                        submit
                                    </button>
                                    @csrf
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('scripts')
    @include("front.buyer.buyer-js")
@endpush