<script>
    $(function () {
        $("#category_id").change(function () {
            console.log($(this).val());
            $.get('/category/find-subcategory/'+$(this).val(), function (res) {
                $("#sub_category_id").html('');
                var html = '<option value="">Choose Sub Category</option>';
                for(var key in res) {
                    html +='<option value="'+key+'">'+res[key]+'</option>';
                }
                $("#sub_category_id").html(html);
            });
        });
        $("#create_request_form").validate({
            rules: {
                description: {
                    required: true,
                    maxlength: 1500,
                    minlength: 5,
                },
                category_id: {required: true},
                sub_category_id: {required: true},
                duration: {required: true},
                budget: {required: true},
            },
            submitHandler: function (form, e) {
                form.submit();
            }
        });
    });
</script>