@extends('front.layouts.default')
@section('main_container')
    <section class="dashboard-area">
        <div class="dashboard_contents">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="dashboard_title_area">
                            <div class="dashboard__title">
                                <h3>Orders</h3>
                            </div>
                        </div>
                    </div>
                    <!-- end /.col-md-12 -->
                </div>
                <!-- end /.row -->

                <div class="row">
                    <div class="col-md-12">
                        <div class="cardify notifications_module">

                            <div class="db-new-main-table align-top js-db-table">
                                <table>
                                    <thead>
                                    <tr class="header-filter">
                                        <td colspan="3" class="js-filter-title">All Orders</td>
                                        <td colspan="3" class="sort-by">
                                            <div class="js-sort-by">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="js-header-titles">
                                        <td width="15%">Date</td>
                                        <td width="10%">Order ID</td>
                                        <td width="40%">Detail</td>
                                        <td width="15%">Type</td>
                                        <td width="10%">Price</td>
                                        <td width="10%">Status</td>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @if($orders)
                                        @foreach($orders as $order)
                                        <tr class="js-header-titles">
                                            <td>
                                                <div class="job-date">
                                                    {{date('d M, Y', strtotime($order->created_at))}}
                                                </div>
                                            </td>
                                            <td>
                                                <div class="order-title">
                                                    <h4><a target="_blank" href="/user/invoice/{{$order->id}}"> #VAPH{{$order->id}}</a>
                                                    </h4>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="order-details-show">
                                                    <div class="order-title">
                                                        <a href="/stint/stint-order/{{$order->id}}">{{$order->notes}}</a>


                                                    </div>
                                                </div>
                                            </td>

                                            <td>
                                                <div class="order-title">
                                                    <p>{{$order->order_type}}-package</p>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="order-price">
                                                    <p>${{($order->quantity*$order->unit_price)+ ($order->extra_price)}}</p>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="order-status">
                                                    <button data-id="#" class="ctm-btn1 offer-btn mtop0">
                                                        @if($order->status == 1)
                                                            In-Progress
                                                        @elseif($order->status == 2)
                                                            Submitted To Review
                                                        @elseif($order->status == 5)
                                                            Paid
                                                        @elseif($order->status == 4)
                                                            Cancelled
                                                        @elseif ($order->status == 3 )
                                                            Completed
                                                        @endif
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>

                            <div class="pagination-area pagination-area2">
                                <nav class="navigation pagination " role="navigation">
                                    <div class="nav-links">
                                        {!!  !empty($orders) and $orders->links() !!}
                                    </div>
                                </nav>
                            </div>
                        </div>
                        <!-- end /.notifications_modules -->
                    </div>
                    <!-- end /.col-md-12 -->
                </div>
                <!-- end /.row -->
            </div>
            <!-- end /.container -->
        </div>
        <!-- end /.dashboard_menu_area -->
    </section>
@endsection