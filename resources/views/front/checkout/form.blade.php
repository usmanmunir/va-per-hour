@extends('front.layouts.default')
@push('styles')
    <link rel="stylesheet" href="/front/css/checkout.css" type="text/css" media="screen"/>
@endpush

@section('main_container')
    <div class="stint-view-page">
        <section class="dashboard-area section--padding2">
            <div class="dashboard_contents pd0imp">
                <div class="container">
                    <div class="checkout_validation_error alert alert-danger" style="display: none;"></div>
                    <form action="/process/payment" class="setting_form" name="payment_form" id="payment_form" method="post">
                        @foreach($order as $key => $value)
                            <input type="hidden" name="{{$key}}" value="{{$value}}">
                        @endforeach
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="information_module">
                                    <div class="toggle_title">
                                        <h4>Biling Information </h4>
                                    </div>
                                    <div class="information__set">
                                        <div class="information_wrapper form--fields">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="first_name">First Name <sup>*</sup>
                                                        </label>
                                                        <input type="text" name="first_name" class="text_field"
                                                               placeholder="John" value="{{Auth::user()->first_name}}">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="last_name">last Name <sup>*</sup>
                                                        </label>
                                                        <input type="text" name="last_name" class="text_field"
                                                               placeholder="Smith" value="{{Auth::user()->last_name}}">
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- end /.row -->

                                            <div class="form-group">
                                                <label for="email">Company Name <sup>*</sup>
                                                </label>
                                                <input type="text" name="company_name" class="text_field"
                                                       placeholder="AazzTech" value="">
                                            </div>

                                            <div class="form-group">
                                                <label for="email1">Email Adress <sup>*</sup>
                                                </label>
                                                <input type="email" name="email" class="text_field"
                                                       placeholder="Email address" value="{{Auth::user()->email}}">
                                            </div>

                                            <div class="form-group">
                                                <label for="country1">Country <sup>*</sup>
                                                </label>
                                                <div class="select-wrap select-wrap2">
                                                    <select name="country_id" class="text_field" id="country_id">
                                                        <option value="">Select Country</option>
                                                        @if($countries)
                                                            @foreach($countries as $country)
                                                                <option value="{{$country->id}}">{{$country->name}}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                    <span class="lnr lnr-chevron-down"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="country1">State <sup>*</sup>
                                                </label>
                                                <div class="select-wrap select-wrap2">
                                                    <select name="state_id" class="text_field" id="state_id">
                                                        <option value="">Select State</option>
                                                        @if($states)
                                                            @foreach($states as $state)
                                                                <option value="{{$state->id}}">{{$state->name}}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                    <span class="lnr lnr-chevron-down"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="country1">City <sup>*</sup>
                                                </label>
                                                <div class="select-wrap select-wrap2">
                                                    <select name="city_id" class="text_field" id="city_id">
                                                        <option value="">Select City</option>
                                                        @if($cities)
                                                            @foreach($cities as $city)
                                                                <option value="{{$city->id}}">{{$city->name}}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                    <span class="lnr lnr-chevron-down"></span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="address1">Address Line 1</label>
                                                <input type="text" name="address1" class="text_field"
                                                       placeholder="Address line one" id="address1">
                                            </div>

                                            <div class="form-group">
                                                <label for="address2">Address Line 2</label>
                                                <input type="text" name="address2" class="text_field"
                                                       placeholder="Address line two" id="address2">
                                            </div>


                                            <div class="form-group">
                                                <label for="zipcode">Zip / Postal Code
                                                    <sup>*</sup>
                                                </label>
                                                <input type="text" name="zip_code" class="text_field"
                                                       placeholder="zip/postal code" id="zip_code">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="information_module order_summary mtop0">
                                    <div class="toggle_title">
                                        <h4>Order Summary</h4>
                                    </div>
                                    <ul>

                                        <li class="item">
                                            <a href="#">{{$notes}}</a>
                                            <span>${{$order['summary']}}</span>
                                        </li>

                                        <li class="total_ammount">
                                            <p>Total</p>
                                            <span>${{$order['summary']}}</span>
                                        </li>
                                    </ul>

                                </div>

                                <div class="information_module payment_options">
                                    <div class="toggle_title">
                                        <h4>Select Payment Method</h4>
                                    </div>
                                    <ul>
                                        <li>
                                            <div class="custom-control custom-radio">
                                                <input type="radio" checked="" id="customRadio1" name="payment_method"
                                                       class="custom-control-input" value="cards">

                                                <label class="custom-control-label" for="customRadio1">
                                                    <img src="/front/images/icons/upaucard.png" width="100"
                                                         alt="Visa Cards">
                                                </label>

                                                <label class="text-right"> &nbsp;&nbsp;&nbsp; if you have not account
                                                    yest! <a
                                                            href=" http://upaycardtracker.net//return/plainclick/campaigns_id/enc5cb754c603e8d/affiliate_id/976713/banner_id/1"
                                                            target="_blank" class="btn btn-link">Register
                                                        Now </a></label>
                                            </div>
                                        </li>
                                        <div class="payment_info modules__content">

                                            <div class="form-group">
                                                <label for="card_number">Username</label>
                                                <input name="upay_username" id="card_number" type="text"
                                                       class="text_field" placeholder="Enter Upaycard username here...">
                                            </div>
                                            <div class="row">
                                                @csrf
                                                <button type="submit" class="ctm-btn-lg btn--round btn--default">Confirm Order
                                                </button>
                                            </div>
                                        </div>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
@endsection
@push('scripts')
    @include('front.checkout.checkout-js');
@endpush