<script>
    $(function () {
        $("#country_id").change(function () {
            $.ajax({
                url: '/load-states/' + $(this).val(),
                type: 'get',
                success: function (res) {
                    $("#state_id").html('');
                    var html='<option value="">Select State</option>';
                    for(var i in res) {
                        html+='<option value="'+res[i].id+'">'+res[i].name+'</option>';
                    }
                    $("#state_id").html(html);
                },
                error: function (err) {
                    toastr.error('Error while loading States');
                }
            });
        });
        $("#state_id").change(function () {
            //load cities
            $.ajax({
                url: '/load-cities/' + $(this).val(),
                type: 'get',
                success: function (res) {
                    $("#city_id").html('');
                    var html='<option value="">Select City</option>';
                    for(var i in res) {
                        html+='<option value="'+res[i].id+'">'+res[i].name+'</option>';
                    }
                    $("#city_id").html(html);
                },
                error: function (err) {
                    toastr.error('Error while loading Cities');
                }
            });
        });

        $("#payment_form").validate({
            rules: {
                first_name: {required: true},
                last_name: {required: true},
                company_name: {required: true},
                email: {required: true},
                country_id: {required: true},
                state_id: {required: true},
                city_id: {required: true},
                address1: {required: true},
                zip_code: {required: true},
            },
            submitHandler: function(form, e) {
                form.submit();
            }
        });

    });
</script>