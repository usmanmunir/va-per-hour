<script>
    $(function () {

        $("#submit_proposal_form").validate({
            rules: {
                cover_letter: {
                    required: true,
                    maxlength: 80,
                },
                bid_price: {required: true},
                estimate_time: {required: true},
            },
            submitHandler: function (form, e) {
                e.preventDefault();

                var fd = new FormData(form);
                $.ajax({
                    type: "post",
                    url: "{{ url('/job/submit-proposal') }}",
                    dataType: "json",
                    "headers": {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
                    data: fd,
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function () {
                        //$("#preview").fadeOut();
                        $("#err").fadeOut();
                        $(".submit").attr('disabled', true).html('<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function (res) {
                        toastr.success('success', 'Success!!', res.message);
                        window.location = '/job';
                    },
                    error: function (data) {
                        toastr.error("Something went wrong");
                    }
                });
            }
        });
    });

</script>