<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 7/31/19
 * Time: 2:30 AM
 */
?>
@extends('front.layouts.default')
@section('main_container')
    <div class="course-syallbus-page">
        <section id="syallbus_area" class="ptop20 bgwhite mtop50">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                        <div class="syallbus_content_wrap">
                            <div class="course_desc_wrap ptop30">
                                <div class="course-title two-part">
                                    <div class="course_title dblock">
                                        <h2>{{$jobDetail->job_box}}</h2>
                                    </div>
                                    <div class="course_count dblock pull-right">
                                        <div class="co_video_count dblock">
                                            <h3><i class="fa fa-book"></i> {{$jobDetail->category->name}}
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="course_desc">
                                    <p>{{$jobDetail->description}}</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-bordered">
                                        <thead>
                                        <th>
                                            Estimate Time :
                                        </th>
                                        <th>
                                            Budget :
                                        </th>
                                        <th>
                                            job posted :
                                        </th>
                                        </thead>
                                        <tbody>
                                        <td>
                                            {{$jobDetail->delivery_time}}
                                        </td>
                                        <td>
                                            {{$jobDetail->budget}}
                                        </td>
                                        <td>
                                            {{$jobDetail->created_at->diffForHumans()}}
                                        </td>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <hr>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                        <div class="syallbus_sidebar">
                        </div>
                        <div class="product-in product-priceing" style="width: 100% !important;">
                            <div class="box">
                                <div class="total-sales">
                                    <div class="purches-item">
                                        <button class="ctm-btn" href="/job/proposals/{{$jobDetail->id}}"><i
                                                    class="fa fa-bell-slash" aria-hidden="true"></i> Submit a
                                            Proposal
                                        </button>
                                    </div>
                                    <div class="product-inforamtion">
                                        <h2 class="title">Client Information</h2>
                                        <div class="profile-info">
                                            <div class="pro-img">
                                                <img src="{{$jobDetail->user->avatar}}">
                                            </div>
                                            <p class="name">
                                                <a href="/user-profile/{{$jobDetail->user->first_name}}"
                                                   target="_blank">
                                                    {{$jobDetail->user->first_name}}{{$jobDetail->user->last_name}} </a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-md-12">
                    <form enctype="multipart/form-data" class="stint-form" method="post" id="submit_proposal_form">
                        <div class="tab-content post-oofer-contant" id="myTabContent">
                            <div class="stint-contant tab-pane active show" id="title" role="tabpanel"
                                 aria-labelledby="title-tab">
                                <div class="form-group">
                                    <label for="exampleFormControlTextarea1">Cover Letter</label>
                                    <textarea name="cover_letter" class="form-control" id="cover_letter"
                                              rows="3"></textarea>
                                    <p id="exampleFormControlTextarea1-error" class="custome_error"></p>
                                    <p class="word-limit">0/80 Max</p>
                                </div>
                                <table class="table table-bordered">
                                    <thead>
                                    <th>
                                        Actual Price
                                    </th>
                                    <th>
                                        Enter your price
                                    </th>
                                    <th>
                                        Estimated Time
                                    </th>
                                    </thead>
                                    <tbody>
                                    <td>
                                        <input type="text" name="actualPrice" readonly value="{{$jobDetail->budget}}">
                                    </td>
                                    <td>
                                        <input type="text" name="bid_price" placeholder="Enter your Bid">
                                    </td>
                                    <td>
                                        <select class="offer-text" name="estimate_time" id="estimate_time">
                                            <option value="" selected="">Select your stint</option>
                                            <?php for($i = 1 ; $i <= 60; $i++){?>
                                            <option value="<?php echo $i?> days"><?php echo $i?> days</option>
                                            <?php } ?>
                                        </select>
                                    </td>
                                    </tbody>
                                </table>
                                <div class="form-group">
                                    <div class="dropzone" id="uploadPdf">
                                        <div class="dz-message ">
                                            <i class="fa fa-picture-o" aria-hidden="true"></i>
                                            Drop files here or click to upload.<br>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="job_id" value="{{$jobDetail->id}}">
                                <div class="col-md-12 submit-cancle-btn">
                                    <button type="submit" class="ctm-btn submit stint-next" data-step="2" id="step1">
                                        Submit proposal
                                    </button>
                                    @csrf
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
@endsection
@push('styles')
    <style>
        .purches-item {
            margin: auto !important;
            float: none;
            width: 85% !important;
            padding: 15px;
        }

        .dropzone {
            max-width: 100%;
        }

        label {
            padding-bottom: 20px;
        }

        .bgwhite {
            background: #f2f3f2 !important;
        }
    </style>

@endpush
@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>

    <script type="text/javascript">
        Dropzone.autoDiscover = false;
        var uploadedDocumentMap = {}
        $(document).ready(function () {
            //    Upload PDF
            $("#uploadPdf").dropzone({
                maxFiles: 1,
                maxFilesize: 500000000,
                // acceptedFiles: ".pdf",
                addRemoveLinks: true,
                timeout: 5000,
                method: "POST",
                url: "{{ url('/job/upload-proposal-doc') }}",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
                success: function (file, response) {
                    // console.log(response);
                    $('form').append('<input type="hidden" name="file" value="' + response + '">')
                }
            });
        })

    </script>
    @include('front.proposal.proposal-js')
@endpush
