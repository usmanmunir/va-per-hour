<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 8/1/19
 * Time: 12:36 AM
 */
?>
@extends('front.layouts.default')
@section('main_container')
    <section class="ctn banner">
        <div class="main-banner">
            <div class="banner-img">
                <!-- <div class="overlay-img"></div> -->
                <img src="/front/images/post-offer.png">
            </div>
            <div class="banner-contant">
                <h2 class="banner-title-2">Job Request</h2>
            </div>
        </div>
    </section>
    <section class="joblist-wrap">
        <div class="container">
            <div class="m50">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12">
                        <div class="tab-content jobliststeptabs" id="myTabContent">
                            <div class="activejob-contant tab-pane fade active show pbot0 m50" id="activejob"
                                 role="tabpanel" aria-labelledby="activejob-tab">
                                <div class="job-list-box">
                                    <div class="db-new-main-table align-top js-db-table">
                                        <table>
                                            <thead>
                                            <tr class="header-filter">
                                                <td colspan="4" class="js-filter-title">Buyer requests</td>
                                                <td colspan="2" class="sort-by">
                                                </td>
                                            </tr>
                                            <tr class="js-header-titles">
                                                <td width="20%">Buyer</td>
                                                <td width="33%">Request</td>
                                                <td width="14%">Date</td>
                                                <td width="8%">Duration</td>
                                                <td width="8%">Budget</td>
                                                <td width="19%">Action</td>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            @foreach($submitProposals as $submitProposal)
                                                <tr class="js-header-titles">
                                                    <td>
                                                        <div class="job-user">
                                                            <div class="job-userImage">
                                                                <img src="{{$submitProposal->user->avatar}}"
                                                                     alt="{{$submitProposal->user->first_name}}"
                                                                     class="job-icon">

                                                            </div>
                                                            <div class="job-userImage">
                                                                <h3>{{$submitProposal->user->first_name}}
                                                                    {{$submitProposal->user->last_name}}</h3>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="job-desc">
                                                            <p>{{$submitProposal->cover_letter}}</p>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="job-date">
                                                            {{$submitProposal->created_at->diffForHumans()}}
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="job-duration">
                                                            {{$submitProposal->estimate_time}}
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="job-budger">
                                                            {{$submitProposal->bid_price}}

                                                            {{--<button data-id="11" class="ctm-btn1 offer-btn">Send offer--}}
                                                            {{--</button>--}}
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <a href="/stint/chat/{{$submitProposal->user->first_name}}/{{$submitProposal->user->id}}"
                                                           class="ctm-btn1 offer-btn">Contact with buyer</a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
