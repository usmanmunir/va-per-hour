<ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="title-tab" data-toggle="tab" href="/stint/buyer-requests">
            Buyer Requests
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="pricing-tab" data-toggle="tab" href="/stint/sent-offers">
            Sent Offers
        </a>
    </li>
    <li></li>
</ul>