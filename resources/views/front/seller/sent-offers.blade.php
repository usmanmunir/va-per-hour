<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 5/11/19
 * Time: 1:59 AM
 */
?>
@extends('front.layouts.default')
@section('main_container')
    <section class="ctn banner">
        <div class="main-banner">
            <div class="banner-img">
                <!-- <div class="overlay-img"></div> -->
                <img src="/front/images/post-offer.png">
            </div>
            <div class="banner-contant">
                <h2 class="banner-title-2">Job Request</h2>
            </div>
        </div>
    </section>
    <section class="joblist-wrap">
        <div class="container">
            <div class="m50">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12">
                        @include('front.seller.partials.buyer-request-menu')
                        <div class="tab-content jobliststeptabs" id="myTabContent">
                            <div class="sentjob-contant tab-pane fade pbot0 m50 active show" id="sentjob" role="tabpanel" aria-labelledby="sentjob-tab">
                                <div class="job-list-box">
                                    <div class="db-new-main-table align-top js-db-table pdextratable">
                                        <table id="example1" class="table">
                                            <thead>
                                            <tr class="header-filter">
                                                <td colspan="6" class="js-filter-title">OFFERS SUBMITTED FOR BUYER REQUESTS</td>
                                            </tr>
                                            <tr class="js-header-titles">
                                                <td width="45%">Offer</td>
                                                <td width="12%">Duration</td>
                                                <td width="8%">Price</td>
                                                <td width="35%" colspan="2">Request</td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if($offers)
                                                @foreach($offers as $offer)
                                                <tr class="js-header-titles">
                                                    <td>
                                                        <div class="job-user">
                                                            <div class="job-offer2">
                                                                {{$offer->proposal}}
                                                                <p>
                                                                    {{$offer->stint->title}}
                                                                </p>
                                                                <span><i class="fa fa-check-circle"></i> {{$offer->revisions}} Revisions</span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="job-desc">
                                                            <p>
                                                                {{$offer->delivery_time}} Days
                                                            </p>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="job-date">
                                                            ${{$offer->amount}}
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="jobreq-icon">
                                                            <span class="roundletter">H</span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="job-request1">
                                                            <div class="jobreq-desc">
                                                                <h2>{{$offer->request->user->first_name.' '.$offer->request->user->last_name}}</h2>
                                                                <p>{{$offer->request->description}}</p>
                                                                <span>Delivery Time - {{$offer->request->duration}} days &nbsp; | &nbsp; Budget - ${{$offer->request->budget}} </span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('styles')
    <style>
        .offer-post .post-all-offers .nav {
            width: 100%;
            background: #fff;
            position: unset;
        }
        .offer-post .post-all-offers .nav .nav-item {
            border: none;
        }
        .offer-post .post-all-offers .nav .nav-item .nav-link {
            padding: 0px 15px;
        }
    </style>
@endpush