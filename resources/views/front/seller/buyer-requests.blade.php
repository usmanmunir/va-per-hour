<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 5/11/19
 * Time: 1:59 AM
 */
?>
@extends('front.layouts.default')
@section('main_container')
    <section class="ctn banner">
        <div class="main-banner">
            <div class="banner-img">
                <!-- <div class="overlay-img"></div> -->
                <img src="/front/images/post-offer.png">
            </div>
            <div class="banner-contant">
                <h2 class="banner-title-2">Job Request</h2>
                <!--             <h5 class="banner-title-3">It is a long established fact that a reader will be distracted by the readable content of a page when looking.</h5>
                 -->        </div>
        </div>
    </section>
    <section class="joblist-wrap dashboard-area">
        <div class="container">
            <div class="m50">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12">
                        @include('front.seller.partials.buyer-request-menu')
                        <div class="tab-content jobliststeptabs" id="myTabContent">
                            <div class="activejob-contant tab-pane fade active show pbot0 m50" id="activejob" role="tabpanel" aria-labelledby="activejob-tab">
                                <div class="job-list-box">
                                    <div class="db-new-main-table align-top js-db-table">
                                        <table>
                                            <thead>
                                                <tr class="header-filter">
                                                    <td colspan="4" class="js-filter-title">Buyer requests</td>
                                                    <td colspan="2" class="sort-by">
                                                    </td>
                                                </tr>
                                                <tr class="js-header-titles">
                                                    <td width="20%">Buyer</td>
                                                    <td width="40%">Request</td>
                                                    <td width="12%">Date</td>
                                                    <td width="8%">Offers</td>
                                                    <td width="10%">Duration</td>
                                                    <td width="10%">Budget</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @if($requests)
                                                @foreach($requests as $job)
                                                <tr class="js-header-titles">
                                                    <td>
                                                        <div class="job-user">
                                                            <div class="job-userImage">
                                                                <img src="{{!empty($job->user->avatar) ? $job->user->avatar : '/uploads/images/default.svg'}}" class="job-icon">

                                                            </div>
                                                            <div class="job-userImage">
                                                                <h3>{{$job->user->first_name. ' '. $job->user->last_name}}</h3>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="job-desc">
                                                            <p>
                                                                {{$job->description}}
                                                            </p>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="job-date">
                                                            {{date("M d, Y", strtotime($job->create_date))}}
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="job-offers">
                                                            {{$job->offers_count}}
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="job-duration">
                                                            {{$job->duration}}
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="job-budger">
                                                            ${{$job->budget}}
                                                            <button data-id="{{csrf_token().$job->id}}" class="ctm-btn1 offer-btn">Send offer</button>
                                                        </div>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="6">No buyer request posted yet</td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal bd-example-modal-lg offer-model show" tabindex="-1" id="offer-model" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Send Offer</h5>
                </div>
                <div class="modal-body">
                    <form action="" method="post" id="sentoffer_form">
                        <div class="so_error_message" style="display: none;"></div>
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="offer-box">
                                    <label>Budget</label>
                                    <input type="text" id="amount" name="amount" placeholder="Enter your budget" class="offer-text" value="">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="offer-box">
                                    <label>Delivery Time</label>
                                    <input type="text" id="delivery_time" name="delivery_time" placeholder="Enter your delivery time" class="offer-text" value="">
                                </div>
                            </div>
                        </div>
                        <div class="row mtop30">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="offer-box">
                                    <label>Revisions</label>
                                    <select id="revisions" name="revisions" class="offer-text">
                                        <?php
                                        for($i = 1; $i < 5; $i++) {
                                        ?>
                                        <option value="{{$i}}">{{$i}}</option>
                                        <?php

                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="offer-box">
                                    <label>Stint</label>
                                    <select class="offer-text" name="stint_id" id="select_id">
                                        <option value="">Select your stint</option>
                                        @if($stints)
                                            @foreach($stints as $stint)
                                                <option value="{{$stint->id}}">{{$stint->title}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row mtop20">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="offer-box">
                                    <label>Description</label>
                                    <textarea rows="5" class="offer-text crelative" name="proposal" id="proposal"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row mtop10">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="offer-box">
                                    <input type="hidden" id="request_id" name="request_id" value="">
                                    <input type="hidden" id="user_id" name="user_id" value="{{csrf_token().Auth::user()->id}}">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <button type="button" class="ctm-btn-lg close-btn pull-left" data-dismiss="modal">Close</button>
                                    <button type="submit" class="ctm-btn-lg pull-right" id="sentOffer-btn" name="sentOffer-btn"> Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <button style="visibility: hidden" class="modal-trigger" data-toggle="modal" data-target="#offer-model">trigger</button>

@endsection
@push('scripts')
    <script>
        $(function () {
            $(".offer-btn").click(function () {
                var id = $(this).attr('data-id');
                $("#request_id").val(id);
                $(".modal-trigger").trigger('click');
            });

            $("#sentoffer_form").validate({
                rules: {
                    proposal: {
                        required: true,
                        maxlength: 1500,
                        minlength: 5,
                    },
                    stint_id: {required: true},
                    amount: {required: true, number: true},
                    amount: {required: true, number: true},
                    delivery_time: {required: true, number: true},
                },
                submitHandler: function (form, e) {
                    e.preventDefault();
                    $.ajax({
                        url: '/job/make/offer/'+$("#request_id").val(),
                        method: 'post',
                        data: $("#sentoffer_form").serialize(),
                        success: function (res) {
                            toastr.success(res.message);
                            $(".close-btn").trigger('click');
                        },
                        error: function (err) {
                            toastr.error( err.responseJSON.message);
                        }
                    });
                }
            });
        })
    </script>
@endpush