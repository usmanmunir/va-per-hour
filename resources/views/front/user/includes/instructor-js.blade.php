<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 5/20/19
 * Time: 6:09 PM
 */
?>
<script>
    $(function () {
        var i = 1;
        $('#add_more').click(function () {
            i++;
            $('#dynamic_field').append(
                '<div class="row courselink">' +
                '<div class="col-md-3">' +
                '<label class="col-sm-12">Course Link' + i + '</label>' +
                '</div>' +
                '<div class="col-md-6">' +
                '<input type="text" class="cv_text" name="showcase_links[]" id = "showcase_links" value = "" >' +
                '</div>' +
                '<div class="col-md-3">' +
                '<i class="fa fa-minus-circle fa-3x" name="remove" id="' + i + '" aria-hidden="true" style="color:red;"></i>'+
                '</div></div>');
        });


        $(document).on('click', '.btn_remove', function () {
            var button_id = $(this).attr("id");
            $('#row' + button_id + '').remove();
        });

        $("#instructor-form").validate({
            rules: {
                category: {required: true},
                course_skills: {required: true},
                expertise_level: {required: true},
                professional_experience: {required: true},
                other_comments: {required: true},
            },
            submitHandler: function (form, e) {
                e.preventDefault();
                var formData = {

                    // category: $("[name='category']").val(),
                    // course_skills: $("[name='course_skills']").val(),
                    // expertise_level: $("[name='expertise_level']:checked").val(),
                    // professional_experience: $("[name='professional_experience']").val(),
                    // other_comments: $("[name='other_comments']").val(),

                };
                $.ajax({
                    type: "post",
                    url: "{{ url('/user/save-instructor') }}",
                    dataType: "json",
                    data: $('#instructor-form').serialize(),
                    "headers": {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
                    success: function (data) {
                        // resetForm();
                        toastr.success(data.message);

                    },
                    error: function (data) {
                        toastr.error("Something went wrong");
                    }
                });
            },
        });
    });

</script>
