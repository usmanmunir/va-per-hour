<script>
    $(function () {
        getUserLanguage();
        getUserSkill();
        $("#language").autocomplete({
            source: "/load-languages",
            minLength: 2,
            select: function (event, ui) {
                // console.log( "Selected: " + ui.item.label + " aka " + ui.item.value );
                event.preventDefault();
                $("#language").val(ui.item.label);
                $("#selectlangid").val(ui.item.value);
                return false;
            },
            focus: function (event, ui) {
                event.preventDefault();
                $("#language").val(ui.item.label);
            }
        });
        //Load skills
        $("#skill").autocomplete({
            source: "/load-skills",
            minLength: 2,
            select: function (event, ui) {
                // console.log( "Selected: " + ui.item.label + " aka " + ui.item.value );
                event.preventDefault();
                $("#skill").val(ui.item.label);
                $("#userskillid").val(ui.item.value);
                return false;
            },
            focus: function (event, ui) {
                event.preventDefault();
                $("#skill").val(ui.item.label);
            }
        });

        $("#language-form").validate({
            rules: {
                language: {required: true},
                proficiency: {required: true},
            },
            submitHandler: function (form, e) {
                e.preventDefault();
                var formData = {

                    selectlangid: $("#selectlangid").val(),
                    proficiency: $("[name='proficiency']").val(),
                    userlanguageId: $("#userlanguageId").val()
                };
                // console.log(formData);
                $.ajax({
                    type: "post",
                    url: "{{ url('/user/save-user-language') }}",
                    dataType: "json",
                    data: formData,
                    "headers": {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
                    success: function (data) {
                        resetForm();
                        getUserLanguage();
                        toastr.success(data.message);

                    },
                    error: function (data) {
                        toastr.error("Something went wrong");
                    }
                });
            }
        });

        $("#skillform").validate({
            rules: {
                skill: {required: true},
                skill_level: {required: true},
            },
            submitHandler: function (form, e) {
                e.preventDefault();
                var formData = {

                    userskillid: $("#userskillid").val(),
                    skill_level: $("[name='skill_level']").val(),
                    skils_id: $("#skils_id").val()
                };
                // console.log(formData);
                $.ajax({
                    type: "post",
                    url: "{{ url('/user/save-user-skill') }}",
                    dataType: "json",
                    data: formData,
                    "headers": {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
                    success: function (data) {
                        resetForm();
                        getUserSkill();
                        toastr.success(data.message);

                    },
                    error: function (data) {
                        toastr.error("Something went wrong");
                    }
                });
            }
        });

        $(document).on("click", ".editUserLanguage", function (e) {
            e.preventDefault();
            $(".languageadd").next('.lang-addbox').slideDown();
            // var currentRecordId = $(this).attr("edu_id");
            var currentRecord = $(this).closest('tr.language-row');
            $("[name='language']").val(currentRecord.find('td:first-child').data('language'));
            $("[name='proficiency']").val(currentRecord.find('td:nth-child(2)').data('proficiency'));
            $("[name='selectlangid']").val(currentRecord.find('td:nth-child(2)').data('langid'));
            $("#userlanguageId").val(currentRecord.data('record'));
        });

        $(document).on("click", ".editUserSkill", function (e) {
            e.preventDefault();
            $("#AddNewSkills").next('.lang-addbox').slideDown();
            // var currentRecordId = $(this).attr("edu_id");
            var currentRecord = $(this).closest('tr.skill-row');
            $("[name='skill']").val(currentRecord.find('td:first-child').data('skill'));
            $("[name='skill_level']").val(currentRecord.find('td:nth-child(2)').data('level'));
            $("[name='userskillid']").val(currentRecord.find('td:nth-child(2)').data('skillid'));
            $("#skils_id").val(currentRecord.data('record'));
        });

        $(document).on("click", ".removeUserLanguage", function (e) {

            e.preventDefault();

            var userLanguageId = $(this).attr("languageId");

            if (confirm("Are you want to sure?")) {

                deleteUserLanguage(userLanguageId);
            }

        });

        $(document).on("click", ".removeUserSkill", function (e) {

            e.preventDefault();

            var skillsId = $(this).attr("skillsId");

            if (confirm("Are you want to sure?")) {

                deleteUserSkill(skillsId);
            }

        });


    });

    function getUserLanguage() {
        var tableData = "";

        $.ajax({
            url: '/user/get-user-language',
            type: 'get',
            dataType: 'json',
            success: function (data) {
                console.log(data.data);
                if (data.length) {
                    $.each(data, function (key, val) {
                        console.log(val);
                        tableData += '<tr class="language-row" data-record="' + val.id + '">';
                        tableData += '<td data-language="' + val.language.name + '">' + val.language.name + '</td>';
                        tableData += '<td data-proficiency="' + val.proficiency + '"  data-langid="' + val.language.id + '">' + val.proficiency + '</td>';
                        // tableData += '<td data-langid="' + val.language.id + '">' + val.language.id + '</td>';
                        tableData += '<td align="right">';
                        tableData += '<a class="types course-model-open1" href="javascript:;">';
                        tableData += '<i class="fa fa-edit editUserLanguage"></i></a>';
                        tableData += '<a class="types" href="javascript:;"> ';
                        tableData += ' <i class="fa fa-trash removeUserLanguage" languageId=' + val.id + '></i></a>';
                        tableData += '</tr>';

                    })
                } else {
                    tableData += '<tr colspan=3>'
                    tableData += '<td>No Data Avaliable</td>';
                    tableData += '<tr>'
                }
                $("#language_table tbody").html(tableData);
            }
        });
    }

    function getUserSkill() {
        var tableData = "";

        $.ajax({
            url: '/user/get-user-skill',
            type: 'get',
            dataType: 'json',
            success: function (data) {
                console.log(data);
                if (data.length) {
                    $.each(data, function (key, val) {
                        console.log(val);
                        tableData += '<tr class="skill-row" data-record="' + val.id + '">';
                        tableData += '<td data-skill="' + val.skill.title + '">' + val.skill.title + '</td>';
                        tableData += '<td data-level="' + val.skill_level + '"  data-skillid="' + val.skill.id + '">' + val.skill_level + '</td>';
                        // tableData += '<td data-langid="' + val.language.id + '">' + val.language.id + '</td>';
                        tableData += '<td align="right">';
                        tableData += '<a class="types course-model-open1" href="javascript:;">';
                        tableData += '<i class="fa fa-edit editUserSkill"></i></a>';
                        tableData += '<a class="types" href="javascript:;"> ';
                        tableData += ' <i class="fa fa-trash removeUserSkill" skillsId=' + val.id + '></i></a>';
                        tableData += '</tr>';

                    })
                } else {
                    tableData += '<tr colspan=3>'
                    tableData += '<td>No Data Avaliable</td>';
                    tableData += '<tr>'
                }
                $("#skillTable tbody").html(tableData);
            }
        });
    }

    function deleteUserLanguage(userLanguageId) {
        $.ajax({
            type: "post",
            url: "/user/delete-user-language/" + userLanguageId,
            dataType: "json",
            "headers": {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
            success: function (data) {
                getUserLanguage();
                toastr.success(data.message);
            },
            error: function (data) {
                toastr.error(data.message);
            }
        });
    }

    function deleteUserSkill(skillsId) {
        $.ajax({
            type: "post",
            url: "/user/delete-user-skill/" + skillsId,
            dataType: "json",
            "headers": {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
            success: function (data) {
                getUserSkill();
                toastr.success(data.message);
            },
            error: function (data) {
                toastr.error(data.message);
            }
        });
    }


    function resetForm() {
        document.getElementById("language-form").reset();
        document.getElementById("skillform").reset();

    }


</script>
