<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 5/16/19
 * Time: 11:39 PM
 */
?>
<script>
    $(window).on("load", function () {
        getUserEducation();
        getUserCertificate();
        $("#education-form").validate({
            rules: {
                institute_name: {required: true},
                degree_name: {required: true},
                year: {required: true},
            },
            submitHandler: function (form, e) {
                e.preventDefault();
                var formData = {

                    institute_name: $("[name='institute_name']").val(),
                    degree_name: $("[name='degree_name']").val(),
                    year: $("[name='year']").val(),
                    id: $("#edu_id").val()
                };
                $.ajax({
                    type: "post",
                    url: "{{ url('/user/save-user-education') }}",
                    dataType: "json",
                    data: formData,
                    "headers": {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
                    success: function (data) {
                        getUserEducation();
                        resetForm();
                        toastr.success(data.message);
                    },
                    error: function (data) {
                        toastr.error(data.message);
                    }

                });
            }
        });

        $(document).on("click", ".editUserEducation", function (e) {
            e.preventDefault();
            $("#addNewEducation").next('.lang-addbox').slideDown();
            // var currentRecordId = $(this).attr("edu_id");
            var currentRecord = $(this).closest('tr.education-row');
            // alert(currentRecord.find('td:first-child').data('institute'))
            $("[name='institute_name']").val(currentRecord.find('td:first-child').data('institute'));
            $("[name='degree_name']").val(currentRecord.find('td:nth-child(2)').data('degree'));
            $("#edu_id").val(currentRecord.data('record'));
            $("[name='year']").val(currentRecord.find('td:nth-child(2)').data('year'));
        });

        $(document).on("click", ".removeUserEducation", function (e) {

            e.preventDefault();
            var userEducationId = $(this).attr("educationId");
            // alert(currentRecordId)
            if (confirm("Are you want to sure?")) {

                deleteUserEducation(userEducationId);
            }

        });

        $("#certificate-form").validate({
            rules: {
                certificate_name: {required: true},
                certificate_type: {required: true},
                certificate_year: {required: true},
            },
            submitHandler: function (form, e) {
                e.preventDefault();
                var formData = {

                    certificate_name: $("[name='certificate_name']").val(),
                    certificate_type: $("[name='certificate_type']").val(),
                    year: $("[name='certificate_year']").val(),
                    cert_id: $("#cert_id").val()
                };
                $.ajax({
                    type: "post",
                    url: "{{ url('/user/save-user-certificate') }}",
                    dataType: "json",
                    data: formData,
                    "headers": {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
                    success: function (data) {
                        getUserCertificate();
                        resetForm();
                        toastr.success(data.message);
                    },
                    error: function (data) {
                        toastr.error("Something Went Wrong");
                    }

                });
            }
        });

        $(document).on("click", ".editUserCertificate", function (e) {
            e.preventDefault();
            $("#AddNewCertificate").next('.lang-addbox').slideDown();
            // var currentRecordId = $(this).attr("edu_id");
            var currentRecord = $(this).closest('tr.certificate-row');
            // alert(currentRecord.data('record'))
            $("[name='certificate_name']").val(currentRecord.find('td:first-child').data('certificate'));
            $("[name='certificate_type']").val(currentRecord.find('td:nth-child(2)').data('certificateletter'));
            $("#cert_id").val(currentRecord.data('record'));
            $("[name='certificate_year']").val(currentRecord.find('td:nth-child(2)').data('year'));
        });

        $(document).on("click", ".removeUserCertificate", function (e) {

            e.preventDefault();
            var userCertificateId = $(this).attr("certificateId");
            // alert(currentRecordId)
            if (confirm("Are you want to sure?")) {

                deleteUserCertificate(userCertificateId);
            }

        });

        $("#websiteForm").validate({
            rules: {
                website: {required: true},
            },
            submitHandler: function (form, e) {
                e.preventDefault();
                var formData = {
                    website: $("[name='website']").val()
                };
                $.ajax({
                    method: 'post',
                    url: "{{ url('/user/save-website') }}",
                    dataType: "json",
                    data: formData,
                    "headers": {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},

                    success: function (data) {
                        getUserEducation();
                        resetForm();
                        toastr.success(data.message);
                    },
                    error: function (data) {
                        toastr.error("Some thng went Wrong");
                    }

                });
            }
        });

        // Years on drop downlist

            var start_year = new Date().getFullYear();
            var html = ''
            for (var i = start_year; i > start_year - 30; i--) {
                html += '<option value="'+i+'">'+i+'</option>';
            }
            $("#user-education-year").html(html)
            $("#user-certificate-year").html(html)
    });

    function getUserEducation() {
        var tableData = "";

        $.ajax({
            url: '/user/get-user-education',
            type: 'get',
            dataType: 'json',
            success: function (data) {
                // console.log(data.data);
                if (data.length) {
                    $.each(data, function (key, val) {
                        // console.log(val);
                        tableData += '<tr class="education-row" data-record="' + val.id + '">';
                        tableData += '<td data-institute="' + val.institute_name + '">' + val.institute_name + '</td>';
                        tableData += '<td data-degree="' + val.degree_name + '" data-year="' + val.year + '">' + val.degree_name + " - " + val.year + '</td>';
                        tableData += '<td align="right">';
                        tableData += '<a class="types course-model-open1" href="javascript:;">';
                        tableData += '<i class="fa fa-edit editUserEducation"></i></a>';
                        tableData += '<a class="types" href="javascript:;"> ';
                        tableData += ' <i class="fa fa-trash removeUserEducation" educationId=' + val.id + '></i></a>';
                        tableData += '</tr>';

                    })
                } else {
                    tableData += '<tr colspan=3>'
                    tableData += '<td>No Data Avaliable</td>';
                    tableData += '<tr>'
                }
                $("#educationTable tbody").html(tableData);
            }
        });
    }

    // $("#saveEducation").click(function (event) {

    function deleteUserEducation(userEducationId) {
        $.ajax({
            type: "post",
            url: "/user/delete-user-education/" + userEducationId,
            dataType: "json",
            "headers": {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
            success: function (data) {
                getUserEducation();
                toastr.success(data.message);
            },
            error: function (data) {
                toastr.error(data.message);
            }
        });
    }

    $(document).on("click", ".editUserEducation", function (e) {

        e.preventDefault();

        // $(".addnew").trigger("click");
        $("#addNewEducation").next('.lang-addbox').slideDown();

        var currentRecordId = $(this).attr("language");

        var language = $(this).closest('tr').find('td:eq(0)').text();

        var proficiency = $(this).closest('tr').find('td:eq(1)').text();

        $("[name='language']").val(language);

        $("[name='proficiency']").val(proficiency);

        // $("[name='lg_id']").val(currentRecordId);

    });

    function getUserCertificate() {
        var tableData = "";

        $.ajax({
            url: '/user/get-user-certificate',
            type: 'get',
            dataType: 'json',
            success: function (data) {
                if (data.length) {
                    $.each(data, function (key, val) {
                        tableData += '<tr class="certificate-row" data-record="' + val.id + '">';
                        tableData += '<td data-certificate="' + val.certificate_name + '">' + val.certificate_name + '</td>';
                        tableData += '<td data-certificateletter="' + val.certificate_type + '" data-year="' + val.year + '">' + val.certificate_type + " - " + val.year + '</td>';
                        tableData += '<td align="right">';
                        tableData += '<a class="types course-model-open1" href="javascript:;">';
                        tableData += '<i class="fa fa-edit editUserCertificate"></i></a>';
                        tableData += '<a class="types" href="javascript:;"> ';
                        tableData += ' <i class="fa fa-trash removeUserCertificate" certificateId=' + val.id + '></i></a>';
                        tableData += '</tr>';


                    })
                } else {
                    tableData += '<tr colspan=3>'
                    tableData += '<td>No Data Avaliable</td>';
                    tableData += '<tr>'
                }
                $("#certificateTable tbody").html(tableData);
            }
        });
    }

    function deleteUserCertificate(userCertificateId) {
        $.ajax({
            type: "post",
            url: "/user/delete-user-certificate/" + userCertificateId,
            dataType: "json",
            "headers": {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
            success: function (data) {
                getUserEducation();
                getUserCertificate();
                toastr.success(data.message);
            },
            error: function (data) {
                toastr.error("Something Went Wrong");
            }
        });
    }

    function resetForm() {
        document.getElementById("websiteForm").reset();
        document.getElementById("education-form").reset();
        document.getElementById("certificate-form").reset();
    }

</script>
