<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 5/10/19
 * Time: 6:55 AM
 */
?>
@extends('front.layouts.default')
@section('main_container')
    <!--================================
            START LOGIN AREA
    =================================-->
    <section class="login_area section--padding2">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    <form action="{{route('signin')}}" method="POST" id="loginForm">
                    {{--<form action="" method="POST">--}}
                        <div class="cardify login">
                            <div class="login--header">
                                <h3>Log in and get to work</h3>
                                <p style="text-align: center;">You can sign in with your Email</p>
                            </div>
                            <!-- end .login_header -->
                            @csrf
                            <div class="login--form">
                                <div class="form-group">
                                    <label for="user_name">Email</label>
                                    <input id="email" type="email" class="text_field"
                                           placeholder="Enter your username..." name="email">
                                    <span class="text-danger">{{ $errors->first('email') }}</span>
                                </div>

                                <div class="form-group">
                                    <label for="pass">Password</label>
                                    <input id="password" type="password" class="text_field"
                                           placeholder="Enter your password..." name="password">
                                    <span class="text-danger">{{ $errors->first('password') }}</span>
                                </div>

                                <div class="form-group">
                                    <div class="custom_checkbox">
                                        <input type="checkbox" id="ch2">
                                        <label for="ch2">
                                            <span class="shadow_checkbox"></span>
                                            <span class="label_text">Remember me</span>
                                        </label>
                                    </div>
                                </div>

                                <button class="btn btn--md btn--round" type="submit">Login Now</button>

                                <div class="login_assist">
                                    <p class="recover">Lost your
                                        {{--<a href="#">username</a> or--}}
                                        <a href="{{route('password.reset')}}">password</a>?</p>
                                    <p class="signup">Don't have an
                                        <a href="{{url('/registeruser')}}">account</a>?</p>
                                </div>
                            </div>
                            <!-- end .login--form -->
                        </div>
                        <!-- end .cardify -->
                    </form>
                </div>
                <!-- end .col-md-6 -->
            </div>
            <!-- end .row -->
        </div>
        <!-- end .container -->
    </section>
    <!--================================
            END LOGIN AREA
    =================================-->

@endsection
