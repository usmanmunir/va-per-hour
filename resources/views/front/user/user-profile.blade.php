<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 5/11/19
 * Time: 1:59 AM
 */
?>
@extends('front.layouts.default')
@section('main_container')
    <div class="profile-page">
        <section class="offer-post section-1 mtop50">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12 post-all-offers">
                        <div class="tab-content post-oofer-contant main-profile" id="myTabContent">
                            <div class="design-contant tab-pane fade show active" id="design" role="tabpanel" aria-labelledby="design-tab">
                                <div class="user-profile-box">
                                    <div class="user-proifle">
                                        <div class="profile-pic">
                                            <img src="{{$user->avatar ?? '/uploads/images/default.svg' }}">

                                        </div>
                                        <div class="profile-details">
                                            <h6 class="name">{{$user->first_name.' '.$user->last_name}}</h6>
                                            <div class="rat-review">
                                                <div class="star">
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                                </div>
                                                <div class="review">
                                                    <span><b>3.5 </b> (4) Reviews</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="user-info">
                                            <div class="info-main">
                                                <div class="info in-1">
                                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                                    <span>From</span>
                                                </div>
                                                <div class="info in-2">
                                                    <span>{{!empty($user->location->city->name) ? $user->location->city->name.', ' : '' }}
                                                        {{!empty($user->location->country->name) ? $user->location->country->name : 'N/A'}}</span>
                                                </div>
                                            </div>
                                            <div class="info-main">
                                                <div class="info in-1">
                                                    <i class="fa fa-user" aria-hidden="true"></i>
                                                    <span>Member Since</span>
                                                </div>
                                                <div class="info in-2">
                                                    <span>{{date("d M, Y", strtotime($user->created_at))}}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="discription">
                                            <p>{{$user->description}}</p>
                                        </div>

                                        <div class="clearfix"></div>
                                        @if(Auth::user()->id == $user->id)
                                        <div class="profile-btn">
                                            <a href="/edit-profile" class="ctm-btn btn-1">Edit Profile</a>
                                        </div>
                                        @endif
                                    </div>

                                    {{--<div class="sidebar_author">--}}
                                        {{--<div class="sidebar-card author-menu">--}}
                                            {{--<ul>--}}
                                                {{--<li><a href="http://dev.vaperhour.coddrule.com/user/profile/vivek/stint/" class="active">View as a Seller</a></li>--}}
                                            {{--</ul>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    <div class="user-languages">
                                        @if(!empty($user->languages))
                                        <div class="Languages user-info-box">
                                            <h5 class="titile">
                                                Languages
                                            </h5>
                                            <p><strong>Java-</strong>Unspecified</p>
                                            <p><strong>PHP-</strong>Unspecified</p>
                                            <p><strong>Nodejs-</strong>Unspecified</p>
                                            <p><strong>Angularjs-</strong>Basic</p>


                                        </div>
                                        @endif
                                        @if(!empty($user->social_links))
                                        <div class="link-account user-info-box">
                                            <h5 class="titile">
                                                Linked Accounts
                                            </h5>

                                            @if(!empty($user->social_links->twitter))
                                            <p><a href="{{$user->social_links->twitter}}" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i>Twitter</a></p>
                                            @endif
                                            @if(!empty($user->social_links->linked_in))
                                            <p><a href="{{$user->social_links->linked_in}}" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i>Linkedin</a></p>
                                            @endif
                                            @if(!empty($user->social_links->google))
                                            <p><a href="{{$user->social_links->google}}" target="_blank"><i class="fa fa-google" aria-hidden="true"></i>Google</a></p>
                                            @endif
                                            @if(!empty($user->social_links->github))
                                            <p><a href="{{$user->social_links->github}}" target="_blank"><i class="fa fa-github" aria-hidden="true"></i>Github</a></p>
                                            @endif
                                            @if(!empty($user->social_links->stack_overflow))
                                            <p><a href="{{$user->social_links->stack_overflow}}" target="_blank"><i class="fa fa-stack-overflow" aria-hidden="true"></i>Stack Overflow</a></p>
                                            @endif
                                        </div>
                                        @endif
                                        @if(!empty($user->skills) && count($user->skills) > 0)
                                        <div class="skills user-info-box">
                                            <h5 class="titile">
                                                Skills
                                            </h5>
                                            @foreach($user->skills as $skill)
                                            <span class="badge badge-light">{{$skill->skill->title}}</span>
                                            @endforeach
                                        </div>
                                        @endif
                                        @if(!empty($user->education))
                                        <div class="education user-info-box">
                                            <h5 class="titile">
                                                Education
                                            </h5>
                                            @foreach($user->education as $education)
                                            <p class="sub-title">{{$education->degree_name}}</p>
                                            <p>{{$education->institute_name}} {{$education->year}}</p>
                                            @endforeach
                                        </div>
                                        @endif
                                        @if(!empty($user->certification))
                                        <div class="link-account user-info-box">
                                            <h5 class="titile">
                                                Certification
                                            </h5>
                                            @foreach($user->certification as $certificate)
                                            <p class="sub-title">{{$certificate->certificate_name}}</p>
                                            <p> {{$certificate->year}}</p>
                                            @endforeach

                                        </div>
                                        @endif
                                    </div>
                                    <div class="membership">
                                        <div class="order-title">
                                            <h5>Membership</h5>
                                        </div>
                                        <div class="plan">
                                            <p class="c-plan">Current Plan</p>
                                            @if(!empty($user->memberships) && count($user->memberships) > 0)
                                                @foreach($user->memberships as $membership)
                                                    <p class="f-plan">{{$membership->membership->title}}</p>
                                                @endforeach
                                            @endif
                                        </div>
                                        @if(Auth::user()->id == $user->id)
                                        <div class="order-btn">
                                            <a href="/user/pricing-plan" class="ctm-btn">View or Edit Membership plan</a>
                                        </div>
                                        @endif
                                    </div>
                                </div>

                                <div class="user-feeds user-profile-feed">
                                    <div class="product-title-area">
                                        <div class="product__title">
                                            <h2>{{$user->first_name.' '.$user->last_name}}'s Stints</h2>
                                        </div>

                                        <a href="/stint/stint-list/{{$user->id}}" class="btn btn--sm seeall" target="_blank">See all Items</a>
                                        <div class="clearfix"></div>
                                    </div>

                                    <div class="product-title-area">
                                        @if(!empty($user->stints))
                                        <div class="col-md-6 item-manag pull-left ">
                                            <div class="item-list">
                                                <div class="list1">
                                                    @foreach($user->stints as $stint)
                                                    <div class="box">
                                                        <div class="item-img">
                                                            <img class="img-responsive" src="{{!empty($stint->gallery[0]->image) ? $stint->gallery[0] : '/uploads/stint/images/default.png'}}" />
                                                        </div>
                                                        <div class="item-contant">
                                                            <h5> <a target="_blank" href="/stint/view/{{$stint->id}}/{{str_replace(" ", "-", strtolower($stint->title))}}" class="title">{{$stint->title}}</a></h5>
                                                            <div class="user-info">
                                                                <div class="img">
                                                                    <img src="{{$user->avatar ?? 'front/images/default.svg'}}">
                                                                </div>
                                                                <a href="/user-profile/{{$user->id}}"><h6>{{$user->first_name}} {{$user->last_name}}</h6></a>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="name-cate"><i class="fa fa-book" aria-hidden="true"></i>{{$stint->category->name}}</div>
                                                        </div>
                                                        <div class="item-pricing">
                                                            <div class="price">
                                                                <a href="#">${{!empty($stint->pricing_plan[0]->price) ? $stint->pricing_plan[0]->price: 0}}</a>
                                                            </div>
                                                            <div class="like"><a class="setting add-stint-to-favorites" href="#" data-stint="It-Is-A-Long-Established-Fact-That-A-Reader-Will-Be-Distracted-By-The-Readable-Content-Of-A-Page-When-Looking-" data-action="unfavourite" data-user="vivek" data-text="false"><i class="fa fa-heart-o" aria-hidden="true"></i> 01 </a></div>
                                                            <div class="cart"><i class="fa fa-shopping-cart" aria-hidden="true"></i> 14  </div>

                                                            <div class="rating-star"><i class="fa fa-star" aria-hidden="true"></i> <h4>3.25 </h4><span> (8)</span></div>

                                                        </div>
                                                    </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="clearfix"></div>




                                    <div class="common-white-back1 col-md-12">
                                        <div class="feed-contant">
                                            <div class="review-feed">
                                                <div class="title-view-all">
                                                    <a href="javascript:;" class="all-feed-reviews">Reviews
                                                        As Seller (4)</a>
                                                </div>

                                                <div class="review-uppend">
                                                    <div class="reviw-one">
                                                        <div class="user-img">
                                                            <img src="http://dev.vaperhour.coddrule.com/uploads/profile/8232b303c165f6e935aa21113bb8734e.jpg">
                                                        </div>
                                                        <div class="user-time-date-star">
                                                            <div class="date-name">
                                                                <a href="#">mohans patels</a>
                                                                <span> 23rd April 2019</span>
                                                            </div>
                                                            <div class="star">
                                                                <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i>
                                                            </div>
                                                            <div class="review-contant">
                                                                <p>nice</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="reviw-one">
                                                        <div class="user-img">
                                                            <img src="http://dev.vaperhour.coddrule.com/uploads/profile/8232b303c165f6e935aa21113bb8734e.jpg">
                                                        </div>
                                                        <div class="user-time-date-star">
                                                            <div class="date-name">
                                                                <a href="#">mohans patels</a>
                                                                <span> 23rd April 2019</span>
                                                            </div>
                                                            <div class="star">
                                                                <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>
                                                            </div>
                                                            <div class="review-contant">
                                                                <p>not good work</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-12 text-center">

                                                    <a href="javascript:;" class="ctm-btn" data-stint="stint" data-user="15" onclick="load_more_user_review('15','stint')">View More</a>

                                                </div>
                                                <div class="clearfix"></div>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@push("styles")
    <style>
        .badge-light {
            color: #212529;
            background-color: #f8f9fa;
        }
        .badge {
            display: inline-block;
            padding: .25em .4em;
            font-size: 75%;
            font-weight: 700;
            line-height: 1;
            text-align: center;
            white-space: nowrap;
            vertical-align: baseline;
            border-radius: .25rem;
        }
    </style>
@endpush