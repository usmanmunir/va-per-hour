<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 5/14/19
 * Time: 2:39 PM
 */
?>
@extends('front.layouts.default')
@section('main_container')
    <section class="offer-post section-1 EditProfile">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12 post-all-offers">
                    @include('front.layouts.profiletabs')
                    <div class="contact-info get-paid">
                        <div class="head-title">
                            <h5>Get Paid</h5>
                        </div>
                        <div class="box account">
                            <div class="title-edit">
                                <h5>Balance</h5>
                                <!-- <a href="#"><i class="fa fa-pencil" aria-hidden="true"></i></a> -->
                            </div>
                            <div class="box-contant">
                                <div class="your-balance">
                                    Your balance is $870,00                                </div>
                                <div class="paid-btn">
                                    <a href="http://vaperhour.local/user/withdrawal" class="ctm-btn">Get Paid Now</a>
                                </div>
                            </div>
                        </div>
                        <!--                        <div class="box account">-->
                        <!--                            <div class="title-edit">-->
                        <!--                                <h5>Payment Details</h5>-->
                        <!--                                 <a href="#"><i class="fa fa-pencil" aria-hidden="true"></i></a> -->
                        <!--                            </div>-->
                        <!--                            <div class="box-contant">-->
                        <!--                                <div class="user-details">-->
                        <!--                                    <h5 class="title">Last Payment</h5>-->
                        <!--                                    <h5 class="sub">$87 on Aug 15,2018 to Direct to Local Bank (INR) - Account ending in 6774</h5>-->
                        <!--                                </div>-->
                        <!--                                <div class="user-details">-->
                        <!--                                    <h5 class="title">Schedule</h5>-->
                        <!--                                    <h5 class="sub">Monthly (next on 09/26/2018) to Direct to Local Bank(INR) - Account e...6774</h5>-->
                        <!--                                    <p>Only when balance is $100.00 or more.</p>-->
                        <!--                                    <a class="ctm-btn" href="#" data-toggle="modal" data-target=".edit-payment-schedule">Edit Schedule</a>-->
                        <!--                                </div>-->
                        <!--                                <div class="user-details payment-method">-->
                        <!--                                    <h5 class="title">Payment Methods</h5>-->
                        <!--                                    <a class="ctm-btn" href="#" data-toggle="modal" data-target=".payment-method-model">Add Methods</a>-->
                        <!--                                </div>-->
                        <!--                                <div class="user-details preferred">-->
                        <!--                                    <h5 class="title">Preferred</h5>-->
                        <!--                                    <img src="images/icons/uni.png">-->
                        <!--                                    <p class="contant">Direct to Local Bank (INR) - Account ending in 6774</p>-->
                        <!--                                    <a href="#"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></a>-->
                        <!--                                </div>-->
                        <!--                            </div>-->
                        <!--                        </div>-->
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade bd-example-modal-lg payment-method-model" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <!-- <img src="images/logo/logo-2.png"> -->
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Add a payment method</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12 m-title">
                                <h5>Recommended for India</h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 img">
                                <img src="images/icons/uni.png">
                            </div>
                            <div class="col-md-6">
                                <h6 class="title">Direct to Local Bank (INS)</h6>
                                <p><i class="fa fa-check" aria-hidden="true"></i>$0.99 USD Per withdrawal</p>
                                <p><i class="fa fa-check" aria-hidden="true"></i>Deposit to your local bank account in INR</p>
                            </div>
                            <div class="col-md-3">
                                <a href="#" class="ctm-btn">Setup</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 m-title">
                                <h5>Also Avilable</h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 img">
                                <img src="images/icons/payple.png">
                            </div>
                            <div class="col-md-6">
                                <h6 class="title">Paypal</h6>
                                <p><i class="fa fa-circle" aria-hidden="true"></i>$2 USD per withdrawal</p>
                                <p><i class="fa fa-circle" aria-hidden="true"></i>Paypal may charge additional fees to send or
                                    withdraw funds, Don’t have a Paypal account?</p>
                            </div>
                            <div class="col-md-3">
                                <a href="#" class="ctm-btn">Setup</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 img">
                                <img src="images/icons/bank.png">
                            </div>
                            <div class="col-md-6">
                                <h6 class="title">Wire Transfer (USD)</h6>
                                <p><i class="fa fa-circle" aria-hidden="true"></i>$30 USD per withdrawal</p>
                                <p><i class="fa fa-circle" aria-hidden="true"></i>Send funds via wire to any bank in USD</p>
                            </div>
                            <div class="col-md-3">
                                <a href="#" class="ctm-btn">Setup</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 img">
                                <img src="images/icons/bank.png">
                            </div>
                            <div class="col-md-6">
                                <h6 class="title">Direct to U.S. Bank (USD)</h6>
                                <p><i class="fa fa-circle" aria-hidden="true"></i>Free payments to U.S. banks</p>
                                <p><i class="fa fa-circle" aria-hidden="true"></i>Deposit to a U.S. bank account in USD</p>
                            </div>
                            <div class="col-md-3">
                                <a href="#" class="ctm-btn">Setup</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade bd-example-modal-lg edit-payment-schedule" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <!-- <img src="images/logo/logo-2.png"> -->
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Edit payment schedule </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="box">
                        <label>Preferred Payment Method</label>
                        <p>STATE BANK OF INDIA, AJMER, JAIPUR...ending in 9...</p>
                    </div>

                    <div class="box">
                        <label>Preferred Payment Schedule</label>
                        <p>Earnings will be released upon your request. <a href="#">Learn more</a></p>
                        <div class="custom-control custom-radio">
                            <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                            <label class="custom-control-label" for="customRadio1">Quaterly (Dec 31,2018)</label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                            <label class="custom-control-label" for="customRadio2">Monthly (last Wednesday of each month)</label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input type="radio" id="customRadio3" name="customRadio" class="custom-control-input">
                            <label class="custom-control-label" for="customRadio3">Twice per month (1st and 3rd Wednesday of each month)</label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input type="radio" id="customRadio4" name="customRadio" class="custom-control-input">
                            <label class="custom-control-label" for="customRadio4">Weekly (every Wednesday)</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Only When Balance is</label>
                        <select class="form-control">
                            <option>$100.00 or more</option>
                        </select>
                        <p class="opasity">Minimize withdrawals fees by choosing larger amounts.</p>
                    </div>
                    <div class="box">
                        <label>Next Payment <span class="opasity">(based on your schedule)</span></label>
                        <p>September 26,2018</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cancle</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>

@endsection
