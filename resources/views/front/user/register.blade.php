<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 5/10/19
 * Time: 1:32 AM
 */
?>
@extends('front.layouts.default')
@section('main_container')

    <section class="signup_area section--padding2">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    <form action="{{action('Front\Auth\RegisterController@create')}}" method="POST" id="registerForm">
                        <div class="cardify signup_form">
                            <div class="login--header">
                                <h3>Create Your Account</h3>
                                <p>Please fill the following fields with appropriate information to register a new
                                    MartPlace
                                    account.
                                </p>
                            </div>
                            <!-- end .login_header -->
                            @csrf
                            <div class="login--form">

                                <div class="form-group">
                                    <label for="urname">First Name</label>
                                    <input type="text" name="first_name" id="first_name" class="text_field" value="{{old('first_name')}}" placeholder="Enter your Name">
                                    <span class="text-danger">{{ $errors->first('first_name') }}</span>
                                </div>
                                <div class="form-group">
                                    <label for="urname">last Name</label>
                                    <input  type="text" name="last_name" id="last_name" class="text_field" value="{{old('last_name')}}" placeholder="Enter your Name">
                                    <span class="text-danger">{{ $errors->first('last_name') }}</span>
                                </div>

                                <div class="form-group">
                                    <label for="email_ad">Email Address</label>
                                    <input  type="text" class="text_field"
                                           placeholder="Enter your email address" id="email" value="{{old('email')}}" name="email">
                                    <span class="text-danger">{{ $errors->first('email') }}</span>
                                </div>
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input  type="password" class="text_field" id="password"  name="password"
                                           placeholder="Enter your password...">
                                    <span class="text-danger">{{ $errors->first('password') }}</span>
                                </div>
                                <?php
                                    $userTypes = config('constants.user_type');
                                ?>
                                <div class="form-group">
                                    <label for="password">Register As</label>
                                    <select class="text_field" name="user_type" id="user_type">
                                        @foreach($userTypes as $key => $value)
                                            <option value="{{$key}}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <button class="btn btn--md btn--round register_btn" type="submit">Register Now</button>

                                <div class="login_assist">
                                    <p>Already have an account?
                                        <a href="{{url('signin')}}">Login</a>
                                    </p>
                                </div>
                            </div>
                            <!-- end .login--form -->
                        </div>
                        <!-- end .cardify -->
                    </form>
                </div>
                <!-- end .col-md-6 -->
            </div>
            <!-- end .row -->
        </div>
        <!-- end .container -->
    </section>
    <!--================================
            END SIGNUP AREA
    =================================-->
@endsection
