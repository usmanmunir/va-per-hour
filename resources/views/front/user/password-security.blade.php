<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 5/14/19
 * Time: 2:50 PM
 */
?>
@extends('front.layouts.default')
@section('main_container')
    <section class="offer-post section-1 EditProfile">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12 post-all-offers">
                    @include('front.layouts.profiletabs')
                    <div class="contact-info passworder-secure">
                        <div class="head-title">
                            <h5>Password & Security</h5>
                        </div>
                        <div class="box password">
                            <div class="title-edit">
                                <h5>Password</h5>
                                <a href="#" data-toggle="modal" data-target=".change-password"><i class="fa fa-pencil"
                                                                                                  aria-hidden="true"></i></a>
                            </div>
                            <div class="box-contant">
                                <div class="user-details">
                                    <h5 class="title"><i class="fa fa-check-circle" aria-hidden="true"></i> Project
                                        preference</h5>
                                    <p>Choose a strong, unique password that’s at least 8 characters long.</p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Modal -->
    <div class="modal fade change-password" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Change Password</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="javascript:;" id="resetpassword">
                    @csrf
                        <div class="form-group">
                            <label for="exampleInputPassword1">Current Password</label>
                            <input type="password" class="form-control" name="current_password"
                                   id="exampleInputPassword1">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword2">New Password</label>
                            <input type="password" class="form-control" name="new_password" id="exampleInputPassword2">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword3">Confirm Password</label>
                            <input type="password" class="form-control" name="confirm_password"
                                   id="exampleInputPassword3">
                        </div>
                </div>
                <div class="modal-footer">
                    {{--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                    <button type="submit" class="btn btn-default">Save changes</button>
                </div>
            </div>
        </div>
    </div>
    @push('scripts')
        <script>
            $(window).on("load", function () {
                $("#resetpassword").validate({
                    rules: {
                        current_password: {required: true},
                        new_password: {required: true},
                        confirm_password: {required: true},
                    },
                    submitHandler: function (form, e) {
                        e.preventDefault();
                        var formData = {

                            current_password: $("[name='current_password']").val(),
                            new_password: $("[name='new_password']").val(),
                            confirm_password: $("[name='confirm_password']").val(),
                        };
                        // console.log(formData);
                        $.ajax({
                            type: "post",
                            url: "{{ url('/password/reset') }}",
                            dataType: "json",
                            data: formData,
                            "headers": {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
                            success: function (data) {
                                resetForm();
                                $("#exampleModalCenter").modal('hide');
                                toastr.success("Password Update successfully");
                            },
                            error: function (data) {
                                toastr.error("The given data was invalid");
                            }

                        });
                    }
                });
                });
            function resetForm() {
                document.getElementById("resetpassword").reset();
            }
        </script>
    @endpush
@endsection
