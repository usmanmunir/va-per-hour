<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 5/14/19
 * Time: 2:59 PM
 */
?>
@extends('front.layouts.default')
@section('main_container')

    <section class="offer-post section-1 EditProfile">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12 post-all-offers">
                    @include('front.layouts.profiletabs')
                    <div class="contact-info passworder-secure">
                        <div class="head-title">
                            <h5>Seller Profile</h5>
                        </div>
                        <div class="box personal-infobox">
                            <div class="title-edit">
                                <h5>Personal Info</h5>
                                <!--                            <a href="#" data-toggle="modal" data-target=".change-password"><i class="fa fa-pencil" aria-hidden="true"></i></a>-->
                            </div>
                            <div class="box-contant">
                                <div class="row pbot20">
                                    <div class="col-md-3 col-sm-12 col-xs-12 pd0">
                                        <div class="form-label">
                                            <span>Languages</span>
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-sm-12 col-xs-12 seller-onboarding">
                                        <div class="field-content ">
                                            <table class="table table-borderless" id="language_table">
                                                <thead>
                                                <tr>
                                                    <th class="main">Language</th>
                                                    <th class="secondary">Proficiency</th>
                                                    <th class="manage">Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="languageadd" id="AddNewLanguage">
                                            <span class="addnew">Add New</span>
                                        </div>
                                        <div class="lang-addbox hide1">
                                            <div class="form-row form-wrapper cf">
                                                <div class="autocomplete-wrap">
                                                    <form action="javascript:;" id="language-form">
                                                        @csrf
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <input type="text" class="cv_text mtop0"
                                                                       placeholder="Language"
                                                                       id="language" name="language">
                                                                <input type="hidden" class="cv_text mtop0"
                                                                       placeholder="Language"
                                                                       id="selectlangid" name="selectlangid">

                                                                <span class="text-danger" id="lg_language_error"></span>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <select class="side-gutter no-value cv_text wauto mtop0"
                                                                        name="proficiency">
                                                                    <option value="">Proficiency</option>
                                                                    <option value="Unspecified">Unspecified</option>
                                                                    <option value="Basic">Basic</option>
                                                                    <option value="Conversational">Conversational
                                                                    </option>
                                                                    <option value="Fluent">Fluent</option>
                                                                    <option value="Native/Bilingual">Native/Bilingual
                                                                    </option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <span class="buttons-wrapper">
                                                                <input name='lg_id' type="hidden" id="userlanguageId">
                                                                    <button class="btn cancel" id="cancelAddLanguage"
                                                                            type="button">Cancel</button>
                                                                 <button class="btn active" id="addLanguage"
                                                                         type="submit"
                                                                         type="button">Add</button>
                                                                 </span>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="box proffesional-infobox">
                                <div class="title-edit">
                                    <h5>Proffesional Info</h5>
                                    <!--                            <a href="#" data-toggle="modal" data-target=".change-password"><i class="fa fa-pencil" aria-hidden="true"></i></a>-->
                                </div>
                                <div class="box-contant">
                                    <div class="row pbot20">
                                        <div class="col-md-3 col-sm-12 col-xs-12 pd0">
                                            <div class="form-label">
                                                <span>Skills</span>
                                            </div>
                                        </div>
                                        <div class="col-md-9 col-sm-12 col-xs-12 seller-onboarding">
                                            <div class="field-content ">
                                                <table class="table table-borderless" id="skillTable">
                                                    <thead>
                                                    <tr>
                                                        <th class="main">Skill</th>
                                                        <th class="secondary">Level</th>
                                                        <th class="manage">Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="languageadd" id="AddNewSkills">
                                                <span class="addnew">Add New</span>
                                            </div>
                                            <div class="lang-addbox hide1">
                                                <div class="form-row form-wrapper cf">
                                                    <div class="autocomplete-wrap">
                                                        <form action="javascript:;" id="skillform">
                                                            @csrf
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <input type="hidden" class="cv_text mtop0"
                                                                           placeholder="Language"
                                                                           id="userskillid" name="userskillid">
                                                                    <input type="text" class="cv_text mtop0"
                                                                           name="skill" id="skill"
                                                                           placeholder="Add skill(E.G. Graphic designer)">
                                                                    <span id="sm_skill_error"></span>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <select class="side-gutter no-value cv_text wauto mtop0"
                                                                            name="skill_level">
                                                                        <option value="">Experience Level</option>
                                                                        <option value="Beginner">Beginner</option>
                                                                        <option value="Intermediate">Intermediate
                                                                        </option>
                                                                        <option value="Expert">Expert</option>
                                                                    </select>
                                                                    <span id="sm_skill_level_error"></span>
                                                                </div>
                                                                <div class="col-md-4">
                                                                <span class="buttons-wrapper">
                                                <input name="skils_id" id="skils_id" type="hidden">
                                                    <button class="btn cancel" id="cancelAddSkills"
                                                            type="button">Cancel</button>
                                                    <button class="btn active" id="addNewSkill"
                                                            type="submit">Add</button>
                                                </span>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                                <p class="skill-error"></p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row pbot20">
                                        <div class="col-md-3 col-sm-12 col-xs-12 pd0">
                                            <div class="form-label">
                                                <span>Education</span>
                                            </div>
                                        </div>
                                        <div class="col-md-9 col-sm-12 col-xs-12 seller-onboarding">
                                            <div class="field-content ">
                                                <table class="table table-borderless" id="educationTable">
                                                    <thead>
                                                    <tr>
                                                        <th class="main">University</th>
                                                        <th class="secondary">Degree & Years</th>
                                                        <th class="manage">Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="languageadd" id="addNewEducation">
                                                <span class="addnew">Add New</span>
                                            </div>
                                            <div class="lang-addbox hide1">
                                                <div class="form-row form-wrapper cf">
                                                    <div class="autocomplete-wrap">
                                                        <form id="education-form" action="avascript:;">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <input type="text" class="cv_text mtop0"
                                                                           placeholder="Add College/University"
                                                                           name="institute_name">
                                                                    <span id="edu_college_error"></span>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <input type="text" class="cv_text mtop0"
                                                                           placeholder="Add Degree" name="degree_name">
                                                                    <span id="edu_degree_error"></span>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <select class="side-gutter no-value cv_text"
                                                                            name="year" id="user-education-year">
                                                                        <option value="">Year</option>
                                                                    </select>
                                                                    <span id="edu_year_error"></span>
                                                                </div>
                                                                <input name="id" type="hidden" id="edu_id">

                                                            </div>
                                                            <span class="buttons-wrapper mtop10">
                                                    <button class="btn cancel" id="cancelAddEducation">Cancel</button>
                                                    <button class="btn active" id="saveEducation"
                                                            type="submit">Save</button>
                                                </span>
                                                        </form>
                                                    </div>
                                                </div>
                                                <p class="education-error"></p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row pbot20">
                                        <div class="col-md-3 col-sm-12 col-xs-12 pd0">
                                            <div class="form-label">
                                                <span>Certification</span>
                                            </div>
                                        </div>
                                        <div class="col-md-9 col-sm-12 col-xs-12 seller-onboarding">
                                            <div class="field-content ">
                                                <table class="table table-borderless" id="certificateTable">
                                                    <thead>
                                                    <tr>
                                                        <th class="main">Certificate or Award</th>
                                                        <th class="secondary">Year</th>
                                                        <th class="manage">Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="languageadd" id="AddNewCertificate">
                                                <span class="addnew">Add New</span>
                                            </div>
                                            <div class="lang-addbox hide1">
                                                <div class="form-row form-wrapper cf">
                                                    <div class="autocomplete-wrap">

                                                        <form id="certificate-form" action="avascript:;">
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <input type="text" class="cv_text mtop0"
                                                                           placeholder="Certificate or Award"
                                                                           name="certificate_name">
                                                                    <span id="certificate_name_error"></span>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="cv_text mtop0"
                                                                           placeholder="Certified From(E.G.Adobe)"
                                                                           name="certificate_type">
                                                                    <span id="certificate_type_error"></span>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <select class="side-gutter no-value cv_text"
                                                                            name="certificate_year"
                                                                            id="user-certificate-year">
                                                                        <option value="">Year</option>

                                                                    </select>
                                                                    <span id="edu_certificate_year_error"></span>
                                                                </div>
                                                                <input name="cert_id" type="hidden" id="cert_id">

                                                            </div>
                                                            <span class="buttons-wrapper mtop10">
                                                            <input name="cer_id" type="hidden">
                                                    <button class="btn cancel" id="cancelAddCertificate">Cancel</button>
                                                    <button class="btn active" id="saveCerificate"
                                                            type="submit">Save</button>
                                                </span>
                                                        </form>

                                                    </div>
                                                </div>
                                                <p class="certificate-error"></p>
                                            </div>
                                        </div>
                                    </div>

                                    <form action="javascript:;" id="websiteForm">
                                        <div class="row pbot20">
                                            <div class="col-md-3 col-sm-12 col-xs-12 pd0">
                                                <div class="form-label">
                                                    <span>Personal website</span>
                                                </div>
                                            </div>

                                            <div class="col-md-9 col-sm-12 col-xs-12">
                                                <div class="cvform-box">
                                                    <input type="text" class="cv_text" name="website"
                                                           value=""
                                                           placeholder="Provide a link to your own professional website">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row pbot20 mtop50">
                                            <div class="col-md-12">
                                                <button id="savewebsite" type="submit"
                                                        class="green-btn pull-right">Save
                                                </button>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    @push('scripts')
        @include('front.user.includes.user-language-js')
        @include('front.user.includes.user-education-js')
    @endpush


@endsection
