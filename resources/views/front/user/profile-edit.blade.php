<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 5/13/19
 * Time: 2:04 PM
 */
?>
@extends('front.layouts.default')
@section('main_container')
    <?php //dd($userLocation);?>
    <section class="offer-post section-1 EditProfile">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12 post-all-offers">
                    @include('front.layouts.profiletabs')
                    <div class="contact-info">

                        <div class="head-title">
                            <h5>Contact Info</h5>
                        </div>

                        <div class="box account">
                            <div class="title-edit">
                                <h5>Profile</h5>
                                <!--                        <a href="#"><i class="fa fa-plus" aria-hidden="true"></i></a>-->
                            </div>

                            <form enctype="multipart/form-data" method="post"
                                  action="{{ action('Front\MasterUserController@updateAvatar') }}">
                                @csrf
                                <div class="box-contant">
                                    <div class="user-details">
                                        @if(Auth::check())
                                            <img src="{{Auth::user()->avatar}}"
                                                 height="100">
                                        @else{
                                        <img src="/uploads/profile/e287a6a37595a05adcb8b9e7648b1b6c.jpg"
                                             height="100">
                                        }
                                        @endif
                                        <h5 class="title">Please select profile photo</h5>
                                        <div class="form-groups">
                                            <input type="file" name="avatar" required="">
                                        </div>
                                    </div>
                                    <div class="submit_box">
                                        <button type="submit" class="send-submit" value="" name="update-submit">
                                            Update
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="box account">
                            <form action="{{ action('Front\MasterUserController@updateUserDetail') }}" method="post">
                                <div class="title-edit">
                                    <h5>Account</h5>
                                    <a href="#" class="edit-profile-user-details"><i class="fa fa-pencil"
                                                                                     aria-hidden="true"></i></a>
                                </div>
                                <div class="box-contant profile-user-details">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">
                                            {{--<div class="user-details">--}}
                                            {{--<h5 class="title">Username</h5>--}}
                                            {{--<h5 class="sub">bhavesh</h5>--}}
                                            {{--<div class="form-groups hide1">--}}
                                            {{--<input type="text" class="ptext w100" value="bhavesh" disabled="">--}}
                                            {{--</div>--}}
                                            {{--</div>--}}
                                            <div class="user-details">
                                                <h5 class="title">First Name</h5>
                                                <h5 class="sub">{{Auth::user()->first_name}}</h5>
                                                <div class="form-groups hide1">
                                                    <input type="text" name="first_name" class="ptext w100"
                                                           value="{{Auth::user()->first_name}}">
                                                </div>
                                            </div>

                                            <div class="user-details">
                                                <h5 class="title">Last Name</h5>
                                                <h5 class="sub">{{Auth::user()->last_name}}</h5>
                                                <div class="form-groups hide1">
                                                    <input type="text" name="last_name" class="ptext w100"
                                                           value="{{Auth::user()->last_name}}">
                                                </div>
                                            </div>
                                            <div class="user-details">
                                                <h5 class="title">Email</h5>
                                                <h5 class="sub">{{Auth::user()->email}}</h5>
                                                <div class="form-groups hide1">
                                                    <input type="email" class="ptext w100 " disabled=""
                                                           value="{{Auth::user()->email}}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <div class="user-details">
                                                <h5 class="title">Write something about you</h5>
                                                <h5 class="sub">{{Auth::user()->description}}</h5>

                                                <div class="form-groups hide1">
                                                    <textarea name="description" class="ptext w100" rows="5">{{Auth::user()->description}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <a href="#" class="close-account">Close My
                                        Account</a>
                                    <div class="cm-error"></div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="submit_box hide1">
                                                <button type="submit" class="send-submit profile-user-details-submit"
                                                        value="" name="update-submit" id="send-submit">Update
                                                </button>
                                            </div>
                                            <div class="submit_box hide1">
                                                <button type="reset" class="default-submit edit-profile-user-details"
                                                        value="" name="cancel-submit"> Cancel
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="box account">
                            <form action="{{ action('Front\MasterUserController@updateUserLocation') }}" method="post"
                                  id="location-profile-update">
                                <div class="title-edit">
                                    <h5>Location</h5>
                                    <a href="#" class="edit-location-icon"><i class="fa fa-pencil"
                                                                              aria-hidden="true"></i></a>
                                </div>
                                @csrf
                                <div class="box-contant">
                                    <div class="row">

                                        <div class="col-md-6 col-sm-12">
                                            <div class="user-details">
                                                <h5 class="title">Time Zone</h5>
                                                <h5 class="sub"></h5>
                                                <div class="form-groups hide1">
                                                    <select class="ptext w100" name="time_zone">
                                                        <option value="UTC 1" selected="">UTC + 05:30 Mumbai, Kolkata,
                                                            Chennai, New Delhi
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="user-details">
                                                <h5 class="title">Address</h5>
                                                <p class="sub">
                                                    {{!empty($userLocation->address) ? $userLocation->address : ''}}
                                                </p>
                                                <div class="form-groups hide1">
                                                    <textarea class="ptext w100" name="address" rows="5">{{!empty($userLocation->address) ? $userLocation->address : ''}}</textarea>
                                                </div>
                                            </div>
                                            <div class="user-details">
                                                <h5 class="title">Phone</h5>
                                                <h5 class="sub">{{!empty($userLocation->phone) ? $userLocation->phone : ''}}</h5>
                                                <div class="form-groups hide1">
                                                    <input type="text" name="phone" class="ptext w100"
                                                           value="{{!empty($userLocation->phone) ? $userLocation->phone : ''}}">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6 col-sm-12">
                                            <div class="user-details">
                                                <h5 class="title">City</h5>
                                                <h5 class="sub">{{!empty($userLocation->city) ? $userLocation->city : ''}}</h5>
                                                <div class="form-groups hide1">
                                                    <input type="text" name="city" class="ptext w100"
                                                           value="{{!empty($userLocation->city) ? $userLocation->city : ''}}"
                                                           placeholder="Enter City">
                                                </div>
                                            </div>

                                            <div class="user-details">
                                                <h5 class="title">Country</h5>
                                                <h5 class="sub">{{!empty($userLocation['Country']->name) ? $userLocation['Country']->name : ''}}</h5>
                                                <div class="form-groups hide1">
                                                    <select name="country" class="ptext w100" name="country">
                                                        @foreach($countries as $country)
                                                            <option value="{{$country->id}}" {{ !empty($userLocation['Country']->id) && $country->id == $userLocation['Country']->id ? 'selected="selected"' : ''}}>{{$country->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                            </div>
                                            <div class="user-details">
                                                <h5 class="title">State</h5>
                                                <h5 class="sub">{{!empty($userLocation->state) ? $userLocation->state: ''}}</h5>
                                                <div class="form-groups hide1">
                                                    <input type="text" name="state" class="ptext w100"
                                                           value="{{!empty($userLocation->state) ? $userLocation->state : ''}}"
                                                           placeholder="Enter state">
                                                </div>
                                            </div>

                                            <div class="user-details">
                                                <h5 class="title">Zip/Postal</h5>
                                                <h5 class="sub">{{!empty($userLocation->zipcode) ? $userLocation->zipcode: ''}}</h5>
                                                <div class="form-groups hide1">
                                                    <input type="text" name="zipcode" class="ptext w100"
                                                           value="{{!empty($userLocation->zipcode) ? $userLocation->zipcode : ''}}"
                                                           placeholder="Zipcode">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="cm-error"></div>

                                        <div class="col-md-12">
                                            <div class="submit_box hide1">
                                                <button type="submit" class="send-submit profile-user-location-submit"
                                                        value="" name="update-submit" id=""> Update
                                                </button>
                                            </div>
                                            <div class="submit_box hide1">
                                                <button type="submit" class="default-submit" value=""
                                                        name="cancel-submit" id="send-submit"> Cancel
                                                </button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="box account " style="display: none;">
                            <div class="title-edit">
                                <h5>Invoice address (Optional)</h5>
                                <a href="#" class="invoce-address-icon"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                            </div>
                            <form id="invoice-address">
                                <div class="box-contant">
                                    <div class="user-details">
                                        <h5 class="sub">This address will be displayed on the invoce sent to
                                            clients.</h5>
                                    </div>
                                    <div class="user-details">
                                        <h5 class="title">Address</h5>
                                        <h5 class="sub">
                                            <div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">


                                            </div>
                                        </h5>
                                        <div class="form-groups hide1">
                                    <textarea class="ptext w50" rows="5" name="invoice_address">&lt;div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;"&gt;

</textarea>
                                        </div>
                                    </div>
                                    <div class="cm-error"></div>
                                    <div class="submit_box hide1">
                                        <button type="submit" class="send-submit invoice-address-submit" value=""
                                                name="update-submit"> Update
                                        </button>
                                    </div>
                                    <div class="submit_box hide1">
                                        <button type="submit" class="default-submit" value="" name="cancel-submit">
                                            Cancel
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection