<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 5/14/19
 * Time: 1:22 AM
 */
?>
@extends('front.layouts.default')
@section('main_container')
    <section class="offer-post section-1 EditProfile">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12 post-all-offers">
                    @include('front.layouts.profiletabs')

                        <div class="membership-contant">
                        <div class="head-title">
                            <h5>Membership & Connects</h5>
                            <div class="paid-btn pull-right" style="margin-top: -15px">
                                <a href="{{action('Front\UserMembershipsController@index')}}" class="ctm-btn">Go to Membership</a>
                            </div>
                        </div>

                        <div class="summary">
                            <div class="title-sub">
                                <h5>Summary</h5>
                            </div>
                            <div class="box current-plan">
                                <p class="title" style="text-align: center">Current Plans</p>
                            </div>
                            @if($memberships)
                                @foreach($memberships as $membership)
                                    <div class="box current-plan">
                                        <p class="title">{{$membership->membership->title}}</p>
                                    </div>
                                    <div class="box availabel-stints membership-desc">
                                        <p class="title">
                                            {!! $membership->membership->description !!}
                                        </p>
                                        <!-- <p class="sub-title">7</p> -->
                                    </div>
                                    <div class="box membership-fee">
                                        <p class="title">Membership Fees</p>
                                        <p class="sub-title">${{$membership->membership->price}} per {{$membership->membership->month}} months </p>
                                    </div>
                                    <div class="box billing">
                                        <p class="title">Expiring On</p>
                                        <p class="sub-title">
                                                {{date("Y-m-d", strtotime($membership->next_due_date))}}
                                        </p>
                                        <a href="#" class="learn-more">Learn More</a>
                                    </div>
                                @endforeach
                            @else
                                <div class="alert alert-warning">no membership of you found</div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
