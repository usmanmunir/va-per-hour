<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 5/20/19
 * Time: 2:45 PM
 */
?>
@extends('front.layouts.default')
@section('main_container')
    <section class="offer-post section-1 EditProfile">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12 post-all-offers">
                    @include('front.layouts.profiletabs')
                    <div class="contact-info user-instructor">
                        <div class="head-title">
                            <h5>Instructor Profile</h5>
                        </div>
                        <div class="box proffesional-infobox">
                            <form action="#" method="post" id="instructor-form">
                                @csrf
                                <div class="title-edit">
                                    <h5>Instructor</h5>
                                    <a href="#" class="edit-instructor"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                </div>
                                <div class="box-contant">
                                    <div class="row pbot20">
                                        <div class="col-md-3 col-sm-12 col-xs-12 pd0">
                                            <div class="form-label">
                                                <span>Category</span>
                                            </div>
                                        </div>
                                        <div class="col-md-9 col-sm-12 col-xs-12">
                                            <label class="sub">
                                                {{!empty($instructorRecord['category']->name) ? $instructorRecord['category']->name : ''}}</label>
                                            <div class="cvform-box hide1">
                                                <select class="cv_text" name="category">
                                                    @foreach($categories as $category)
                                                        {{--<option value=""> {{!empty($instructorRecord->category) ? $instructorRecord->category : ''}}</option>--}}
                                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row pbot20">
                                        <div class="col-md-3 col-sm-12 col-xs-12 pd0">
                                            <div class="form-label">
                                                <span>Course Cover</span>
                                            </div>
                                        </div>
                                        <div class="col-md-9 col-sm-12 col-xs-12">
                                            <label class="sub"> {{!empty($instructorRecord->course_skills) ? $instructorRecord->course_skills : ''}}</label>
                                            <div class="cvform-box hide1">
                                                <textarea class="cv_text"
                                                          name="course_skills">{{!empty($instructorRecord->course_skills) ? $instructorRecord->course_skills : ''}}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row pbot20">
                                        <div class="col-md-3 col-sm-12 col-xs-12 pd0">
                                            <div class="form-label">
                                                <span>Level Expertise</span>
                                            </div>
                                        </div>
                                        <div class="col-md-9 col-sm-12 col-xs-12">
                                            <label class="sub">{{$instructorRecord && !empty($instructorRecord->expertise_level) ? $instructorRecord->expertise_level : ''}}</label>
                                            <div class="cvform-box hide1">
                                                <div class="queston_option">
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="expertise_level1" name="expertise_level"
                                                               class="custom-control-input" value="Intermediate"
                                                        <?php echo ($instructorRecord && $instructorRecord->expertise_level == 'Intermediate') ? 'checked' : '' ?>>

                                                        <label class="custom-control-label" for="expertise_level1">
                                                            Intermediate
                                                        </label>
                                                    </div>
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="expertise_level2" name="expertise_level"
                                                               class="custom-control-input" value="Advanced"
                                                        <?php echo ($instructorRecord && $instructorRecord->expertise_level == 'Advanced') ? 'checked' : '' ?>>

                                                        <label class="custom-control-label" for="expertise_level2">
                                                            Advanced
                                                        </label>
                                                    </div>
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="expertise_level3" name="expertise_level"
                                                               class="custom-control-input" value="Expert"
                                                        <?php echo ($instructorRecord && $instructorRecord->expertise_level == 'Expert') ? 'checked' : '' ?>
                                                        >

                                                        <label class="custom-control-label" for="expertise_level3">
                                                            Expert
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row pbot20">
                                        <div class="col-md-3 col-sm-12 col-xs-12 pd0">
                                            <div class="form-label">
                                                <span>Professional Experience</span>
                                            </div>
                                        </div>
                                        <div class="col-md-9 col-sm-12 col-xs-12">
                                            <label class="sub">{{!empty($instructorRecord->professional_experience) ? $instructorRecord->professional_experience : ''}}</label>
                                            <div class="cvform-box hide1">
                                                <textarea class="cv_text"
                                                          name="professional_experience">{{!empty($instructorRecord->professional_experience) ? $instructorRecord->professional_experience : ''}}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row pbot20">
                                        <div class="col-md-3 col-sm-12 col-xs-12 pd0">
                                            <div class="form-label">
                                                <span>Courses Links</span>
                                            </div>
                                        </div>
                                        <div class="col-md-9 col-sm-12 col-xs-12">
                                            <label class="sub col-sm-12 pbot10">professional experience 1</label>
                                            <div class="cvform-box pbot20 hide1" id="dynamic_field">
                                                <div class="row courselink">
                                                    <div class="col-md-3">
                                                        <label class="col-sm-12">Course Link</label>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type="text" class="cv_text" name="showcase_links[]"
                                                               id="showcase_links" value="professional experience 1">
                                                    </div>
                                                    <div class="col-md-3">
                                                        <i class="fa fa-plus-circle fa-3x" aria-hidden="true"
                                                           name="add_more" id="add_more" style="color:green;"></i>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="row pbot20">
                                        <div class="col-md-3 col-sm-12 col-xs-12 pd0">
                                            <div class="form-label">
                                                <span>Comments</span>
                                            </div>
                                        </div>
                                        <div class="col-md-9 col-sm-12 col-xs-12">
                                            <label class="sub">{{!empty($instructorRecord->comments) ? $instructorRecord->comments : ''}}</label>
                                            <div class="cvform-box hide1">
                                                <textarea class="cv_text"
                                                          name="other_comments">{{!empty($instructorRecord->comments) ? $instructorRecord->comments : ''}}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row pbot20 mtop50 submit_box hide1">
                                        <div class="col-md-12">
                                            <button type="submit" class="green-btn pull-right">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    @push('scripts')
        @include('front.user.includes.instructor-js')
    @endpush

@endsection