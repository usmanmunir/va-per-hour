<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 5/14/19
 * Time: 1:32 AM
 */
?>
@extends('front.layouts.default')
@section('main_container')
    <section class="offer-post section-1 EditProfile">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12 post-all-offers">

                    @include('front.layouts.profiletabs')
                    <div class="contact-info my-profile-setting">

                        <div class="head-title">
                            <h5>Profile Settings</h5>
                        </div>
                        <div class="box linked-accounts">
                            <div class="title-edit">
                                <h5>Linked accounts</h5>
                                <a href="#" class="edit-social-link-icon"><i class="fa fa-pencil"
                                                                             aria-hidden="true"></i></a>
                            </div>
                            <div class="box-contant">

                                <form action="{{action('Front\MasterUserController@linkdAccounts')}}" method="post">

                                    <div class="social-link-save sub">
                                        <span class="socail-link active">
                                            <a href="" target="_blank">
                                                <i class="fa fa-google" aria-hidden="true"></i>Linkedin</a></span>
                                    </div>
                                    @csrf
                                    <span class="socail-link form-groups hide1">
                                    <div class="social-link-box">
                                        <input type="text" class="cv_text" name="linked_in"
                                               placeholder="Enter Linked account link"
                                               value="{{$userAccountLinks ? $userAccountLinks->linked_in : ''}}">
                                      </div>
                                         <span class="text-danger">{{ $errors->first('linked_in') }}</span>
                                </span>

                                    <div class="social-link-save sub">
                                        <span class="socail-link active">
                                            <a href="" target="_blank">
                                                <i class="fa fa-google" aria-hidden="true"></i>Google</a></span>
                                    </div>

                                    <span class="socail-link form-groups hide1">
                                    <div class="social-link-box">
                                        <input type="text" class="cv_text" name="google"
                                               placeholder="Enter Google link" value="{{$userAccountLinks ? $userAccountLinks->google : ''}}">
                                      </div>
                                        <span class="text-danger">{{ $errors->first('google') }}</span>
                                </span>

                                    <div class="social-link-save sub">
                                        <span class="socail-link active">
                                            <a href="" target="_blank">
                                                <i class="fa fa-facebook" aria-hidden="true"></i>Facebook</a></span>
                                    </div>

                                    <span class="socail-link form-groups hide1">
                                    <div class="social-link-box">
                                        <input type="text" class="cv_text" name="facebook"
                                               placeholder="Enter facebook account link"
                                               value="{{$userAccountLinks ? $userAccountLinks->facebook : ''}}">
                                      </div>
                                         <span class="text-danger">{{ $errors->first('facebook') }}</span>

                                </span>

                                    <div class="social-link-save sub">
                                        <span class="socail-link active">
                                            <a href=""><i class="fa fa-twitter"
                                                                                        aria-hidden="true"></i>
                                                Twitter</a></span>
                                    </div>

                                    <span class="socail-link form-groups hide1">
                                    <div class="social-link-box">
                                        <input type="text" class="cv_text" name="twitter"
                                               placeholder="Enter Twitter account link"
                                               value="{{$userAccountLinks ? $userAccountLinks->twitter : ''}}">
                                      </div>
                                        <span class="text-danger">{{ $errors->first('twitter') }}</span>
                                </span>

                                    <div class="social-link-save sub">
                                        <span class="socail-link active"><a href=""><i
                                                        class="fa fa-github" aria-hidden="true"></i>Github</a></span>
                                    </div>

                                    <span class="socail-link form-groups hide1">
                                    <div class="social-link-box">
                                        <input type="text" class="cv_text" name="github"
                                               placeholder="Enter Github account link"
                                               value="{{$userAccountLinks ? $userAccountLinks->github : ''}}">
                                    </div>
                                          <span class="text-danger">{{ $errors->first('github') }}</span>
                                </span>

                                    <div class="social-link-save sub">
                                        <span class="socail-link active"><a
                                                    href=""><i
                                                        class="fa fa-stack-overflow" aria-hidden="true"></i>stack overflow</a></span>
                                    </div>

                                    <span class="socail-link form-groups hide1">
                                    <div class="social-link-box">
                                        <input type="text" class="cv_text" name="stack_overflow"
                                               placeholder="Enter stack overflow account link"
                                               value="{{$userAccountLinks ? $userAccountLinks->stack_overflow : ''}}">
                                      </div>
                                        <span class="text-danger">{{ $errors->first('stack_overflow') }}</span>
                                </span>

                                    <div class="social-link-save sub">
                                        <span class="socail-link active"><a href="#"><i
                                                        class="fa fa-behance" aria-hidden="true"></i>Behance</a></span>
                                    </div>

                                    <span class="socail-link form-groups hide1">
                                    <div class="social-link-box">
                                        <input type="text" class="cv_text" name="behance"
                                               placeholder="Enter Behance account link"
                                               value="{{$userAccountLinks ? $userAccountLinks->behance : ''}}">
                                      </div>
                                         <span class="text-danger">{{ $errors->first('behance') }}</span>
                                </span>

                                    <div class="col-md-12 row">
                                        <div class="submit_box hide1">
                                            <button type="submit" class="send-submit" value="" name="update-submit"
                                                    id=""> Update
                                            </button>
                                        </div>
                                        <div class="submit_box hide1">
                                            <button type="reset" class="default-submit" value="" name="cancel-submit">
                                                Cancel
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
