<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 7/15/19
 * Time: 1:31 PM
 */
?>

@extends('front.layouts.default')
@section('main_container')

    <section id="chat_wrap" class="chat_main_wrap">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="chatbox">
                        <div class="row cmleft0">
                            <div class="col-md-4 pdright0">
                                <div class="list_header">
                                    <div class="chat_convert">
                                        <div class="dropdown show">
                                            <a class="dropdown-toggles" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                All Conversations ({{count($rooms)}})
                                            </a>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="chat_user_list_box">
                                    @if($rooms)
                                        @foreach($rooms as $room)
                                            <?php $participant = Auth::user()->id == $room->participant_2 ? $room->user_one : $room->user_two ?>
                                            <div class="single_user_chat_box ">
                                                <a href="/messages/rooms/{{$room->id}}">

                                                    <div class="user_image">
                                                        <img src="{{$participant->avatar}}" class="img img-responsive" height="50" width="50">
                                                    </div>
                                                    <div class="chat_user_content">
                                                        <div class="user_line_one">
                                                            <div class="user_name_chat">
                                                                <h3>{{$participant->first_name}} {{$participant->last_name}}</h3>
                                                            </div>
                                                            <div class="user_time_chat">
                                                                <h4>{{date("M d, Y, H:i", strtotime($room->created_at))}}</h4>
                                                            </div>
                                                        </div>
                                                        <div class="user_line_second">
                                                            <p>{{$room->username}}</p>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>

                            <div class="chatbox-container pdleft0 col-md-8">
                                <?php $recipient =  Auth::user()->id == $room->participant_2 ? $room->user_one : $room->user_two;
                                ?>
                                <div class="user_message_box" style="height: 498px;">
                                    @if(empty($openRoom))
                                    <div class="chat-star-screen">
                                        <img src="/front/assets/images/chat.png">
                                        <h2>Select a Conversation</h2>
                                        <p>Try selecting a conversation or searching <br>for someone specific.</p>
                                    </div>
                                    @else
                                        @foreach($openRoom->messages as $msg)
                                        <div class="user_cn_msg {{$msg->direction}}">
                                            <div class="user_image">
                                                <img src="{{$msg->direction == 'in' ? $recipient->avatar : Auth::user()->avatar}}" class="img img-responsive" height="50" width="50">
                                            </div>
                                            <div class="chat_user_content">
                                                <div class="user_line_one">
                                                    <div class="user_name_chat">
                                                        <h3>{{$msg->direction == 'in' ? $recipient->first_name.' '.$recipient->last_name: Auth::user()->first_name.' '.Auth::user()->last_name}}</h3>
                                                    </div>
                                                    <div class="user_time_chat">
                                                        <h4>{{date('M d, Y H:i', strtotime($msg->created_at))}}</h4>
                                                    </div>
                                                </div>
                                                <div class="user_line_second">
                                                    <p class="delivery-msg">{{$msg->message}}</p>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    @endif
                                </div>
                                @if(!empty($openRoom))
                                <div class="user_message_write">
                                    <form action="" method="post">
                                        <div class="file_box">

                                            <div class="file_attch">
                                                <label for="file-input">
                                                    <i class="fa fa-paperclip"></i>
                                                </label>

                                        </div>
                                        <div class="write_box">
                                            <input type="hidden" name="receiver_id" id="receiver_id" value="{{$recipient->id}}">
                                            <textarea class="chat_textarea" id="message" name="message"></textarea>
                                            <div class="dropzone needsclick chat-file-upload hide1 dz-clickable" id="chat-file-upload">
                                                <div class="dz-message ">
                                                    <i class="fa fa-picture-o" aria-hidden="true"></i>
                                                    Drop files here or click to upload.<br>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="submit_box">
                                            @if(session('role') == 'seller')
                                            <button type="button" class="btn btn-block" data-toggle="modal" data-target="#offer-model">Create Offer</button>
                                            @endif
                                            <input type="hidden" name="offer_id" id="offer_id"/>
                                            <button type="button" class="send-submit" value="" name="send-submit" id="chat-send"> Send</button>
                                        </div>
                                    </form>
                                    <div class="chat-error"></div>
                                </div>
                                @endif
                                <input type="hidden" class="chat-type" value="stint">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @if(session('role') == 'seller')
    <div class="modal bd-example-modal-lg offer-model" tabindex="-1" id="offer-model" role="dialog" aria-labelledby="myLargeModalLabel" aria-modal="true">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Send Offer</h5>
                </div>
                <div class="modal-body">
                    <form action="" method="post" id="sentoffer_form" novalidate="novalidate">
                        <div class="so_error_message" style="display: none;"></div>
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="offer-box">
                                    <label>Budget</label>
                                    <input type="text" id="amount" name="amount" placeholder="Enter your budget" class="offer-text" value="">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="offer-box">
                                    <label>Delivery Time</label>
                                    <input type="text" id="delivery_time" name="delivery_time" placeholder="Enter your delivery time" class="offer-text" value="">
                                </div>
                            </div>
                        </div>
                        <div class="row mtop30">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="offer-box">
                                    <label>Revisions</label>
                                    <select id="revisions" name="revisions" class="offer-text valid" aria-invalid="false">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row mtop20">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="offer-box">
                                    <label>Description</label>
                                    <textarea rows="5" class="offer-text crelative" name="proposal" id="proposal"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row mtop10">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="offer-box">
                                    <input type="hidden" id="user_id" name="user_id" value="{{Auth::user()->id}}">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="hidden" name="stint_id" value="{{!empty($openRoom) ? $openRoom->stint_id : ''}}" />
                                    <button type="button" class="ctm-btn-lg close-btn pull-left" data-dismiss="modal">Close</button>
                                    <button type="submit" class="ctm-btn-lg pull-right" id="sentOffer-btn" name="sentOffer-btn"> Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @endif
@endsection
@if($openRoom)
@push('scripts')
    <script>
        $(function () {
            $("#chat-send").click(function(){
                $.ajax({
                    url: '/messages/rooms/{{$openRoom->id}}/send',
                    type: 'POST',
                    data: {
                        message: $("#message").val(),
                        receiver_id: $("#receiver_id").val(),
                        offer_id: $("#offer_id").val(),
                        _token: "{{csrf_token()}}"
                    },
                    beforeSend:function () {
                        $("#chat-send").html('<i class="fa fa-spin fa-spinner"></i>')
                    },
                    success: function (res) {
                        toastr.success('Message has been sent!');
                        $("#message").val('');
                        $("#chat-send").html('Send');
                    },
                    error: function (err) {
                        toastr.error('Something went wrong, please try again!');
                        $("#chat-send").html('Send');
                    }
                })
            });
            @if(session('role') == 'seller')
            $("#sentoffer_form").validate({
                rules: {
                    proposal: {
                        required: true,
                        maxlength: 1500,
                        minlength: 5,
                    },
                    stint_id: {required: true},
                    amount: {required: true, number: true},
                    delivery_time: {required: true, number: true},
                },
                submitHandler: function (form, e) {
                    e.preventDefault();
                    $.ajax({
                        url: '/job/make/offer',
                        method: 'post',
                        data: $("#sentoffer_form").serialize(),
                        success: function (res) {
                            toastr.success(res.message);
                            $(".close-btn").trigger('click');
                            $("#offer_id").val(res.data.id);
                            $("#message").val($("#proposal").val());
                            $("#chat-send").trigger('click');
                        },
                        error: function (err) {
                            toastr.error( err.responseJSON.message);
                        }
                    });
                }
            });
            @endif
        })
    </script>
@endpush
@endif
