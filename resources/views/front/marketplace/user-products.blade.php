<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 6/20/19
 * Time: 3:54 PM
 */
?>
@extends('front.layouts.default')
@section('main_container')
    @push('styles')
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css">
        <style>
            .dropzone {
                background: white;
                border-radius: 5px;
                border: 2px dashed rgb(0, 135, 247);
                border-image: none;
                max-width: 500px;
                margin-left: auto;
                margin-right: auto;
            }

            .btn {
                background: none;
                box-shadow: none;
            }

            section.section-1.section--padding-marketplace {
                padding: 50px 70px;
                background-color: #eef1f5;
            }

            .top-menu-area {
                padding: 0px 0 !important;
            }

            .btn-seller {
                float: left;
                width: auto;
                margin-top: 42px;
            }

            .other-info-2 .card-body label {
                width: auto !important;
            }

        </style>

    @endpush
    <section class="ctn banner">
        <div class="main-banner">
            <div class="banner-img">
                <!-- <div class="overlay-img"></div> -->
                <img src="/front/images/post-offer.png">
            </div>
            <div class="banner-contant">
                <h2 class="banner-title-2"> My MarketPlace Products </h2>
                <h5 class="banner-title-3">Products being offered by me</h5>
            </div>
        </div>
    </section>
    <section class="offer-post section-1">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12 post-all-offers">
                    <div class="tab-content post-oofer-contant" id="myTabContent">
                        <div class="priority-contant tab-pane fade show active" id="priority" role="tabpanel"
                             aria-labelledby="priority-tab">
                            <div class="orders-format priority-orders">
                                <div class="table-responsive mtop20">
                                    <table class="table table-bordred" id="userCourseTable">
                                        <thead>
                                        <tr>
                                            <th width="5%">#</th>
                                            <th width="15%">Name</th>
                                            <th width="25%">Thumbnail Image</th>
                                            <th width="10%">Price</th>
                                            <th width="20%">Created</th>
                                            <th width="20%">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($userMarketPlaceProducts)
                                            @foreach($userMarketPlaceProducts as $userMarketPlaceProduct)
                                                <tr class="course-row" data-record="{{$userMarketPlaceProduct->id}}">
                                                    <td data-id="{{$userMarketPlaceProduct->id}}">{{$userMarketPlaceProduct->id}}</td>
                                                    <td data-name="{{$userMarketPlaceProduct->name}}">{{$userMarketPlaceProduct->name}}</td>
                                                    <td data-image="{{$userMarketPlaceProduct->id}}"><img
                                                                src="{{$userMarketPlaceProduct->thumbnail_image}}"
                                                                width="40%"></td>
                                                    <td data-price="{{$userMarketPlaceProduct->regular_license}}">{{$userMarketPlaceProduct->regular_license}}</td>
                                                    <td data-created="{{$userMarketPlaceProduct->created_at}}">{{$userMarketPlaceProduct->created_at}}</td>
                                                    <td>
                                                        <a class="types course-model-open1" title="edit"
                                                           href="/marketplace/user/edit-product/{{$userMarketPlaceProduct->id}}">
                                                            <i class="fa fa-edit editUserCourse"
                                                               style="color: #0e73e6 !important;"></i>
                                                        </a>
                                                        <a class="types" href="javascript:;" title="Remove it">
                                                            <i class="fa fa-trash removeUserMarketplaceProduct"
                                                               style="color: #0e73e6 !important;"
                                                               marketplaceProductId='{{$userMarketPlaceProduct->id}}'></i>
                                                        </a>
                                                        <a class="types"
                                                           href="/elearning/course/{{$userMarketPlaceProduct->id}}/chapters"
                                                           title="View Content">
                                                            <i class="fa fa-eye" style="color: #0e73e6 !important;"
                                                               coueseId='{{$userMarketPlaceProduct->id}}'></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                    {{ $userMarketPlaceProducts->onEachSide(1)->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>
    <script type="text/javascript">
        Dropzone.autoDiscover = false;
        Dropzone.options.dropzone =
            {
                maxFilesize: 500000000,
                renameFile: function (file) {
                    var dt = new Date();
                    var time = dt.getTime();
                    return time + file.name;
                },
                acceptedFiles: "gif|jpg|png|jpeg",
                addRemoveLinks: true,
                timeout: 5000,
                success: function (file, response) {
                    console.log(response);
                },
                error: function (file, response) {
                    return false;
                }
            };
    </script>
    @include('front.marketplace.marketplace-js')

@endpush

