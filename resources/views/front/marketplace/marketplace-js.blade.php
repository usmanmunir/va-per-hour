<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 6/17/19
 * Time: 2:43 PM
 */
?>
<script>
    $(function () {
        $("#marketplaceForm").validate({
            rules: {
                category_id: {required: true},
                name: {required: true},
                description: {required: true},
                files: {required: true},
                mainfile: {required: true},
                regular_license: {required: true},
                extended_license: {required: true},
            },
            submitHandler: function (form, e) {
                e.preventDefault();
                //then get the form data
                for (instance in CKEDITOR.instances) {
                    CKEDITOR.instances[instance].updateElement();
                }
                var bla = $('#description').val();
                //then get the form data
                var fd = new FormData(form);
                fd.append('description', bla);
                $.ajax({
                    type: "post",
                    url: "{{ url('/marketplace/save-marketplace-product') }}",
                    dataType: "json",
                    "headers": {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
                    data: fd,
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function () {
                        //$("#preview").fadeOut();
                        $("#err").fadeOut();
                    },
                    success: function (data) {
                        toastr.success(data.message);
                        window.location = '/marketplace/home';
                    },
                    error: function (data) {
                        toastr.error("Something went wrong");
                    }
                });
            }
        });
        $("#editMarketplaceForm").validate({
            rules: {
                category_id: {required: true},
                name: {required: true},
                description: {required: true},
                files: {required: true},
                mainfile: {required: true},
                regular_license: {required: true},
                extended_license: {required: true},
            },
            submitHandler: function (form, e) {
                e.preventDefault();
                //then get the form data
                for (instance in CKEDITOR.instances) {
                    CKEDITOR.instances[instance].updateElement();
                }
                var bla = $('#description').val();
                //then get the form data
                var fd = new FormData(form);
                fd.append('description', bla);
                $.ajax({
                    type: "post",
                    url: "{{ url('/marketplace/edit-marketplace-product') }}",
                    dataType: "json",
                    "headers": {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
                    data: fd,
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function () {
                        //$("#preview").fadeOut();
                        $("#err").fadeOut();
                    },
                    success: function (data) {
                        toastr.success(data.message);
                        window.location = '/marketplace/home';
                    },
                    error: function (data) {
                        toastr.error("Something went wrong");
                    }
                });
            }
        });

    });
    $(document).on("click", ".removeUserMarketplaceProduct", function (e) {

        e.preventDefault();

        var userMarketplaceProductId = $(this).attr("marketplaceProductId");

        if (confirm("Are you want to sure?")) {

            deleteUserMarketplaceProduct(userMarketplaceProductId);
        }

    });

    function deleteUserMarketplaceProduct(userMarketplaceProductId) {
        $.ajax({
            type: "post",
            url: "/marketplace/user/delete-product/" + userMarketplaceProductId,
            dataType: "json",
            "headers": {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
            success: function (data) {
                toastr.success(data.message);
                window.location = '';
            },
            error: function (data) {
                toastr.error("Something Went Wrong");
            }
        });
    }
</script>
