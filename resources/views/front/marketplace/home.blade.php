<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 6/12/19
 * Time: 3:50 PM
 */
?>
@extends('front.layouts.default')
@section('main_container')
    @push('styles')
        <style>
            .common-backcolor {
                background: none !important;
                opacity: 1 !important;
            }

            .top-menu-area {
                padding: 0px 0 !important;
            }

            .btn-seller {
                float: left;
                width: auto;
                margin-top: 42px;
            }

        </style>

    @endpush
    <section id="marketplace-search">
        <div class="msearch_back">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="big-text1">
                            <h1>10,000 WordPress Themes & Website Templates From</h1>
                            <p class="subtitle">WordPress themes, web templates and more. Brought to you by the largest
                                global community of creatives.</p>
                        </div>
                        <form role="search" method="get" action="#">
                            <div class="main-box" data-test-selector="autosuggest">
                                <input type="search" class="searct-text" placeholder="e.g.WordPress Blog Theme"
                                       name="search" aria-label="search" autocomplete="off">
                                <button class="msearch-btn"><i class="fa fa-search"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="features section--padding1 bgwhite">
        <!-- start container -->
        <div class="container">
            <!-- start row -->
            <div class="row">
                <!-- start search-area -->
                <div class="col-lg-4">
                    <div class="feature">
                        <div class="feature__img">
                            <img src="/front/images/feature1.png" alt="feature">
                        </div>
                        <div class="feature__title">
                            <h3>Best UX Research</h3>
                        </div>
                        <div class="feature__desc">
                            <p>Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque the
                                mattis,
                                leo quam aliquet diam congue is laoreet elit metus.</p>
                        </div>
                    </div>
                    <!-- end /.feature -->
                </div>
                <!-- end /.col-md-4 -->

                <!-- start search-area -->
                <div class="col-lg-4">
                    <div class="feature">
                        <div class="feature__img">
                            <img src="/front/images/feature2.png" alt="feature">
                        </div>
                        <div class="feature__title">
                            <h3>Fully Responsive</h3>
                        </div>
                        <div class="feature__desc">
                            <p>Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque the
                                mattis,
                                leo quam aliquet diam congue is laoreet elit metus.</p>
                        </div>
                    </div>
                    <!-- end /.feature -->
                </div>
                <!-- end /.col-md-4 -->

                <!-- start search-area -->
                <div class="col-lg-4">
                    <div class="feature">
                        <div class="feature__img">
                            <img src="/front/images/feature3.png" alt="feature">
                        </div>
                        <div class="feature__title">
                            <h3>Buy & Sell Easily</h3>
                        </div>
                        <div class="feature__desc">
                            <p>Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque the
                                mattis,
                                leo quam aliquet diam congue is laoreet elit metus.</p>
                        </div>
                    </div>
                    <!-- end /.feature -->
                </div>
                <!-- end /.col-md-4 -->
            </div>
            <!-- end /.row -->
        </div>
        <!-- end /.container -->
    </section>

    <!--================================
    START FEATURED PRODUCT AREA
=================================-->

    <section class="featured-products bgcolor2 section--padding1">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title">
                        <h1>Our Featured
                            <span class="highlighted">Products</span>
                        </h1>
                        <p>Laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis omnis
                            fugats. Lid
                            est laborum dolo rumes fugats untras.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach($userMarketplaceProducts as $userMarketplaceProduct)
                    <div class="col-lg-4 col-md-6">
                        <div class="product product--card product--card-small">
                            <div class="product__thumbnail">
                                <img src="{{$userMarketplaceProduct->thumbnail_image}}">

                            </div>
                            <div class="product-desc">
                                <a href="#" target="_blank" class="product_title">
                                    <h4>{{$userMarketplaceProduct->name}}</h4>
                                </a>
                                <div class="user_grid">
                                    <img class="auth-img"
                                         src="{{Auth::user()->avatar}}"
                                         alt="author image">
                                    <span><a href="">{{Auth::user()->first_name}}</a> </span>
                                </div>
                                <div class="stint_cart_star">
                                    <div class="rating-star">
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <h4>0</h4>
                                        <span> (0)</span>
                                    </div>
                                    <div class="stint_cart">
                                        <i class="fa fa-shopping-cart"></i>
                                        <span>
                                    0                                    </span>
                                    </div>
                                </div>
                            </div>
                            <div class="product-purchase">
                                <div class="price_love">
                                    <span>${{$userMarketplaceProduct->regular_license}}</span>
                                </div>
                                <div class="view_link">
                                    <a href="/marketplace/product-info/{{$userMarketplaceProduct->id}}" target="_blank">Preview</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach

            </div>
            {{ $userMarketplaceProducts->onEachSide(1)->links() }}
        </div>
    </section>

    <section class="promotion-area bgwhite section--padding1">
        <div class="container">
            <div class="row">
                <div class="col-md-6 v_middle">
                    <div class="promotion-img">
                        <img src="/front/images/bubdle.jpg" alt="Promotion image">
                    </div>
                    <!-- end /.promotion-img -->
                </div>
                <!-- end /.col-md-6 -->

                <div class="col-lg-5 offset-lg-1 col-md-6 v_middle">
                    <div class="promotion-content">
                        <h3 class="promotion__subtitle">Big Extension Bundle</h3>
                        <h1 class="promotion__title">Get Them All For Only $289!
                            <span>Save 35%</span>
                        </h1>
                        <p>Get all of our products in one bundle and save %35 of regular price. Over 50 plugins, 20
                            extensions
                            and 30 components will be yours!</p>
                        <a href="#" class="cmbtn btn-round cmbtn-sm">View Details</a>
                    </div>
                    <!-- end /.promotion-content -->
                </div>
                <!-- end /.col-md-5 -->
            </div>
            <!-- end /.row -->
        </div>
        <!-- end /.container -->
    </section>

    <section class="proposal-area">
        <!-- start container-fluid -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 no-padding">
                    <div class="proposal proposal--left bgimage">
                        <div class="content_above">
                            <div class="proposal__icon ">
                                <img src="/front/images/buy.png" alt="Buy icon">
                            </div>
                            <div class="proposal__content ">
                                <h1 class="text--white">Sell Your Products</h1>
                                <p class="text--white">Nunc placerat mi id nisi interdum mollis. Praesent pharetra,
                                    justo ut scelerisque the mattis,
                                    leo quam aliquet diamcongue is laoreet elit metus.</p>
                            </div>
                            <a href="#" class="cmbtn btn-round-big cmbtn-lg btn--white">Become a Merchant</a>
                        </div>
                    </div>
                    <!-- end /.proposal -->
                </div>

                <div class="col-md-6 no-padding">
                    <div class="proposal proposal--right bgimage">
                        <div class="content_above">
                            <div class="proposal__icon">
                                <img src="/front/images/sell.png" alt="Sell icon">
                            </div>
                            <div class="proposal__content ">
                                <h1 class="text--white">Start Shopping Today</h1>
                                <p class="text--white">Nunc placerat mi id nisi interdum mollis. Praesent pharetra,
                                    justo ut scelerisque the mattis,
                                    leo quam aliquet diamcongue is laoreet elit metus.</p>
                            </div>
                            <a href="#" class="cmbtn btn-round-big cmbtn-lg btn--white">Start Shopping</a>
                        </div>
                    </div>
                    <!-- end /.proposal -->
                </div>
            </div>
        </div>
        <!-- start container-fluid -->
    </section>
    <section class="partner-area section--padding1 bgwhite">
        <!-- start container -->
        <div class="container">
            <!-- start row -->
            <div class="row">
                <!-- start col-md-12 -->
                <div class="col-md-12">
                    <div class="section-title">
                        <h1>Our Branding
                            <span class="highlighted">Partners</span>
                        </h1>
                        <p>Laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis omnis
                            fugats. Lid
                            est laborum dolo rumes fugats untras.</p>
                    </div>
                </div>
                <!-- end /.col-md-12 -->
            </div>
            <!-- end /.row -->

            <div class="row">
                <div class="col-md-12">
                    <div class="partners">
                        <div class="partner">
                            <img src="/front/images/cl1.png" alt="partner image">
                        </div>
                        <div class="partner">
                            <img src="/front/images/cl2.png" alt="partner image">
                        </div>
                        <div class="partner">
                            <img src="/front/images/cl3.png" alt="partner image">
                        </div>
                        <div class="partner">
                            <img src="/front/images/cl4.png" alt="partner image">
                        </div>
                        <div class="partner">
                            <img src="/front/images/cl2.png" alt="partner image">
                        </div>
                    </div>
                </div>
            </div>
            <!-- end /.row -->
        </div>
        <!-- end /.container -->
    </section>
    <!--================================
        END OVERVIEW AREA
    =================================-->


    <!--================================
        START CALL TO ACTION AREA
    =================================-->
    <section class="call-to-action bgimage">
        <div class="common-backcolor ptopb150">
            <div class="container content_above">
                <div class="row">
                    <div class="col-md-12">
                        <div class="call-to-wrap">
                            <h1 class="text--white">Ready to Join Our Marketplace!</h1>
                            <h4 class="text--white">Over 25,000 designers and developers trust the Varperhour.</h4>
                            <a href="#" class="cmbtn btn-round-big cmbtn-lg white-blank">Join Us Today</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
