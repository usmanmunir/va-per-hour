<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 6/21/19
 * Time: 3:54 PM
 */
?>
@extends('front.layouts.default')
@section('main_container')
    @push('styles')
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css">
        <style>
            .dropzone {
                background: white;
                border-radius: 5px;
                border: 2px dashed rgb(0, 135, 247);
                border-image: none;
                max-width: 500px;
                margin-left: auto;
                margin-right: auto;
            }
            .btn {
                background: none;
                box-shadow: none;
            }

            section.section-1.section--padding-marketplace {
                padding: 50px 70px;
                background-color: #eef1f5;
            }

            .top-menu-area {
                padding: 0px 0 !important;
            }

            .btn-seller {
                float: left;
                width: auto;
                margin-top: 42px;
            }
            .other-info-2 .card-body label{
                width: auto !important;
            }

        </style>

    @endpush
    <section class="section-1 section--padding-marketplace">
        <div class="container">
            <div class="row">
                <div class="item-manag">
                    <div class="title-filter">
                        <div class="box">
                            <h3>Product Upload</h3>
                        </div>
                    </div>
                    <div class="setting-contant">
                        <div class="row">
                            <div class="col-md-6 col-lg-8 col-sm-12 col-xs-12">
                                <div class="box account-setting product-upload">
                                    <form method="POST" id="editMarketplaceForm" enctype="multipart/form-data">
                                        @csrf
                                        <div id="" class="profile-contant">
                                            <div class="card">
                                                <div class="card-header" id="headingOne">
                                                    <a href="#" class="mb-0" data-toggle="collapse"
                                                       data-target="#collapseOne" aria-expanded="true"
                                                       aria-controls="collapseOne">
                                                        <button class="btn btn-link">
                                                            Item Name & Description
                                                        </button>
                                                        <i class="fa fa-chevron-down" aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                                                     data-parent="#accordion">
                                                    <div class="card-body">

                                                        <div class="form-group">
                                                            <label for="exampleInputPassword1">Select Category </label>
                                                            <select class="form-control" name="category_id"
                                                                    id="product-category">
                                                                @foreach($marketPlaceCategories as $marketPlaceCategory)
                                                                    <option value="{{$marketPlaceCategory->id}}"
                                                                            @if($marketplaceUserProduct['category']->id == $marketPlaceCategory->id) selected @endif>
                                                                        {{$marketPlaceCategory->name}}</option>
                                                                @endforeach
                                                                <option value="">Default select</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleInputPassword6">Product Name
                                                                <p>(Max 100 Characters )</p>
                                                            </label>
                                                            <input type="text" class="form-control" name="name"
                                                                   placeholder="Enter your product name here..."
                                                                   value="{{$marketplaceUserProduct->name}}">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleFormControlTextarea1">Product
                                                                Description</label>
                                                            <div class="editor">
                                                                <textarea rows="3" name="description" id="description"
                                                                          placeholder="Write Description here..."
                                                                          class="ckeditor">{{$marketplaceUserProduct->description}}</textarea>
                                                            </div>
                                                        </div>
                                                        <div class="alert_message" style="display: none;"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="profile-contant" style="">
                                            <div class="card">
                                                <div class="card-header" id="headingOne">
                                                    <a href="#" class="mb-0" data-toggle="collapse"
                                                       data-target="#collapsetwo" aria-expanded="true"
                                                       aria-controls="collapseOne">
                                                        <button class="btn btn-link">
                                                            Upload File
                                                        </button>
                                                        <i class="fa fa-chevron-down" aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                                <div id="collapsetwo" class="collapse show" aria-labelledby="headingtwo"
                                                     data-parent="#accordion2">
                                                    <div class="card-body">
                                                        <div class="dropzone" id="thumbnail">
                                                            <div class="dz-message" data-dz-message><span><i class="fa fa-upload" aria-hidden="true"></i></span></div>
                                                            <div class="dz-message" data-dz-message><span>Upload Thumbnail Image</span></div>
                                                            <input type="hidden" name="thumbnailimage" id="thumbnailimage" value="{{$marketplaceUserProduct->thumbnail_image}}">
                                                            <img src="{{$marketplaceUserProduct->thumbnail_image}}">
                                                        </div>
                                                        <div class="file-list" style="padding-top: 15px;">
                                                            <div class="dropzone" id="zipfiles">
                                                                <div class="dz-message" data-dz-message><span>
                                                                        <i class="fa fa-file-archive-o" aria-hidden="true"></i></span></div>
                                                                <div class="dz-message" data-dz-message><span>Upload Zip file</span></div>
                                                                <input type="hidden" name="zipfile" id="zipfile" value="{{$marketplaceUserProduct->mainfile}}">

                                                            </div>
                                                            {{--<a href="{{$marketplaceUserProduct->mainfile}}" download=""><i class="fa fa-file fa-10x"></i> </a>--}}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="profile-contant other-info-2" style="">
                                            <div class="card">
                                                <div class="card-header" id="headingOne">
                                                    <a href="#" class="mb-0" data-toggle="collapse"
                                                       data-target="#collapsetwo" aria-expanded="true"
                                                       aria-controls="collapseOne">
                                                        <button class="btn btn-link">
                                                            Others Information
                                                        </button>
                                                        <i class="fa fa-chevron-down" aria-hidden="true"></i>
                                                    </a>
                                                </div>

                                                <div id="collapsetwo" class="collapse show" aria-labelledby="headingtwo"
                                                     data-parent="#accordion">
                                                    <div class="card-body">
                                                        <div class="dual-column">
                                                            <div class="form-group">
                                                                <span>Regular License</span>
                                                                <label for="inputPassword"
                                                                       class="col-sm-1 col-form-label">$</label>
                                                                <div class="col-sm-11">
                                                                    <input type="text" class="form-control"
                                                                           name="regular_license" placeholder="00.00"
                                                                           value="{{$marketplaceUserProduct->regular_license}}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="dual-column">
                                                            <div class="form-group">
                                                                <span>Extended License</span>
                                                                <label for="inputPassword"
                                                                       class="col-sm-1 col-form-label">$</label>
                                                                <div class="col-sm-11">
                                                                    <input type="text" class="form-control"
                                                                           name="extended_license" placeholder="00.00"
                                                                           value="{{$marketplaceUserProduct->extended_license}}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="com-md-12 save-profile profile-contant" style="">

                                            {{--<button type="button" class="ctm-btn update-marketplace">Update</button>--}}
                                            <input type="hidden" name="marketplaceproductId" id="marketplaceproductId" value="{{$marketplaceUserProduct->id}}">
                                            <button type="submit" class="ctm-btn" id="">Save</button>


                                        </div>
                                    </form>
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-4 col-sm-12 col-xs-12">
                                <div class="box product-upload-2">
                                    <div id="accordion3" class="profile-contant">
                                        <div class="card">
                                            <div class="card-header" id="headingthree">
                                                <a href="#" class="mb-0" data-toggle="collapse"
                                                   data-target="#collapsethree" aria-expanded="true"
                                                   aria-controls="collapsethree">
                                                    <button class="btn btn-link">
                                                        Quick Upload Rules
                                                    </button>
                                                    <i class="fa fa-chevron-down" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                            <div id="collapsethree" class="collapse show" aria-labelledby="headingthree"
                                                 data-parent="#accordion3">
                                                <div class="card-body ">
                                                    <div class="quick-list">
                                                        <a href="#">Image Upload</a>
                                                        <p>Nunc placerat mi id nisi interdum mollis. Praesent there
                                                            pharetra, justo ut sceleris que the mattis interdum.</p>
                                                    </div>
                                                    <div class="quick-list">
                                                        <a href="#">File Upload</a>
                                                        <p>Nunc placerat mi id nisi interdum mollis. Praesent there
                                                            pharetra, justo ut sceleris que the mattis interdum.</p>
                                                    </div>
                                                    <div class="quick-list">
                                                        <a href="#">Vector Upload</a>
                                                        <p>Nunc placerat mi id nisi interdum mollis. Praesent there
                                                            pharetra, justo ut sceleris que the mattis interdum.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="accordion4" class="profile-contant">
                                        <div class="card">
                                            <div class="card-header" id="headingfour">
                                                <a href="#" class="mb-0" data-toggle="collapse"
                                                   data-target="#collapsefour" aria-expanded="true"
                                                   aria-controls="collapsefour">
                                                    <button class="btn btn-link">
                                                        Trouble Uploading?
                                                    </button>
                                                    <i class="fa fa-chevron-down" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                            <div id="collapsefour" class="collapse show" aria-labelledby="headingfour"
                                                 data-parent="#accordion4">
                                                <div class="card-body truble-upload">
                                                    <p>Nunc placerat mi id nisi interdum mollis. Praesent there
                                                        pharetra, justo ut sceleris que the mattis interdum.</p>

                                                    <ol>
                                                        <li>Consectetur elit, sed do eiusmod the labore et dolore
                                                            magna
                                                        </li>
                                                        <li>Consectetur elit, sed do eiusmod the labore et dolore
                                                            magna
                                                        </li>
                                                        <li>Consectetur elit, sed do eiusmod the labore et dolore
                                                            magna
                                                        </li>
                                                        <li>Consectetur elit, sed do eiusmod the labore et dolore
                                                            magna
                                                        </li>
                                                        <li>Consectetur elit, sed do eiusmod the labore et dolore
                                                            magna
                                                        </li>
                                                    </ol>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="accordion5" class="profile-contant">
                                        <div class="card">
                                            <div class="card-header" id="headingfive">
                                                <a href="#" class="mb-0" data-toggle="collapse"
                                                   data-target="#collapsefive" aria-expanded="true"
                                                   aria-controls="collapsefive">
                                                    <button class="btn btn-link">
                                                        More Upload Info
                                                    </button>
                                                    <i class="fa fa-chevron-down" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                            <div id="collapsefive" class="collapse show" aria-labelledby="headingfive"
                                                 data-parent="#accordion5">
                                                <div class="card-body truble-upload">
                                                    <p>Nunc placerat mi id nisi interdum mollis. Praesent there
                                                        pharetra, justo ut sceleris que the mattis interdum.</p>
                                                    <ol>
                                                        <li>Consectetur elit, sed do eiusmod the labore et dolore
                                                            magna
                                                        </li>
                                                        <li>Consectetur elit, sed do eiusmod the labore et dolore
                                                            magna
                                                        </li>
                                                        <li>Consectetur elit, sed do eiusmod the labore et dolore
                                                            magna
                                                        </li>
                                                        <li>Consectetur elit, sed do eiusmod the labore et dolore
                                                            magna
                                                        </li>
                                                        <li>Consectetur elit, sed do eiusmod the labore et dolore
                                                            magna
                                                        </li>
                                                    </ol>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>
    <script type="text/javascript">
        Dropzone.autoDiscover = false;
        $(document).ready(function () {
            $("#thumbnail").dropzone({
                maxFiles: 2000,
                method:"POST",
                url:"{{ url('marketplace/save-marketplace-product-thumbnail-image') }}",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
                success: function (file, response) {
                    console.log(response);
                    $('#thumbnailimage').val(response);
                }
            });
        })
        $(document).ready(function () {
            $("#zipfiles").dropzone({
                maxFiles: 2000,
                method:"POST",
                acceptedFiles: ".zip",
                url:"{{ url('marketplace/save-marketplace-product-zipfile') }}",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
                success: function (file, response) {
                    console.log(response);
                    $('#zipfile').val(response);
                }
            });
        })
        Dropzone.options.dropzone =
            {
                maxFilesize: 500000000,
                renameFile: function(file) {
                    var dt = new Date();
                    var time = dt.getTime();
                    return time+file.name;
                },
                acceptedFiles: "gif|jpg|png|jpeg",
                addRemoveLinks: true,
                timeout: 5000,
                success: function(file, response)
                {
                    console.log(response);
                },
                error: function(file, response)
                {
                    return false;
                }
            };
    </script>
    @include('front.marketplace.marketplace-js')

@endpush

