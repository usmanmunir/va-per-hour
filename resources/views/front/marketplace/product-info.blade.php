<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 6/19/19
 * Time: 3:10 PM
 */?>
@extends('front.layouts.default')
@section('main_container')
    @push('styles')
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css">
        <style>
            .dropzone {
                background: white;
                border-radius: 5px;
                border: 2px dashed rgb(0, 135, 247);
                border-image: none;
                max-width: 500px;
                margin-left: auto;
                margin-right: auto;
            }

            .btn {
                background: none;
                box-shadow: none;
            }

            section.section-1.section--padding-marketplace {
                padding: 50px 70px;
                background-color: #eef1f5;
            }

            .top-menu-area {
                padding: 0px 0 !important;
            }

            .btn-seller {
                float: left;
                width: auto;
                margin-top: 42px;
            }

            .other-info-2 .card-body label {
                width: auto !important;
            }

        </style>

    @endpush
    <div class="product-info-page">
        <section class="product-info-contant p50">
            <div class="container">
                <div class="row">

                    <div class="product-in product-details">
                        <div class="box">
                            <div class="product-img">
                                <div class="img-slider">
                                    <div id="slider" class="flexslider">
                                        <ul class="slides">

                                            <li>
                                                <img src="{{$singleMarketplaceProduct->thumbnail_image}}">
                                            </li>

                                        </ul>
                                    </div>
                                    <div id="carousel" class="flexslider bootom-slider">
                                        <ul class="slides ">
                                            <li>
                                                {{--<img src="{{$singleMarketplaceProduct->thumbnail_image}}">--}}
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                                <div class="priview">
                                    <a href="#" class="ctm-btn live-pre">Live Preview</a>
                                    <!-- <a href="#" class="ctm-btn favorite">To Favorites</a>-->
                                </div>
                            </div>
                            <div class="product-info-tab">
                                <ul class="nav nav-tabs ctm-tabs" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#home" role="tab"
                                           aria-controls="home" aria-selected="true">Item Details</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#review-tab" role="tabpanel"
                                           aria-controls="review-tab" aria-selected="false">Reviews</a>
                                    </li>
                                </ul>
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade show  ctm-item-detail active" id="home" role="tabpanel"
                                         aria-labelledby="item-tab">
                                        <div class="box">
                                            <h1 class="title">Item Name</h1>
                                            <span>{{$singleMarketplaceProduct->name}}</span>
                                        </div>
                                        <div class="box">
                                            <h1 class="title">Item Details</h1>
                                            <p>
                                                {!! $singleMarketplaceProduct->description !!}
                                            </p>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade ctm-item-detail" id="review-tab" role="tabpanel"
                                         aria-labelledby="comment-tab">
                                        <div class="feed-contant">
                                            <div class="box">
                                                <div class="review-feed">


                                                    <div class="well well-sm">
                                                        <div class="" id="post-review-box">
                                                            <form action="" method="post">
                                                                <input id="ratings-hidden" name="" type="hidden">
                                                                <textarea class="form-control animated" cols="50"
                                                                          id="new-review" name="comment"
                                                                          placeholder="Write your comment here..."
                                                                          rows="5"></textarea>
                                                                <div class="text-right col-md-12">
                                                                    <fieldset class="rating">
                                                                        <input type="radio" id="star5" name="rating"
                                                                               value="5"/>
                                                                        <label class="full" for="star5"
                                                                               title="Awesome - 5 stars"></label>
                                                                        <input type="radio" id="star4" name="rating"
                                                                               value="4"/>
                                                                        <label class="full" for="star4"
                                                                               title="Pretty good - 4 stars"></label>

                                                                        <input type="radio" id="star3" name="rating"
                                                                               value="3"/>
                                                                        <label class="full" for="star3"
                                                                               title="Meh - 3 stars"></label>

                                                                        <input type="radio" id="star2" name="rating"
                                                                               value="2"/>
                                                                        <label class="full" for="star2"
                                                                               title="Kinda bad - 2 stars"></label>

                                                                        <input type="radio" id="star1" name="rating"
                                                                               value="1"/>
                                                                        <label class="full" for="star1"
                                                                               title="Sucks big time - 1 star"></label>
                                                                    </fieldset>
                                                                    <button class="btn ctm-btn btn-lg" type="submit"
                                                                            name="submit" value="save">Save
                                                                    </button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>


                                                    <div class="col-md-12">
                                                        <span class="label label-default"><strong> Your Review</strong></span>

                                                        <div class="reviw-one">
                                                            <span class="label"></span>
                                                            <div class="user-img">
                                                                <img src="">
                                                            </div>
                                                            <div class="user-time-date-star">
                                                                <div class="date-name">
                                                                    <a href="javascript:;">

                                                                    </a>
                                                                    <span>
															</span>
                                                                </div>
                                                                <div class="star yellows">
                                                                </div>
                                                                <div class="review-contant">
                                                                    <p>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="feed-contant elearn_review pbot100 dblock">
                                                        <div class="review-feed">
                                                            <div class="title-view-all">
                                                                <a href="#" class="all-feed-reviews">Reviews

                                                                </a>
                                                            </div>
                                                            <div class="reviw-one">
                                                                <div class="user-img">
                                                                    <img src="{{$singleMarketplaceProduct['user'][0]->avatar}}">
                                                                </div>

                                                                <div class="user-time-date-star">
                                                                    <div class="date-name">
                                                                        <a href="javascript:;">
                                                                            {{$singleMarketplaceProduct->first_name}}
                                                                            &nbsp;{{$singleMarketplaceProduct->last_name}}
                                                                        </a>
                                                                        <span>
															            {{$singleMarketplaceProduct->created_at}}
															</span>
                                                                    </div>

                                                                    <div class="star yellows">

                                                                    </div>


                                                                    <div class="review-contant">
                                                                        <p><?php //echo($value['r_review']); ?></p>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>


                                                        <div class="center-button blue-btn-o load_review"
                                                             data-id="">
                                                            <a href="javascript:;">Load More</a>
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="product-in product-priceing">
                        <div class="box">
                            <form action="<?php //echo marketplace_url('marketplace/addtocart/' . $mp_data[0]['mp_id']); ?>"
                                  method="post">
                                <div class="priceing-info">
                                    <label class="price-label">
                                        ${{$singleMarketplaceProduct->regular_license}} - ${{$singleMarketplaceProduct->extended_license}}
                                    </label>
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="customRadio" name="mp_variation_id"
                                               class="custom-control-input" value="">
                                        <label class="custom-control-label" for="customRadio">
                                            <?php //echo str_replace('_', ' ', $value['mv_type']) . ' - $' . $value['mv_price']; ?>
                                        </label>
                                    </div>


                                    <div class="purches-item">
                                        <button class="ctm-btn" onclick="return addtocart();" type="submit"><i
                                                    class="fa fa-shopping-cart" aria-hidden="true"></i> Add To Cart
                                        </button>
                                    </div>
                                </div>
                            </form>
                            <div class="total-sales">
                                <div class="list">
                                    <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                    <span>Total Sales</span>
                                    <span class="counter"> 	0 </span>
                                </div>
                                <div class="list">
                                    <i class="fa fa-heart-o" aria-hidden="true"></i>
                                    <span>Favourites</span>
                                    <span class="counter">0</span>
                                </div>
                                <!-- <div class="list">
                                    <i class="fa fa-comment-o" aria-hidden="true"></i>
                                    <span>Comments</span>
                                    <span class="counter"></span>
                                </div> -->
                                <div class="star">
                                    <i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>		            <span><b>0.0</b></span>
                                    <span style="float: right;color: #000000;font-weight: bold;">0</span>
                                </div>
                                <div class="action-list-wrap mtop20">

                                    <div class="save-stint common-fav" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Tooltip on bottom">
                                        <a data-id="16" data-action="favourite" data-type="marketplace" class="add-to-favorites"><i class="fa fa-heart-o"></i>
                                            <span class="heart-text"> Favorite </span>
                                        </a>
                                    </div>

                                    <div class="save-stint common-fav">
                                        <a href="" data-toggle="modal" data-target=".flagmodel"><i class="fa fa-flag"></i></a>
                                    </div>
                                    <div class="save-stint common-fav">
                                        <a href="" data-toggle="modal" data-target=".share-model"><i class="fa fa-share-alt"></i> <span class="heart-text">share</span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="product-inforamtion">
                                <h2 class="title">Merchant Information</h2>
                                <div class="profile-info">
                                    <div class="pro-img">
                                        <img src="{{$singleMarketplaceProduct['user'][0]->avatar}}">
                                    </div>
                                    <p class="name">
                                        <a href="<?php// echo base_url('user/profile/' . $mp_data[0]['u_username']); ?>"
                                           target="_blank">
                                            {{$singleMarketplaceProduct['user'][0]->first_name}}
                                            &nbsp; {{$singleMarketplaceProduct['user'][0]->last_name}}
                                        </a>
                                    </p>
                                    <p class="sign-up-time">Create At :
                                        {{$singleMarketplaceProduct['user'][0]->created_at}}
                                    </p>
                                    <div class="social">
                                        <a href="<?php //echo isset($seller_usermeta_data['_user_social_facebook_link']) ? $seller_usermeta_data['_user_social_facebook_link'] : '#'; ?>"
                                           target="_blank" class="facebook"><i class="fa fa-facebook-official"
                                                                               aria-hidden="true"></i></a>


                                        <?php //if(!empty($seller_usermeta_data['_user_social_twitter_link'])){
                                        ?>
                                        <a href="<?php //echo isset($seller_usermeta_data['_user_social_twitter_link']) ? $seller_usermeta_data['_user_social_twitter_link'] : '#'; ?>"
                                           target="_blank" class="twiter"><i class="fa fa-twitter-square"
                                                                             aria-hidden="true"></i></a>

                                        <?php //if(!empty($seller_usermeta_data['_user_social_twitter_link'])){
                                        ?>
                                        <a href="<?php //echo isset($seller_usermeta_data['_user_social_linkedin_link']) ? $seller_usermeta_data['_user_social_linkedin_link'] : '#'; ?>"
                                           target="_blank" class="dribble"><span><i class="fa fa-linkedin"
                                                                                    aria-hidden="true"></i></span></a>


                                    </div>
                                    <div class="profile-info-btn center-btn product-centerbtn">
                                        <a href=""
                                           class="ctm-btn" target="_blank">View Portfolio</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </section>
    </div>

    <div class="modal fade bd-example-modal-md flagmodel Newmodeldesign" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Report This Product</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="model-report">
                        <h2>Let us know why you would like to report this Product.</h2>
                        <p>Your report will be kept anonymous</p>
                        <div class="error-report"></div>
                        <div class="issue-points">
                            <div class="custom-control custom-radio">
                                <input type="radio" name="report_reason" id="customRadio_report_1"
                                       class="custom-control-input" value="Non Original Content">
                                <label class="custom-control-label" for="customRadio_report_1">Non Original
                                    Content</label>
                            </div>
                            <div class="custom-control custom-radio">
                                <input type="radio" name="report_reason" id="customRadio_report_2"
                                       class="custom-control-input" value="Inappropriate Stint">
                                <label class="custom-control-label" for="customRadio_report_2">Inappropriate
                                    Stint</label>
                            </div>

                            <div class="custom-control custom-radio">
                                <input type="radio" name="report_reason" id="customRadio_report_3"
                                       class="custom-control-input" value="Trademark Violation">
                                <label class="custom-control-label" for="customRadio_report_3">Trademark
                                    Violation</label>
                            </div>

                            <div class="custom-control custom-radio">
                                <input type="radio" name="report_reason" id="customRadio_report_4"
                                       class="custom-control-input" value="Copyrights Violation">
                                <label class="custom-control-label" for="customRadio_report_4">Copyrights
                                    Violation</label>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary"
                            onclick="">Request Report
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bd-example-modal-md share-model Newmodeldesign" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header modelbackwhite">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="model-common-title text-center mbot20">
                        <h2>Share This Product</h2>
                        <p>Spread the word about this Stint on Vaper Hour</p>
                        <input id="myInput" type="text" readonly="" class="form-control"
                               value="">
                    </div>
                    <div class="social social--color--filled">
                        <ul>
                            <li>

                                <a onclick=""
                                   href="javascript:void(0)" target="_blank">
                                    <span class="fa fa-facebook"></span>
                                </a>
                            </li>
                            <li>
                                <a onclick=""
                                   href="javascript:void(0)" href="javascript:void(0)">
                                    <span class="fa fa-twitter"></span>
                                </a>
                            </li>
                            <li>
                                <a onclick=""
                                   href="javascript:void(0)" href="javascript:void(0)">
                                    <span class="fa fa-pinterest"></span>
                                </a>
                            </li>
                            <li>
                                <a onclick=""
                                   href="javascript:void(0)">
                                    <span class="fa fa-linkedin"></span>
                                </a>
                            </li>
                            <li>
                                <a href="skype:vaph">
                                    <span class="fa fa-skype"></span>
                                </a>
                            </li>
                            <li>
                                <a href="#"
                                   onclick='document.getElementById("myInput").select(); document.execCommand("copy");'
                                   data-toggle="tooltip" title="Copy to clipboard">
                                    <span class="fa fa-link"></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('scripts')
@endpush