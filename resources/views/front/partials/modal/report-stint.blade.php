<div class="modal fade bd-example-modal-md flagmodel Newmodeldesign" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Report This Stint</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="model-report">
                    <h2>Let us know why you would like to report this Stint</h2>
                    <p>Your report will be kept anonymous</p>
                    <div class="error-report"></div>
                    <div class="issue-points">
                        <div class="custom-control custom-radio">
                            <input type="radio" name="report_reason" id="customRadio_report_1"
                                   class="custom-control-input" value="Non Original Content">
                            <label class="custom-control-label" for="customRadio_report_1">Non Original
                                Content</label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input type="radio" name="report_reason" id="customRadio_report_2"
                                   class="custom-control-input" value="Inappropriate Stint">
                            <label class="custom-control-label" for="customRadio_report_2">Inappropriate
                                Stint</label>
                        </div>

                        <div class="custom-control custom-radio">
                            <input type="radio" name="report_reason" id="customRadio_report_3"
                                   class="custom-control-input" value="Trademark Violation">
                            <label class="custom-control-label" for="customRadio_report_3">Trademark
                                Violation</label>
                        </div>

                        <div class="custom-control custom-radio">
                            <input type="radio" name="report_reason" id="customRadio_report_4"
                                   class="custom-control-input" value="Copyrights Violation">
                            <label class="custom-control-label" for="customRadio_report_4">Copyrights
                                Violation</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary close" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary submit-btn" onclick="report('{{$stint->id}}');">Request Report</button>
            </div>
        </div>
    </div>
</div>