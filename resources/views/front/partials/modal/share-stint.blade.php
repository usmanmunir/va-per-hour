<div class="modal fade bd-example-modal-md share-model Newmodeldesign" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header modelbackwhite">
                <h5 class="modal-title" id="exampleModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="model-common-title text-center mbot20">
                    <h2>Share This Stint</h2>
                    <p>Spread the word about this Stint on Vaper Hour</p>

                    <input id="myInput" type="text" readonly="" class="form-control"
                           value="{{url('/stint/view/'.$stint->id.'/'.str_replace(" ", "-", strtolower($stint->title))) }}">
                </div>
                <div class="social social--color--filled">
                    <ul>
                        <li>
                            <a onclick="window.open('http://www.facebook.com/sharer.php?u={{url('/stint/view/'.$stint->id.'/'.str_replace(" ", "-", strtolower($stint->title))) }}','sharer','toolbar=0,status=0,width=648,height=395');"
                               href="javascript:void(0)" target="_blank">

                                <span class="fa fa-facebook"></span>

                            </a>
                        </li>
                        <li>
                            <a onclick="window.open('http://twitter.com/share?text={{url('/stint/view/'.$stint->id.'/'.str_replace(" ", "-", strtolower($stint->title))) }}&','sharer','toolbar=0,status=0,width=648,height=395');"
                               href="javascript:void(0)" href="javascript:void(0)">
                                <span class="fa fa-twitter"></span>
                            </a>
                        </li>
                        <li>
                            <a onclick="window.open('http://pinterest.com/pin/create/button/?url={{url('/stint/view/'.$stint->id.'/'.str_replace(" ", "-", strtolower($stint->title))) }}','sharer','toolbar=0,status=0,width=648,height=395');"
                               href="javascript:void(0)" href="javascript:void(0)">
                                <span class="fa fa-pinterest"></span>
                            </a>
                        </li>
                        <li>
                            <a onclick="window.open('http://www.linkedin.com/shareArticle?mini=true&url={{url('/stint/view/'.$stint->id.'/'.str_replace(" ", "-", strtolower($stint->title))) }}','sharer','toolbar=0,status=0,width=648,height=395');"
                               href="javascript:void(0)">
                                <span class="fa fa-linkedin"></span>
                            </a>
                        </li>
                        <li>
                            <a href="skype:vaph">
                                <span class="fa fa-skype"></span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;"
                               onclick='document.getElementById("myInput").select(); document.execCommand("copy");'
                               data-toggle="tooltip" title="Copy to clipboard">
                                <span class="fa fa-link"></span>
                            </a>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>