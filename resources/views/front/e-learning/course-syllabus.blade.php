<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 7/2/19
 * Time: 5:32 PM
 */
?>
@extends('front.layouts.default')
@section('main_container')
    @push('styles')
        <style>
            .common-backcolor {
                background: none !important;
                opacity: 1 !important;
            }

            .top-menu-area {
                padding: 0px 0 !important;
            }

            .btn-seller {
                float: left;
                width: auto;
                margin-top: 42px;
            }

        </style>

    @endpush
    <div class="course-syallbus-page">
        <section id="breadcumb" class="bgwhite height0">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="breadcrumbsNew">
                            <a href="#">Course</a>
                            <a href="#"></a>
                        </div>
                    </div>
                    <div class="section-title pbot30">
                        <h2></h2>
                    </div>
                </div>
            </div>
        </section>
        <section id="syallbus_area" class="ptop20 bgwhite mtop50">
            <div class="container">


                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="section-title1 pbot30">
                            <h2>{{$singleCourse->name}}</h2>
                            <div class="rat-review">
                                <div class="star dblock yellows">
                                    <i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o"
                                                                                      aria-hidden="true"></i><i
                                            class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o"
                                                                                           aria-hidden="true"></i><i
                                            class="fa fa-star-o" aria-hidden="true"></i>
                                </div>
                                <div class="review dblock">
                                    <span><b>0.0</b></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="sya_price_wrap">
                            <div class="sya_price dblock">
                                <h4>${{$singleCourse->price}}</h4>
                            </div>
                            <div class="blue-btn-o dblock">
                                <a href="#">Buy Course</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                        <div class="syallbus_content_wrap">

                            <div class="main_video">
                                <img width="100%" height="450px" class="most_video"
                                     src="{{$singleCourse->image}}"/>
                            </div>

                            <div class="course_desc_wrap ptop30">
                                <div class="course-title two-part">
                                    <div class="course_title dblock">
                                        <h2>Course Description</h2>
                                    </div>
                                    <div class="course_count dblock pull-right">
                                        <div class="co_video_count dblock">
                                            <h3><i class="fa fa-book"></i> {{count($singleCourse['chapters'])}} chapters
                                            </h3>
                                        </div>
                                    </div>
                                </div>

                                <div class="course_desc">
                                    {!! $singleCourse->description !!}
                                </div>
                                <div class="clearfix"></div>
                                <div class="blue-btn-o buy_top">
                                    <a href="#">Buy Course</a>


                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                </div>
                            </div>


                            <div class="feed-contant elearn_review pbot100 dblock">
                                <div class="review-feed">
                                    <div class="title-view-all">
                                        <a href="#" class="all-feed-reviews">Reviews (0)</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                        <div class="syallbus_sidebar">
                            <div class="gray_sidebar_back pside">
                                <div class="sidebar_main_title">
                                    <h1>Course Syllabus</h1>
                                </div>
                                <?php $i = 1; ?>
                                @foreach($singleCourse['chapters'] as $chapter)
                                    <div class="chapter-box">
                                        <div class="chapter_title">
                                            <h2>Chapter <?php echo $i; ?> </h2>

                                            <a href="#" style="cursor: not-allowed;">
                                                <h3> {{$chapter->chapter_name}} </h3>
                                            </a>
                                        </div>

                                    </div>
                                    <?php $i = 1; ?>
                                    @foreach($singleCourse['lectures'] as $lecture)
                                        <div class="chapter-box">
                                            <div class="chapter_title">
                                                <h2>Lecture <?php echo $i; ?> </h2>

                                                <a href="#" style="cursor: not-allowed;">
                                                    <h3> {{$lecture->title}} </h3>
                                                </a>
                                            </div>

                                        </div>
                                        <?php $i++?>
                                    @endforeach
                                    <?php $i++; ?>
                                @endforeach
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>


    </div>
@endsection

