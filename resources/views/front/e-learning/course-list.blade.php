<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 7/2/19
 * Time: 12:38 PM
 */
?>
@extends('front.layouts.default')
@section('main_container')
    @push('styles')
        <style>
            .common-backcolor {
                background: none !important;
                opacity: 1 !important;
            }

            .top-menu-area {
                padding: 0px 0 !important;
            }

            .btn-seller {
                float: left;
                width: auto;
                margin-top: 42px;
            }

        </style>

    @endpush
    <section id="course-search">
        <div class="msearch_back dblue-back-search">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="big-text1">
                            <h1>Find The Perfect Course</h1>
                            <p class="subtitle">Popular stints are Logo Design, Voice Over, Articles & Blog design,
                                Design & Development</p>
                        </div>
                        <form action="" method="get">
                            <div class="main-box" data-test-selector="autosuggest">
                                <input type="search" class="searct-text" name="search" placeholder="Search Course"
                                       value="" aria-label="search" autocomplete="off">
                                <button type="submit" class="msearch-btn"><i class="fa fa-search"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="category_box" class="bgwhite ptop50 pbot100">
        <div class="container">

            <div class="filter">
                <form action="http://vaperhour.local/elearning/course/category/Design" method="get">
                </form>
            </div>

            <!--		<div class="row">-->
            <!--			<div class="col-md-12">-->
            <!--                <div class="breadcrumbsNew">-->
            <!--                    <a href="#">Course</a>-->
            <!--                    <a href="#">--><!--</a>-->
            <!--                </div>-->
            <!--            </div>-->
            <!--			<div class="section-title pbot30">-->
            <!--				<h2>--><!--</h2>-->
            <!--			</div>-->
            <!--		</div>-->
            <!--	-->


            <div class="row">
                @if(count($courses) > 0)
                    @foreach($courses as $course)
                        <div class="col-md-4 col-sm-4 col-xs-12 ">
                            <div class="create-box">
                                <div class="cbox-image">
                                    <img src="{{$course->image}}"
                                         class="img img-responsive" alt="">
                                </div>
                                <div class="cbox-title" style="word-break:break-all;">
                                    <a href="/elearning/course/course-syllabus/{{$course->id}}"><h3>{{$course->name}}</h3></a>
                                    <p>
                                    <p>{!! $course->description !!}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="col-md-12 text-center bolt"><h3>Recode not found.</h3></div>
                @endif
                <div class="clearfix"></div>

            </div>
            {{ $courses->links() }}
        </div>
    </section>

@endsection
