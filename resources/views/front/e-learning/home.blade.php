<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 7/2/19
 * Time: 12:03 AM
 */
?>
@extends('front.layouts.default')
@section('main_container')
    @push('styles')
        <style>
            .common-backcolor {
                background: none !important;
                opacity: 1 !important;
            }

            .top-menu-area {
                padding: 0px 0 !important;
            }

            .btn-seller {
                float: left;
                width: auto;
                margin-top: 42px;
            }

        </style>

    @endpush
    <div class="course-home-page">

        <section id="more" class="bgwhite ptop100 pbot100">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="big-title pbot80">
                            <h1>Learn More, Earn More!</h1>
                            <p class="big_subtitle">Boost your sales, by boosting your skills. </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="cm_more_box">
                            <div class="more_image center_img mh150">
                                <img src="/front/images/cm_trophy.png"
                                     class="img sm-img-responsive"
                                     alt="">
                            </div>
                            <div class="more_title text-center">
                                <h2 class="mb20">Stand Out</h2>
                                <p>Show off your expertise with badges earned for every course you take. </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="cm_more_box">
                            <div class="more_image center_img mh150">
                                <img src="/front/images/cm_customer.png"
                                     class="img sm-img-responsive"
                                     alt="">
                            </div>
                            <div class="more_title text-center">
                                <h2 class="mb20">Learn From The Best</h2>
                                <p>Courses tailored for your professional needs, by experts distinguished in their
                                    field.</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="cm_more_box">
                            <div class="more_image center_img mh150">
                                <img src="/front/images/cm_rank.png" class="img sm-img-responsive" alt="">
                            </div>
                            <div class="more_title text-center">
                                <h2 class="mb20">Improve Your Ranking</h2>
                                <p>Gain more visibility and increase your traffic as you grow your skill set. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="bgwhite">
            <div class="container">
                <div class="row">
                    <div class="border-line-box">
                        <div class="border-line"></div>
                    </div>
                </div>
            </div>
        </section>

        <section id="more" class="bgwhite ptop50">
            <div class="container">
                @foreach($courseCategories as $courseCategory)
                    <div class="row">
                        <div class="col-md-12">
                            <div class="section-title pbot30 text-left">
                                <h2>{{$courseCategory->name}}</h2>
                            </div>
                        </div>
                    </div>

                    <div class="row" id="">
                    @foreach($courses = \App\Models\Course::where('category_id','=',$courseCategory->id)->paginate(3,['*'],$courseCategory->name.'page') as $course)
                            <div class="col-md-4 col-sm-4 col-xs-12 cat_11">
                                <div class="create-box">
                                    <div class="cbox-image">
                                        <img src="{{$course->image}}"
                                             class="img img-responsive" alt="">
                                    </div>
                                    <div class="cbox-title">
                                        <h3>
                                            <a href="/elearning/course/course-syllabus/{{$course->id}}">{{$course->name}} </a>
                                        </h3>
                                        {!! $course->description !!}
                                    </div>
                                </div>
                            </div>
                        @endforeach

                        </div>
                    <div class="pagination-area pagination-area2 col-md-12">
                        <nav class="navigation pagination " role="navigation">
                            <div class="nav-links">
                                {{ $courses->links() }}
                            </div>
                        </nav>
                    </div>
                        <div class="clearfix"></div>

                        <div class="row mtop0">
                            <div class="border-line-box">
                                <div class="border-line"></div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </section>
        <div class="pagination-area pagination-area2 col-md-12">
            <nav class="navigation pagination " role="navigation">
                <div class="nav-links">
                    {{ $courseCategories->links() }}
                </div>
            </nav>
        </div>
        </div>
    @endsection
