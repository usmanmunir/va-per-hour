@extends('layouts.admin.default')

{{-- Web site Title --}}
@section('page_title', 'User Groups')
@section('sub_title','')

@section('right_buttons')
    <a class='btn btn-primary pull-right' href="/superaccess/groups/create">Create Group</a>
@endsection

{{-- Content --}}
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="table-responsive">
                <table id="tech-companies-1" class="table table-hover table-striped">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Permissions</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($groups as $group)
                        <tr>
                            <td>
                                <a href="{{ route('sentinel.groups.show', $group->hash) }}">{{ $group->name }}</a>
                            </td>
                            <td>
                                <?php
                                $permissions = $group->getPermissions();
                                $keys = array_keys($permissions);
                                $last_key = end($keys);
                                $permissionGroup = [
                                    'admin' => [],
                                    'user' => [],
                                ];
                                ?>
                                @foreach ($permissions as $key => $value)
                                    <?php
                                    $value = implode(" ", explode("_", $key));

                                    if (strpos(strtolower('____' . $value), 'admin')) {
                                        $permissionGroup['admin'][] = $value;
                                    } elseif (strpos(strtolower('____' . $value), 'user')) {
                                        $permissionGroup['user'][] = $value;
                                    } elseif (strpos(strtolower('____' . $value), 'group')) {
                                        $permissionGroup['groups'][] = $value;
                                    }
                                    ?>

                                @endforeach
                                <ul>
                                    @foreach ($permissionGroup as $pg=>$value)
                                        <li>
                                            {{ucwords(implode(" ", explode("_", $pg)))}}
                                            @if(is_array($value) and !empty($value))
                                                <ul>
                                                    @foreach($value as $permission)
                                                        <li>{{ucwords($permission)}}</li>
                                                    @endforeach
                                                </ul>
                                            @endif
                                        </li>
                                    @endforeach
                                </ul>
                            </td>

                            <td class="actionbutton">
                                @if(Sentry::getUser()->hasAccess('edit_groups'))
                                    <button class="btn btn-primary btn-md" uib-tooltip="Edit Role"
                                            onClick="location.href='{{ route('sentinel.groups.edit', [$group->hash]) }}'">
                                        Edit
                                    </button>
                                @endif
                                @if(Sentry::getUser()->hasAccess('delete_groups'))
                                    <form class="delete-form"
                                          uib-tooltip="Delete Role"
                                          {{--onsubmit=return switalert();--}}
                                          action="{{ route('sentinel.groups.destroy', [$group->hash]) }}"
                                          method="post">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                        <input type="hidden" name="_method" value="DELETE"/>
                                        <button class="btn btn-danger btn-md action_confirm" onclick="deleteRole()">
                                            Delete
                                        </button>
                                    </form>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        {!! $groups->render() !!}
    </div>
@stop

