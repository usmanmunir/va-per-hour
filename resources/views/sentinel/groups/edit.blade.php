@extends('layouts.admin.default')

{{-- Web site Title --}}
@section('page_title', 'Edit Group')

{{-- Content --}}
@section('content')
<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <form method="POST" action="{{ route('sentinel.groups.update', $group->hash) }}" accept-charset="UTF-8">
            <div class="form-group row">
                <label for="hori-pass1" class="col-sm-3 col-form-label">Name<span
                            class="text-danger">*</span></label>
                <div class="col-sm-9">
                    <div class="form-group {{ ($errors->has('name')) ? 'has-error' : '' }}">
                        <input class="form-control" placeholder="Name" name="name"
                               value="{{ Request::old('name') ? Request::old('name') : $group->name }}"
                               type="text">
                        {{ ($errors->has('name') ? $errors->first('name') : '') }}
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <label for="hori-pass1" class="col-sm-3 col-form-label">Permissions<span
                            class="text-danger">*</span></label>
                <div class="col-sm-9">
                    <?php $defaultPermissions = \Illuminate\Support\Facades\DB::table('permissions')->get(); //config('sentinel.default_permissions', []);
                    $permissionGroup = [
                        'admin' => [],
                        'user' => [],
                    ];
                    ?>
                    @foreach ($defaultPermissions as $permission)
                        <?php
                        $value = $permission->slug;

                        if (strpos(strtolower('____' . $value), 'admin')) {
                            $permissionGroup['admin'][] =  $permission;
                        } elseif (strpos(strtolower('____' . $value), 'user')) {
                            $permissionGroup['user'][] =  $permission;
                        } elseif (strpos(strtolower('____' . $value), 'group')) {
                            $permissionGroup['groups'][] =  $permission;
                        }
                        ?>
                    @endforeach
                    <ul>
                        @foreach ($permissionGroup as $key => $dPermissions)
                            <li>
                                {{ucwords(implode(" ", explode("_", $key)))}}
                                @if(is_array($dPermissions) && !empty($dPermissions))
                                    <ul>
                                        @foreach($dPermissions as $permission)
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input name="permissions[{{ $permission->slug }}]" value="1" type="checkbox" {{
                                       (isset($permissions[$permission->slug]) ? 'checked' : '') }}>
                                                    {{ ucwords($permission->title) }}
                                                </label>
                                            </li>
                                        @endforeach
                                    </ul>
                                @endif
                            </li>

                        @endforeach

                    </ul>
                </div>
            </div>

            <input name="_method" value="PUT" type="hidden">
            <input name="_token" value="{{ csrf_token() }}" type="hidden">
            <input class="btn btn-primary" value="Save Changes" type="submit">
        </form>
    </div>
</div>

@stop