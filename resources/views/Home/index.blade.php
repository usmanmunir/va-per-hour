@extends('layouts.default')
@section('main_container')
    <section id="stint-search">
        <div class="msearch_back">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="big-text1">
                            <h1>Find The Perfect Freelance Services, For Your Business</h1>
                            <p class="subtitle">Popular stints are Logo Design, Voice Over, Articles & Blog design,
                                Design & Development</p>
                        </div>
                        <form role="search" method="get" action="#">
                            <div class="main-box" data-test-selector="autosuggest">
                                <input type="search" class="searct-text" name="search" placeholder="e.g.Logo Design"
                                       value="" aria-label="search" autocomplete="off">
                                <button class="msearch-btn"><i class="fa fa-search"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!---->
    <!--<section id="three-parts">-->
    <!--    <div class="white-back">-->
    <!--        <div class="container">-->
    <!--            <div class="row">-->
    <!--                <div class="col-md-12">-->
    <!--                    <div class="home-tilte1">-->
    <!--                        <h1>Our Products</h1>-->
    <!--                        <p>We carefully review new entries from our community one by one to make sure they meet high-quality design and functionality standards. you’ll always find something that catches your eye.</p>-->
    <!--                    </div>-->
    <!--                   <div class="three-btns mtop30">-->
    <!--                       <a href="--><!--stint" class="gray-btn">Freelancer</a>-->
    <!--                       <a href="--><!--marketplace/marketplace/home" class="gray-btn">Marketplace</a>-->
    <!--                       <a href="--><!--elearning/course/" class="gray-btn">E-learning</a>-->
    <!--                   </div>-->
    <!--                </div>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->
    <!--</section>-->
    <!---->

    <section id="selling-proposition" class="bgfb section--padding1">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="sell-content pright50">
                        <h2>Get Work Done Faster On VAPERHOUR, With Confidence</h2>
                    </div>
                    <div class="common-points-list">
                        <ul>
                            <li>
                                <h6>Payment Protection, Guaranteed</h6>
                                <p>Payment is released to the freelancer once you’re pleased and approve the work you
                                    get.</p>
                            </li>
                            <li>
                                <h6>Know The Price Upfront</h6>
                                <p>Find any service within minutes and know exactly what you’ll pay. No hourly rates,
                                    just a fixed price.</p>
                            </li>
                            <li>
                                <h6>We’re Here For You 24/7</h6>
                                <p>VAPERHOUR is here for you, anything from answering any questions to resolving any
                                    issues, at any time.</p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 pd0">
                    <div class="video-thumbnail">
                        <a data-fancybox data-ratio="2" href="https://vimeo.com/307960269">
                            <img src="/assets/images/lapy.jpg" alt="Video thumbnail"
                                 class="img img-responsive"/>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="home section--padding1 bgwhite">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title bold-title">
                        <h1>Explore
                            <span class="highlighted">VAPerHour</span>
                        </h1>
                        <p>Laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis omnis
                            fugats. Lid
                            est laborum dolo rumes fugats untras.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 pd0">
                    <div class="explore-box border-right">
                        <img src="/assets/images/home/icon_1b.svg" class="size52">
                        <h3>Freelance Marketplace</h3>
                    </div>
                </div>
                <div class="col-md-3 pd0">
                    <div class="explore-box border-right">
                        <img src="/assets/images/home/icon_2b.svg" class="size52">
                        <h3>Learning Zone</h3>
                    </div>
                </div>
                <div class="col-md-3 pd0">
                    <div class="explore-box border-right">
                        <img src="/assets/images/home/icon_3b.svg" class="size52">
                        <h3>Software Marketplace</h3>
                    </div>
                </div>
                <div class="col-md-3 pd0">
                    <div class="explore-box">
                        <img src="/assets/images/home/icon_4b.svg" class="size52">
                        <h3>Advertising</h3>
                    </div>
                </div>


            </div>
        </div>
    </section>
    <section class="home section-2 section--padding1">
        <div class="container">
            <div class="row">
                <div class="title-discription">
                    <h2 class="home-title mbot15">Explore The Catagories</h2>
                    <p class="discription">On the other hand, we denounce with righteous indignation and dislike men who
                        are so beguiled and demoralized</p>
                </div>


                <div data-aos-offset="200" data-aos="fade-right" data-aos-delay="500"
                     class="col-xs-12 col-sm-6 col-md-4 col-xl-4 catagories-box">
                    <div class="box">
                        <h2 class="title">Programming & Tech</h2>
                        <p class="discription">A programming language is a formal language, which comprises a set of
                            instructions used to produce various kinds of output. Programming languages are used in
                            computer programming t</p>
                        <a href="/stint/category/programming-tech">See More ></a>
                    </div>
                </div>


                <div data-aos-offset="200" data-aos="fade-right" data-aos-delay="500"
                     class="col-xs-12 col-sm-6 col-md-4 col-xl-4 catagories-box">
                    <div class="box">
                        <h2 class="title">E-Marketing</h2>
                        <p class="discription">You've got the idea, now make it official with the perfect logo or
                            design</p>
                        <a href="/stint/category/E-Marketing">See More ></a>
                    </div>
                </div>


                <!--        <div data-aos-offset="200" data-aos="fade-down" data-aos-delay="500" class="col-xs-12 col-sm-6 col-md-4 col-xl-4 catagories-box">-->
                <!--            <div class="box">-->
                <!--                <h2 class="title">Website Design</h2>-->
                <!--                <div class="des"></div>-->
                <!--                <p class="discription">On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized</p>-->
                <!--                <a href="#">See More ></a>-->
                <!--            </div>-->
                <!--        </div>-->

            </div>
        </div>
    </section>
    <section class="home section-3 bgfb section--padding1">
        <div class="container">
            <div class="row">
                <div class="title-discription">
                    <h2 class="title home-title mbot15">Why Choose <span>VaperHour</span></h2>
                    <p class="discription">On the other hand, we denounce with righteous indignation and dislike men who
                        are so beguiled and demoralized</p>
                </div>
                <div data-aos-offset="200" data-aos="fade-right" data-aos-delay="500"
                     class="col-xs-12 col-sm-6 col-md-4 col-xl-4 choose-box">
                    <div class="box choose-1">
                        <div class="box-no">01</div>
                        <i class="fa fa-money" aria-hidden="true"></i>
                        <h2 class="title">one time payment</h2>
                        <div class="des"></div>
                        <p class="discription">Nunc placerat mi id nisi interdum mollis.
                            Praesent pharetra, justo ut scelerisque
                            the mattis, leo quam aliquet diam
                            congue is laoreet elit metus.</p>
                    </div>
                </div>
                <div data-aos-offset="200" data-aos="fade-down" data-aos-delay="500"
                     class="col-xs-12 col-sm-6 col-md-4 col-xl-4 choose-box">
                    <div class="box choose-2">
                        <div class="box-no">02</div>
                        <i class="fa fa-magic" aria-hidden="true"></i>
                        <h2 class="title">Quality Products</h2>
                        <div class="des"></div>
                        <p class="discription">Nunc placerat mi id nisi interdum mollis.
                            Praesent pharetra, justo ut scelerisque
                            the mattis, leo quam aliquet diam
                            congue is laoreet elit metus.</p>
                    </div>
                </div>
                <div data-aos-offset="200" data-aos="fade-left" data-aos-delay="500"
                     class="col-xs-12 col-sm-6 col-md-4 col-xl-4 choose-box">
                    <div class="box choose-3">
                        <div class="box-no">03</div>
                        <i class="fa fa-lock" aria-hidden="true"></i>
                        <h2 class="title">100% Secure Payment</h2>
                        <div class="des"></div>
                        <p class="discription">Nunc placerat mi id nisi interdum mollis.
                            Praesent pharetra, justo ut scelerisque
                            the mattis, leo quam aliquet diam
                            congue is laoreet elit metus.</p>
                    </div>
                </div>
                <div data-aos-offset="200" data-aos="fade-right" data-aos-delay="500"
                     class="col-xs-12 col-sm-6 col-md-4 col-xl-4 choose-box">
                    <div class="box choose-4">
                        <div class="box-no">04</div>
                        <i class="fa fa-code" aria-hidden="true"></i>
                        <h2 class="title">Well Organized Code</h2>
                        <div class="des"></div>
                        <p class="discription">Nunc placerat mi id nisi interdum mollis.
                            Praesent pharetra, justo ut scelerisque
                            the mattis, leo quam aliquet diam
                            congue is laoreet elit metus.</p>
                    </div>
                </div>

                <div data-aos-offset="200" data-aos="fade-up" data-aos-delay="500"
                     class="col-xs-12 col-sm-6 col-md-4 col-xl-4 choose-box">
                    <div class="box choose-5">
                        <div class="box-no">05</div>
                        <i class="fa fa-leaf" aria-hidden="true"></i>
                        <h2 class="title">Lifetime Free Update</h2>
                        <div class="des"></div>
                        <p class="discription">Nunc placerat mi id nisi interdum mollis.
                            Praesent pharetra, justo ut scelerisque
                            the mattis, leo quam aliquet diam
                            congue is laoreet elit metus.</p>
                    </div>
                </div>

                <div data-aos-offset="200" data-aos="fade-left" data-aos-delay="500"
                     class="col-xs-12 col-sm-6 col-md-4 col-xl-4 choose-box">
                    <div class="box choose-6">
                        <div class="box-no">06</div>
                        <i class="fa fa-phone" aria-hidden="true"></i>
                        <h2 class="title">Fast & Friendly Support</h2>
                        <div class="des"></div>
                        <p class="discription">Nunc placerat mi id nisi interdum mollis.
                            Praesent pharetra, justo ut scelerisque
                            the mattis, leo quam aliquet diam
                            congue is laoreet elit metus.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="partner-area section--padding1 bgwhite">
        <!-- start container -->
        <div class="container">
            <!-- start row -->
            <div class="row">
                <!-- start col-md-12 -->
                <div class="col-md-12">
                    <div class="section-title bold-title">
                        <h1>Our Branding
                            <span class="highlighted">Sponsors</span>
                        </h1>
                        <p>Laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis omnis
                            fugats. Lid
                            est laborum dolo rumes fugats untras.</p>
                    </div>
                </div>
                <!-- end /.col-md-12 -->
            </div>
            <!-- end /.row -->

            <div class="row">
                <div class="col-md-12">
                    <div class="partners">

                        <section class="sponsor-logos slider">
                            <div class="slide partners">
                                <a target="_blank" href="https://facebook.com">
                                    <img src="/uploads/sponsor/b4c78c0365be9d8d453eecd52e889a04.jpg"
                                         alt="partner image" height="100px">
                                </a>
                            </div>
                            <div class="slide partners">
                                <a target="_blank"
                                   href="/blog/web-design/adding-an-infinite-client-logo-carousel-to-your-website.stml#">
                                    <img src="/uploads/sponsor/13215c9e688816515e14e60a25d41a58.jpg"
                                         alt="partner image" height="100px">
                                </a>
                            </div>
                            <div class="slide partners">
                                <a target="_blank" href="https://www.revechat.com/">
                                    <img src="/uploads/sponsor/f0b4e73c6f3b22f864d139535d5ef23c.jpg"
                                         alt="partner image" height="100px">
                                </a>
                            </div>

                        </section>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="home section-4 bgfb section--padding1">
        <div class="container">
            <div class="row">
                <div class="title-discription">
                    <h2 class="title home-title mbot15">Extensive Range Of Stints!</h2>
                    <p class="discription">Explore ,Analyse & Communicate with freelancer you prefer</p>
                </div>

                <div class="col-md-12 item-manag">
                    <div class="item-list">
                        <div class="list">
                            <div class="box">
                                <div class="item-img">
                                    <img src="/assets/images/default/image_not_found.jpeg">
                                </div>
                                <div class="item-contant">
                                    <h5>
                                        <a href="/stint/stint-detail/Will-Design-Your-Social-Media-Banners-1/mohan">
                                            Will Designing Your-facebook-Media-Banners</a></h5>
                                    <div class="user-info">
                                        <div class="img">
                                            <img src="/uploads/profile/8232b303c165f6e935aa21113bb8734e.jpg">
                                        </div>
                                        <a href="/user/profile/mohan"><h6>mohans patels</h6>
                                        </a>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="name-cate"><i class="fa fa-book" aria-hidden="true"></i> SEO</div>
                                </div>
                                <div class="item-pricing">
                                    <div class="price">
                                        <a href="#">$10</a>
                                    </div>

                                    <div class="like"><i class="fa fa-heart-o" aria-hidden="true"></i> 0</div>

                                    <div class="cart"><i class="fa fa-shopping-cart" aria-hidden="true"></i> 4</div>

                                    <div class="rating-star"><i class="fa fa-star" aria-hidden="true"></i> <h4>4 </h4>
                                        <span> (1)</span></div>

                                </div>
                            </div>
                        </div>
                        <div class="list">
                            <div class="box">
                                <div class="item-img">
                                    <img src="/uploads/stint-gallery/8910e7fcd2e4ade5e81e586c17f0e484.png">
                                </div>
                                <div class="item-contant">
                                    <h5>
                                        <a href="/stint/stint-detail/I-Will-Setup-All-Web-Analytics-Tools-With-GTM/gaurang">
                                            I Will Setup All Web Analytics Tools With GTM</a></h5>
                                    <div class="user-info">
                                        <div class="img">
                                            <img src="/assets/images/default/user.png">
                                        </div>
                                        <a href="/user/profile/gaurang"><h6>Gaurang Patel</h6>
                                        </a>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="name-cate"><i class="fa fa-book" aria-hidden="true"></i> Web Anyaltics
                                    </div>
                                </div>
                                <div class="item-pricing">
                                    <div class="price">
                                        <a href="#">$10</a>
                                    </div>

                                    <div class="like"><i class="fa fa-heart-o" aria-hidden="true"></i> 2</div>

                                    <div class="cart"><i class="fa fa-shopping-cart" aria-hidden="true"></i> 22</div>

                                    <div class="rating-star"><i class="fa fa-star" aria-hidden="true"></i> <h4>
                                            4.33 </h4><span> (3)</span></div>

                                </div>
                            </div>
                        </div>
                        <div class="list">
                            <div class="box">
                                <div class="item-img">
                                    <img src="/uploads/stint-gallery/8f38bac52f48374f92fba5f82bdac4f8.png">
                                </div>
                                <div class="item-contant">
                                    <h5>
                                        <a href="/stint/stint-detail/I-Will-Do-Web-Programming-And-Web-Developing/gaurang">
                                            I Will Do Web Programming And Web Developing</a></h5>
                                    <div class="user-info">
                                        <div class="img">
                                            <img src="/assets/images/default/user.png">
                                        </div>
                                        <a href="/user/profile/gaurang"><h6>Gaurang Patel</h6>
                                        </a>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="name-cate"><i class="fa fa-book" aria-hidden="true"></i> Web Programming
                                    </div>
                                </div>
                                <div class="item-pricing">
                                    <div class="price">
                                        <a href="#">$10</a>
                                    </div>

                                    <div class="like"><i class="fa fa-heart-o" aria-hidden="true"></i> 1</div>

                                    <div class="cart"><i class="fa fa-shopping-cart" aria-hidden="true"></i> 6</div>

                                    <div class="rating-star"><i class="fa fa-star" aria-hidden="true"></i> <h4>3 </h4>
                                        <span> (2)</span></div>

                                </div>
                            </div>
                        </div>
                        <div class="list">
                            <div class="box">
                                <div class="item-img">
                                    <img src="/uploads/stint-gallery/f92637bd713b4f2efe4e2ecfc3b9bf85.jpg">
                                </div>
                                <div class="item-contant">
                                    <h5>
                                        <a href="/stint/stint-detail/I-Will-Create-A-Full-SEO-Campaign-For-Your-Website/bhavesh">
                                            I Will Create A Full SEO Campaign For Your Website</a></h5>
                                    <div class="user-info">
                                        <div class="img">
                                            <img src="/uploads/profile/e287a6a37595a05adcb8b9e7648b1b6c.jpg">
                                        </div>
                                        <a href="/user/profile/bhavesh"><h6>Bhavesh Jadav</h6>
                                        </a>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="name-cate"><i class="fa fa-book" aria-hidden="true"></i> SEO</div>
                                </div>
                                <div class="item-pricing">
                                    <div class="price">
                                        <a href="#">$10</a>
                                    </div>

                                    <div class="like"><i class="fa fa-heart-o" aria-hidden="true"></i> 0</div>

                                    <div class="cart"><i class="fa fa-shopping-cart" aria-hidden="true"></i> 4</div>

                                    <div class="rating-star"><i class="fa fa-star" aria-hidden="true"></i> <h4>4 </h4>
                                        <span> (1)</span></div>

                                </div>
                            </div>
                        </div>
                        <div class="list">
                            <div class="box">
                                <div class="item-img">
                                    <img src="/uploads/stint-gallery/410b9d5611ada7163ea67d98b8769e6d.jpg">
                                </div>
                                <div class="item-contant">
                                    <h5>
                                        <a href="/stint/stint-detail/I-Will-Build-Wordpress-Website-Wordpress-Design-Wordpress-Customization/bhavesh">
                                            I Will Build Wordpress Website, Wordpress Design, Wordpress ..</a></h5>
                                    <div class="user-info">
                                        <div class="img">
                                            <img src="/uploads/profile/e287a6a37595a05adcb8b9e7648b1b6c.jpg">
                                        </div>
                                        <a href="/user/profile/bhavesh"><h6>Bhavesh Jadav</h6>
                                        </a>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="name-cate"><i class="fa fa-book" aria-hidden="true"></i> WordPress</div>
                                </div>
                                <div class="item-pricing">
                                    <div class="price">
                                        <a href="#">$10</a>
                                    </div>

                                    <div class="like"><i class="fa fa-heart-o" aria-hidden="true"></i> 1</div>

                                    <div class="cart"><i class="fa fa-shopping-cart" aria-hidden="true"></i> 12</div>

                                    <div class="rating-star"><i class="fa fa-star" aria-hidden="true"></i> <h4>
                                            3.71 </h4><span> (7)</span></div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="home section-5 section--padding1">
        <div class="container">
            <div class="row">
                <div class="title-discription">
                    <h2 class="title home-title">Invite Friends & You Both Get Up To $100</h2>
                    <p class="discription">Introduce your friends to the easiest way to get things done</p>
                </div>
                <div class="col-md-12 invite">
                    <div class="invite-process">
                        <img src="/assets/images/invite.png">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="home section-6 section--padding1">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-6 col-sm-6 col-xl-6 elearning-img">
                    <img src="/assets/images/elearning.png">
                </div>
                <div class="col-xs-12 col-md-6 col-sm-6 col-xl-6 elearning-contant">
                    <h2 class="title home-title">Get Started with eLearning</h2>
                    <p class="discription">Would you like to introduce eLearning into your organisation? <br>
                        Take your first steps with our team guiding you along your journey.</p>
                    <a href="#" class="ctm-btn" data-toggle="modal" data-target=".signin-model">Register for
                        certificate</a>
                </div>
            </div>
        </div>
    </section>
@endsection