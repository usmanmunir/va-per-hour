<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdditionalFieldsToStintOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stint_orders', function (Blueprint $table) {
            $table->dateTime('due_on')->nullable();
            $table->boolean('is_starred')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stint_orders', function (Blueprint $table) {
            $table->dropColumn('due_on');
            $table->dropColumn('is_starred');
        });
    }
}
