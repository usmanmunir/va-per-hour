<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePricingPlansTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stint_pricing_plans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('stint_id', false);
            $table->integer('plan', false);
            $table->string('title', 80);
            $table->text('description');
            $table->string('delivery_time', 10);
            $table->string('revisions', 10);
            $table->string('price', 10);
            $table->tinyInteger('status', false)->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stint_pricing_plans');
    }
}
