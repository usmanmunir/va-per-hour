<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RedesignConversationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('conversation');
        Schema::create('conversations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('participant_1', false);
            $table->integer('participant_2', false);
            $table->integer('stint_id', false);
            $table->integer('job_id', false)->nullable();
            $table->dateTime('started_at')->default(DB::raw('NOW()'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conversations');
        Schema::create('conversation', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('sender_id');
            $table->integer('receiver_id');
            $table->string('message')->nullable();
            $table->timestamps();
        });
    }
}
