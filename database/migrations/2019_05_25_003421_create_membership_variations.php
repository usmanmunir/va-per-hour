<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembershipVariations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('membership_variations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('membership_id', false);
            $table->string('name', 100)->nullable();
            $table->text('description')->nullable();
            $table->string('month', 15)->nullable();
            $table->string('price', 15)->nullable();
            $table->string('user_type', 30)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('membership_variations');
    }
}
