<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateStintOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stint_orders', function (Blueprint $table) {
            $table->dropColumn('user_id');
            $table->dropColumn('order_total');
            $table->integer('offer_id')->after('id');
            $table->integer('buyer_id')->after('id');
            $table->integer('seller_id')->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->integer('user_id');
            $table->string('order_total', 25);
            $table->dropColumn('offer_id');
            $table->dropColumn('buyer_id');
            $table->dropColumn('seller_id');
        });
    }
}
