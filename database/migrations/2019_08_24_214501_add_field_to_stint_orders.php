<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldToStintOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stint_orders', function (Blueprint $table) {
            $table->string('order_type', false)->default('stint');
            $table->tinyInteger('payment_status', false)->nullable(0);
            $table->integer('stint_extra_id', false)->nullable();
            $table->string('extra_price', false)->nullable();
            $table->string('unit_price', false)->nullable();
            $table->string('quantity', 25)->nullable();
            $table->integer('transaction_id', false)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stint_orders', function (Blueprint $table) {
            $table->dropColumn('order_type');
            $table->dropColumn('payment_status');
            $table->dropColumn('extra_price');
            $table->dropColumn('stint_extra_id');
            $table->dropColumn('unit_price');
            $table->dropColumn('quantity');
            $table->dropColumn('transaction_id');
        });
    }
}
