<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfferScopeTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer_scopes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('offer_id', false);
            $table->boolean('design_customization')->default(0);
            $table->boolean('content_upload')->default(0);
            $table->boolean('responsive_design')->default(0);
            $table->boolean('include_source_code')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offer_scopes');
    }
}
