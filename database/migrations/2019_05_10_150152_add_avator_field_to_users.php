<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAvatorFieldToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->tinyinteger('is_seller')->default(0);
            $table->tinyinteger('is_buyer')->default(0);
            $table->tinyinteger('is_active')->default(0);
            $table->boolean('verified')->default(false);
            $table->string('website')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
            $table->dropColumn('is_seller');
            $table->dropColumn('is_buyer');
            $table->dropColumn('is_active');
            $table->dropColumn('verified');
            $table->dropColumn('website');
        });
    }
}
