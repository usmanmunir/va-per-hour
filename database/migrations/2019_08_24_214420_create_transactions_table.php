<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id', false);
            $table->string('first_name', 100);
            $table->string('last_name', 100);
            $table->string('company_name', 150);
            $table->string('email', 150);
            $table->integer('country_id', false);
            $table->integer('state_id', false);
            $table->integer('city_id', false);
            $table->string('address1', 150);
            $table->string('address2', 150)->nullable();
            $table->string('zip_code', 15);
            $table->string('summary', 150);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
