<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserMaster extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('user_master', function ($table) {
            $table->increments('id');
            $table->integer('user_id', false);
            $table->tinyinteger('is_seller')->default(0);
            $table->tinyinteger('is_buyer')->default(0);
            $table->tinyinteger('is_active')->default(0);
            $table->boolean('verified')->default(false);
            $table->string('website')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('user_master');
    }
}
