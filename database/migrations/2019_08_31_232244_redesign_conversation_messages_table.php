<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RedesignConversationMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('chat');
        Schema::create('conversation_messages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('sender_id');
            $table->integer('receiver_id');
            $table->string('message');
            $table->string('attachment')->nullable();
            $table->string('file_type')->nullable();
            $table->integer('is_read')->default(0);
            $table->integer('conversation_id', false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conversation_messages');
        Schema::create('chat', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('sender_id');
            $table->integer('receiver_id');
            $table->string('message')->nullable();
            $table->string('message_type');
            $table->string('file')->nullable();
            $table->string('file_type')->nullable();
            $table->string('chat_type');
            $table->integer('is_read')->default(0);
            $table->integer('conversation_id');
            $table->timestamps();
        });
    }
}
