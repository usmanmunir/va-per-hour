<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembershipSchema extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('memberships', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('membership_for', false);
            $table->string('title', 50);
            $table->string('user_type', 25);
            $table->text('description');
            $table->string('price', 15);
            $table->string('month', 15);
            $table->tinyInteger('can_view_course_details')->default(0);
            $table->tinyInteger('can_view_product_details')->default(0);
            $table->tinyInteger('can_invite_friends')->default(0);
            $table->tinyInteger('visible_in_seller_search')->default(0);
            $table->string('number_of_stints_allowed_single_user', 3)->default(0);
            $table->string('number_of_stints_allowed_agency_user', 3)->default(0);
            $table->tinyInteger('can_contact_seller')->default(0);
            $table->tinyInteger('can_submit_proposals')->default(0);
            $table->tinyInteger('can_register_to_course')->default(0);
            $table->tinyInteger('can_add_product_to_market_place')->default(0);
            $table->tinyInteger('eligible_for_cash_reward')->default(0);
            $table->string('number_of_proposal_allowed', 3)->default(0);
            $table->tinyInteger('can_purchase_product')->default(0);
            $table->tinyInteger('can_submit_bid')->default(0);
            $table->tinyInteger('can_create_course')->default(0);
            $table->tinyInteger('can_order_stint')->default(0);
            $table->tinyInteger('seller_contact_limit_single_user')->default(0);
            $table->string('seller_contact_limit_agency_user')->default(0);
            $table->string('status', false)->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('memberships');
    }
}
