<?php

use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions= [
            //'admin', 'users'
            ['title' => 'Admin', 'slug' => 'admin', 'created_at' => date('Y-m-d H:i:s', time()), 'updated_at'=>date('Y-m-d H:i:s', time())],
            ['title' => 'Users', 'slug' => 'users', 'created_at' => date('Y-m-d H:i:s', time()), 'updated_at'=>date('Y-m-d H:i:s', time())],
            ['title' => 'View Users', 'slug' => 'view_users', 'created_at' => date('Y-m-d H:i:s', time()), 'updated_at'=>date('Y-m-d H:i:s', time())],
            ['title' => 'Create Users', 'slug' => 'create_users', 'created_at' => date('Y-m-d H:i:s', time()), 'updated_at' => date('Y-m-d H:i:s', time())],
            ['title' => 'Edit Users', 'slug' => 'edit_users', 'created_at' => date('Y-m-d H:i:s', time()), 'updated_at' => date('Y-m-d H:i:s', time())],
            ['title' => 'Delete Users', 'slug' => 'delete_users', 'created_at' => date('Y-m-d H:i:s', time()), 'updated_at' => date('Y-m-d H:i:s', time())],
            ['title' => 'View Groups', 'slug' => 'view_groups', 'created_at' => date('Y-m-d H:i:s', time()), 'updated_at' => date('Y-m-d H:i:s', time())],
            ['title' => 'Create Groups', 'slug' => 'create_groups', 'created_at' => date('Y-m-d H:i:s', time()), 'updated_at' => date('Y-m-d H:i:s', time())],
            ['title' => 'Edit Groups', 'slug' => 'edit_groups', 'created_at' => date('Y-m-d H:i:s', time()), 'updated_at' => date('Y-m-d H:i:s', time())],
            ['title' => 'Delete Groups', 'slug' => 'delete_groups', 'created_at' => date('Y-m-d H:i:s', time()), 'updated_at' => date('Y-m-d H:i:s', time())],
        ];

        DB::table('permissions')->insert($permissions);
    }
}
