<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(LanguageTableSeeder::class);
        // $this->call(UsersTableSeeder::class);
        $this->call(PermissionTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
    }

    public function call($class, $silent = false)
    {
        // check if the seeder already run
        $seeder = DB::table('seeders')->where('class', $class)->first();
        if ($seeder) {
            $this->command->warn("Already seeded {$class}");
            return;
        }

        // run the seeder
        parent::call($class);

        // add the seeder to seeded list
        DB::table('seeders')->insert([
            'class' => $class,
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);
    }
}
